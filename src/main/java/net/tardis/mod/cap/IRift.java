package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.artron.IArtronProducer;

public interface IRift extends IArtronProducer, INBTSerializable<CompoundNBT> {

    void addEnergy(float energy);

    float getRiftEnergy();

    boolean isRift();

    void setRift(boolean rift);

    void tick();

    public static class Storage implements IStorage<IRift> {

        @Override
        public INBT writeNBT(Capability<IRift> capability, IRift instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IRift> capability, IRift instance, Direction side, INBT nbt) {
            instance.deserializeNBT((CompoundNBT) nbt);
        }

    }

    public static class Provider implements ICapabilitySerializable<CompoundNBT> {

        final LazyOptional<IRift> provide;

        public Provider(IRift rift) {
            this.provide = LazyOptional.of(() -> rift);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.RIFT ? (LazyOptional<T>) provide : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return this.provide.orElse(null).serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            this.provide.orElse(null).deserializeNBT(nbt);
        }

    }

}
