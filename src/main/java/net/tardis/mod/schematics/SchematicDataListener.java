package net.tardis.mod.schematics;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IFutureReloadListener;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Unit;
import net.tardis.mod.Tardis;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.types.SchematicType;
import org.apache.logging.log4j.Level;

import java.io.InputStreamReader;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class SchematicDataListener implements IFutureReloadListener {

    public static final IFutureReloadListener INSTANCE = new SchematicDataListener();


    @Override
    public CompletableFuture<Void> reload(IStage stage, IResourceManager resourceManager, IProfiler preparationsProfiler, IProfiler reloadProfiler, Executor backgroundExecutor, Executor gameExecutor) {
        return stage.markCompleteAwaitingOthers(Unit.INSTANCE).thenRunAsync(() -> SchematicDataListener.read(resourceManager));
    }

    public static void read(IResourceManager manager){

        Schematics.SCHEMATICS.clear();

        for(ResourceLocation loc : manager.getAllResourceLocations("schematics", str -> str.endsWith(".json"))){

            try{

                JsonObject root = new JsonParser().parse(new InputStreamReader(manager.getResource(loc).getInputStream())).getAsJsonObject();

                ResourceLocation type = new ResourceLocation(root.get("type").getAsString());

                SchematicType schematicType = SchematicTypes.REGISTRY.get().getValue(type);

                if(schematicType != null){

                    ResourceLocation key = stripResourceLocation(loc);

                    Schematic schematic = schematicType.deserialize(key, root);
                    schematic.setId(key);
                    schematic.setDisplayName(SchematicType.readDisplayName(root));
                    Schematics.SCHEMATICS.put(key, schematic);

                }
                else Tardis.LOGGER.log(Level.ERROR, String.format("No SchematicType called %s!", type));

            }
            catch(Exception e){
                Tardis.LOGGER.log(Level.ERROR, String.format("Invalid json for schematic %s!", loc));
                Tardis.LOGGER.catching(Level.ERROR, e);
            }

        }

    }

    public static ResourceLocation stripResourceLocation(ResourceLocation loc){
        return new ResourceLocation(loc.getNamespace(), loc.getPath().replace("schematics/", "").replace(".json", ""));
    }
}
