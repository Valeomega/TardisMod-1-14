package net.tardis.mod.world.dimensions;
//
//import net.minecraft.block.BlockState;
//import net.minecraft.block.Blocks;
//import net.minecraft.entity.Entity;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.ChunkPos;
//import net.minecraft.util.math.Vec3d;
//import net.minecraft.world.World;
//import net.minecraft.world.biome.provider.SingleBiomeProvider;
//import net.minecraft.world.biome.provider.SingleBiomeProviderSettings;
//import net.minecraft.world.chunk.Chunk;
//import net.minecraft.world.dimension.Dimension;
//import net.minecraft.world.dimension.DimensionType;
//import net.minecraft.world.gen.ChunkGenerator;
//import net.minecraft.world.gen.OverworldGenSettings;
//import net.minecraftforge.api.distmarker.Dist;
//import net.minecraftforge.api.distmarker.OnlyIn;
//import net.minecraftforge.client.IRenderHandler;
//import net.tardis.api.space.dimension.IDimProperties;
//import net.tardis.mod.client.renderers.sky.MoonSkyRenderer;
//import net.tardis.mod.dimensions.chunkgen.MoonChunkGenerator;
//

/**
 * Legacy code for the 1.14 Moon Dimension. Remains as a reference for old logic
 */
@Deprecated
public class MoonDimension {
//public class MoonDimension extends Dimension implements IDimProperties{
//
//	public static final SingleBiomeProvider BIOME = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(TDimensions.MOON_FIELD));
//	
//	public MoonDimension(World worldIn, DimensionType typeIn) {
//		super(worldIn, typeIn);
//	}
//
//	@Override
//	public ChunkGenerator<?> createChunkGenerator() {
//		return new MoonChunkGenerator(this.world, BIOME, new MoonGenSettings());
//	}
//
//	@Override
//	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
//		return null;
//	}
//
//	@Override
//	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
//		return null;
//	}
//
//	@Override
//	public float calculateCelestialAngle(long worldTime, float partialTicks) {
//		
//		return (worldTime / 24000.0F) % 1.0F;
//	}
//
//	@Override
//	public boolean isSurfaceWorld() {
//		return false;
//	}
//
//	@Override
//	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
//		return new Vec3d(0, 0, 0);
//	}
//
//	@Override
//	public boolean canRespawnHere() {
//		return true;
//	}
//
//	@Override
//	public boolean doesXZShowFog(int x, int z) {
//		return false;
//	}
//	
//	@Override
//	public Vec3d modMotion(Entity mot) {
//		if(mot instanceof PlayerEntity) {
//			if(((PlayerEntity)mot).abilities.isFlying)
//				return mot.getMotion();
//		}
//		if(!mot.hasNoGravity()) {
//			mot.fallDistance -= 0.25;
//			return mot.getMotion().add(0, 0.05, 0);
//		}
//		return mot.getMotion();
//	}
//
//	public static class MoonGenSettings extends OverworldGenSettings{
//
//		@Override
//		public int getRiverSize() {
//			return 0;
//		}
//
//		@Override
//		public int getStrongholdCount() {
//			return 0;
//		}
//
//		@Override
//		public BlockState getDefaultFluid() {
//			return Blocks.AIR.getDefaultState();
//		}
//		
//	}
//
//	@Override
//	public boolean hasAir() {
//		return false;
//	}
//
//	@Override
//	public boolean canDoLightning(Chunk chunk) {
//		return false;
//	}
//
//	@Override
//	public boolean canDoRainSnowIce(Chunk chunk) {
//		return false;
//	}
//
//	@Override
//	public void calculateInitialWeather() {}
//
//	@Override
//	public boolean doesWaterVaporize() {
//		return true;
//	}
//
//	@OnlyIn(Dist.CLIENT)
//	@Override
//	public IRenderHandler getSkyRenderer() {
//		return MoonSkyRenderer.INSTANCE;
//	}
//
//	@Override
//	public Vec3d getSkyColor(BlockPos cameraPos, float partialTicks) {
//		return new Vec3d(0, 0, 0);
//	}
//
//	@Override
//	public boolean isSkyColored() {
//		return true;
//	}
//
//	@Override
//	public float[] calcSunriseSunsetColors(float celestialAngle, float partialTicks) {
//		return new float[] {0, 0, 0, 0, 0, 0};
//	}
//
}
