package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class XionConsoleTile extends ConsoleTile{


	public XionConsoleTile() {
		this(TTiles.CONSOLE_XION.get());
		this.registerControlEntry(ControlRegistry.MONITOR.get());
		this.variants = ConsoleTextureVariants.XION;
	}
	
	public XionConsoleTile(TileEntityType<?> type) {
		super(type);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).expand(3, 4, 3);
	}
}
