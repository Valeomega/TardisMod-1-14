package net.tardis.mod.registries;

import java.util.ArrayList;
import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.exterior.DisguiseExterior;
import net.tardis.mod.exterior.TwoBlockBasicExterior;
import net.tardis.mod.misc.DoorSounds;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.texturevariants.TextureVariants;

public class ExteriorRegistry {
	
    public static final DeferredRegister<AbstractExterior> EXTERIORS = DeferredRegister.create(AbstractExterior.class, Tardis.MODID);
    
    public static Supplier<IForgeRegistry<AbstractExterior>> EXTERIOR_REGISTRY = EXTERIORS.makeRegistry("exterior", () -> new RegistryBuilder<AbstractExterior>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static AbstractExterior getExterior(ResourceLocation key){
		return EXTERIOR_REGISTRY.get().getValue(key);
	}
	
	public static ArrayList<AbstractExterior> getDefaultExteriors() {
		ArrayList<AbstractExterior> list = new ArrayList<AbstractExterior>();
		for(AbstractExterior ext : EXTERIOR_REGISTRY.get().getValues()) {
			if(ext.isUnlockedByDefault())
				list.add(ext);
		}
		return list;
	}
	
	public static final RegistryObject<AbstractExterior> STEAMPUNK = EXTERIORS.register("steampunk", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_steampunk.get().getDefaultState(), true, EnumDoorType.STEAM, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/steam.png"), TextureVariants.STEAM));
	public static final RegistryObject<AbstractExterior> TRUNK = EXTERIORS.register("trunk", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_trunk.get().getDefaultState(), false, EnumDoorType.TRUNK, DoorSounds.WOOD, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/trunk.png"), TextureVariants.TRUNK));
	public static final RegistryObject<AbstractExterior> TELEPHONE = EXTERIORS.register("telephone", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_telephone.get().getDefaultState(), true, EnumDoorType.TELEPHONE, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/telephone_red.png"), TextureVariants.TELEPHONE));
	public static final RegistryObject<AbstractExterior> FORTUNE = EXTERIORS.register("fortune", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_fortune.get().getDefaultState(), false, EnumDoorType.FORTUNE, DoorSounds.WOOD, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/fortune.png"), TextureVariants.FORTUNE));
	public static final RegistryObject<AbstractExterior> SAFE = EXTERIORS.register("safe", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_safe.get().getDefaultState(), false, EnumDoorType.SAFE, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/safe.png")));
	public static final RegistryObject<AbstractExterior> TT_CAPSULE = EXTERIORS.register("tt_capsule", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_tt_capsule.get().getDefaultState(), true, EnumDoorType.TT_CAPSULE, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/tt_capsule.png")));
	public static final RegistryObject<AbstractExterior> CLOCK = EXTERIORS.register("clock", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_clock.get().getDefaultState(), true, EnumDoorType.CLOCK, DoorSounds.WOOD, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/clock.png")));
	public static final RegistryObject<AbstractExterior> TT_2020 = EXTERIORS.register("tt_2020", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_tt2020.get().getDefaultState(), false, EnumDoorType.TT_2020_CAPSULE, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/tt_2020.png")));
	public static final RegistryObject<AbstractExterior> JAPAN = EXTERIORS.register("japan", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_japan.get().getDefaultState(), false, EnumDoorType.JAPAN, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/japan.png")));
	public static final RegistryObject<AbstractExterior> APERTURE = EXTERIORS.register("aperture", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_aperture.get().getDefaultState(), false, EnumDoorType.APERTURE, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/aperture.png")));
	public static final RegistryObject<AbstractExterior> POLICE_BOX = EXTERIORS.register("police_box", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_police_box.get().getDefaultState(), false, EnumDoorType.POLICE_BOX, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/police_box.png")));
	public static final RegistryObject<AbstractExterior> MODERN_POLICE_BOX = EXTERIORS.register("modern_police_box", () -> new TwoBlockBasicExterior(() -> TBlocks.exterior_modern_police_box.get().getDefaultState(), false, EnumDoorType.MODERN_POLICE_BOX, DoorSounds.BASE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/new_who.png")));

	public static final RegistryObject<AbstractExterior> DISGUISE = EXTERIORS.register("disguise", () -> new DisguiseExterior(() -> TBlocks.exterior_disguise.get().getDefaultState(), true, EnumDoorType.TT_CAPSULE, new ResourceLocation(Tardis.MODID, "textures/gui/disguise.png")));

}
