package net.tardis.mod.client.renderers.exteriors;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.tileentities.exteriors.DisguiseExteriorTile;

public class DisguiseExteriorTileRenderer extends ExteriorRenderer<DisguiseExteriorTile> {
	

	public DisguiseExteriorTileRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void render(DisguiseExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		
		Disguise disguise = tile.disguise;
		if(disguise != null) {
			matrixStackIn.push();
			Minecraft.getInstance().getBlockRendererDispatcher().renderModel(disguise.getTopState(), tile.getPos(), tile.getWorld(), matrixStackIn, bufferIn.getBuffer(RenderTypeLookup.getChunkRenderType(disguise.getTopState())), false, tile.getWorld().rand, tile.getModelData());
			matrixStackIn.pop();
			
			matrixStackIn.push();
			matrixStackIn.translate(0, -1, 0); //Have to translate this one down as it'll render at the top state position by default
			Minecraft.getInstance().getBlockRendererDispatcher().renderModel(disguise.getBottomState(), tile.getPos().down(), tile.getWorld(), matrixStackIn, bufferIn.getBuffer(RenderTypeLookup.getChunkRenderType(disguise.getBottomState())), false, tile.getWorld().rand, tile.getModelData());
			matrixStackIn.pop();
			
		}
		
		matrixStackIn.pop();
		
		if(!(tile.getBlockState().getBlock() instanceof ExteriorBlock))
			return;

		//Door
		matrixStackIn.push();
		RenderSystem.enableBlend();
		RenderSystem.enableAlphaTest();
		RenderSystem.blendFunc(SourceFactor.DST_COLOR, DestFactor.ZERO);
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		
		matrixStackIn.translate(0.5125, 0, 0.5);
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(360 - WorldHelper.getAngleFromFacing(tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING))));
		RenderSystem.disableTexture();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		double width = tile.getOpen() == EnumDoorState.CLOSED ? 0 : (tile.getOpen() == EnumDoorState.ONE ? 0.25 : 0.4);
		bb.pos(-width, -1, -0.53).color(0F, 0, 0, 1).tex(0, 0).endVertex();
		bb.pos(-width, 1, -0.53).color(0F, 0, 0, 1).tex(0, 0).endVertex();
		bb.pos(width, 1, -0.53).color(0F, 0, 0, 1).tex(0, 0).endVertex();
		bb.pos(width, -1, -0.53).color(0F, 0, 0, 1).tex(0, 0).endVertex();
		Tessellator.getInstance().draw();
		RenderSystem.enableTexture();
		RenderSystem.disableAlphaTest();
		RenderSystem.disableBlend();
		matrixStackIn.pop();
		
	}
	
	@Override
	public void renderExterior(DisguiseExteriorTile tile, float partialTicks, MatrixStack matrixStackIn,
			IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		
	}

}
