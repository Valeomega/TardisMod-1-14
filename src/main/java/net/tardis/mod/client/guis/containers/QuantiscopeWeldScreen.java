package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.containers.QuantiscopeWeldContainer;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.tileentities.QuantiscopeTile;
import net.tardis.mod.tileentities.QuantiscopeTile.EnumMode;

public class QuantiscopeWeldScreen extends ContainerScreen<QuantiscopeWeldContainer> {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/weld.png");
    public static final ResourceLocation TEXTURE_IRON = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/weld_iron.png");
    public static TranslationTextComponent weld_tab = new TranslationTextComponent("gui.quantiscope.mode.weld");
    public static TranslationTextComponent sonic_tab = new TranslationTextComponent("gui.quantiscope.mode.sonic");
    private QuantiscopeTile tile;

    public QuantiscopeWeldScreen(QuantiscopeWeldContainer cont, PlayerInventory inv, ITextComponent titleIn) {
        super(cont, inv, titleIn);
        this.tile = cont.getQuantiscope();
    }

    @Override
    protected void init() {
        super.init();
          //Right Arrow
          addModeButton(-71, 21, 193, 1);
          addModeButton(-7, 21, 188, -1);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
        this.renderBackground(matrixStack);
        int texWidth = 176;
        int textHeight = 166;
        ResourceLocation texture = this.tile.getBlockState().getBlock() == TBlocks.quantiscope_brass.get() ? TEXTURE : TEXTURE_IRON;
        this.minecraft.getTextureManager().bindTexture(texture);
        blit(matrixStack, width / 2 - texWidth / 2, height / 2 - textHeight / 2, 0, 0, texWidth, textHeight);

        blit(matrixStack, width / 2 - 2, height / 2 - 53, 178, 0, (int) (25 * tile.getProgress()), 17);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
        TranslationTextComponent modeTitle = this.tile.getMode() == EnumMode.WELD ? weld_tab : sonic_tab;
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, modeTitle.mergeStyle(TextFormatting.GREEN), this.width / 2 + 42, this.guiTop + 62, 0xFFFFFF);
    }

	@Override
	protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int x, int y) {
		//Remove super class call so we don't show our container title
		this.font.drawText(matrixStack, this.playerInventory.getDisplayName(), (float)this.playerInventoryTitleX, (float)this.playerInventoryTitleY, 4210752);
	}
	
	public void addModeButton(int x, int y, int uOffset, int modIndex) {
		 this.addButton(new ImageButton(width / 2 - x, height / 2 - y, 4, 8, uOffset, 17, 9, TEXTURE, but -> {
			 EnumMode currentMode = tile.getMode();
			 int index = currentMode.ordinal() + modIndex;
			 if (index < EnumMode.values().length && index >= 0) {
				 currentMode = 	EnumMode.values()[index];
			 }
			 else if (index < 0) {
				 currentMode = EnumMode.values()[EnumMode.values().length - 1];
			 }
			 Network.sendToServer(new QuantiscopeTabMessage(this.getContainer().getQuantiscope().getPos(), currentMode));
		 }));
	}
    
    
}
