package net.tardis.mod.tileentities.console.misc;

import com.google.common.collect.Lists;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.misc.Console;
import net.tardis.mod.network.packets.console.UnlockData;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * To Fix
 * @author Spectre0987
 *
 */
public class UnlockManager implements INBTSerializable<CompoundNBT>{

	private ConsoleTile tile;
	
	private List<Console> unlockedConsoles = Lists.newArrayList();
	private List<AbstractExterior> unlockedExteriors = Lists.newArrayList();
	private List<ConsoleRoom> unlockedConsoleRoom = Lists.newArrayList();
	
	public UnlockManager(ConsoleTile tile) {
		this.tile = tile;
		this.unlockedExteriors.addAll(ExteriorRegistry.getDefaultExteriors());
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		//Console Units
		ListNBT consolesList = new ListNBT();
		for(Console console : this.unlockedConsoles) {
			consolesList.add(StringNBT.valueOf(console.getRegistryName().toString()));
		}
		tag.put("consoles", consolesList);
		
		//Exteriors
		ListNBT exteriorList = new ListNBT();
		for(AbstractExterior ext : this.unlockedExteriors) {
			exteriorList.add(StringNBT.valueOf(ext.getRegistryName().toString()));
		}
		tag.put("exteriors", exteriorList);
		
		//Console Room
		ListNBT consoleRoomList = new ListNBT();
		for(ConsoleRoom room : this.unlockedConsoleRoom) {
			if (room.getRegistryName() != null)
				consoleRoomList.add(StringNBT.valueOf(room.getRegistryName().toString()));
		}
		tag.put("console_rooms", consoleRoomList);
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		
		this.unlockedExteriors.clear();
		this.unlockedConsoleRoom.clear();
		this.unlockedConsoles.clear();
		
		//Exteriors
		ListNBT exteriorList = tag.getList("exteriors", NBT.TAG_STRING);
		for(INBT nbt : exteriorList) {
			AbstractExterior ext = ExteriorRegistry.getExterior(new ResourceLocation(((StringNBT)nbt).getString()));
			if(ext != null && !this.unlockedExteriors.contains(ext))
				this.unlockedExteriors.add(ext);
		}
		
		//ConsoleRooms
		ListNBT roomList = tag.getList("console_rooms", NBT.TAG_STRING);
		for(INBT nbt : roomList) {
			ResourceLocation key = new ResourceLocation(((StringNBT)nbt).getString());
			ConsoleRoom room = ConsoleRoom.getRegistry().get(key);
			if(room != null)
				this.unlockedConsoleRoom.add(room);
		}

		//Consoles
		ListNBT consoleList = tag.getList("consoles", NBT.TAG_STRING);
		for(INBT nbt : consoleList){
			ResourceLocation loc = new ResourceLocation(((StringNBT)nbt).getString());
			Console console = ConsoleRegistry.CONSOLE_REGISTRY.get().getValue(loc);
			if(console != null)
				this.unlockedConsoles.add(console);
		}
		
	}
	
	public Collection<AbstractExterior> getUnlockedExteriors() {
		return Collections.unmodifiableCollection(this.unlockedExteriors);
	}

	public void addExterior(AbstractExterior ext) {
		if (!this.unlockedExteriors.contains(ext))
		    this.unlockedExteriors.add(ext);
		tile.updateClient();
	}
	
	public Collection<ConsoleRoom> getUnlockedConsoleRooms() {
		return Collections.unmodifiableCollection(this.unlockedConsoleRoom);
	}

	public void addConsoleRoom(ConsoleRoom room) {
		if (!this.unlockedConsoleRoom.contains(room))
		    this.unlockedConsoleRoom.add(room);
		tile.updateClient();
	}

    public Collection<Console> getUnlockedConsoles() {
		return Collections.unmodifiableCollection(this.unlockedConsoles);
    }

    public void addConsole(Console console){
		if(!this.unlockedConsoles.contains(console)) {
			this.unlockedConsoles.add(console);
			tile.updateTrackingClients(new UnlockData(this));
		}
	}

	public void merge(UnlockManager manager) {
		//Consoles
		for(Console console : manager.unlockedConsoles){
			if(!this.unlockedConsoles.contains(console))
				this.unlockedConsoles.add(console);
		}
		//Console Rooms
		for(ConsoleRoom interior : manager.unlockedConsoleRoom){
			if(!this.unlockedConsoleRoom.contains(interior))
				this.unlockedConsoleRoom.add(interior);
		}
		//Exteriors
		for(AbstractExterior ext : this.unlockedExteriors){
			if(!this.unlockedExteriors.contains(ext))
				this.unlockedExteriors.add(ext);
		}
	}
}
