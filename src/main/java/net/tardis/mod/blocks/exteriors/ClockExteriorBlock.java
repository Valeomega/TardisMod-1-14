package net.tardis.mod.blocks.exteriors;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.helper.VoxelShapeUtils;

public class ClockExteriorBlock extends ExteriorBlock {


    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {

        Direction dir = state.get(BlockStateProperties.HORIZONTAL_FACING);

        return VoxelShapeUtils.rotateHorizontal(VoxelShapes.create(0, 0, 0.35, 1, 1, 0.85), dir);
    }

}
