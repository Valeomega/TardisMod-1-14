package net.tardis.mod.recipe.serializers;

import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.recipe.SpectrometerRecipe;

import javax.annotation.Nullable;

public class SpectrometerRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<SpectrometerRecipe>{

    @Override
    public SpectrometerRecipe read(ResourceLocation recipeId, JsonObject json) {

        int ticks = 200;
        if(json.has("processing_ticks"))
            ticks = json.get("processing_ticks").getAsInt();

        SpectrometerRecipe recipe = new SpectrometerRecipe(ticks, Ingredient.deserialize(json.get("ingredient")), new ResourceLocation(json.get("result").getAsString()));
        recipe.setRegistryName(recipeId);
        return recipe;
    }

    @Nullable
    @Override
    public SpectrometerRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {

        SpectrometerRecipe recipe = new SpectrometerRecipe(buffer.readInt(), Ingredient.read(buffer), buffer.readResourceLocation());
        recipe.setRegistryName(recipeId);
        return recipe;
    }

    @Override
    public void write(PacketBuffer buffer, SpectrometerRecipe recipe) {
        buffer.writeInt(recipe.getTicks());
        recipe.getIngredient().write(buffer);
        buffer.writeResourceLocation(recipe.getSchematic());
    }

}
