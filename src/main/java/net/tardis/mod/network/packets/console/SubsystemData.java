package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class SubsystemData implements ConsoleData{

	private boolean canBeUsed;
	private ResourceLocation key;
	private boolean activated;
	
	public SubsystemData(ResourceLocation key, boolean canBeUsed, boolean activated) {
		this.key = key;
		this.canBeUsed = canBeUsed;
		this.activated = activated;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		for(Subsystem s : tile.getSubSystems()){
			if(s.getEntry().getRegistryName().equals(key)) {
				s.setCanBeUsed(this.canBeUsed);
				s.setActivated(activated);
				break;
			}
		}
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeResourceLocation(key);
		buff.writeBoolean(canBeUsed);
		buff.writeBoolean(this.activated);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.key = buff.readResourceLocation();
		this.canBeUsed = buff.readBoolean();
		this.activated = buff.readBoolean();
	}

	@Override
	public ConsoleUpdateMessage.DataTypes getDataType() {
		return ConsoleUpdateMessage.DataTypes.SUBSYSTEM;
	}

}
