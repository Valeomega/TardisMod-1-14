package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.KeltConsoleModel;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;

public class KeltConsoleRenderer extends TileEntityRenderer<KeltConsoleTile> {
	
	public static final KeltConsoleModel MODEL = new KeltConsoleModel();

	public KeltConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(KeltConsoleTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 1.8, 0.5);
		RenderSystem.enableRescaleNormal();
		matrixStackIn.scale(1.2F, 1.2F, 1.2F);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/kelt.png");
		MODEL.render(tile, 1.2F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
		RenderSystem.disableRescaleNormal();
		matrixStackIn.pop();
	}
}
