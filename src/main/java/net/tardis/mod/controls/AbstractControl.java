package net.tardis.mod.controls;
import javax.annotation.Nullable;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;
/** Template properties for a Control's logic class
 * <p> A Control is made up of a {@linkplain ControlEntity} and a data class that inherits from an implementation of this template class
 * <br> The ControlEntity is the physical manifestation of the Control
 * <br> The logic handling is all controlled by classes that implement this class
 * <br> Author: @Spectre0987
*/
public abstract class AbstractControl implements INBTSerializable<CompoundNBT> {
	
	private final ControlEntry entry;
	
	public AbstractControl(ControlEntry entry) {
		this.entry = entry;
	}
	
	/** Determine how large the control entity which this control will spawn is*/
	public EntitySize getSize() {return EntitySize.flexible(1, 1);}
	
	/**
	 * Returns true if whatever the point of this control worked
	 * @param console
	 * @param player
	 * @return
	 */
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {return true;}
	
	/**
	 * The offset amount which this control's entity will be.
	 * <br> Starts from the middle of the console block
	 */
	public abstract Vector3d getPos();
	
	/**
	 * Determine what happens if the player left click (punches) the control's entity
	 * @param console
	 * @param player
	 * @return
	 */
    public boolean onHit(ConsoleTile console, PlayerEntity player) {
		return this.onRightClicked(console, player);
	}
	
    /** Sets the ConsoleTile instance that this ControlEntity will be bound to. 
	 * <br> We need to know the ConsoleTile instance so we can determine the entity's position and size around the console*/
	public abstract void setConsole(ConsoleTile console, ControlEntity entity);
	
	/** Gets the current ConsoleTile instance that this entity is bound to*/
	public abstract ConsoleTile getConsole();
    
	/** The sound event to be played when the control fails to activate*/
	public abstract SoundEvent getFailSound(ConsoleTile console);

	public abstract SoundEvent getSuccessSound(ConsoleTile console);
    
	/** The string based key that can be used for data generators*/
	public abstract String getTranslationKey();
	
	public abstract TranslationTextComponent getDisplayName();
	
	/** Get the amount of ticks to be used in determining an animation's length in the Console's model file
	 * <br> 1.17: Deprecate this and figure out new system for animations*/
	public abstract int getAnimationTicks();
	public abstract void setAnimationTicks(int ticks);
	public abstract int getMaxAnimationTicks();
	
	/** Gets the time at which the current progress of this control's animation*/
	public abstract float getAnimationProgress();
	
	/** Extra functions that will be handled when the control entity is updated. 
	 * <br> E.g. Resetting values*/
	public abstract void onPacketUpdate();
	
	/** Mark this control as needing a packet update*/
	public abstract void markDirty();
	
	/** Clean up the control's existing data when it's being updated
	 * <br> Handle other things during data cleanup here, if needed for special cases*/
	public abstract void clean();
	
	/** Determine if this control needs a packet update*/
	public abstract boolean isDirty();
	
	/** Determine if this control entity needs to be glowing
	 * <br> E.g. If we want to highlight this control for a tutorial, or for when flight events occur
	 * <br> Currently unused*/
	public abstract boolean isGlowing();
	
	public abstract void setGlow(boolean glow);
	
	public abstract ControlEntity getEntity();
	
	/** The key used to get additional nbt data for this control.
	 * <br> E.g. Handbrake amount*/
	@Nullable
	public abstract ResourceLocation getAdditionalDataSaveKey();
	
	/** Gets the Registry Object instance for this control
	 * <br> This is needed when we want to get a specific control*/
	public ControlEntry getEntry() {
		return this.entry;
	}

}
