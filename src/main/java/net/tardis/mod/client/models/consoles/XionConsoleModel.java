package net.tardis.mod.client.models.consoles;

import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.IHaveMonitor;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.TileModel;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

// Made with Blockbench 3.7.5
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class XionConsoleModel extends Model implements TileModel<XionConsoleTile>, IHaveMonitor{
	private final LightModelRenderer glow_rotor;
	private final ModelRenderer glow_rotorbase;
	private final ModelRenderer glow_rotor_spike;
	private final LightModelRenderer glow;
	private final ModelRenderer glow_glass1;
	private final ModelRenderer panel2;
	private final ModelRenderer plain3;
	private final ModelRenderer bevel3;
	private final ModelRenderer front3;
	private final ModelRenderer underlip3;
	private final ModelRenderer undercurve3;
	private final ModelRenderer backbend3;
	private final ModelRenderer shin3;
	private final ModelRenderer ankle3;
	private final ModelRenderer foot3;
	private final ModelRenderer spine2;
	private final ModelRenderer rib2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer plain4;
	private final ModelRenderer bevel4;
	private final ModelRenderer front4;
	private final ModelRenderer underlip4;
	private final ModelRenderer undercurve4;
	private final ModelRenderer backbend4;
	private final ModelRenderer shin4;
	private final ModelRenderer ankle4;
	private final ModelRenderer foot4;
	private final ModelRenderer glow_glass2;
	private final ModelRenderer panel3;
	private final ModelRenderer plain2;
	private final ModelRenderer bevel2;
	private final ModelRenderer front5;
	private final ModelRenderer underlip2;
	private final ModelRenderer undercurve5;
	private final ModelRenderer backbend2;
	private final ModelRenderer shin2;
	private final ModelRenderer ankle2;
	private final ModelRenderer foot2;
	private final ModelRenderer spine3;
	private final ModelRenderer rib3;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer plain5;
	private final ModelRenderer bevel5;
	private final ModelRenderer front6;
	private final ModelRenderer underlip5;
	private final ModelRenderer undercurve6;
	private final ModelRenderer backbend5;
	private final ModelRenderer shin5;
	private final ModelRenderer ankle5;
	private final ModelRenderer foot5;
	private final ModelRenderer glow_glass3;
	private final ModelRenderer panel4;
	private final ModelRenderer plain6;
	private final ModelRenderer bevel6;
	private final ModelRenderer front7;
	private final ModelRenderer underlip6;
	private final ModelRenderer undercurve7;
	private final ModelRenderer backbend6;
	private final ModelRenderer shin6;
	private final ModelRenderer ankle6;
	private final ModelRenderer foot6;
	private final ModelRenderer spine4;
	private final ModelRenderer rib4;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer plain7;
	private final ModelRenderer bevel7;
	private final ModelRenderer front8;
	private final ModelRenderer underlip7;
	private final ModelRenderer undercurve8;
	private final ModelRenderer backbend7;
	private final ModelRenderer shin7;
	private final ModelRenderer ankle7;
	private final ModelRenderer foot7;
	private final ModelRenderer glow_glass4;
	private final ModelRenderer panel5;
	private final ModelRenderer plain8;
	private final ModelRenderer bevel8;
	private final ModelRenderer front9;
	private final ModelRenderer underlip8;
	private final ModelRenderer undercurve9;
	private final ModelRenderer backbend8;
	private final ModelRenderer shin8;
	private final ModelRenderer ankle8;
	private final ModelRenderer foot8;
	private final ModelRenderer spine5;
	private final ModelRenderer rib5;
	private final ModelRenderer bone9;
	private final ModelRenderer bone10;
	private final ModelRenderer plain9;
	private final ModelRenderer bevel9;
	private final ModelRenderer front10;
	private final ModelRenderer underlip9;
	private final ModelRenderer undercurve10;
	private final ModelRenderer backbend9;
	private final ModelRenderer shin9;
	private final ModelRenderer ankle9;
	private final ModelRenderer foot9;
	private final ModelRenderer glow_glass5;
	private final ModelRenderer panel6;
	private final ModelRenderer plain10;
	private final ModelRenderer bevel10;
	private final ModelRenderer front11;
	private final ModelRenderer underlip10;
	private final ModelRenderer undercurve11;
	private final ModelRenderer backbend10;
	private final ModelRenderer shin10;
	private final ModelRenderer ankle10;
	private final ModelRenderer foot10;
	private final ModelRenderer spine6;
	private final ModelRenderer rib6;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer plain11;
	private final ModelRenderer bevel11;
	private final ModelRenderer front12;
	private final ModelRenderer underlip11;
	private final ModelRenderer undercurve12;
	private final ModelRenderer backbend11;
	private final ModelRenderer shin11;
	private final ModelRenderer ankle11;
	private final ModelRenderer foot11;
	private final ModelRenderer glow_glass6;
	private final ModelRenderer panel7;
	private final ModelRenderer plain12;
	private final ModelRenderer bevel12;
	private final ModelRenderer front13;
	private final ModelRenderer underlip12;
	private final ModelRenderer undercurve13;
	private final ModelRenderer backbend12;
	private final ModelRenderer shin12;
	private final ModelRenderer ankle12;
	private final ModelRenderer foot12;
	private final ModelRenderer spine7;
	private final ModelRenderer rib7;
	private final ModelRenderer bone20;
	private final ModelRenderer bone21;
	private final ModelRenderer plain13;
	private final ModelRenderer bevel13;
	private final ModelRenderer front14;
	private final ModelRenderer underlip13;
	private final ModelRenderer undercurve14;
	private final ModelRenderer backbend13;
	private final ModelRenderer shin13;
	private final ModelRenderer ankle13;
	private final ModelRenderer foot13;
	private final ModelRenderer glow_guts;
	private final ModelRenderer stations;
	private final ModelRenderer coralribs1;
	private final ModelRenderer panel0;
	private final ModelRenderer front2;
	private final ModelRenderer undercurve2;
	private final ModelRenderer spine0;
	private final ModelRenderer rib0;
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer plain0;
	private final ModelRenderer bevel0;
	private final ModelRenderer front0;
	private final ModelRenderer underlip0;
	private final ModelRenderer undercurve0;
	private final ModelRenderer backbend0;
	private final ModelRenderer shin0;
	private final ModelRenderer ankle0;
	private final ModelRenderer foot0;
	private final ModelRenderer coralribs2;
	private final ModelRenderer panel8;
	private final ModelRenderer front15;
	private final ModelRenderer undercurve15;
	private final ModelRenderer spine8;
	private final ModelRenderer rib8;
	private final ModelRenderer bone22;
	private final ModelRenderer bone23;
	private final ModelRenderer plain14;
	private final ModelRenderer bevel14;
	private final ModelRenderer front16;
	private final ModelRenderer underlip14;
	private final ModelRenderer undercurve16;
	private final ModelRenderer backbend14;
	private final ModelRenderer shin14;
	private final ModelRenderer ankle14;
	private final ModelRenderer foot14;
	private final ModelRenderer coralribs3;
	private final ModelRenderer panel9;
	private final ModelRenderer front17;
	private final ModelRenderer undercurve17;
	private final ModelRenderer spine9;
	private final ModelRenderer rib9;
	private final ModelRenderer bone24;
	private final ModelRenderer bone25;
	private final ModelRenderer plain15;
	private final ModelRenderer bevel15;
	private final ModelRenderer front18;
	private final ModelRenderer underlip15;
	private final ModelRenderer undercurve18;
	private final ModelRenderer backbend15;
	private final ModelRenderer shin15;
	private final ModelRenderer ankle15;
	private final ModelRenderer foot15;
	private final ModelRenderer coralribs4;
	private final ModelRenderer panel10;
	private final ModelRenderer front19;
	private final ModelRenderer undercurve19;
	private final ModelRenderer spine10;
	private final ModelRenderer rib10;
	private final ModelRenderer bone26;
	private final ModelRenderer bone27;
	private final ModelRenderer plain16;
	private final ModelRenderer bevel16;
	private final ModelRenderer front20;
	private final ModelRenderer underlip16;
	private final ModelRenderer undercurve20;
	private final ModelRenderer backbend16;
	private final ModelRenderer shin16;
	private final ModelRenderer ankle16;
	private final ModelRenderer foot16;
	private final ModelRenderer coralribs5;
	private final ModelRenderer panel11;
	private final ModelRenderer front21;
	private final ModelRenderer undercurve21;
	private final ModelRenderer spine11;
	private final ModelRenderer rib11;
	private final ModelRenderer bone28;
	private final ModelRenderer bone29;
	private final ModelRenderer plain17;
	private final ModelRenderer bevel17;
	private final ModelRenderer front22;
	private final ModelRenderer underlip17;
	private final ModelRenderer undercurve22;
	private final ModelRenderer backbend17;
	private final ModelRenderer shin17;
	private final ModelRenderer ankle17;
	private final ModelRenderer foot17;
	private final ModelRenderer coralribs6;
	private final ModelRenderer panel12;
	private final ModelRenderer front23;
	private final ModelRenderer undercurve23;
	private final ModelRenderer spine12;
	private final ModelRenderer rib12;
	private final ModelRenderer bone30;
	private final ModelRenderer bone31;
	private final ModelRenderer plain18;
	private final ModelRenderer bevel18;
	private final ModelRenderer front24;
	private final ModelRenderer underlip18;
	private final ModelRenderer undercurve24;
	private final ModelRenderer backbend18;
	private final ModelRenderer shin18;
	private final ModelRenderer ankle18;
	private final ModelRenderer foot18;
	private final ModelRenderer controls;
	private final ModelRenderer panels;
	private final ModelRenderer north;
	private final ModelRenderer ctrlset_6;
	private final ModelRenderer top_6;
	private final ModelRenderer glow_eye;
	private final ModelRenderer telepathics;
	private final ModelRenderer glow_strips;
	private final ModelRenderer rim_6;
	private final ModelRenderer keypad_b6;
	private final ModelRenderer glow_buttons_6;
	private final ModelRenderer siderib_6;
	private final ModelRenderer northwest;
	private final ModelRenderer ctrlset_2;
	private final ModelRenderer top_2;
	private final ModelRenderer fusebox;
	private final ModelRenderer button_trip;
	private final ModelRenderer lamp;
	private final ModelRenderer glow_bulb;
	private final ModelRenderer glow_tube;
	private final ModelRenderer mid_2;
	private final ModelRenderer wires_2;
	private final ModelRenderer gauge;
	private final ModelRenderer rim_2;
	private final ModelRenderer button_switch;
	private final ModelRenderer coms;
	private final ModelRenderer phone_base;
	private final ModelRenderer handset;
	private final ModelRenderer cord;
	private final ModelRenderer bone15;
	private final ModelRenderer bone16;
	private final ModelRenderer phone_grill;
	private final ModelRenderer phone_keys;
	private final ModelRenderer siderib_2;
	private final ModelRenderer northeast;
	private final ModelRenderer ctrlset_3;
	private final ModelRenderer top_3;
	private final ModelRenderer mid_3;
	private final ModelRenderer bone34;
	private final ModelRenderer rim_3;
	private final ModelRenderer keyboard;
	private final ModelRenderer randomizer;
	private final ModelRenderer spinner;
	private final ModelRenderer bone17;
	private final ModelRenderer bone18;
	private final ModelRenderer bone19;
	private final ModelRenderer siderib_3;
	private final ModelRenderer south;
	private final ModelRenderer ctrlset_4;
	private final ModelRenderer top_4;
	private final ModelRenderer mid_4;
	private final ModelRenderer glow_spin_glass_x;
	private final ModelRenderer base_4;
	private final ModelRenderer rim_4;
	private final ModelRenderer keypad_a4;
	private final ModelRenderer keypad_b4;
	private final ModelRenderer siderib_4;
	private final ModelRenderer snake_wire_4;
	private final ModelRenderer gear_spin;
	private final ModelRenderer spike_4;
	private final ModelRenderer spike_4b;
	private final ModelRenderer bend_4;
	private final ModelRenderer slope_4;
	private final ModelRenderer vert_4;
	private final ModelRenderer glow_vert_4;
	private final ModelRenderer southeast;
	private final ModelRenderer ctrlset_1;
	private final ModelRenderer top_1;
	private final ModelRenderer sonic_port;
	private final ModelRenderer xyz_cords;
	private final ModelRenderer inc_x;
	private final ModelRenderer inc_y;
	private final ModelRenderer inc_z;
	private final ModelRenderer rim_1;
	private final ModelRenderer keypad_a1;
	private final ModelRenderer keypad_b1;
	private final ModelRenderer glow_buttons_b1;
	private final ModelRenderer siderib_1;
	private final ModelRenderer southwest;
	private final ModelRenderer ctrlset_5;
	private final ModelRenderer top_5;
	private final ModelRenderer dim_select2;
	private final ModelRenderer wheel_rotate_z3;
	private final ModelRenderer bone32;
	private final ModelRenderer bone33;
	private final ModelRenderer mid_5;
	private final ModelRenderer dummy_buttons_5;
	private final ModelRenderer glow_dummy_5;
	private final ModelRenderer globe;
	private final ModelRenderer spin_y;
	private final ModelRenderer glow_globe;
	private final ModelRenderer rim_5;
	private final ModelRenderer keypad_a5;
	private final ModelRenderer keypad_b5;
	private final ModelRenderer dummy_buttons_2;
	private final ModelRenderer siderib_5;
	private final ModelRenderer rib_controls;
	private final ModelRenderer west_rib;
	private final ModelRenderer upper_rib;
	private final ModelRenderer big_gauge;
	private final ModelRenderer big_gauge2;
	private final ModelRenderer jumbo_slider;
	private final ModelRenderer jumbo_rotate_x;
	private final ModelRenderer base;
	private final ModelRenderer bevel_rib;
	private final ModelRenderer handbrake;
	private final ModelRenderer handbreak_rotate_x;
	private final ModelRenderer vertical_rib;
	private final ModelRenderer med_plate;
	private final ModelRenderer nw_rib;
	private final ModelRenderer throttle;
	private final ModelRenderer throttle_rotate_x;
	private final ModelRenderer throttle_base;
	private final ModelRenderer lever_mount;
	private final ModelRenderer upper_rib3;
	private final ModelRenderer fast_return;
	private final ModelRenderer switches;
	private final ModelRenderer dummy_button2;
	private final ModelRenderer dummy_button3;
	private final ModelRenderer bevel_nwr;
	private final ModelRenderer vertical_rib3;
	private final ModelRenderer med_plate3;
	private final ModelRenderer sw_rib;
	private final ModelRenderer upper_rib6;
	private final ModelRenderer big_plate6;
	private final ModelRenderer resitsters;
	private final ModelRenderer landing_rotate_x;
	private final ModelRenderer bevel_rib5;
	private final ModelRenderer small_plate5;
	private final ModelRenderer small_plate6;
	private final ModelRenderer vertical_rib6;
	private final ModelRenderer east_rib;
	private final ModelRenderer upper_rib5;
	private final ModelRenderer refuler;
	private final ModelRenderer refuelknob_rotate;
	private final ModelRenderer tanks;
	private final ModelRenderer bevel_rib4;
	private final ModelRenderer small_plate3;
	private final ModelRenderer glow_dial;
	private final ModelRenderer plate3_needle;
	private final ModelRenderer small_plate4;
	private final ModelRenderer vertical_rib5;
	private final ModelRenderer facing;
	private final ModelRenderer facing_rotate_z;
	private final ModelRenderer bone13;
	private final ModelRenderer bone14;
	private final ModelRenderer winch_plate;
	private final ModelRenderer ne_rib;
	private final ModelRenderer upper_rib4;
	private final ModelRenderer stablizers;
	private final ModelRenderer pull_rotate_x;
	private final ModelRenderer pull_leaverbase;
	private final ModelRenderer deskbell;
	private final ModelRenderer bevel_rib3;
	private final ModelRenderer small_plate2;
	private final ModelRenderer vertical_rib4;
	private final ModelRenderer med_plate4;
	private final ModelRenderer se_rib;
	private final ModelRenderer upper_rib2;
	private final ModelRenderer monitor;
	private final ModelRenderer screen3;
	private final ModelRenderer door_switch;
	private final ModelRenderer capasitors;
	private final ModelRenderer buttons;
	private final ModelRenderer bevel_rib2;
	private final ModelRenderer increment_select;
	private final ModelRenderer crank_rotate_y;
	private final ModelRenderer crank_mount2;
	private final ModelRenderer vertical_rib2;
	private final ModelRenderer med_plate2;
	private final ModelRenderer guts;
	private final ModelRenderer bone35;

	public XionConsoleModel(Function<ResourceLocation, RenderType> function) {
		super(function);
		textureWidth = 512;
		textureHeight = 512;

		glow_rotor = new LightModelRenderer(this);
		glow_rotor.setRotationPoint(0.0F, 25.0F, 0.0F);
		

		glow_rotorbase = new ModelRenderer(this);
		glow_rotorbase.setRotationPoint(0.0F, -111.0F, -0.5F);
		glow_rotor.addChild(glow_rotorbase);
		glow_rotorbase.setTextureOffset(0, 122).addBox(-18.0F, -2.0F, -9.5F, 35.0F, 4.0F, 20.0F, 0.0F, false);
		glow_rotorbase.setTextureOffset(41, 133).addBox(-15.0F, -2.0F, 10.5F, 34.0F, 4.0F, 4.0F, 0.0F, false);
		glow_rotorbase.setTextureOffset(41, 133).addBox(-15.0F, -2.0F, -13.5F, 34.0F, 4.0F, 4.0F, 0.0F, false);
		glow_rotorbase.setTextureOffset(41, 133).addBox(-11.0F, -2.0F, 14.5F, 22.0F, 4.0F, 4.0F, 0.0F, false);
		glow_rotorbase.setTextureOffset(41, 133).addBox(-11.0F, -2.0F, -17.5F, 22.0F, 4.0F, 4.0F, 0.0F, false);
		glow_rotorbase.setTextureOffset(41, 133).addBox(-4.0F, -2.0F, 18.5F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		glow_rotorbase.setTextureOffset(41, 133).addBox(-4.0F, -2.0F, -21.5F, 8.0F, 4.0F, 4.0F, 0.0F, false);

		glow_rotor_spike = new ModelRenderer(this);
		glow_rotor_spike.setRotationPoint(-1.25F, -133.0F, 0.0F);
		glow_rotor.addChild(glow_rotor_spike);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-5.5F, -3.0F, -9.5F, 15.0F, 9.0F, 15.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-2.5F, -7.0F, -6.5F, 8.0F, 4.0F, 9.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-7.5F, 6.0F, -11.5F, 19.0F, 14.0F, 19.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-6.5F, -4.0F, 1.5F, 10.0F, 10.0F, 5.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(4.5F, 3.0F, 2.5F, 8.0F, 24.0F, 6.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-12.5F, 4.0F, -5.5F, 7.0F, 23.0F, 10.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-9.5F, 9.0F, 4.5F, 7.0F, 18.0F, 5.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-0.5F, 9.0F, 7.5F, 7.0F, 18.0F, 5.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(6.5F, 12.0F, -7.5F, 7.0F, 8.0F, 7.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(0.5F, 5.0F, -12.5F, 4.0F, 22.0F, 3.0F, 0.0F, false);
		glow_rotor_spike.setTextureOffset(11, 130).addBox(-6.5F, 0.0F, -10.5F, 3.0F, 6.0F, 7.0F, 0.0F, false);

		glow = new LightModelRenderer(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		glow_glass1 = new ModelRenderer(this);
		glow_glass1.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_glass1);
		

		panel2 = new ModelRenderer(this);
		panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass1.addChild(panel2);
		

		plain3 = new ModelRenderer(this);
		plain3.setRotationPoint(0.0F, -70.9898F, -44.2791F);
		panel2.addChild(plain3);
		setRotationAngle(plain3, -1.1345F, 0.0F, 0.0F);
		plain3.setTextureOffset(42, 22).addBox(-28.0F, 1.8312F, -1.0089F, 56.0F, 16.0F, 8.0F, 0.0F, false);
		plain3.setTextureOffset(46, 6).addBox(-24.0F, -14.1688F, -1.0089F, 48.0F, 16.0F, 8.0F, 0.0F, false);

		bevel3 = new ModelRenderer(this);
		bevel3.setRotationPoint(0.0F, -60.6347F, -60.7874F);
		panel2.addChild(bevel3);
		setRotationAngle(bevel3, -0.6981F, 0.0F, 0.0F);
		bevel3.setTextureOffset(38, 40).addBox(-32.0F, -2.8097F, -2.4602F, 64.0F, 12.0F, 8.0F, 0.0F, false);

		front3 = new ModelRenderer(this);
		front3.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel2.addChild(front3);
		front3.setTextureOffset(38, 54).addBox(-32.0F, -5.1759F, -4.5795F, 64.0F, 16.0F, 8.0F, 0.0F, false);

		underlip3 = new ModelRenderer(this);
		underlip3.setRotationPoint(0.0F, -43.7571F, -64.7704F);
		panel2.addChild(underlip3);
		setRotationAngle(underlip3, 0.6109F, 0.0F, 0.0F);
		underlip3.setTextureOffset(38, 81).addBox(-32.0F, 1.5679F, -5.7479F, 64.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve3 = new ModelRenderer(this);
		undercurve3.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel2.addChild(undercurve3);
		setRotationAngle(undercurve3, 1.0472F, 0.0F, 0.0F);
		

		backbend3 = new ModelRenderer(this);
		backbend3.setRotationPoint(0.0F, -20.714F, -43.1042F);
		panel2.addChild(backbend3);
		setRotationAngle(backbend3, 0.3491F, 0.0F, 0.0F);
		

		shin3 = new ModelRenderer(this);
		shin3.setRotationPoint(0.0F, -15.9761F, -43.8165F);
		panel2.addChild(shin3);
		setRotationAngle(shin3, -0.0873F, 0.0F, 0.0F);
		

		ankle3 = new ModelRenderer(this);
		ankle3.setRotationPoint(0.0F, -1.3682F, -47.2239F);
		panel2.addChild(ankle3);
		setRotationAngle(ankle3, -0.8727F, 0.0F, 0.0F);
		

		foot3 = new ModelRenderer(this);
		foot3.setRotationPoint(0.0F, 0.0068F, -56.2239F);
		panel2.addChild(foot3);
		setRotationAngle(foot3, -1.5708F, 0.0F, 0.0F);
		

		spine2 = new ModelRenderer(this);
		spine2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass1.addChild(spine2);
		setRotationAngle(spine2, 0.0F, -0.5236F, 0.0F);
		

		rib2 = new ModelRenderer(this);
		rib2.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine2.addChild(rib2);
		setRotationAngle(rib2, 0.0F, 0.5236F, 0.0F);
		rib2.setTextureOffset(65, 133).addBox(-12.0F, -30.0F, -48.75F, 24.0F, 20.0F, 12.0F, 0.0F, false);
		rib2.setTextureOffset(104, 116).addBox(-2.9874F, -118.7731F, -24.218F, 6.0F, 48.0F, 6.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib2.addChild(bone3);
		setRotationAngle(bone3, -0.3491F, 0.0F, 0.0F);
		

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib2.addChild(bone4);
		setRotationAngle(bone4, -0.3491F, 0.0F, 0.0F);
		

		plain4 = new ModelRenderer(this);
		plain4.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine2.addChild(plain4);
		setRotationAngle(plain4, -1.1345F, 0.0F, 0.0F);
		

		bevel4 = new ModelRenderer(this);
		bevel4.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine2.addChild(bevel4);
		setRotationAngle(bevel4, -0.6981F, 0.0F, 0.0F);
		

		front4 = new ModelRenderer(this);
		front4.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine2.addChild(front4);
		front4.setTextureOffset(66, 129).addBox(-13.6077F, 16.0F, 25.4308F, 28.0F, 24.0F, 8.0F, 0.0F, false);
		front4.setTextureOffset(38, 114).addBox(-9.0F, -67.7731F, 50.3863F, 19.0F, 47.0F, 4.0F, 0.0F, false);

		underlip4 = new ModelRenderer(this);
		underlip4.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine2.addChild(underlip4);
		setRotationAngle(underlip4, 0.6109F, 0.0F, 0.0F);
		

		undercurve4 = new ModelRenderer(this);
		undercurve4.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine2.addChild(undercurve4);
		setRotationAngle(undercurve4, 1.0472F, 0.0F, 0.0F);
		

		backbend4 = new ModelRenderer(this);
		backbend4.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine2.addChild(backbend4);
		setRotationAngle(backbend4, 0.3491F, 0.0F, 0.0F);
		

		shin4 = new ModelRenderer(this);
		shin4.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine2.addChild(shin4);
		setRotationAngle(shin4, -0.0873F, 0.0F, 0.0F);
		

		ankle4 = new ModelRenderer(this);
		ankle4.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine2.addChild(ankle4);
		setRotationAngle(ankle4, -0.8727F, 0.0F, 0.0F);
		

		foot4 = new ModelRenderer(this);
		foot4.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine2.addChild(foot4);
		setRotationAngle(foot4, -1.5708F, 0.0F, 0.0F);
		

		glow_glass2 = new ModelRenderer(this);
		glow_glass2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_glass2);
		setRotationAngle(glow_glass2, 0.0F, -1.0472F, 0.0F);
		

		panel3 = new ModelRenderer(this);
		panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass2.addChild(panel3);
		

		plain2 = new ModelRenderer(this);
		plain2.setRotationPoint(0.0F, -70.9898F, -44.2791F);
		panel3.addChild(plain2);
		setRotationAngle(plain2, -1.1345F, 0.0F, 0.0F);
		plain2.setTextureOffset(42, 22).addBox(-28.0F, 1.8312F, -1.0089F, 56.0F, 16.0F, 8.0F, 0.0F, false);
		plain2.setTextureOffset(46, 6).addBox(-24.0F, -14.1688F, -1.0089F, 48.0F, 16.0F, 8.0F, 0.0F, false);

		bevel2 = new ModelRenderer(this);
		bevel2.setRotationPoint(0.0F, -60.6347F, -60.7874F);
		panel3.addChild(bevel2);
		setRotationAngle(bevel2, -0.6981F, 0.0F, 0.0F);
		bevel2.setTextureOffset(38, 40).addBox(-32.0F, -2.8097F, -2.4602F, 64.0F, 12.0F, 8.0F, 0.0F, false);

		front5 = new ModelRenderer(this);
		front5.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel3.addChild(front5);
		front5.setTextureOffset(38, 54).addBox(-32.0F, -5.1759F, -4.5795F, 64.0F, 16.0F, 8.0F, 0.0F, false);

		underlip2 = new ModelRenderer(this);
		underlip2.setRotationPoint(0.0F, -43.7571F, -64.7704F);
		panel3.addChild(underlip2);
		setRotationAngle(underlip2, 0.6109F, 0.0F, 0.0F);
		underlip2.setTextureOffset(38, 81).addBox(-32.0F, 1.5679F, -5.7479F, 64.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve5 = new ModelRenderer(this);
		undercurve5.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel3.addChild(undercurve5);
		setRotationAngle(undercurve5, 1.0472F, 0.0F, 0.0F);
		

		backbend2 = new ModelRenderer(this);
		backbend2.setRotationPoint(0.0F, -20.714F, -43.1042F);
		panel3.addChild(backbend2);
		setRotationAngle(backbend2, 0.3491F, 0.0F, 0.0F);
		

		shin2 = new ModelRenderer(this);
		shin2.setRotationPoint(0.0F, -15.9761F, -43.8165F);
		panel3.addChild(shin2);
		setRotationAngle(shin2, -0.0873F, 0.0F, 0.0F);
		

		ankle2 = new ModelRenderer(this);
		ankle2.setRotationPoint(0.0F, -1.3682F, -47.2239F);
		panel3.addChild(ankle2);
		setRotationAngle(ankle2, -0.8727F, 0.0F, 0.0F);
		

		foot2 = new ModelRenderer(this);
		foot2.setRotationPoint(0.0F, 0.0068F, -56.2239F);
		panel3.addChild(foot2);
		setRotationAngle(foot2, -1.5708F, 0.0F, 0.0F);
		

		spine3 = new ModelRenderer(this);
		spine3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass2.addChild(spine3);
		setRotationAngle(spine3, 0.0F, -0.5236F, 0.0F);
		

		rib3 = new ModelRenderer(this);
		rib3.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine3.addChild(rib3);
		setRotationAngle(rib3, 0.0F, 0.5236F, 0.0F);
		rib3.setTextureOffset(65, 133).addBox(-12.0F, -30.0F, -48.75F, 24.0F, 20.0F, 12.0F, 0.0F, false);
		rib3.setTextureOffset(104, 116).addBox(-2.9874F, -118.7731F, -24.218F, 6.0F, 48.0F, 6.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib3.addChild(bone5);
		setRotationAngle(bone5, -0.3491F, 0.0F, 0.0F);
		

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib3.addChild(bone6);
		setRotationAngle(bone6, -0.3491F, 0.0F, 0.0F);
		

		plain5 = new ModelRenderer(this);
		plain5.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine3.addChild(plain5);
		setRotationAngle(plain5, -1.1345F, 0.0F, 0.0F);
		

		bevel5 = new ModelRenderer(this);
		bevel5.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine3.addChild(bevel5);
		setRotationAngle(bevel5, -0.6981F, 0.0F, 0.0F);
		

		front6 = new ModelRenderer(this);
		front6.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine3.addChild(front6);
		front6.setTextureOffset(66, 129).addBox(-13.6077F, 16.0F, 25.4308F, 28.0F, 24.0F, 8.0F, 0.0F, false);
		front6.setTextureOffset(38, 114).addBox(-9.0F, -67.7731F, 50.3863F, 19.0F, 47.0F, 4.0F, 0.0F, false);

		underlip5 = new ModelRenderer(this);
		underlip5.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine3.addChild(underlip5);
		setRotationAngle(underlip5, 0.6109F, 0.0F, 0.0F);
		

		undercurve6 = new ModelRenderer(this);
		undercurve6.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine3.addChild(undercurve6);
		setRotationAngle(undercurve6, 1.0472F, 0.0F, 0.0F);
		

		backbend5 = new ModelRenderer(this);
		backbend5.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine3.addChild(backbend5);
		setRotationAngle(backbend5, 0.3491F, 0.0F, 0.0F);
		

		shin5 = new ModelRenderer(this);
		shin5.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine3.addChild(shin5);
		setRotationAngle(shin5, -0.0873F, 0.0F, 0.0F);
		

		ankle5 = new ModelRenderer(this);
		ankle5.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine3.addChild(ankle5);
		setRotationAngle(ankle5, -0.8727F, 0.0F, 0.0F);
		

		foot5 = new ModelRenderer(this);
		foot5.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine3.addChild(foot5);
		setRotationAngle(foot5, -1.5708F, 0.0F, 0.0F);
		

		glow_glass3 = new ModelRenderer(this);
		glow_glass3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_glass3);
		setRotationAngle(glow_glass3, 0.0F, -2.0944F, 0.0F);
		

		panel4 = new ModelRenderer(this);
		panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass3.addChild(panel4);
		

		plain6 = new ModelRenderer(this);
		plain6.setRotationPoint(0.0F, -70.9898F, -44.2791F);
		panel4.addChild(plain6);
		setRotationAngle(plain6, -1.1345F, 0.0F, 0.0F);
		plain6.setTextureOffset(42, 22).addBox(-28.0F, 1.8312F, -1.0089F, 56.0F, 16.0F, 8.0F, 0.0F, false);
		plain6.setTextureOffset(46, 6).addBox(-24.0F, -14.1688F, -1.0089F, 48.0F, 16.0F, 8.0F, 0.0F, false);

		bevel6 = new ModelRenderer(this);
		bevel6.setRotationPoint(0.0F, -60.6347F, -60.7874F);
		panel4.addChild(bevel6);
		setRotationAngle(bevel6, -0.6981F, 0.0F, 0.0F);
		bevel6.setTextureOffset(38, 40).addBox(-32.0F, -2.8097F, -2.4602F, 64.0F, 12.0F, 8.0F, 0.0F, false);

		front7 = new ModelRenderer(this);
		front7.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel4.addChild(front7);
		front7.setTextureOffset(38, 54).addBox(-32.0F, -5.1759F, -4.5795F, 64.0F, 16.0F, 8.0F, 0.0F, false);

		underlip6 = new ModelRenderer(this);
		underlip6.setRotationPoint(0.0F, -43.7571F, -64.7704F);
		panel4.addChild(underlip6);
		setRotationAngle(underlip6, 0.6109F, 0.0F, 0.0F);
		underlip6.setTextureOffset(38, 81).addBox(-32.0F, 1.5679F, -5.7479F, 64.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve7 = new ModelRenderer(this);
		undercurve7.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel4.addChild(undercurve7);
		setRotationAngle(undercurve7, 1.0472F, 0.0F, 0.0F);
		

		backbend6 = new ModelRenderer(this);
		backbend6.setRotationPoint(0.0F, -20.714F, -43.1042F);
		panel4.addChild(backbend6);
		setRotationAngle(backbend6, 0.3491F, 0.0F, 0.0F);
		

		shin6 = new ModelRenderer(this);
		shin6.setRotationPoint(0.0F, -15.9761F, -43.8165F);
		panel4.addChild(shin6);
		setRotationAngle(shin6, -0.0873F, 0.0F, 0.0F);
		

		ankle6 = new ModelRenderer(this);
		ankle6.setRotationPoint(0.0F, -1.3682F, -47.2239F);
		panel4.addChild(ankle6);
		setRotationAngle(ankle6, -0.8727F, 0.0F, 0.0F);
		

		foot6 = new ModelRenderer(this);
		foot6.setRotationPoint(0.0F, 0.0068F, -56.2239F);
		panel4.addChild(foot6);
		setRotationAngle(foot6, -1.5708F, 0.0F, 0.0F);
		

		spine4 = new ModelRenderer(this);
		spine4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass3.addChild(spine4);
		setRotationAngle(spine4, 0.0F, -0.5236F, 0.0F);
		

		rib4 = new ModelRenderer(this);
		rib4.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine4.addChild(rib4);
		setRotationAngle(rib4, 0.0F, 0.5236F, 0.0F);
		rib4.setTextureOffset(65, 133).addBox(-12.0F, -30.0F, -48.75F, 24.0F, 20.0F, 12.0F, 0.0F, false);
		rib4.setTextureOffset(104, 116).addBox(-2.9874F, -118.7731F, -24.218F, 6.0F, 48.0F, 6.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib4.addChild(bone7);
		setRotationAngle(bone7, -0.3491F, 0.0F, 0.0F);
		

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib4.addChild(bone8);
		setRotationAngle(bone8, -0.3491F, 0.0F, 0.0F);
		

		plain7 = new ModelRenderer(this);
		plain7.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine4.addChild(plain7);
		setRotationAngle(plain7, -1.1345F, 0.0F, 0.0F);
		

		bevel7 = new ModelRenderer(this);
		bevel7.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine4.addChild(bevel7);
		setRotationAngle(bevel7, -0.6981F, 0.0F, 0.0F);
		

		front8 = new ModelRenderer(this);
		front8.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine4.addChild(front8);
		front8.setTextureOffset(66, 129).addBox(-13.6077F, 16.0F, 25.4308F, 28.0F, 24.0F, 8.0F, 0.0F, false);
		front8.setTextureOffset(38, 114).addBox(-9.0F, -67.7731F, 50.3863F, 19.0F, 47.0F, 4.0F, 0.0F, false);

		underlip7 = new ModelRenderer(this);
		underlip7.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine4.addChild(underlip7);
		setRotationAngle(underlip7, 0.6109F, 0.0F, 0.0F);
		

		undercurve8 = new ModelRenderer(this);
		undercurve8.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine4.addChild(undercurve8);
		setRotationAngle(undercurve8, 1.0472F, 0.0F, 0.0F);
		

		backbend7 = new ModelRenderer(this);
		backbend7.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine4.addChild(backbend7);
		setRotationAngle(backbend7, 0.3491F, 0.0F, 0.0F);
		

		shin7 = new ModelRenderer(this);
		shin7.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine4.addChild(shin7);
		setRotationAngle(shin7, -0.0873F, 0.0F, 0.0F);
		

		ankle7 = new ModelRenderer(this);
		ankle7.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine4.addChild(ankle7);
		setRotationAngle(ankle7, -0.8727F, 0.0F, 0.0F);
		

		foot7 = new ModelRenderer(this);
		foot7.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine4.addChild(foot7);
		setRotationAngle(foot7, -1.5708F, 0.0F, 0.0F);
		

		glow_glass4 = new ModelRenderer(this);
		glow_glass4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_glass4);
		setRotationAngle(glow_glass4, 0.0F, 3.1416F, 0.0F);
		

		panel5 = new ModelRenderer(this);
		panel5.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass4.addChild(panel5);
		

		plain8 = new ModelRenderer(this);
		plain8.setRotationPoint(0.0F, -70.9898F, -44.2791F);
		panel5.addChild(plain8);
		setRotationAngle(plain8, -1.1345F, 0.0F, 0.0F);
		plain8.setTextureOffset(42, 22).addBox(-28.0F, 1.8312F, -1.0089F, 56.0F, 16.0F, 8.0F, 0.0F, false);
		plain8.setTextureOffset(46, 6).addBox(-24.0F, -14.1688F, -1.0089F, 48.0F, 16.0F, 8.0F, 0.0F, false);

		bevel8 = new ModelRenderer(this);
		bevel8.setRotationPoint(0.0F, -60.6347F, -60.7874F);
		panel5.addChild(bevel8);
		setRotationAngle(bevel8, -0.6981F, 0.0F, 0.0F);
		bevel8.setTextureOffset(38, 40).addBox(-32.0F, -2.8097F, -2.4602F, 64.0F, 12.0F, 8.0F, 0.0F, false);

		front9 = new ModelRenderer(this);
		front9.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel5.addChild(front9);
		front9.setTextureOffset(38, 54).addBox(-32.0F, -5.1759F, -4.5795F, 64.0F, 16.0F, 8.0F, 0.0F, false);

		underlip8 = new ModelRenderer(this);
		underlip8.setRotationPoint(0.0F, -43.7571F, -64.7704F);
		panel5.addChild(underlip8);
		setRotationAngle(underlip8, 0.6109F, 0.0F, 0.0F);
		underlip8.setTextureOffset(38, 81).addBox(-32.0F, 1.5679F, -5.7479F, 64.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve9 = new ModelRenderer(this);
		undercurve9.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel5.addChild(undercurve9);
		setRotationAngle(undercurve9, 1.0472F, 0.0F, 0.0F);
		

		backbend8 = new ModelRenderer(this);
		backbend8.setRotationPoint(0.0F, -20.714F, -43.1042F);
		panel5.addChild(backbend8);
		setRotationAngle(backbend8, 0.3491F, 0.0F, 0.0F);
		

		shin8 = new ModelRenderer(this);
		shin8.setRotationPoint(0.0F, -15.9761F, -43.8165F);
		panel5.addChild(shin8);
		setRotationAngle(shin8, -0.0873F, 0.0F, 0.0F);
		

		ankle8 = new ModelRenderer(this);
		ankle8.setRotationPoint(0.0F, -1.3682F, -47.2239F);
		panel5.addChild(ankle8);
		setRotationAngle(ankle8, -0.8727F, 0.0F, 0.0F);
		

		foot8 = new ModelRenderer(this);
		foot8.setRotationPoint(0.0F, 0.0068F, -56.2239F);
		panel5.addChild(foot8);
		setRotationAngle(foot8, -1.5708F, 0.0F, 0.0F);
		

		spine5 = new ModelRenderer(this);
		spine5.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass4.addChild(spine5);
		setRotationAngle(spine5, 0.0F, -0.5236F, 0.0F);
		

		rib5 = new ModelRenderer(this);
		rib5.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine5.addChild(rib5);
		setRotationAngle(rib5, 0.0F, 0.5236F, 0.0F);
		rib5.setTextureOffset(65, 133).addBox(-12.0F, -30.0F, -48.75F, 24.0F, 20.0F, 12.0F, 0.0F, false);
		rib5.setTextureOffset(104, 116).addBox(-2.9874F, -118.7731F, -24.218F, 6.0F, 48.0F, 6.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib5.addChild(bone9);
		setRotationAngle(bone9, -0.3491F, 0.0F, 0.0F);
		

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib5.addChild(bone10);
		setRotationAngle(bone10, -0.3491F, 0.0F, 0.0F);
		

		plain9 = new ModelRenderer(this);
		plain9.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine5.addChild(plain9);
		setRotationAngle(plain9, -1.1345F, 0.0F, 0.0F);
		

		bevel9 = new ModelRenderer(this);
		bevel9.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine5.addChild(bevel9);
		setRotationAngle(bevel9, -0.6981F, 0.0F, 0.0F);
		

		front10 = new ModelRenderer(this);
		front10.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine5.addChild(front10);
		front10.setTextureOffset(66, 129).addBox(-13.6077F, 16.0F, 25.4308F, 28.0F, 24.0F, 8.0F, 0.0F, false);
		front10.setTextureOffset(38, 114).addBox(-9.0F, -67.7731F, 50.3863F, 19.0F, 47.0F, 4.0F, 0.0F, false);

		underlip9 = new ModelRenderer(this);
		underlip9.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine5.addChild(underlip9);
		setRotationAngle(underlip9, 0.6109F, 0.0F, 0.0F);
		

		undercurve10 = new ModelRenderer(this);
		undercurve10.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine5.addChild(undercurve10);
		setRotationAngle(undercurve10, 1.0472F, 0.0F, 0.0F);
		

		backbend9 = new ModelRenderer(this);
		backbend9.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine5.addChild(backbend9);
		setRotationAngle(backbend9, 0.3491F, 0.0F, 0.0F);
		

		shin9 = new ModelRenderer(this);
		shin9.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine5.addChild(shin9);
		setRotationAngle(shin9, -0.0873F, 0.0F, 0.0F);
		

		ankle9 = new ModelRenderer(this);
		ankle9.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine5.addChild(ankle9);
		setRotationAngle(ankle9, -0.8727F, 0.0F, 0.0F);
		

		foot9 = new ModelRenderer(this);
		foot9.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine5.addChild(foot9);
		setRotationAngle(foot9, -1.5708F, 0.0F, 0.0F);
		

		glow_glass5 = new ModelRenderer(this);
		glow_glass5.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_glass5);
		setRotationAngle(glow_glass5, 0.0F, 2.0944F, 0.0F);
		

		panel6 = new ModelRenderer(this);
		panel6.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass5.addChild(panel6);
		

		plain10 = new ModelRenderer(this);
		plain10.setRotationPoint(0.0F, -70.9898F, -44.2791F);
		panel6.addChild(plain10);
		setRotationAngle(plain10, -1.1345F, 0.0F, 0.0F);
		plain10.setTextureOffset(42, 22).addBox(-28.0F, 1.8312F, -1.0089F, 56.0F, 16.0F, 8.0F, 0.0F, false);
		plain10.setTextureOffset(46, 6).addBox(-24.0F, -14.1688F, -1.0089F, 48.0F, 16.0F, 8.0F, 0.0F, false);

		bevel10 = new ModelRenderer(this);
		bevel10.setRotationPoint(0.0F, -60.6347F, -60.7874F);
		panel6.addChild(bevel10);
		setRotationAngle(bevel10, -0.6981F, 0.0F, 0.0F);
		bevel10.setTextureOffset(38, 40).addBox(-32.0F, -2.8097F, -2.4602F, 64.0F, 12.0F, 8.0F, 0.0F, false);

		front11 = new ModelRenderer(this);
		front11.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel6.addChild(front11);
		front11.setTextureOffset(38, 54).addBox(-32.0F, -5.1759F, -4.5795F, 64.0F, 16.0F, 8.0F, 0.0F, false);

		underlip10 = new ModelRenderer(this);
		underlip10.setRotationPoint(0.0F, -43.7571F, -64.7704F);
		panel6.addChild(underlip10);
		setRotationAngle(underlip10, 0.6109F, 0.0F, 0.0F);
		underlip10.setTextureOffset(38, 81).addBox(-32.0F, 1.5679F, -5.7479F, 64.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve11 = new ModelRenderer(this);
		undercurve11.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel6.addChild(undercurve11);
		setRotationAngle(undercurve11, 1.0472F, 0.0F, 0.0F);
		

		backbend10 = new ModelRenderer(this);
		backbend10.setRotationPoint(0.0F, -20.714F, -43.1042F);
		panel6.addChild(backbend10);
		setRotationAngle(backbend10, 0.3491F, 0.0F, 0.0F);
		

		shin10 = new ModelRenderer(this);
		shin10.setRotationPoint(0.0F, -15.9761F, -43.8165F);
		panel6.addChild(shin10);
		setRotationAngle(shin10, -0.0873F, 0.0F, 0.0F);
		

		ankle10 = new ModelRenderer(this);
		ankle10.setRotationPoint(0.0F, -1.3682F, -47.2239F);
		panel6.addChild(ankle10);
		setRotationAngle(ankle10, -0.8727F, 0.0F, 0.0F);
		

		foot10 = new ModelRenderer(this);
		foot10.setRotationPoint(0.0F, 0.0068F, -56.2239F);
		panel6.addChild(foot10);
		setRotationAngle(foot10, -1.5708F, 0.0F, 0.0F);
		

		spine6 = new ModelRenderer(this);
		spine6.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass5.addChild(spine6);
		setRotationAngle(spine6, 0.0F, -0.5236F, 0.0F);
		

		rib6 = new ModelRenderer(this);
		rib6.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine6.addChild(rib6);
		setRotationAngle(rib6, 0.0F, 0.5236F, 0.0F);
		rib6.setTextureOffset(65, 133).addBox(-12.0F, -30.0F, -48.75F, 24.0F, 20.0F, 12.0F, 0.0F, false);
		rib6.setTextureOffset(104, 116).addBox(-2.9874F, -118.7731F, -24.218F, 6.0F, 48.0F, 6.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib6.addChild(bone11);
		setRotationAngle(bone11, -0.3491F, 0.0F, 0.0F);
		

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib6.addChild(bone12);
		setRotationAngle(bone12, -0.3491F, 0.0F, 0.0F);
		

		plain11 = new ModelRenderer(this);
		plain11.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine6.addChild(plain11);
		setRotationAngle(plain11, -1.1345F, 0.0F, 0.0F);
		

		bevel11 = new ModelRenderer(this);
		bevel11.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine6.addChild(bevel11);
		setRotationAngle(bevel11, -0.6981F, 0.0F, 0.0F);
		

		front12 = new ModelRenderer(this);
		front12.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine6.addChild(front12);
		front12.setTextureOffset(66, 129).addBox(-13.6077F, 16.0F, 25.4308F, 28.0F, 24.0F, 8.0F, 0.0F, false);
		front12.setTextureOffset(38, 114).addBox(-9.0F, -67.7731F, 50.3863F, 19.0F, 47.0F, 4.0F, 0.0F, false);

		underlip11 = new ModelRenderer(this);
		underlip11.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine6.addChild(underlip11);
		setRotationAngle(underlip11, 0.6109F, 0.0F, 0.0F);
		

		undercurve12 = new ModelRenderer(this);
		undercurve12.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine6.addChild(undercurve12);
		setRotationAngle(undercurve12, 1.0472F, 0.0F, 0.0F);
		

		backbend11 = new ModelRenderer(this);
		backbend11.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine6.addChild(backbend11);
		setRotationAngle(backbend11, 0.3491F, 0.0F, 0.0F);
		

		shin11 = new ModelRenderer(this);
		shin11.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine6.addChild(shin11);
		setRotationAngle(shin11, -0.0873F, 0.0F, 0.0F);
		

		ankle11 = new ModelRenderer(this);
		ankle11.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine6.addChild(ankle11);
		setRotationAngle(ankle11, -0.8727F, 0.0F, 0.0F);
		

		foot11 = new ModelRenderer(this);
		foot11.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine6.addChild(foot11);
		setRotationAngle(foot11, -1.5708F, 0.0F, 0.0F);
		

		glow_glass6 = new ModelRenderer(this);
		glow_glass6.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_glass6);
		setRotationAngle(glow_glass6, 0.0F, 1.0472F, 0.0F);
		

		panel7 = new ModelRenderer(this);
		panel7.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass6.addChild(panel7);
		

		plain12 = new ModelRenderer(this);
		plain12.setRotationPoint(0.0F, -70.9898F, -44.2791F);
		panel7.addChild(plain12);
		setRotationAngle(plain12, -1.1345F, 0.0F, 0.0F);
		plain12.setTextureOffset(42, 22).addBox(-28.0F, 1.8312F, -1.0089F, 56.0F, 16.0F, 8.0F, 0.0F, false);
		plain12.setTextureOffset(46, 6).addBox(-24.0F, -14.1688F, -1.0089F, 48.0F, 16.0F, 8.0F, 0.0F, false);

		bevel12 = new ModelRenderer(this);
		bevel12.setRotationPoint(0.0F, -60.6347F, -60.7874F);
		panel7.addChild(bevel12);
		setRotationAngle(bevel12, -0.6981F, 0.0F, 0.0F);
		bevel12.setTextureOffset(38, 40).addBox(-32.0F, -2.8097F, -2.4602F, 64.0F, 12.0F, 8.0F, 0.0F, false);

		front13 = new ModelRenderer(this);
		front13.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel7.addChild(front13);
		front13.setTextureOffset(38, 54).addBox(-32.0F, -5.1759F, -4.5795F, 64.0F, 16.0F, 8.0F, 0.0F, false);

		underlip12 = new ModelRenderer(this);
		underlip12.setRotationPoint(0.0F, -43.7571F, -64.7704F);
		panel7.addChild(underlip12);
		setRotationAngle(underlip12, 0.6109F, 0.0F, 0.0F);
		underlip12.setTextureOffset(38, 81).addBox(-32.0F, 1.5679F, -5.7479F, 64.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve13 = new ModelRenderer(this);
		undercurve13.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel7.addChild(undercurve13);
		setRotationAngle(undercurve13, 1.0472F, 0.0F, 0.0F);
		

		backbend12 = new ModelRenderer(this);
		backbend12.setRotationPoint(0.0F, -20.714F, -43.1042F);
		panel7.addChild(backbend12);
		setRotationAngle(backbend12, 0.3491F, 0.0F, 0.0F);
		

		shin12 = new ModelRenderer(this);
		shin12.setRotationPoint(0.0F, -15.9761F, -43.8165F);
		panel7.addChild(shin12);
		setRotationAngle(shin12, -0.0873F, 0.0F, 0.0F);
		

		ankle12 = new ModelRenderer(this);
		ankle12.setRotationPoint(0.0F, -1.3682F, -47.2239F);
		panel7.addChild(ankle12);
		setRotationAngle(ankle12, -0.8727F, 0.0F, 0.0F);
		

		foot12 = new ModelRenderer(this);
		foot12.setRotationPoint(0.0F, 0.0068F, -56.2239F);
		panel7.addChild(foot12);
		setRotationAngle(foot12, -1.5708F, 0.0F, 0.0F);
		

		spine7 = new ModelRenderer(this);
		spine7.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_glass6.addChild(spine7);
		setRotationAngle(spine7, 0.0F, -0.5236F, 0.0F);
		

		rib7 = new ModelRenderer(this);
		rib7.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine7.addChild(rib7);
		setRotationAngle(rib7, 0.0F, 0.5236F, 0.0F);
		rib7.setTextureOffset(65, 133).addBox(-12.0F, -30.0F, -48.75F, 24.0F, 20.0F, 12.0F, 0.0F, false);
		rib7.setTextureOffset(104, 116).addBox(-2.9874F, -118.7731F, -24.218F, 6.0F, 48.0F, 6.0F, 0.0F, false);

		bone20 = new ModelRenderer(this);
		bone20.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib7.addChild(bone20);
		setRotationAngle(bone20, -0.3491F, 0.0F, 0.0F);
		

		bone21 = new ModelRenderer(this);
		bone21.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib7.addChild(bone21);
		setRotationAngle(bone21, -0.3491F, 0.0F, 0.0F);
		

		plain13 = new ModelRenderer(this);
		plain13.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine7.addChild(plain13);
		setRotationAngle(plain13, -1.1345F, 0.0F, 0.0F);
		

		bevel13 = new ModelRenderer(this);
		bevel13.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine7.addChild(bevel13);
		setRotationAngle(bevel13, -0.6981F, 0.0F, 0.0F);
		

		front14 = new ModelRenderer(this);
		front14.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine7.addChild(front14);
		front14.setTextureOffset(66, 129).addBox(-13.6077F, 16.0F, 25.4308F, 28.0F, 24.0F, 8.0F, 0.0F, false);
		front14.setTextureOffset(38, 114).addBox(-9.0F, -67.7731F, 50.3863F, 19.0F, 47.0F, 4.0F, 0.0F, false);

		underlip13 = new ModelRenderer(this);
		underlip13.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine7.addChild(underlip13);
		setRotationAngle(underlip13, 0.6109F, 0.0F, 0.0F);
		

		undercurve14 = new ModelRenderer(this);
		undercurve14.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine7.addChild(undercurve14);
		setRotationAngle(undercurve14, 1.0472F, 0.0F, 0.0F);
		

		backbend13 = new ModelRenderer(this);
		backbend13.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine7.addChild(backbend13);
		setRotationAngle(backbend13, 0.3491F, 0.0F, 0.0F);
		

		shin13 = new ModelRenderer(this);
		shin13.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine7.addChild(shin13);
		setRotationAngle(shin13, -0.0873F, 0.0F, 0.0F);
		

		ankle13 = new ModelRenderer(this);
		ankle13.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine7.addChild(ankle13);
		setRotationAngle(ankle13, -0.8727F, 0.0F, 0.0F);
		

		foot13 = new ModelRenderer(this);
		foot13.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine7.addChild(foot13);
		setRotationAngle(foot13, -1.5708F, 0.0F, 0.0F);
		

		glow_guts = new ModelRenderer(this);
		glow_guts.setRotationPoint(0.0F, -12.0F, 0.0F);
		glow.addChild(glow_guts);
		glow_guts.setTextureOffset(41, 131).addBox(-6.0F, -7.0F, -6.0F, 12.0F, 19.0F, 12.0F, 0.0F, false);
		glow_guts.setTextureOffset(47, 133).addBox(8.0F, -16.0F, -10.0F, 3.0F, 9.0F, 14.0F, 0.0F, false);
		glow_guts.setTextureOffset(47, 133).addBox(-15.0F, -16.0F, 13.0F, 13.0F, 8.0F, 2.0F, 0.0F, false);
		glow_guts.setTextureOffset(47, 133).addBox(-24.0F, 7.0F, 0.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		glow_guts.setTextureOffset(47, 133).addBox(21.0F, 7.0F, 11.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		glow_guts.setTextureOffset(47, 133).addBox(12.0F, 7.0F, -19.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);
		glow_guts.setTextureOffset(47, 133).addBox(-15.0F, -17.0F, -1.0F, 13.0F, 8.0F, 2.0F, 0.0F, false);

		stations = new ModelRenderer(this);
		stations.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		coralribs1 = new ModelRenderer(this);
		coralribs1.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(coralribs1);
		

		panel0 = new ModelRenderer(this);
		panel0.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs1.addChild(panel0);
		

		front2 = new ModelRenderer(this);
		front2.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel0.addChild(front2);
		front2.setTextureOffset(160, 96).addBox(-29.25F, -1.1759F, -5.5795F, 58.0F, 9.0F, 1.0F, 0.0F, false);
		front2.setTextureOffset(161, 74).addBox(-29.5F, -0.4259F, -6.0795F, 59.0F, 4.0F, 1.0F, 0.0F, false);

		undercurve2 = new ModelRenderer(this);
		undercurve2.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel0.addChild(undercurve2);
		setRotationAngle(undercurve2, 1.0472F, 0.0F, 0.0F);
		undercurve2.setTextureOffset(160, 124).addBox(-28.0F, -7.1975F, -5.7981F, 56.0F, 16.0F, 8.0F, 0.0F, false);

		spine0 = new ModelRenderer(this);
		spine0.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs1.addChild(spine0);
		setRotationAngle(spine0, 0.0F, -0.5236F, 0.0F);
		

		rib0 = new ModelRenderer(this);
		rib0.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine0.addChild(rib0);
		setRotationAngle(rib0, 0.0F, 0.5236F, 0.0F);
		rib0.setTextureOffset(157, 135).addBox(-24.0F, -11.0F, -50.0F, 48.0F, 4.0F, 12.0F, 0.0F, false);
		rib0.setTextureOffset(164, 136).addBox(-22.0F, -22.0F, -51.0F, 44.0F, 4.0F, 12.0F, 0.0F, false);
		rib0.setTextureOffset(176, 17).addBox(-9.8534F, -103.7731F, -35.8783F, 19.0F, 3.0F, 10.0F, 0.0F, false);
		rib0.setTextureOffset(154, 132).addBox(-2.9874F, -77.7731F, -30.218F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		rib0.setTextureOffset(178, 61).addBox(-6.8534F, -100.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib0.setTextureOffset(206, 47).addBox(-6.8534F, -106.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib0.setTextureOffset(182, 48).addBox(-6.8534F, -112.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib0.setTextureOffset(186, 58).addBox(-6.8534F, -116.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib0.setTextureOffset(173, 7).addBox(-6.8534F, -114.7731F, -30.8783F, 14.0F, 3.0F, 7.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib0.addChild(bone);
		setRotationAngle(bone, -0.3491F, 0.0F, 0.0F);
		bone.setTextureOffset(236, 134).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(206, 138).addBox(-3.5F, -1.5F, -1.5F, 6.0F, 5.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(208, 68).addBox(-8.5F, -6.8474F, -1.7798F, 15.0F, 3.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(166, 60).addBox(-8.5F, -11.5459F, -3.4899F, 15.0F, 3.0F, 4.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib0.addChild(bone2);
		setRotationAngle(bone2, -0.3491F, 0.0F, 0.0F);
		bone2.setTextureOffset(184, 136).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		plain0 = new ModelRenderer(this);
		plain0.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine0.addChild(plain0);
		setRotationAngle(plain0, -1.1345F, 0.0F, 0.0F);
		plain0.setTextureOffset(241, 38).addBox(-8.0F, -22.1688F, -1.0089F, 16.0F, 40.0F, 8.0F, 0.0F, false);
		plain0.setTextureOffset(243, 38).addBox(-6.0F, -25.4559F, -5.7139F, 12.0F, 19.0F, 8.0F, 0.0F, false);
		plain0.setTextureOffset(173, 36).addBox(-10.0F, -23.0751F, -0.5863F, 20.0F, 40.0F, 12.0F, 0.0F, false);

		bevel0 = new ModelRenderer(this);
		bevel0.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine0.addChild(bevel0);
		setRotationAngle(bevel0, -0.6981F, 0.0F, 0.0F);
		bevel0.setTextureOffset(241, 72).addBox(-8.0F, -2.8097F, -2.4602F, 16.0F, 12.0F, 8.0F, 0.0F, false);
		bevel0.setTextureOffset(243, 54).addBox(-6.0F, -26.8097F, 3.5398F, 12.0F, 12.0F, 8.0F, 0.0F, false);
		bevel0.setTextureOffset(204, 72).addBox(-10.0F, -3.4524F, -1.6941F, 20.0F, 12.0F, 11.0F, 0.0F, false);

		front0 = new ModelRenderer(this);
		front0.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine0.addChild(front0);
		front0.setTextureOffset(194, 52).addBox(-10.0F, -43.2731F, 40.3863F, 20.0F, 16.0F, 4.0F, 0.0F, false);
		front0.setTextureOffset(203, 12).addBox(-11.0F, -54.2731F, 35.3863F, 22.0F, 4.0F, 13.0F, 0.0F, false);
		front0.setTextureOffset(194, 52).addBox(-10.0F, -51.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front0.setTextureOffset(194, 52).addBox(-10.0F, -57.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front0.setTextureOffset(194, 52).addBox(-10.0F, -63.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front0.setTextureOffset(194, 52).addBox(-10.0F, -67.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front0.setTextureOffset(208, 10).addBox(-11.0F, -65.2731F, 41.3863F, 22.0F, 4.0F, 9.0F, 0.0F, false);
		front0.setTextureOffset(194, 52).addBox(-13.0F, -25.7731F, 42.3863F, 23.0F, 9.0F, 8.0F, 0.0F, false);
		front0.setTextureOffset(245, 53).addBox(-8.0F, -44.2731F, 38.3863F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		front0.setTextureOffset(241, 76).addBox(-8.0F, -5.1759F, -4.5795F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		front0.setTextureOffset(194, 73).addBox(-10.0F, -5.1759F, -3.5795F, 20.0F, 16.0F, 13.0F, 0.0F, false);

		underlip0 = new ModelRenderer(this);
		underlip0.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine0.addChild(underlip0);
		setRotationAngle(underlip0, 0.6109F, 0.0F, 0.0F);
		underlip0.setTextureOffset(246, 80).addBox(-8.0F, 1.5679F, -5.7479F, 16.0F, 8.0F, 8.0F, 0.0F, false);
		underlip0.setTextureOffset(196, 128).addBox(-12.0F, 1.1453F, -6.8416F, 24.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve0 = new ModelRenderer(this);
		undercurve0.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine0.addChild(undercurve0);
		setRotationAngle(undercurve0, 1.0472F, 0.0F, 0.0F);
		undercurve0.setTextureOffset(241, 52).addBox(-8.0F, -7.1975F, -5.7981F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		undercurve0.setTextureOffset(196, 52).addBox(-10.0F, -7.1975F, -4.7981F, 20.0F, 29.0F, 11.0F, 0.0F, false);

		backbend0 = new ModelRenderer(this);
		backbend0.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine0.addChild(backbend0);
		setRotationAngle(backbend0, 0.3491F, 0.0F, 0.0F);
		backbend0.setTextureOffset(241, 66).addBox(-8.0F, -10.0774F, -5.2695F, 16.0F, 12.0F, 8.0F, 0.0F, false);

		shin0 = new ModelRenderer(this);
		shin0.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine0.addChild(shin0);
		setRotationAngle(shin0, -0.0873F, 0.0F, 0.0F);
		shin0.setTextureOffset(240, 61).addBox(-8.0F, -3.8125F, -3.6666F, 16.0F, 11.0F, 9.0F, 0.0F, false);

		ankle0 = new ModelRenderer(this);
		ankle0.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine0.addChild(ankle0);
		setRotationAngle(ankle0, -0.8727F, 0.0F, 0.0F);
		ankle0.setTextureOffset(245, 75).addBox(-8.0F, -8.325F, -6.5104F, 16.0F, 13.0F, 4.0F, 0.0F, false);

		foot0 = new ModelRenderer(this);
		foot0.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine0.addChild(foot0);
		setRotationAngle(foot0, -1.5708F, 0.0F, 0.0F);
		foot0.setTextureOffset(244, 79).addBox(-9.0F, -6.0F, -4.0F, 18.0F, 20.0F, 4.0F, 0.0F, false);
		foot0.setTextureOffset(301, 73).addBox(-1.25F, 9.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot0.setTextureOffset(303, 74).addBox(-7.0F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot0.setTextureOffset(298, 81).addBox(4.5F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		coralribs2 = new ModelRenderer(this);
		coralribs2.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(coralribs2);
		setRotationAngle(coralribs2, 0.0F, -1.0472F, 0.0F);
		

		panel8 = new ModelRenderer(this);
		panel8.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs2.addChild(panel8);
		

		front15 = new ModelRenderer(this);
		front15.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel8.addChild(front15);
		front15.setTextureOffset(160, 96).addBox(-29.25F, -1.1759F, -5.5795F, 58.0F, 9.0F, 1.0F, 0.0F, false);
		front15.setTextureOffset(161, 74).addBox(-29.5F, -0.4259F, -6.0795F, 59.0F, 4.0F, 1.0F, 0.0F, false);

		undercurve15 = new ModelRenderer(this);
		undercurve15.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel8.addChild(undercurve15);
		setRotationAngle(undercurve15, 1.0472F, 0.0F, 0.0F);
		undercurve15.setTextureOffset(160, 124).addBox(-28.0F, -7.1975F, -5.7981F, 56.0F, 16.0F, 8.0F, 0.0F, false);

		spine8 = new ModelRenderer(this);
		spine8.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs2.addChild(spine8);
		setRotationAngle(spine8, 0.0F, -0.5236F, 0.0F);
		

		rib8 = new ModelRenderer(this);
		rib8.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine8.addChild(rib8);
		setRotationAngle(rib8, 0.0F, 0.5236F, 0.0F);
		rib8.setTextureOffset(152, 136).addBox(-24.0F, -11.0F, -50.0F, 48.0F, 4.0F, 12.0F, 0.0F, false);
		rib8.setTextureOffset(160, 133).addBox(-22.0F, -22.0F, -51.0F, 44.0F, 4.0F, 12.0F, 0.0F, false);
		rib8.setTextureOffset(176, 17).addBox(-9.8534F, -103.7731F, -35.8783F, 19.0F, 3.0F, 10.0F, 0.0F, false);
		rib8.setTextureOffset(154, 132).addBox(-2.9874F, -77.7731F, -30.218F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		rib8.setTextureOffset(178, 61).addBox(-6.8534F, -100.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib8.setTextureOffset(206, 47).addBox(-6.8534F, -106.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib8.setTextureOffset(182, 48).addBox(-6.8534F, -112.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib8.setTextureOffset(186, 58).addBox(-6.8534F, -116.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib8.setTextureOffset(173, 7).addBox(-6.8534F, -114.7731F, -30.8783F, 14.0F, 3.0F, 7.0F, 0.0F, false);

		bone22 = new ModelRenderer(this);
		bone22.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib8.addChild(bone22);
		setRotationAngle(bone22, -0.3491F, 0.0F, 0.0F);
		bone22.setTextureOffset(236, 134).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
		bone22.setTextureOffset(206, 138).addBox(-3.5F, -1.5F, -1.5F, 6.0F, 5.0F, 4.0F, 0.0F, false);
		bone22.setTextureOffset(208, 68).addBox(-8.5F, -6.8474F, -1.7798F, 15.0F, 3.0F, 4.0F, 0.0F, false);
		bone22.setTextureOffset(166, 60).addBox(-8.5F, -11.5459F, -3.4899F, 15.0F, 3.0F, 4.0F, 0.0F, false);

		bone23 = new ModelRenderer(this);
		bone23.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib8.addChild(bone23);
		setRotationAngle(bone23, -0.3491F, 0.0F, 0.0F);
		bone23.setTextureOffset(184, 136).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		plain14 = new ModelRenderer(this);
		plain14.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine8.addChild(plain14);
		setRotationAngle(plain14, -1.1345F, 0.0F, 0.0F);
		plain14.setTextureOffset(241, 38).addBox(-8.0F, -22.1688F, -1.0089F, 16.0F, 40.0F, 8.0F, 0.0F, false);
		plain14.setTextureOffset(173, 36).addBox(-10.0F, -23.0751F, -0.5863F, 20.0F, 40.0F, 12.0F, 0.0F, false);

		bevel14 = new ModelRenderer(this);
		bevel14.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine8.addChild(bevel14);
		setRotationAngle(bevel14, -0.6981F, 0.0F, 0.0F);
		bevel14.setTextureOffset(241, 72).addBox(-8.0F, -2.8097F, -2.4602F, 16.0F, 12.0F, 8.0F, 0.0F, false);
		bevel14.setTextureOffset(204, 72).addBox(-10.0F, -3.4524F, -1.6941F, 20.0F, 12.0F, 11.0F, 0.0F, false);

		front16 = new ModelRenderer(this);
		front16.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine8.addChild(front16);
		front16.setTextureOffset(194, 52).addBox(-10.0F, -43.2731F, 40.3863F, 20.0F, 16.0F, 4.0F, 0.0F, false);
		front16.setTextureOffset(203, 12).addBox(-11.0F, -54.2731F, 35.3863F, 22.0F, 4.0F, 13.0F, 0.0F, false);
		front16.setTextureOffset(194, 52).addBox(-10.0F, -51.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front16.setTextureOffset(194, 52).addBox(-10.0F, -57.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front16.setTextureOffset(194, 52).addBox(-10.0F, -63.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front16.setTextureOffset(194, 52).addBox(-10.0F, -67.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front16.setTextureOffset(208, 10).addBox(-11.0F, -65.2731F, 41.3863F, 22.0F, 4.0F, 9.0F, 0.0F, false);
		front16.setTextureOffset(194, 52).addBox(-13.0F, -25.7731F, 42.3863F, 23.0F, 9.0F, 8.0F, 0.0F, false);
		front16.setTextureOffset(245, 53).addBox(-8.0F, -44.2731F, 38.3863F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		front16.setTextureOffset(241, 76).addBox(-8.0F, -5.1759F, -4.5795F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		front16.setTextureOffset(194, 73).addBox(-10.0F, -5.1759F, -3.5795F, 20.0F, 16.0F, 13.0F, 0.0F, false);

		underlip14 = new ModelRenderer(this);
		underlip14.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine8.addChild(underlip14);
		setRotationAngle(underlip14, 0.6109F, 0.0F, 0.0F);
		underlip14.setTextureOffset(246, 80).addBox(-8.0F, 1.5679F, -5.7479F, 16.0F, 8.0F, 8.0F, 0.0F, false);
		underlip14.setTextureOffset(196, 128).addBox(-12.0F, 1.1453F, -6.8416F, 24.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve16 = new ModelRenderer(this);
		undercurve16.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine8.addChild(undercurve16);
		setRotationAngle(undercurve16, 1.0472F, 0.0F, 0.0F);
		undercurve16.setTextureOffset(241, 52).addBox(-8.0F, -7.1975F, -5.7981F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		undercurve16.setTextureOffset(196, 52).addBox(-10.0F, -7.1975F, -4.7981F, 20.0F, 29.0F, 11.0F, 0.0F, false);

		backbend14 = new ModelRenderer(this);
		backbend14.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine8.addChild(backbend14);
		setRotationAngle(backbend14, 0.3491F, 0.0F, 0.0F);
		backbend14.setTextureOffset(241, 66).addBox(-8.0F, -10.0774F, -5.2695F, 16.0F, 12.0F, 8.0F, 0.0F, false);

		shin14 = new ModelRenderer(this);
		shin14.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine8.addChild(shin14);
		setRotationAngle(shin14, -0.0873F, 0.0F, 0.0F);
		shin14.setTextureOffset(240, 61).addBox(-8.0F, -3.8125F, -3.6666F, 16.0F, 11.0F, 9.0F, 0.0F, false);

		ankle14 = new ModelRenderer(this);
		ankle14.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine8.addChild(ankle14);
		setRotationAngle(ankle14, -0.8727F, 0.0F, 0.0F);
		ankle14.setTextureOffset(245, 75).addBox(-8.0F, -8.325F, -6.5104F, 16.0F, 13.0F, 4.0F, 0.0F, false);

		foot14 = new ModelRenderer(this);
		foot14.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine8.addChild(foot14);
		setRotationAngle(foot14, -1.5708F, 0.0F, 0.0F);
		foot14.setTextureOffset(244, 79).addBox(-9.0F, -6.0F, -4.0F, 18.0F, 20.0F, 4.0F, 0.0F, false);
		foot14.setTextureOffset(301, 73).addBox(-1.25F, 9.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot14.setTextureOffset(303, 74).addBox(-7.0F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot14.setTextureOffset(298, 81).addBox(4.5F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		coralribs3 = new ModelRenderer(this);
		coralribs3.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(coralribs3);
		setRotationAngle(coralribs3, 0.0F, -2.0944F, 0.0F);
		

		panel9 = new ModelRenderer(this);
		panel9.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs3.addChild(panel9);
		

		front17 = new ModelRenderer(this);
		front17.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel9.addChild(front17);
		front17.setTextureOffset(160, 96).addBox(-29.25F, -1.1759F, -5.5795F, 58.0F, 9.0F, 1.0F, 0.0F, false);
		front17.setTextureOffset(161, 74).addBox(-29.5F, -0.4259F, -6.0795F, 59.0F, 4.0F, 1.0F, 0.0F, false);

		undercurve17 = new ModelRenderer(this);
		undercurve17.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel9.addChild(undercurve17);
		setRotationAngle(undercurve17, 1.0472F, 0.0F, 0.0F);
		undercurve17.setTextureOffset(160, 124).addBox(-28.0F, -7.1975F, -5.7981F, 56.0F, 16.0F, 8.0F, 0.0F, false);

		spine9 = new ModelRenderer(this);
		spine9.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs3.addChild(spine9);
		setRotationAngle(spine9, 0.0F, -0.5236F, 0.0F);
		

		rib9 = new ModelRenderer(this);
		rib9.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine9.addChild(rib9);
		setRotationAngle(rib9, 0.0F, 0.5236F, 0.0F);
		rib9.setTextureOffset(155, 131).addBox(-24.0F, -11.0F, -50.0F, 48.0F, 4.0F, 12.0F, 0.0F, false);
		rib9.setTextureOffset(160, 125).addBox(-22.0F, -22.0F, -51.0F, 44.0F, 4.0F, 12.0F, 0.0F, false);
		rib9.setTextureOffset(176, 17).addBox(-9.8534F, -103.7731F, -35.8783F, 19.0F, 3.0F, 10.0F, 0.0F, false);
		rib9.setTextureOffset(154, 132).addBox(-2.9874F, -77.7731F, -30.218F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		rib9.setTextureOffset(178, 61).addBox(-6.8534F, -100.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib9.setTextureOffset(206, 47).addBox(-6.8534F, -106.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib9.setTextureOffset(182, 48).addBox(-6.8534F, -112.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib9.setTextureOffset(186, 58).addBox(-6.8534F, -116.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib9.setTextureOffset(173, 7).addBox(-6.8534F, -114.7731F, -30.8783F, 14.0F, 3.0F, 7.0F, 0.0F, false);

		bone24 = new ModelRenderer(this);
		bone24.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib9.addChild(bone24);
		setRotationAngle(bone24, -0.3491F, 0.0F, 0.0F);
		bone24.setTextureOffset(236, 134).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
		bone24.setTextureOffset(206, 138).addBox(-3.5F, -1.5F, -1.5F, 6.0F, 5.0F, 4.0F, 0.0F, false);
		bone24.setTextureOffset(208, 68).addBox(-8.5F, -6.8474F, -1.7798F, 15.0F, 3.0F, 4.0F, 0.0F, false);
		bone24.setTextureOffset(166, 60).addBox(-8.5F, -11.5459F, -3.4899F, 15.0F, 3.0F, 4.0F, 0.0F, false);

		bone25 = new ModelRenderer(this);
		bone25.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib9.addChild(bone25);
		setRotationAngle(bone25, -0.3491F, 0.0F, 0.0F);
		bone25.setTextureOffset(184, 136).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		plain15 = new ModelRenderer(this);
		plain15.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine9.addChild(plain15);
		setRotationAngle(plain15, -1.1345F, 0.0F, 0.0F);
		plain15.setTextureOffset(241, 38).addBox(-8.0F, -22.1688F, -1.0089F, 16.0F, 40.0F, 8.0F, 0.0F, false);
		plain15.setTextureOffset(243, 38).addBox(-6.0F, -25.4559F, -5.7139F, 12.0F, 19.0F, 8.0F, 0.0F, false);
		plain15.setTextureOffset(173, 36).addBox(-10.0F, -23.0751F, -0.5863F, 20.0F, 40.0F, 12.0F, 0.0F, false);

		bevel15 = new ModelRenderer(this);
		bevel15.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine9.addChild(bevel15);
		setRotationAngle(bevel15, -0.6981F, 0.0F, 0.0F);
		bevel15.setTextureOffset(241, 72).addBox(-8.0F, -2.8097F, -2.4602F, 16.0F, 12.0F, 8.0F, 0.0F, false);
		bevel15.setTextureOffset(243, 54).addBox(-6.0F, -26.8097F, 3.5398F, 12.0F, 12.0F, 8.0F, 0.0F, false);
		bevel15.setTextureOffset(204, 72).addBox(-10.0F, -3.4524F, -1.6941F, 20.0F, 12.0F, 11.0F, 0.0F, false);

		front18 = new ModelRenderer(this);
		front18.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine9.addChild(front18);
		front18.setTextureOffset(194, 52).addBox(-10.0F, -43.2731F, 40.3863F, 20.0F, 16.0F, 4.0F, 0.0F, false);
		front18.setTextureOffset(203, 12).addBox(-11.0F, -54.2731F, 35.3863F, 22.0F, 4.0F, 13.0F, 0.0F, false);
		front18.setTextureOffset(194, 52).addBox(-10.0F, -51.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front18.setTextureOffset(194, 52).addBox(-10.0F, -57.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front18.setTextureOffset(194, 52).addBox(-10.0F, -63.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front18.setTextureOffset(194, 52).addBox(-10.0F, -67.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front18.setTextureOffset(208, 10).addBox(-11.0F, -65.2731F, 41.3863F, 22.0F, 4.0F, 9.0F, 0.0F, false);
		front18.setTextureOffset(194, 52).addBox(-13.0F, -25.7731F, 42.3863F, 23.0F, 9.0F, 8.0F, 0.0F, false);
		front18.setTextureOffset(245, 53).addBox(-8.0F, -44.2731F, 38.3863F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		front18.setTextureOffset(241, 76).addBox(-8.0F, -5.1759F, -4.5795F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		front18.setTextureOffset(194, 73).addBox(-10.0F, -5.1759F, -3.5795F, 20.0F, 16.0F, 13.0F, 0.0F, false);

		underlip15 = new ModelRenderer(this);
		underlip15.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine9.addChild(underlip15);
		setRotationAngle(underlip15, 0.6109F, 0.0F, 0.0F);
		underlip15.setTextureOffset(246, 80).addBox(-8.0F, 1.5679F, -5.7479F, 16.0F, 8.0F, 8.0F, 0.0F, false);
		underlip15.setTextureOffset(196, 128).addBox(-12.0F, 1.1453F, -6.8416F, 24.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve18 = new ModelRenderer(this);
		undercurve18.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine9.addChild(undercurve18);
		setRotationAngle(undercurve18, 1.0472F, 0.0F, 0.0F);
		undercurve18.setTextureOffset(241, 52).addBox(-8.0F, -7.1975F, -5.7981F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		undercurve18.setTextureOffset(196, 52).addBox(-10.0F, -7.1975F, -4.7981F, 20.0F, 29.0F, 11.0F, 0.0F, false);

		backbend15 = new ModelRenderer(this);
		backbend15.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine9.addChild(backbend15);
		setRotationAngle(backbend15, 0.3491F, 0.0F, 0.0F);
		backbend15.setTextureOffset(241, 66).addBox(-8.0F, -10.0774F, -5.2695F, 16.0F, 12.0F, 8.0F, 0.0F, false);

		shin15 = new ModelRenderer(this);
		shin15.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine9.addChild(shin15);
		setRotationAngle(shin15, -0.0873F, 0.0F, 0.0F);
		shin15.setTextureOffset(240, 61).addBox(-8.0F, -3.8125F, -3.6666F, 16.0F, 11.0F, 9.0F, 0.0F, false);

		ankle15 = new ModelRenderer(this);
		ankle15.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine9.addChild(ankle15);
		setRotationAngle(ankle15, -0.8727F, 0.0F, 0.0F);
		ankle15.setTextureOffset(245, 75).addBox(-8.0F, -8.325F, -6.5104F, 16.0F, 13.0F, 4.0F, 0.0F, false);

		foot15 = new ModelRenderer(this);
		foot15.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine9.addChild(foot15);
		setRotationAngle(foot15, -1.5708F, 0.0F, 0.0F);
		foot15.setTextureOffset(244, 79).addBox(-9.0F, -6.0F, -4.0F, 18.0F, 20.0F, 4.0F, 0.0F, false);
		foot15.setTextureOffset(301, 73).addBox(-1.25F, 9.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot15.setTextureOffset(303, 74).addBox(-7.0F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot15.setTextureOffset(298, 81).addBox(4.5F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		coralribs4 = new ModelRenderer(this);
		coralribs4.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(coralribs4);
		setRotationAngle(coralribs4, 0.0F, 3.1416F, 0.0F);
		

		panel10 = new ModelRenderer(this);
		panel10.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs4.addChild(panel10);
		

		front19 = new ModelRenderer(this);
		front19.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel10.addChild(front19);
		front19.setTextureOffset(160, 96).addBox(-29.25F, -1.1759F, -5.5795F, 58.0F, 9.0F, 1.0F, 0.0F, false);
		front19.setTextureOffset(161, 74).addBox(-29.5F, -0.4259F, -6.0795F, 59.0F, 4.0F, 1.0F, 0.0F, false);

		undercurve19 = new ModelRenderer(this);
		undercurve19.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel10.addChild(undercurve19);
		setRotationAngle(undercurve19, 1.0472F, 0.0F, 0.0F);
		undercurve19.setTextureOffset(160, 124).addBox(-28.0F, -7.1975F, -5.7981F, 56.0F, 16.0F, 8.0F, 0.0F, false);

		spine10 = new ModelRenderer(this);
		spine10.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs4.addChild(spine10);
		setRotationAngle(spine10, 0.0F, -0.5236F, 0.0F);
		

		rib10 = new ModelRenderer(this);
		rib10.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine10.addChild(rib10);
		setRotationAngle(rib10, 0.0F, 0.5236F, 0.0F);
		rib10.setTextureOffset(153, 135).addBox(-24.0F, -11.0F, -50.0F, 48.0F, 4.0F, 12.0F, 0.0F, false);
		rib10.setTextureOffset(163, 132).addBox(-22.0F, -22.0F, -51.0F, 44.0F, 4.0F, 12.0F, 0.0F, false);
		rib10.setTextureOffset(176, 17).addBox(-9.8534F, -103.7731F, -35.8783F, 19.0F, 3.0F, 10.0F, 0.0F, false);
		rib10.setTextureOffset(154, 132).addBox(-2.9874F, -77.7731F, -30.218F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		rib10.setTextureOffset(178, 61).addBox(-6.8534F, -100.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib10.setTextureOffset(206, 47).addBox(-6.8534F, -106.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib10.setTextureOffset(182, 48).addBox(-6.8534F, -112.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib10.setTextureOffset(186, 58).addBox(-6.8534F, -116.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib10.setTextureOffset(173, 7).addBox(-6.8534F, -114.7731F, -30.8783F, 14.0F, 3.0F, 7.0F, 0.0F, false);

		bone26 = new ModelRenderer(this);
		bone26.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib10.addChild(bone26);
		setRotationAngle(bone26, -0.3491F, 0.0F, 0.0F);
		bone26.setTextureOffset(236, 134).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
		bone26.setTextureOffset(206, 138).addBox(-3.5F, -1.5F, -1.5F, 6.0F, 5.0F, 4.0F, 0.0F, false);
		bone26.setTextureOffset(208, 68).addBox(-8.5F, -6.8474F, -1.7798F, 15.0F, 3.0F, 4.0F, 0.0F, false);
		bone26.setTextureOffset(166, 60).addBox(-8.5F, -11.5459F, -3.4899F, 15.0F, 3.0F, 4.0F, 0.0F, false);

		bone27 = new ModelRenderer(this);
		bone27.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib10.addChild(bone27);
		setRotationAngle(bone27, -0.3491F, 0.0F, 0.0F);
		bone27.setTextureOffset(184, 136).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		plain16 = new ModelRenderer(this);
		plain16.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine10.addChild(plain16);
		setRotationAngle(plain16, -1.1345F, 0.0F, 0.0F);
		plain16.setTextureOffset(241, 38).addBox(-8.0F, -22.1688F, -1.0089F, 16.0F, 40.0F, 8.0F, 0.0F, false);
		plain16.setTextureOffset(173, 36).addBox(-10.0F, -23.0751F, -0.5863F, 20.0F, 40.0F, 12.0F, 0.0F, false);

		bevel16 = new ModelRenderer(this);
		bevel16.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine10.addChild(bevel16);
		setRotationAngle(bevel16, -0.6981F, 0.0F, 0.0F);
		bevel16.setTextureOffset(241, 72).addBox(-8.0F, -2.8097F, -2.4602F, 16.0F, 12.0F, 8.0F, 0.0F, false);
		bevel16.setTextureOffset(204, 72).addBox(-10.0F, -3.4524F, -1.6941F, 20.0F, 12.0F, 11.0F, 0.0F, false);

		front20 = new ModelRenderer(this);
		front20.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine10.addChild(front20);
		front20.setTextureOffset(194, 52).addBox(-10.0F, -43.2731F, 40.3863F, 20.0F, 16.0F, 4.0F, 0.0F, false);
		front20.setTextureOffset(203, 12).addBox(-11.0F, -54.2731F, 35.3863F, 22.0F, 4.0F, 13.0F, 0.0F, false);
		front20.setTextureOffset(194, 52).addBox(-10.0F, -51.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front20.setTextureOffset(194, 52).addBox(-10.0F, -57.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front20.setTextureOffset(194, 52).addBox(-10.0F, -63.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front20.setTextureOffset(194, 52).addBox(-10.0F, -67.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front20.setTextureOffset(208, 10).addBox(-11.0F, -65.2731F, 41.3863F, 22.0F, 4.0F, 9.0F, 0.0F, false);
		front20.setTextureOffset(194, 52).addBox(-13.0F, -25.7731F, 42.3863F, 23.0F, 9.0F, 8.0F, 0.0F, false);
		front20.setTextureOffset(245, 53).addBox(-8.0F, -44.2731F, 38.3863F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		front20.setTextureOffset(241, 76).addBox(-8.0F, -5.1759F, -4.5795F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		front20.setTextureOffset(194, 73).addBox(-10.0F, -5.1759F, -3.5795F, 20.0F, 16.0F, 13.0F, 0.0F, false);

		underlip16 = new ModelRenderer(this);
		underlip16.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine10.addChild(underlip16);
		setRotationAngle(underlip16, 0.6109F, 0.0F, 0.0F);
		underlip16.setTextureOffset(246, 80).addBox(-8.0F, 1.5679F, -5.7479F, 16.0F, 8.0F, 8.0F, 0.0F, false);
		underlip16.setTextureOffset(196, 128).addBox(-12.0F, 1.1453F, -6.8416F, 24.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve20 = new ModelRenderer(this);
		undercurve20.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine10.addChild(undercurve20);
		setRotationAngle(undercurve20, 1.0472F, 0.0F, 0.0F);
		undercurve20.setTextureOffset(241, 52).addBox(-8.0F, -7.1975F, -5.7981F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		undercurve20.setTextureOffset(196, 52).addBox(-10.0F, -7.1975F, -4.7981F, 20.0F, 29.0F, 11.0F, 0.0F, false);

		backbend16 = new ModelRenderer(this);
		backbend16.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine10.addChild(backbend16);
		setRotationAngle(backbend16, 0.3491F, 0.0F, 0.0F);
		backbend16.setTextureOffset(241, 66).addBox(-8.0F, -10.0774F, -5.2695F, 16.0F, 12.0F, 8.0F, 0.0F, false);

		shin16 = new ModelRenderer(this);
		shin16.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine10.addChild(shin16);
		setRotationAngle(shin16, -0.0873F, 0.0F, 0.0F);
		shin16.setTextureOffset(240, 61).addBox(-8.0F, -3.8125F, -3.6666F, 16.0F, 11.0F, 9.0F, 0.0F, false);

		ankle16 = new ModelRenderer(this);
		ankle16.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine10.addChild(ankle16);
		setRotationAngle(ankle16, -0.8727F, 0.0F, 0.0F);
		ankle16.setTextureOffset(245, 75).addBox(-8.0F, -8.325F, -6.5104F, 16.0F, 13.0F, 4.0F, 0.0F, false);

		foot16 = new ModelRenderer(this);
		foot16.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine10.addChild(foot16);
		setRotationAngle(foot16, -1.5708F, 0.0F, 0.0F);
		foot16.setTextureOffset(244, 79).addBox(-9.0F, -6.0F, -4.0F, 18.0F, 20.0F, 4.0F, 0.0F, false);
		foot16.setTextureOffset(301, 73).addBox(-1.25F, 9.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot16.setTextureOffset(303, 74).addBox(-7.0F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot16.setTextureOffset(298, 81).addBox(4.5F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		coralribs5 = new ModelRenderer(this);
		coralribs5.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(coralribs5);
		setRotationAngle(coralribs5, 0.0F, 2.0944F, 0.0F);
		

		panel11 = new ModelRenderer(this);
		panel11.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs5.addChild(panel11);
		

		front21 = new ModelRenderer(this);
		front21.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel11.addChild(front21);
		front21.setTextureOffset(160, 96).addBox(-29.25F, -1.1759F, -5.5795F, 58.0F, 9.0F, 1.0F, 0.0F, false);
		front21.setTextureOffset(161, 74).addBox(-29.5F, -0.4259F, -6.0795F, 59.0F, 4.0F, 1.0F, 0.0F, false);

		undercurve21 = new ModelRenderer(this);
		undercurve21.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel11.addChild(undercurve21);
		setRotationAngle(undercurve21, 1.0472F, 0.0F, 0.0F);
		undercurve21.setTextureOffset(160, 124).addBox(-28.0F, -7.1975F, -5.7981F, 56.0F, 16.0F, 8.0F, 0.0F, false);

		spine11 = new ModelRenderer(this);
		spine11.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs5.addChild(spine11);
		setRotationAngle(spine11, 0.0F, -0.5236F, 0.0F);
		

		rib11 = new ModelRenderer(this);
		rib11.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine11.addChild(rib11);
		setRotationAngle(rib11, 0.0F, 0.5236F, 0.0F);
		rib11.setTextureOffset(156, 135).addBox(-24.0F, -11.0F, -50.0F, 48.0F, 4.0F, 12.0F, 0.0F, false);
		rib11.setTextureOffset(163, 133).addBox(-22.0F, -22.0F, -51.0F, 44.0F, 4.0F, 12.0F, 0.0F, false);
		rib11.setTextureOffset(176, 17).addBox(-9.8534F, -103.7731F, -35.8783F, 19.0F, 3.0F, 10.0F, 0.0F, false);
		rib11.setTextureOffset(154, 132).addBox(-2.9874F, -77.7731F, -30.218F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		rib11.setTextureOffset(178, 61).addBox(-6.8534F, -100.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib11.setTextureOffset(206, 47).addBox(-6.8534F, -106.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib11.setTextureOffset(182, 48).addBox(-6.8534F, -112.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib11.setTextureOffset(186, 58).addBox(-6.8534F, -116.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib11.setTextureOffset(173, 7).addBox(-6.8534F, -114.7731F, -30.8783F, 14.0F, 3.0F, 7.0F, 0.0F, false);

		bone28 = new ModelRenderer(this);
		bone28.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib11.addChild(bone28);
		setRotationAngle(bone28, -0.3491F, 0.0F, 0.0F);
		bone28.setTextureOffset(236, 134).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
		bone28.setTextureOffset(206, 138).addBox(-3.5F, -1.5F, -1.5F, 6.0F, 5.0F, 4.0F, 0.0F, false);
		bone28.setTextureOffset(208, 68).addBox(-8.5F, -6.8474F, -1.7798F, 15.0F, 3.0F, 4.0F, 0.0F, false);
		bone28.setTextureOffset(166, 60).addBox(-8.5F, -11.5459F, -3.4899F, 15.0F, 3.0F, 4.0F, 0.0F, false);

		bone29 = new ModelRenderer(this);
		bone29.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib11.addChild(bone29);
		setRotationAngle(bone29, -0.3491F, 0.0F, 0.0F);
		bone29.setTextureOffset(184, 136).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		plain17 = new ModelRenderer(this);
		plain17.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine11.addChild(plain17);
		setRotationAngle(plain17, -1.1345F, 0.0F, 0.0F);
		plain17.setTextureOffset(241, 38).addBox(-8.0F, -22.1688F, -1.0089F, 16.0F, 40.0F, 8.0F, 0.0F, false);
		plain17.setTextureOffset(243, 38).addBox(-6.0F, -25.4559F, -5.7139F, 12.0F, 19.0F, 8.0F, 0.0F, false);
		plain17.setTextureOffset(173, 36).addBox(-10.0F, -23.0751F, -0.5863F, 20.0F, 40.0F, 12.0F, 0.0F, false);

		bevel17 = new ModelRenderer(this);
		bevel17.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine11.addChild(bevel17);
		setRotationAngle(bevel17, -0.6981F, 0.0F, 0.0F);
		bevel17.setTextureOffset(241, 72).addBox(-8.0F, -2.8097F, -2.4602F, 16.0F, 12.0F, 8.0F, 0.0F, false);
		bevel17.setTextureOffset(243, 54).addBox(-6.0F, -26.8097F, 3.5398F, 12.0F, 12.0F, 8.0F, 0.0F, false);
		bevel17.setTextureOffset(204, 72).addBox(-10.0F, -3.4524F, -1.6941F, 20.0F, 12.0F, 11.0F, 0.0F, false);

		front22 = new ModelRenderer(this);
		front22.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine11.addChild(front22);
		front22.setTextureOffset(194, 52).addBox(-10.0F, -43.2731F, 40.3863F, 20.0F, 16.0F, 4.0F, 0.0F, false);
		front22.setTextureOffset(203, 12).addBox(-11.0F, -54.2731F, 35.3863F, 22.0F, 4.0F, 13.0F, 0.0F, false);
		front22.setTextureOffset(194, 52).addBox(-10.0F, -51.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front22.setTextureOffset(194, 52).addBox(-10.0F, -57.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front22.setTextureOffset(194, 52).addBox(-10.0F, -63.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front22.setTextureOffset(194, 52).addBox(-10.0F, -67.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front22.setTextureOffset(208, 10).addBox(-11.0F, -65.2731F, 41.3863F, 22.0F, 4.0F, 9.0F, 0.0F, false);
		front22.setTextureOffset(194, 52).addBox(-13.0F, -25.7731F, 42.3863F, 23.0F, 9.0F, 8.0F, 0.0F, false);
		front22.setTextureOffset(245, 53).addBox(-8.0F, -44.2731F, 38.3863F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		front22.setTextureOffset(241, 76).addBox(-8.0F, -5.1759F, -4.5795F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		front22.setTextureOffset(194, 73).addBox(-10.0F, -5.1759F, -3.5795F, 20.0F, 16.0F, 13.0F, 0.0F, false);

		underlip17 = new ModelRenderer(this);
		underlip17.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine11.addChild(underlip17);
		setRotationAngle(underlip17, 0.6109F, 0.0F, 0.0F);
		underlip17.setTextureOffset(246, 80).addBox(-8.0F, 1.5679F, -5.7479F, 16.0F, 8.0F, 8.0F, 0.0F, false);
		underlip17.setTextureOffset(196, 128).addBox(-12.0F, 1.1453F, -6.8416F, 24.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve22 = new ModelRenderer(this);
		undercurve22.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine11.addChild(undercurve22);
		setRotationAngle(undercurve22, 1.0472F, 0.0F, 0.0F);
		undercurve22.setTextureOffset(241, 52).addBox(-8.0F, -7.1975F, -5.7981F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		undercurve22.setTextureOffset(196, 52).addBox(-10.0F, -7.1975F, -4.7981F, 20.0F, 29.0F, 11.0F, 0.0F, false);

		backbend17 = new ModelRenderer(this);
		backbend17.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine11.addChild(backbend17);
		setRotationAngle(backbend17, 0.3491F, 0.0F, 0.0F);
		backbend17.setTextureOffset(241, 66).addBox(-8.0F, -10.0774F, -5.2695F, 16.0F, 12.0F, 8.0F, 0.0F, false);

		shin17 = new ModelRenderer(this);
		shin17.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine11.addChild(shin17);
		setRotationAngle(shin17, -0.0873F, 0.0F, 0.0F);
		shin17.setTextureOffset(240, 61).addBox(-8.0F, -3.8125F, -3.6666F, 16.0F, 11.0F, 9.0F, 0.0F, false);

		ankle17 = new ModelRenderer(this);
		ankle17.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine11.addChild(ankle17);
		setRotationAngle(ankle17, -0.8727F, 0.0F, 0.0F);
		ankle17.setTextureOffset(245, 75).addBox(-8.0F, -8.325F, -6.5104F, 16.0F, 13.0F, 4.0F, 0.0F, false);

		foot17 = new ModelRenderer(this);
		foot17.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine11.addChild(foot17);
		setRotationAngle(foot17, -1.5708F, 0.0F, 0.0F);
		foot17.setTextureOffset(244, 79).addBox(-9.0F, -6.0F, -4.0F, 18.0F, 20.0F, 4.0F, 0.0F, false);
		foot17.setTextureOffset(301, 73).addBox(-1.25F, 9.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot17.setTextureOffset(303, 74).addBox(-7.0F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot17.setTextureOffset(298, 81).addBox(4.5F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		coralribs6 = new ModelRenderer(this);
		coralribs6.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(coralribs6);
		setRotationAngle(coralribs6, 0.0F, 1.0472F, 0.0F);
		

		panel12 = new ModelRenderer(this);
		panel12.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs6.addChild(panel12);
		

		front23 = new ModelRenderer(this);
		front23.setRotationPoint(0.0F, -50.0F, -64.0F);
		panel12.addChild(front23);
		front23.setTextureOffset(160, 96).addBox(-29.25F, -1.1759F, -5.5795F, 58.0F, 9.0F, 1.0F, 0.0F, false);
		front23.setTextureOffset(161, 74).addBox(-29.5F, -0.4259F, -6.0795F, 59.0F, 4.0F, 1.0F, 0.0F, false);

		undercurve23 = new ModelRenderer(this);
		undercurve23.setRotationPoint(0.0F, -34.0452F, -54.8586F);
		panel12.addChild(undercurve23);
		setRotationAngle(undercurve23, 1.0472F, 0.0F, 0.0F);
		undercurve23.setTextureOffset(160, 124).addBox(-28.0F, -7.1975F, -5.7981F, 56.0F, 16.0F, 8.0F, 0.0F, false);

		spine12 = new ModelRenderer(this);
		spine12.setRotationPoint(0.0F, 0.0F, 0.0F);
		coralribs6.addChild(spine12);
		setRotationAngle(spine12, 0.0F, -0.5236F, 0.0F);
		

		rib12 = new ModelRenderer(this);
		rib12.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine12.addChild(rib12);
		setRotationAngle(rib12, 0.0F, 0.5236F, 0.0F);
		rib12.setTextureOffset(156, 136).addBox(-24.0F, -11.0F, -50.0F, 48.0F, 4.0F, 12.0F, 0.0F, false);
		rib12.setTextureOffset(162, 137).addBox(-22.0F, -22.0F, -51.0F, 44.0F, 4.0F, 12.0F, 0.0F, false);
		rib12.setTextureOffset(176, 17).addBox(-9.8534F, -103.7731F, -35.8783F, 19.0F, 3.0F, 10.0F, 0.0F, false);
		rib12.setTextureOffset(154, 132).addBox(-2.9874F, -77.7731F, -30.218F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		rib12.setTextureOffset(178, 61).addBox(-6.8534F, -100.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib12.setTextureOffset(206, 47).addBox(-6.8534F, -106.7731F, -30.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib12.setTextureOffset(182, 48).addBox(-6.8534F, -112.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib12.setTextureOffset(186, 58).addBox(-6.8534F, -116.7731F, -28.8783F, 14.0F, 3.0F, 5.0F, 0.0F, false);
		rib12.setTextureOffset(173, 7).addBox(-6.8534F, -114.7731F, -30.8783F, 14.0F, 3.0F, 7.0F, 0.0F, false);

		bone30 = new ModelRenderer(this);
		bone30.setRotationPoint(0.3966F, -78.5765F, -30.9312F);
		rib12.addChild(bone30);
		setRotationAngle(bone30, -0.3491F, 0.0F, 0.0F);
		bone30.setTextureOffset(236, 134).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);
		bone30.setTextureOffset(206, 138).addBox(-3.5F, -1.5F, -1.5F, 6.0F, 5.0F, 4.0F, 0.0F, false);
		bone30.setTextureOffset(208, 68).addBox(-8.5F, -6.8474F, -1.7798F, 15.0F, 3.0F, 4.0F, 0.0F, false);
		bone30.setTextureOffset(166, 60).addBox(-8.5F, -11.5459F, -3.4899F, 15.0F, 3.0F, 4.0F, 0.0F, false);

		bone31 = new ModelRenderer(this);
		bone31.setRotationPoint(-10.6034F, -78.5765F, -30.9312F);
		rib12.addChild(bone31);
		setRotationAngle(bone31, -0.3491F, 0.0F, 0.0F);
		bone31.setTextureOffset(184, 136).addBox(2.5F, -2.0F, -2.0F, 5.0F, 5.0F, 4.0F, 0.0F, false);

		plain18 = new ModelRenderer(this);
		plain18.setRotationPoint(0.0F, -70.9898F, -52.2791F);
		spine12.addChild(plain18);
		setRotationAngle(plain18, -1.1345F, 0.0F, 0.0F);
		plain18.setTextureOffset(241, 38).addBox(-8.0F, -22.1688F, -1.0089F, 16.0F, 40.0F, 8.0F, 0.0F, false);
		plain18.setTextureOffset(173, 36).addBox(-10.0F, -23.0751F, -0.5863F, 20.0F, 40.0F, 12.0F, 0.0F, false);

		bevel18 = new ModelRenderer(this);
		bevel18.setRotationPoint(0.0F, -60.6347F, -68.7874F);
		spine12.addChild(bevel18);
		setRotationAngle(bevel18, -0.6981F, 0.0F, 0.0F);
		bevel18.setTextureOffset(241, 72).addBox(-8.0F, -2.8097F, -2.4602F, 16.0F, 12.0F, 8.0F, 0.0F, false);
		bevel18.setTextureOffset(204, 72).addBox(-10.0F, -3.4524F, -1.6941F, 20.0F, 12.0F, 11.0F, 0.0F, false);

		front24 = new ModelRenderer(this);
		front24.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine12.addChild(front24);
		front24.setTextureOffset(194, 52).addBox(-10.0F, -43.2731F, 40.3863F, 20.0F, 16.0F, 4.0F, 0.0F, false);
		front24.setTextureOffset(203, 12).addBox(-11.0F, -54.2731F, 35.3863F, 22.0F, 4.0F, 13.0F, 0.0F, false);
		front24.setTextureOffset(194, 52).addBox(-10.0F, -51.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front24.setTextureOffset(194, 52).addBox(-10.0F, -57.2731F, 41.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front24.setTextureOffset(194, 52).addBox(-10.0F, -63.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front24.setTextureOffset(194, 52).addBox(-10.0F, -67.2731F, 43.3863F, 20.0F, 4.0F, 7.0F, 0.0F, false);
		front24.setTextureOffset(208, 10).addBox(-11.0F, -65.2731F, 41.3863F, 22.0F, 4.0F, 9.0F, 0.0F, false);
		front24.setTextureOffset(194, 52).addBox(-13.0F, -25.7731F, 42.3863F, 23.0F, 9.0F, 8.0F, 0.0F, false);
		front24.setTextureOffset(245, 53).addBox(-8.0F, -44.2731F, 38.3863F, 16.0F, 16.0F, 4.0F, 0.0F, false);
		front24.setTextureOffset(241, 76).addBox(-8.0F, -5.1759F, -4.5795F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		front24.setTextureOffset(194, 73).addBox(-10.0F, -5.1759F, -3.5795F, 20.0F, 16.0F, 13.0F, 0.0F, false);

		underlip18 = new ModelRenderer(this);
		underlip18.setRotationPoint(0.0F, -43.7571F, -72.7704F);
		spine12.addChild(underlip18);
		setRotationAngle(underlip18, 0.6109F, 0.0F, 0.0F);
		underlip18.setTextureOffset(246, 80).addBox(-8.0F, 1.5679F, -5.7479F, 16.0F, 8.0F, 8.0F, 0.0F, false);
		underlip18.setTextureOffset(196, 128).addBox(-12.0F, 1.1453F, -6.8416F, 24.0F, 8.0F, 8.0F, 0.0F, false);

		undercurve24 = new ModelRenderer(this);
		undercurve24.setRotationPoint(0.0F, -34.0452F, -62.8586F);
		spine12.addChild(undercurve24);
		setRotationAngle(undercurve24, 1.0472F, 0.0F, 0.0F);
		undercurve24.setTextureOffset(241, 52).addBox(-8.0F, -7.1975F, -5.7981F, 16.0F, 16.0F, 8.0F, 0.0F, false);
		undercurve24.setTextureOffset(196, 52).addBox(-10.0F, -7.1975F, -4.7981F, 20.0F, 29.0F, 11.0F, 0.0F, false);

		backbend18 = new ModelRenderer(this);
		backbend18.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine12.addChild(backbend18);
		setRotationAngle(backbend18, 0.3491F, 0.0F, 0.0F);
		backbend18.setTextureOffset(241, 66).addBox(-8.0F, -10.0774F, -5.2695F, 16.0F, 12.0F, 8.0F, 0.0F, false);

		shin18 = new ModelRenderer(this);
		shin18.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine12.addChild(shin18);
		setRotationAngle(shin18, -0.0873F, 0.0F, 0.0F);
		shin18.setTextureOffset(240, 61).addBox(-8.0F, -3.8125F, -3.6666F, 16.0F, 11.0F, 9.0F, 0.0F, false);

		ankle18 = new ModelRenderer(this);
		ankle18.setRotationPoint(0.0F, -1.3682F, -55.2239F);
		spine12.addChild(ankle18);
		setRotationAngle(ankle18, -0.8727F, 0.0F, 0.0F);
		ankle18.setTextureOffset(245, 75).addBox(-8.0F, -8.325F, -6.5104F, 16.0F, 13.0F, 4.0F, 0.0F, false);

		foot18 = new ModelRenderer(this);
		foot18.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		spine12.addChild(foot18);
		setRotationAngle(foot18, -1.5708F, 0.0F, 0.0F);
		foot18.setTextureOffset(244, 79).addBox(-9.0F, -6.0F, -4.0F, 18.0F, 20.0F, 4.0F, 0.0F, false);
		foot18.setTextureOffset(301, 73).addBox(-1.25F, 9.0F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot18.setTextureOffset(303, 74).addBox(-7.0F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		foot18.setTextureOffset(298, 81).addBox(4.5F, 0.5F, -5.0F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		controls = new ModelRenderer(this);
		controls.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		panels = new ModelRenderer(this);
		panels.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(panels);
		

		north = new ModelRenderer(this);
		north.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(north);
		

		ctrlset_6 = new ModelRenderer(this);
		ctrlset_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		north.addChild(ctrlset_6);
		

		top_6 = new ModelRenderer(this);
		top_6.setRotationPoint(4.0F, -72.8333F, -39.5F);
		ctrlset_6.addChild(top_6);
		setRotationAngle(top_6, 0.4363F, 0.0F, 0.0F);
		top_6.setTextureOffset(231, 147).addBox(-6.0F, -3.9167F, -4.0F, 2.0F, 4.0F, 8.0F, 0.0F, false);

		glow_eye = new ModelRenderer(this);
		glow_eye.setRotationPoint(-4.0F, 72.8333F, 39.5F);
		top_6.addChild(glow_eye);
		glow_eye.setTextureOffset(226, 196).addBox(0.0F, -76.5F, -43.0F, 2.0F, 4.0F, 7.0F, 0.0F, false);
		glow_eye.setTextureOffset(231, 197).addBox(2.0F, -76.5F, -42.5F, 1.0F, 4.0F, 6.0F, 0.0F, false);
		glow_eye.setTextureOffset(226, 192).addBox(-4.0F, -76.5F, -43.0F, 2.0F, 4.0F, 7.0F, 0.0F, false);
		glow_eye.setTextureOffset(229, 205).addBox(-5.0F, -76.5F, -42.5F, 1.0F, 4.0F, 6.0F, 0.0F, false);

		telepathics = new ModelRenderer(this);
		telepathics.setRotationPoint(-0.5F, -67.0F, -57.0F);
		ctrlset_6.addChild(telepathics);
		setRotationAngle(telepathics, 0.4363F, 0.0F, 0.0F);
		telepathics.setTextureOffset(212, 147).addBox(-10.5F, 0.375F, -2.0F, 21.0F, 2.0F, 15.0F, 0.0F, false);
		telepathics.setTextureOffset(212, 147).addBox(-5.5F, 0.375F, 13.0F, 10.0F, 2.0F, 10.0F, 0.0F, false);
		telepathics.setTextureOffset(212, 147).addBox(10.5F, 0.375F, 0.0F, 3.0F, 2.0F, 11.0F, 0.0F, false);
		telepathics.setTextureOffset(212, 147).addBox(-13.5F, 0.375F, 0.0F, 3.0F, 2.0F, 11.0F, 0.0F, false);
		telepathics.setTextureOffset(212, 147).addBox(13.5F, 0.375F, 3.0F, 2.0F, 2.0F, 5.0F, 0.0F, false);
		telepathics.setTextureOffset(212, 147).addBox(-15.5F, 0.375F, 3.0F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		glow_strips = new ModelRenderer(this);
		glow_strips.setRotationPoint(0.5F, 73.0F, 64.75F);
		telepathics.addChild(glow_strips);
		glow_strips.setTextureOffset(95, 203).addBox(-10.0F, -72.75F, -55.0F, 19.0F, 2.0F, 2.0F, 0.0F, false);
		glow_strips.setTextureOffset(133, 201).addBox(-13.0F, -72.75F, -57.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		glow_strips.setTextureOffset(153, 213).addBox(-15.0F, -72.75F, -60.25F, 29.0F, 2.0F, 2.0F, 0.0F, false);
		glow_strips.setTextureOffset(164, 204).addBox(-13.0F, -72.75F, -63.0F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		glow_strips.setTextureOffset(182, 202).addBox(-10.0F, -72.75F, -65.75F, 19.0F, 2.0F, 2.0F, 0.0F, false);

		rim_6 = new ModelRenderer(this);
		rim_6.setRotationPoint(-0.5F, -60.5F, -65.0F);
		ctrlset_6.addChild(rim_6);
		setRotationAngle(rim_6, 0.8727F, 0.0F, 0.0F);
		rim_6.setTextureOffset(317, 191).addBox(-19.75F, 0.0F, -4.5F, 24.0F, 11.0F, 8.0F, 0.0F, false);
		rim_6.setTextureOffset(317, 191).addBox(-19.25F, -0.75F, -4.25F, 23.0F, 1.0F, 1.0F, 0.0F, false);
		rim_6.setTextureOffset(317, 191).addBox(-19.25F, -0.5F, 2.0F, 23.0F, 1.0F, 1.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-19.25F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-16.25F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-13.25F, -0.75F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-10.25F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-7.25F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-4.25F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-1.25F, -0.5F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(1.75F, -1.0F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-19.25F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-16.25F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-13.25F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-10.25F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-7.25F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-4.25F, -0.5F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(-1.25F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		rim_6.setTextureOffset(339, 129).addBox(1.75F, -1.0F, -3.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		keypad_b6 = new ModelRenderer(this);
		keypad_b6.setRotationPoint(39.5F, 63.0F, 74.0F);
		rim_6.addChild(keypad_b6);
		keypad_b6.setTextureOffset(218, 136).addBox(-31.0F, -64.75F, -81.5F, 10.0F, 3.0F, 13.0F, 0.0F, false);

		glow_buttons_6 = new ModelRenderer(this);
		glow_buttons_6.setRotationPoint(0.0F, 0.0F, -2.25F);
		keypad_b6.addChild(glow_buttons_6);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-29.5F, -65.25F, -70.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-27.0F, -65.25F, -70.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-24.5F, -65.25F, -70.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-29.5F, -65.25F, -73.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-27.0F, -65.25F, -73.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-24.5F, -65.25F, -73.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-29.5F, -65.25F, -75.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-27.0F, -65.25F, -75.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-24.5F, -65.25F, -75.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-29.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-27.0F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_6.setTextureOffset(148, 206).addBox(-24.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		siderib_6 = new ModelRenderer(this);
		siderib_6.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
		ctrlset_6.addChild(siderib_6);
		setRotationAngle(siderib_6, 0.4363F, -0.5236F, 0.0F);
		

		northwest = new ModelRenderer(this);
		northwest.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(northwest);
		setRotationAngle(northwest, 0.0F, -1.0472F, 0.0F);
		

		ctrlset_2 = new ModelRenderer(this);
		ctrlset_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		northwest.addChild(ctrlset_2);
		

		top_2 = new ModelRenderer(this);
		top_2.setRotationPoint(-0.5F, -73.5F, -44.5F);
		ctrlset_2.addChild(top_2);
		setRotationAngle(top_2, 0.4363F, 0.0F, 0.0F);
		

		fusebox = new ModelRenderer(this);
		fusebox.setRotationPoint(0.5F, 73.5F, 44.5F);
		top_2.addChild(fusebox);
		fusebox.setTextureOffset(359, 72).addBox(-9.0F, -75.5F, -50.0F, 12.0F, 4.0F, 6.0F, 0.0F, false);
		fusebox.setTextureOffset(359, 72).addBox(-2.0F, -76.5F, -46.0F, 3.0F, 4.0F, 4.0F, 0.0F, false);

		button_trip = new ModelRenderer(this);
		button_trip.setRotationPoint(0.0F, 0.0F, 0.0F);
		fusebox.addChild(button_trip);
		button_trip.setTextureOffset(354, 188).addBox(-1.75F, -76.0F, -49.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		button_trip.setTextureOffset(354, 188).addBox(-4.75F, -76.0F, -49.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		button_trip.setTextureOffset(354, 188).addBox(-7.75F, -76.0F, -49.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		lamp = new ModelRenderer(this);
		lamp.setRotationPoint(0.5F, 73.5F, 44.5F);
		top_2.addChild(lamp);
		lamp.setTextureOffset(341, 175).addBox(-9.0F, -73.5F, -40.0F, 5.0F, 2.0F, 5.0F, 0.0F, false);
		lamp.setTextureOffset(341, 175).addBox(-9.0F, -75.0F, -40.0F, 5.0F, 1.0F, 5.0F, 0.0F, false);
		lamp.setTextureOffset(341, 175).addBox(-9.0F, -76.5F, -40.0F, 5.0F, 1.0F, 5.0F, 0.0F, false);

		glow_bulb = new ModelRenderer(this);
		glow_bulb.setRotationPoint(0.0F, 0.0F, 0.0F);
		lamp.addChild(glow_bulb);
		glow_bulb.setTextureOffset(192, 194).addBox(-8.5F, -78.5F, -39.75F, 4.0F, 6.0F, 4.0F, 0.0F, false);

		glow_tube = new ModelRenderer(this);
		glow_tube.setRotationPoint(0.0F, 0.0F, 0.0F);
		lamp.addChild(glow_tube);
		glow_tube.setTextureOffset(192, 194).addBox(-5.5F, -75.5F, -34.75F, 4.0F, 2.0F, 2.0F, 0.0F, false);
		glow_tube.setTextureOffset(192, 194).addBox(-7.5F, -75.5F, -35.75F, 2.0F, 3.0F, 3.0F, 0.0F, false);
		glow_tube.setTextureOffset(192, 194).addBox(-1.5F, -75.5F, -44.75F, 2.0F, 2.0F, 12.0F, 0.0F, false);

		mid_2 = new ModelRenderer(this);
		mid_2.setRotationPoint(-0.5F, -67.0F, -57.0F);
		ctrlset_2.addChild(mid_2);
		setRotationAngle(mid_2, 0.4363F, 0.0F, 0.0F);
		

		wires_2 = new ModelRenderer(this);
		wires_2.setRotationPoint(0.5F, 74.0F, 66.75F);
		mid_2.addChild(wires_2);
		wires_2.setTextureOffset(345, 166).addBox(10.5F, -74.25F, -56.75F, 3.0F, 2.0F, 3.0F, 0.0F, false);
		wires_2.setTextureOffset(207, 134).addBox(11.5F, -73.75F, -73.0F, 1.0F, 2.0F, 17.0F, 0.0F, false);
		wires_2.setTextureOffset(203, 133).addBox(-17.0F, -74.5F, -55.75F, 8.0F, 2.0F, 1.0F, 0.0F, false);
		wires_2.setTextureOffset(203, 133).addBox(-1.0F, -74.5F, -69.75F, 10.0F, 2.0F, 1.0F, 0.0F, false);

		gauge = new ModelRenderer(this);
		gauge.setRotationPoint(-1.5F, 68.75F, 57.5F);
		mid_2.addChild(gauge);
		gauge.setTextureOffset(345, 166).addBox(10.25F, -70.5F, -61.5F, 8.0F, 4.0F, 8.0F, 0.0F, false);
		gauge.setTextureOffset(334, 122).addBox(12.25F, -70.75F, -60.5F, 4.0F, 1.0F, 6.0F, 0.0F, false);
		gauge.setTextureOffset(334, 122).addBox(11.25F, -70.75F, -60.0F, 1.0F, 1.0F, 5.0F, 0.0F, false);
		gauge.setTextureOffset(334, 122).addBox(16.25F, -70.75F, -60.0F, 1.0F, 1.0F, 5.0F, 0.0F, false);

		rim_2 = new ModelRenderer(this);
		rim_2.setRotationPoint(-0.5F, -60.5F, -65.0F);
		ctrlset_2.addChild(rim_2);
		setRotationAngle(rim_2, 0.8727F, 0.0F, 0.0F);
		

		button_switch = new ModelRenderer(this);
		button_switch.setRotationPoint(39.5F, 65.0F, 74.0F);
		rim_2.addChild(button_switch);
		button_switch.setTextureOffset(327, 180).addBox(-28.5F, -66.25F, -73.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);
		button_switch.setTextureOffset(327, 180).addBox(-29.0F, -65.5F, -73.5F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		button_switch.setTextureOffset(263, 195).addBox(-28.0F, -66.75F, -72.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		coms = new ModelRenderer(this);
		coms.setRotationPoint(34.5F, 65.0F, 74.0F);
		rim_2.addChild(coms);
		

		phone_base = new ModelRenderer(this);
		phone_base.setRotationPoint(0.0F, 0.0F, 0.0F);
		coms.addChild(phone_base);
		phone_base.setTextureOffset(330, 125).addBox(-46.75F, -65.75F, -79.0F, 12.0F, 4.0F, 15.0F, 0.0F, false);
		phone_base.setTextureOffset(330, 125).addBox(-46.375F, -67.75F, -69.625F, 5.0F, 4.0F, 1.0F, 0.0F, false);

		handset = new ModelRenderer(this);
		handset.setRotationPoint(-41.5F, -51.875F, -55.875F);
		coms.addChild(handset);
		handset.setTextureOffset(330, 125).addBox(-4.0F, -17.125F, -21.625F, 3.0F, 1.0F, 12.0F, 0.0F, false);
		handset.setTextureOffset(320, 132).addBox(-4.5F, -16.875F, -22.625F, 4.0F, 3.0F, 4.0F, 0.0F, false);
		handset.setTextureOffset(330, 125).addBox(-4.5F, -16.875F, -12.625F, 4.0F, 3.0F, 4.0F, 0.0F, false);

		cord = new ModelRenderer(this);
		cord.setRotationPoint(35.25F, 50.875F, 50.625F);
		handset.addChild(cord);
		

		bone15 = new ModelRenderer(this);
		bone15.setRotationPoint(0.0F, 0.0F, -3.25F);
		cord.addChild(bone15);
		bone15.setTextureOffset(216, 145).addBox(-38.25F, -66.25F, -71.75F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		bone15.setTextureOffset(216, 145).addBox(-39.25F, -66.25F, -72.75F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone15.setTextureOffset(216, 145).addBox(-39.25F, -66.25F, -74.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		bone16 = new ModelRenderer(this);
		bone16.setRotationPoint(-37.8333F, -64.1667F, -78.1667F);
		cord.addChild(bone16);
		setRotationAngle(bone16, 0.6981F, 0.0F, 0.0F);
		bone16.setTextureOffset(216, 145).addBox(-1.1667F, -0.8333F, -0.5833F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone16.setTextureOffset(216, 145).addBox(-1.1667F, -0.8333F, -3.0833F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone16.setTextureOffset(216, 145).addBox(-1.1667F, -0.8333F, -5.5833F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone16.setTextureOffset(216, 145).addBox(-1.1667F, -0.8333F, -1.8333F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone16.setTextureOffset(216, 145).addBox(-1.1667F, -0.8333F, -4.3333F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone16.setTextureOffset(216, 145).addBox(-1.1667F, -0.8333F, -6.8333F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone16.setTextureOffset(216, 145).addBox(-0.4167F, -0.3333F, -5.8333F, 1.0F, 1.0F, 8.0F, 0.0F, false);

		phone_grill = new ModelRenderer(this);
		phone_grill.setRotationPoint(0.0F, 0.0F, 0.0F);
		coms.addChild(phone_grill);
		phone_grill.setTextureOffset(216, 145).addBox(-39.5F, -66.25F, -77.5F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		phone_grill.setTextureOffset(216, 145).addBox(-38.25F, -66.25F, -78.0F, 1.0F, 1.0F, 5.0F, 0.0F, false);
		phone_grill.setTextureOffset(216, 145).addBox(-37.0F, -66.25F, -77.5F, 1.0F, 1.0F, 4.0F, 0.0F, false);

		phone_keys = new ModelRenderer(this);
		phone_keys.setRotationPoint(0.0F, 0.0F, 0.0F);
		coms.addChild(phone_keys);
		phone_keys.setTextureOffset(306, 147).addBox(-37.0F, -66.75F, -67.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-37.0F, -66.75F, -69.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-37.0F, -66.75F, -70.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-38.25F, -66.75F, -67.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-38.25F, -66.75F, -69.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-38.25F, -66.75F, -70.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-38.25F, -66.75F, -72.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-37.0F, -66.75F, -72.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-39.5F, -66.75F, -72.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-39.5F, -66.75F, -67.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-39.5F, -66.75F, -69.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		phone_keys.setTextureOffset(306, 147).addBox(-39.5F, -66.75F, -70.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		siderib_2 = new ModelRenderer(this);
		siderib_2.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
		ctrlset_2.addChild(siderib_2);
		setRotationAngle(siderib_2, 0.4363F, -0.5236F, 0.0F);
		

		northeast = new ModelRenderer(this);
		northeast.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(northeast);
		setRotationAngle(northeast, 0.0F, 1.0472F, 0.0F);
		

		ctrlset_3 = new ModelRenderer(this);
		ctrlset_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		northeast.addChild(ctrlset_3);
		

		top_3 = new ModelRenderer(this);
		top_3.setRotationPoint(-0.5F, -73.5F, -44.5F);
		ctrlset_3.addChild(top_3);
		setRotationAngle(top_3, 0.4363F, 0.0F, 0.0F);
		top_3.setTextureOffset(359, 72).addBox(-7.5F, -5.0F, 4.5F, 7.0F, 6.0F, 2.0F, 0.0F, false);
		top_3.setTextureOffset(359, 72).addBox(-7.5F, -5.0F, -11.0F, 7.0F, 6.0F, 2.0F, 0.0F, false);
		top_3.setTextureOffset(324, 109).addBox(-6.5F, -3.75F, -10.5F, 5.0F, 4.0F, 15.0F, 0.0F, false);
		top_3.setTextureOffset(324, 109).addBox(-5.5F, -3.75F, -12.5F, 3.0F, 3.0F, 2.0F, 0.0F, false);

		mid_3 = new ModelRenderer(this);
		mid_3.setRotationPoint(-0.5F, -67.0F, -57.0F);
		ctrlset_3.addChild(mid_3);
		setRotationAngle(mid_3, 0.4363F, 0.0F, 0.0F);
		mid_3.setTextureOffset(377, 77).addBox(-7.5F, -0.5F, -1.0F, 7.0F, 1.0F, 4.0F, 0.0F, false);
		mid_3.setTextureOffset(367, 173).addBox(-12.75F, -0.5F, -5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		mid_3.setTextureOffset(367, 173).addBox(5.0F, -0.5F, -3.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		mid_3.setTextureOffset(367, 173).addBox(5.0F, -0.5F, 5.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		mid_3.setTextureOffset(367, 173).addBox(5.0F, -0.5F, 1.0F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		mid_3.setTextureOffset(353, 62).addBox(9.0F, -1.5F, -3.0F, 7.0F, 2.0F, 11.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(14.5F, -0.5F, 1.0F, 7.0F, 1.0F, 1.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(8.0F, -0.5F, 2.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(8.0F, -0.5F, -2.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(8.0F, -0.5F, 6.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(2.5F, -0.5F, 15.0F, 12.0F, 1.0F, 1.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(2.5F, -0.5F, 16.0F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		mid_3.setTextureOffset(219, 142).addBox(-4.5F, -0.5F, 16.0F, 1.0F, 1.0F, 12.0F, 0.0F, false);

		bone34 = new ModelRenderer(this);
		bone34.setRotationPoint(0.5F, 71.0F, 66.0F);
		mid_3.addChild(bone34);
		bone34.setTextureOffset(322, 206).addBox(9.0F, -74.5F, -60.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(9.0F, -74.5F, -68.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(9.0F, -74.5F, -62.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(9.0F, -74.5F, -66.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(9.0F, -74.5F, -64.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(14.0F, -74.5F, -60.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(14.0F, -74.5F, -68.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(14.0F, -74.5F, -62.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(14.0F, -74.5F, -66.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(322, 206).addBox(14.0F, -74.5F, -64.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(252, 185).addBox(10.0F, -74.5F, -60.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(252, 185).addBox(10.0F, -74.5F, -68.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(350, 193).addBox(10.0F, -74.5F, -62.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(269, 208).addBox(10.0F, -74.5F, -66.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);
		bone34.setTextureOffset(252, 185).addBox(10.0F, -74.5F, -64.0F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		rim_3 = new ModelRenderer(this);
		rim_3.setRotationPoint(-0.5F, -60.5F, -65.0F);
		ctrlset_3.addChild(rim_3);
		setRotationAngle(rim_3, 0.8727F, 0.0F, 0.0F);
		

		keyboard = new ModelRenderer(this);
		keyboard.setRotationPoint(39.5F, 65.0F, 74.0F);
		rim_3.addChild(keyboard);
		keyboard.setTextureOffset(345, 202).addBox(-37.5F, -65.25F, -79.5F, 20.0F, 8.0F, 10.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-35.875F, -66.25F, -78.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-20.875F, -66.25F, -78.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-20.875F, -66.25F, -75.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-30.375F, -66.25F, -75.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-23.625F, -66.25F, -75.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-33.125F, -66.25F, -75.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-32.875F, -66.25F, -78.5F, 11.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-26.375F, -66.25F, -75.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-35.875F, -66.25F, -75.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-20.875F, -66.25F, -72.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-30.375F, -66.25F, -72.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-23.625F, -66.25F, -72.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-33.125F, -66.25F, -72.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-26.375F, -66.25F, -72.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		keyboard.setTextureOffset(347, 65).addBox(-35.875F, -66.25F, -72.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		randomizer = new ModelRenderer(this);
		randomizer.setRotationPoint(2.9661F, 81.4F, 47.285F);
		rim_3.addChild(randomizer);
		randomizer.setTextureOffset(309, 47).addBox(-19.0F, -81.5F, -52.0F, 9.0F, 1.0F, 9.0F, 0.0F, false);

		spinner = new ModelRenderer(this);
		spinner.setRotationPoint(-14.2797F, -85.39F, -47.371F);
		randomizer.addChild(spinner);
		spinner.setTextureOffset(296, 128).addBox(-3.7203F, -0.11F, -3.629F, 7.0F, 4.0F, 7.0F, 0.0F, false);
		spinner.setTextureOffset(296, 128).addBox(-3.2203F, -1.36F, -3.129F, 6.0F, 2.0F, 6.0F, 0.0F, false);

		bone17 = new ModelRenderer(this);
		bone17.setRotationPoint(14.2797F, 85.39F, 47.371F);
		spinner.addChild(bone17);
		bone17.setTextureOffset(296, 128).addBox(-17.0F, -84.5F, -54.0F, 5.0F, 3.0F, 3.0F, 0.0F, false);

		bone18 = new ModelRenderer(this);
		bone18.setRotationPoint(-0.2203F, 2.39F, -0.379F);
		spinner.addChild(bone18);
		setRotationAngle(bone18, 0.0F, -2.0944F, 0.0F);
		bone18.setTextureOffset(296, 128).addBox(-2.5F, -1.5F, -6.25F, 5.0F, 3.0F, 5.0F, 0.0F, false);

		bone19 = new ModelRenderer(this);
		bone19.setRotationPoint(-0.2203F, 2.39F, -0.379F);
		spinner.addChild(bone19);
		setRotationAngle(bone19, 0.0F, 2.0944F, 0.0F);
		bone19.setTextureOffset(296, 128).addBox(-2.5F, -1.5F, -6.25F, 5.0F, 3.0F, 4.0F, 0.0F, false);

		siderib_3 = new ModelRenderer(this);
		siderib_3.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
		ctrlset_3.addChild(siderib_3);
		setRotationAngle(siderib_3, 0.4363F, -0.5236F, 0.0F);
		

		south = new ModelRenderer(this);
		south.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(south);
		setRotationAngle(south, 0.0F, 3.1416F, 0.0F);
		

		ctrlset_4 = new ModelRenderer(this);
		ctrlset_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		south.addChild(ctrlset_4);
		

		top_4 = new ModelRenderer(this);
		top_4.setRotationPoint(-0.5F, -73.5F, -44.5F);
		ctrlset_4.addChild(top_4);
		setRotationAngle(top_4, 0.4363F, 0.0F, 0.0F);
		top_4.setTextureOffset(368, 187).addBox(-3.5F, 0.0F, -0.5F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		top_4.setTextureOffset(263, 204).addBox(-3.0F, -1.0F, -0.25F, 3.0F, 2.0F, 3.0F, 0.0F, false);
		top_4.setTextureOffset(234, 149).addBox(-2.5F, 1.0F, 3.5F, 1.0F, 1.0F, 10.0F, 0.0F, false);

		mid_4 = new ModelRenderer(this);
		mid_4.setRotationPoint(2.5F, -67.0F, -57.0F);
		ctrlset_4.addChild(mid_4);
		setRotationAngle(mid_4, 0.4363F, 0.0F, 0.0F);
		

		glow_spin_glass_x = new ModelRenderer(this);
		glow_spin_glass_x.setRotationPoint(2.5F, -4.5F, -0.5F);
		mid_4.addChild(glow_spin_glass_x);
		setRotationAngle(glow_spin_glass_x, 1.309F, 0.0F, 0.0F);
		glow_spin_glass_x.setTextureOffset(89, 142).addBox(-8.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, 0.0F, false);
		glow_spin_glass_x.setTextureOffset(89, 142).addBox(5.0F, -2.0F, -3.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		glow_spin_glass_x.setTextureOffset(89, 142).addBox(-7.0F, -2.0F, -1.25F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		glow_spin_glass_x.setTextureOffset(89, 142).addBox(-1.0F, -2.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		glow_spin_glass_x.setTextureOffset(89, 142).addBox(1.5F, -3.0F, -1.75F, 3.0F, 5.0F, 5.0F, 0.0F, false);
		glow_spin_glass_x.setTextureOffset(89, 142).addBox(-4.5F, -3.0F, -2.75F, 3.0F, 5.0F, 5.0F, 0.0F, false);

		base_4 = new ModelRenderer(this);
		base_4.setRotationPoint(0.5F, 73.0F, 65.0F);
		mid_4.addChild(base_4);
		base_4.setTextureOffset(320, 60).addBox(-9.0F, -73.75F, -72.0F, 22.0F, 5.0F, 11.0F, 0.0F, false);
		base_4.setTextureOffset(320, 60).addBox(-8.0F, -79.75F, -68.0F, 2.0F, 6.0F, 5.0F, 0.0F, false);
		base_4.setTextureOffset(320, 60).addBox(10.0F, -79.75F, -68.0F, 2.0F, 6.0F, 5.0F, 0.0F, false);

		rim_4 = new ModelRenderer(this);
		rim_4.setRotationPoint(-0.5F, -60.5F, -65.0F);
		ctrlset_4.addChild(rim_4);
		setRotationAngle(rim_4, 0.8727F, 0.0F, 0.0F);
		

		keypad_a4 = new ModelRenderer(this);
		keypad_a4.setRotationPoint(3.5F, 65.0F, 74.0F);
		rim_4.addChild(keypad_a4);
		keypad_a4.setTextureOffset(218, 136).addBox(-3.0F, -65.75F, -78.5F, 2.0F, 3.0F, 8.0F, 0.0F, false);
		keypad_a4.setTextureOffset(337, 198).addBox(-4.5F, -66.75F, -82.5F, 4.0F, 7.0F, 5.0F, 0.0F, false);
		keypad_a4.setTextureOffset(337, 198).addBox(0.75F, -66.75F, -82.5F, 4.0F, 7.0F, 5.0F, 0.0F, false);
		keypad_a4.setTextureOffset(317, 206).addBox(-5.375F, -66.5F, -82.0F, 11.0F, 6.0F, 4.0F, 0.0F, false);
		keypad_a4.setTextureOffset(218, 136).addBox(1.0F, -65.75F, -78.5F, 2.0F, 3.0F, 8.0F, 0.0F, false);

		keypad_b4 = new ModelRenderer(this);
		keypad_b4.setRotationPoint(39.5F, 65.0F, 74.0F);
		rim_4.addChild(keypad_b4);
		keypad_b4.setTextureOffset(336, 172).addBox(-22.5F, -65.25F, -79.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b4.setTextureOffset(265, 219).addBox(-22.0F, -66.25F, -78.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		siderib_4 = new ModelRenderer(this);
		siderib_4.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
		ctrlset_4.addChild(siderib_4);
		setRotationAngle(siderib_4, 0.4363F, -0.5236F, 0.0F);
		

		snake_wire_4 = new ModelRenderer(this);
		snake_wire_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		ctrlset_4.addChild(snake_wire_4);
		

		gear_spin = new ModelRenderer(this);
		gear_spin.setRotationPoint(-15.0F, -68.0F, -57.25F);
		snake_wire_4.addChild(gear_spin);
		

		spike_4 = new ModelRenderer(this);
		spike_4.setRotationPoint(-0.0243F, 0.1842F, 0.0346F);
		gear_spin.addChild(spike_4);
		setRotationAngle(spike_4, -0.7854F, 0.0F, 0.0F);
		spike_4.setTextureOffset(308, 139).addBox(-1.5F, -3.0F, -3.0F, 3.0F, 6.0F, 6.0F, 0.0F, false);

		spike_4b = new ModelRenderer(this);
		spike_4b.setRotationPoint(-0.0243F, 0.1842F, 0.0346F);
		gear_spin.addChild(spike_4b);
		setRotationAngle(spike_4b, -1.5708F, 0.0F, 0.0F);
		spike_4b.setTextureOffset(308, 139).addBox(-1.0F, -3.0F, -3.0F, 2.0F, 6.0F, 6.0F, 0.0F, false);

		bend_4 = new ModelRenderer(this);
		bend_4.setRotationPoint(-14.25F, -71.9216F, -65.3358F);
		snake_wire_4.addChild(bend_4);
		bend_4.setTextureOffset(216, 145).addBox(-3.2743F, 3.8558F, 2.6204F, 5.0F, 6.0F, 11.0F, 0.0F, false);
		bend_4.setTextureOffset(216, 145).addBox(-3.2743F, 1.8558F, 7.1204F, 5.0F, 2.0F, 2.0F, 0.0F, false);

		slope_4 = new ModelRenderer(this);
		slope_4.setRotationPoint(-15.0F, -59.75F, -68.5F);
		snake_wire_4.addChild(slope_4);
		setRotationAngle(slope_4, -0.7854F, 0.0F, 0.0F);
		slope_4.setTextureOffset(216, 145).addBox(-1.5F, -8.1716F, 1.4142F, 3.0F, 11.0F, 2.0F, 0.0F, false);

		vert_4 = new ModelRenderer(this);
		vert_4.setRotationPoint(23.75F, 3.5F, 6.5F);
		snake_wire_4.addChild(vert_4);
		vert_4.setTextureOffset(216, 145).addBox(-40.25F, -60.25F, -76.0F, 3.0F, 6.0F, 2.0F, 0.0F, false);
		vert_4.setTextureOffset(325, 183).addBox(-42.25F, -57.5F, -77.25F, 7.0F, 4.0F, 3.0F, 0.0F, false);

		glow_vert_4 = new ModelRenderer(this);
		glow_vert_4.setRotationPoint(0.75F, 2.75F, -4.25F);
		vert_4.addChild(glow_vert_4);
		glow_vert_4.setTextureOffset(61, 142).addBox(-39.0F, -59.25F, -74.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_vert_4.setTextureOffset(61, 142).addBox(-42.25F, -59.25F, -74.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		southeast = new ModelRenderer(this);
		southeast.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(southeast);
		setRotationAngle(southeast, 0.0F, 2.0944F, 0.0F);
		

		ctrlset_1 = new ModelRenderer(this);
		ctrlset_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		southeast.addChild(ctrlset_1);
		

		top_1 = new ModelRenderer(this);
		top_1.setRotationPoint(-0.5F, -73.5F, -44.5F);
		ctrlset_1.addChild(top_1);
		setRotationAngle(top_1, 0.4363F, 0.0F, 0.0F);
		top_1.setTextureOffset(359, 72).addBox(-15.5F, -1.0F, -11.5F, 4.0F, 4.0F, 5.0F, 0.0F, false);
		top_1.setTextureOffset(214, 141).addBox(-13.5F, -1.0F, -6.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		top_1.setTextureOffset(214, 141).addBox(2.0F, -1.0F, -12.5F, 1.0F, 1.0F, 10.0F, 0.0F, false);
		top_1.setTextureOffset(214, 141).addBox(0.0F, 0.0F, 2.5F, 1.0F, 1.0F, 11.0F, 0.0F, false);
		top_1.setTextureOffset(214, 141).addBox(-13.5F, -1.0F, -4.5F, 12.0F, 1.0F, 1.0F, 0.0F, false);
		top_1.setTextureOffset(214, 141).addBox(-2.5F, -1.0F, -3.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		sonic_port = new ModelRenderer(this);
		sonic_port.setRotationPoint(-1.5F, 73.5F, 47.5F);
		top_1.addChild(sonic_port);
		sonic_port.setTextureOffset(389, 175).addBox(-2.0F, -75.5F, -50.0F, 7.0F, 5.0F, 2.0F, 0.0F, false);
		sonic_port.setTextureOffset(199, 140).addBox(-1.0F, -74.5F, -49.0F, 5.0F, 4.0F, 5.0F, 0.0F, false);
		sonic_port.setTextureOffset(389, 175).addBox(-2.0F, -75.5F, -45.0F, 7.0F, 5.0F, 2.0F, 0.0F, false);
		sonic_port.setTextureOffset(389, 175).addBox(-3.0F, -75.5F, -48.0F, 3.0F, 5.0F, 3.0F, 0.0F, false);
		sonic_port.setTextureOffset(389, 175).addBox(3.0F, -75.5F, -48.0F, 3.0F, 5.0F, 3.0F, 0.0F, false);

		xyz_cords = new ModelRenderer(this);
		xyz_cords.setRotationPoint(-0.5F, -67.0F, -57.0F);
		ctrlset_1.addChild(xyz_cords);
		setRotationAngle(xyz_cords, 0.4363F, 0.0F, 0.0F);
		xyz_cords.setTextureOffset(320, 60).addBox(-10.5F, -0.25F, -6.0F, 23.0F, 3.0F, 10.0F, 0.0F, false);

		inc_x = new ModelRenderer(this);
		inc_x.setRotationPoint(0.5F, 73.0F, 65.0F);
		xyz_cords.addChild(inc_x);
		inc_x.setTextureOffset(210, 133).addBox(-10.0F, -73.75F, -64.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_x.setTextureOffset(210, 133).addBox(-10.0F, -73.75F, -67.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_x.setTextureOffset(210, 133).addBox(-7.0F, -73.75F, -64.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_x.setTextureOffset(210, 133).addBox(-7.0F, -73.75F, -67.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		inc_y = new ModelRenderer(this);
		inc_y.setRotationPoint(0.5F, 73.0F, 65.0F);
		xyz_cords.addChild(inc_y);
		inc_y.setTextureOffset(210, 133).addBox(-2.0F, -73.75F, -64.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_y.setTextureOffset(210, 133).addBox(-2.0F, -73.75F, -67.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_y.setTextureOffset(210, 133).addBox(1.0F, -73.75F, -64.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_y.setTextureOffset(210, 133).addBox(1.0F, -73.75F, -67.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		inc_z = new ModelRenderer(this);
		inc_z.setRotationPoint(0.5F, 73.0F, 65.0F);
		xyz_cords.addChild(inc_z);
		inc_z.setTextureOffset(210, 133).addBox(6.0F, -73.75F, -64.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_z.setTextureOffset(210, 133).addBox(6.0F, -73.75F, -67.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_z.setTextureOffset(210, 133).addBox(9.0F, -73.75F, -64.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		inc_z.setTextureOffset(210, 133).addBox(9.0F, -73.75F, -67.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		rim_1 = new ModelRenderer(this);
		rim_1.setRotationPoint(-0.5F, -60.5F, -65.0F);
		ctrlset_1.addChild(rim_1);
		setRotationAngle(rim_1, 0.8727F, 0.0F, 0.0F);
		rim_1.setTextureOffset(218, 136).addBox(-12.5F, 0.25F, -4.5F, 25.0F, 2.0F, 8.0F, 0.0F, false);

		keypad_a1 = new ModelRenderer(this);
		keypad_a1.setRotationPoint(3.5F, 65.0F, 74.0F);
		rim_1.addChild(keypad_a1);
		keypad_a1.setTextureOffset(315, 201).addBox(-25.0F, -66.75F, -78.5F, 6.0F, 4.0F, 6.0F, 0.0F, false);

		keypad_b1 = new ModelRenderer(this);
		keypad_b1.setRotationPoint(39.5F, 65.0F, 74.0F);
		rim_1.addChild(keypad_b1);
		keypad_b1.setTextureOffset(351, 192).addBox(-20.0F, -64.75F, -78.5F, 3.0F, 2.0F, 5.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-51.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-48.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-45.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-42.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-39.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-36.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-33.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(330, 174).addBox(-30.5F, -65.25F, -75.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_b1.setTextureOffset(309, 8).addBox(-42.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		glow_buttons_b1 = new ModelRenderer(this);
		glow_buttons_b1.setRotationPoint(0.0F, 0.0F, 0.0F);
		keypad_b1.addChild(glow_buttons_b1);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-30.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-51.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-48.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-45.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-39.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-36.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_buttons_b1.setTextureOffset(309, 8).addBox(-33.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		siderib_1 = new ModelRenderer(this);
		siderib_1.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
		ctrlset_1.addChild(siderib_1);
		setRotationAngle(siderib_1, 0.4363F, -0.5236F, 0.0F);
		

		southwest = new ModelRenderer(this);
		southwest.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(southwest);
		setRotationAngle(southwest, 0.0F, -2.0944F, 0.0F);
		

		ctrlset_5 = new ModelRenderer(this);
		ctrlset_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		southwest.addChild(ctrlset_5);
		

		top_5 = new ModelRenderer(this);
		top_5.setRotationPoint(-0.5F, -73.5F, -44.5F);
		ctrlset_5.addChild(top_5);
		setRotationAngle(top_5, 0.4363F, 0.0F, 0.0F);
		top_5.setTextureOffset(224, 142).addBox(-2.5F, 1.0F, -11.5F, 1.0F, 2.0F, 8.0F, 0.0F, false);
		top_5.setTextureOffset(359, 72).addBox(-6.5F, -2.0F, -4.5F, 14.0F, 5.0F, 15.0F, 0.0F, false);
		top_5.setTextureOffset(297, 203).addBox(-2.5F, -4.0F, 0.5F, 6.0F, 2.0F, 6.0F, 0.0F, false);
		top_5.setTextureOffset(297, 203).addBox(3.5F, -3.0F, 1.25F, 2.0F, 2.0F, 5.0F, 0.0F, false);
		top_5.setTextureOffset(297, 203).addBox(-4.5F, -3.0F, 1.25F, 2.0F, 2.0F, 5.0F, 0.0F, false);
		top_5.setTextureOffset(297, 203).addBox(-2.0F, -3.0F, -1.5F, 5.0F, 1.0F, 2.0F, 0.0F, false);
		top_5.setTextureOffset(297, 203).addBox(-2.0F, -3.0F, 6.5F, 5.0F, 1.0F, 2.0F, 0.0F, false);
		top_5.setTextureOffset(359, 72).addBox(-4.5F, -1.0F, 9.5F, 1.0F, 4.0F, 5.0F, 0.0F, false);
		top_5.setTextureOffset(359, 72).addBox(4.5F, -1.0F, 9.5F, 1.0F, 4.0F, 5.0F, 0.0F, false);
		top_5.setTextureOffset(359, 72).addBox(0.0F, -1.0F, 9.5F, 1.0F, 4.0F, 5.0F, 0.0F, false);

		dim_select2 = new ModelRenderer(this);
		dim_select2.setRotationPoint(0.5741F, -4.7897F, 1.6538F);
		top_5.addChild(dim_select2);
		setRotationAngle(dim_select2, -1.5708F, 0.0F, 0.0F);
		

		wheel_rotate_z3 = new ModelRenderer(this);
		wheel_rotate_z3.setRotationPoint(0.0821F, -1.2728F, -3.1538F);
		dim_select2.addChild(wheel_rotate_z3);
		

		bone32 = new ModelRenderer(this);
		bone32.setRotationPoint(24.8099F, 74.4625F, -77.465F);
		wheel_rotate_z3.addChild(bone32);
		bone32.setTextureOffset(398, 203).addBox(-28.3179F, -80.9793F, 77.5226F, 7.0F, 2.0F, 2.0F, 0.0F, false);
		bone32.setTextureOffset(398, 203).addBox(-28.3179F, -69.9793F, 77.5226F, 7.0F, 2.0F, 2.0F, 0.0F, false);
		bone32.setTextureOffset(398, 203).addBox(-20.3179F, -77.9793F, 77.5226F, 2.0F, 7.0F, 2.0F, 0.0F, false);
		bone32.setTextureOffset(398, 203).addBox(-31.3179F, -77.9793F, 77.5226F, 2.0F, 7.0F, 2.0F, 0.0F, false);
		bone32.setTextureOffset(398, 203).addBox(-29.8179F, -74.9793F, 78.2726F, 10.0F, 1.0F, 1.0F, 0.0F, false);
		bone32.setTextureOffset(396, 64).addBox(-25.8179F, -75.4793F, 77.2726F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		bone32.setTextureOffset(396, 64).addBox(-20.0679F, -75.4793F, 73.2726F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		bone32.setTextureOffset(364, 183).addBox(-19.5679F, -74.9793F, 73.0226F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		bone32.setTextureOffset(398, 203).addBox(-25.3179F, -78.9793F, 78.2726F, 1.0F, 9.0F, 1.0F, 0.0F, false);

		bone33 = new ModelRenderer(this);
		bone33.setRotationPoint(15.3099F, 19.4625F, -146.215F);
		wheel_rotate_z3.addChild(bone33);
		setRotationAngle(bone33, 0.0F, 0.0F, 0.7854F);
		bone33.setTextureOffset(398, 203).addBox(-27.1054F, -9.4425F, 146.5226F, 5.0F, 2.0F, 2.0F, 0.0F, false);
		bone33.setTextureOffset(398, 203).addBox(-27.1054F, 1.5575F, 146.5226F, 5.0F, 2.0F, 2.0F, 0.0F, false);
		bone33.setTextureOffset(398, 203).addBox(-20.1054F, -5.4425F, 146.5226F, 2.0F, 5.0F, 2.0F, 0.0F, false);
		bone33.setTextureOffset(398, 203).addBox(-31.1054F, -5.4425F, 146.5226F, 2.0F, 5.0F, 2.0F, 0.0F, false);

		mid_5 = new ModelRenderer(this);
		mid_5.setRotationPoint(-0.5F, -67.0F, -57.0F);
		ctrlset_5.addChild(mid_5);
		setRotationAngle(mid_5, 0.4363F, 0.0F, 0.0F);
		mid_5.setTextureOffset(320, 60).addBox(-6.5F, -1.75F, -4.0F, 10.0F, 6.0F, 8.0F, 0.0F, false);

		dummy_buttons_5 = new ModelRenderer(this);
		dummy_buttons_5.setRotationPoint(0.5F, 73.0F, 65.0F);
		mid_5.addChild(dummy_buttons_5);
		dummy_buttons_5.setTextureOffset(362, 59).addBox(7.0F, -72.75F, -69.0F, 4.0F, 2.0F, 9.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(196, 139).addBox(8.0F, -73.75F, -63.25F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(271, 193).addBox(14.0F, -73.25F, -63.25F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(258, 198).addBox(8.0F, -73.25F, -65.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(196, 139).addBox(14.0F, -73.75F, -65.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(196, 139).addBox(8.0F, -73.75F, -68.25F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(196, 139).addBox(14.0F, -73.75F, -68.25F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dummy_buttons_5.setTextureOffset(362, 59).addBox(13.0F, -72.75F, -69.0F, 4.0F, 2.0F, 9.0F, 0.0F, false);

		glow_dummy_5 = new ModelRenderer(this);
		glow_dummy_5.setRotationPoint(3.5F, 72.5F, 65.0F);
		mid_5.addChild(glow_dummy_5);
		glow_dummy_5.setTextureOffset(34, 145).addBox(-9.0F, -74.75F, -68.0F, 2.0F, 6.0F, 4.0F, 0.0F, false);
		glow_dummy_5.setTextureOffset(34, 145).addBox(-3.0F, -74.75F, -68.0F, 2.0F, 6.0F, 4.0F, 0.0F, false);
		glow_dummy_5.setTextureOffset(34, 145).addBox(-6.0F, -74.75F, -68.0F, 2.0F, 6.0F, 4.0F, 0.0F, false);

		globe = new ModelRenderer(this);
		globe.setRotationPoint(0.5F, 73.0F, 65.0F);
		mid_5.addChild(globe);
		globe.setTextureOffset(320, 60).addBox(-18.0F, -72.75F, -73.0F, 10.0F, 4.0F, 12.0F, 0.0F, false);
		globe.setTextureOffset(320, 60).addBox(-17.0F, -73.75F, -72.0F, 8.0F, 1.0F, 9.0F, 0.0F, false);

		spin_y = new ModelRenderer(this);
		spin_y.setRotationPoint(-12.725F, -76.4688F, -67.825F);
		globe.addChild(spin_y);
		spin_y.setTextureOffset(60, 82).addBox(0.475F, -2.2813F, -2.3F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		spin_y.setTextureOffset(326, 60).addBox(-1.775F, -0.7813F, -1.175F, 3.0F, 5.0F, 3.0F, 0.0F, false);
		spin_y.setTextureOffset(60, 82).addBox(-2.9F, -2.875F, 0.95F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		spin_y.setTextureOffset(60, 82).addBox(-0.525F, 0.7188F, -2.3F, 3.0F, 1.0F, 2.0F, 0.0F, false);

		glow_globe = new ModelRenderer(this);
		glow_globe.setRotationPoint(13.225F, 74.9688F, 67.825F);
		spin_y.addChild(glow_globe);
		glow_globe.setTextureOffset(70, 129).addBox(-16.0F, -77.75F, -70.0F, 5.0F, 5.0F, 5.0F, 0.0F, false);

		rim_5 = new ModelRenderer(this);
		rim_5.setRotationPoint(-0.5F, -60.5F, -65.0F);
		ctrlset_5.addChild(rim_5);
		setRotationAngle(rim_5, 0.8727F, 0.0F, 0.0F);
		

		keypad_a5 = new ModelRenderer(this);
		keypad_a5.setRotationPoint(3.5F, 65.0F, 74.0F);
		rim_5.addChild(keypad_a5);
		keypad_a5.setTextureOffset(218, 136).addBox(-18.0F, -64.75F, -74.5F, 20.0F, 2.0F, 1.0F, 0.0F, false);
		keypad_a5.setTextureOffset(218, 136).addBox(-18.0F, -64.75F, -73.5F, 1.0F, 2.0F, 4.0F, 0.0F, false);
		keypad_a5.setTextureOffset(218, 136).addBox(12.75F, -64.75F, -79.5F, 7.0F, 2.0F, 7.0F, 0.0F, false);

		keypad_b5 = new ModelRenderer(this);
		keypad_b5.setRotationPoint(39.5F, 65.0F, 74.0F);
		rim_5.addChild(keypad_b5);
		keypad_b5.setTextureOffset(385, 170).addBox(-22.0F, -65.25F, -75.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		keypad_b5.setTextureOffset(309, 8).addBox(-22.0F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		keypad_b5.setTextureOffset(309, 8).addBox(-19.5F, -65.25F, -75.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		keypad_b5.setTextureOffset(331, 181).addBox(-19.5F, -65.25F, -78.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		dummy_buttons_2 = new ModelRenderer(this);
		dummy_buttons_2.setRotationPoint(25.5F, 65.375F, 73.375F);
		rim_5.addChild(dummy_buttons_2);
		dummy_buttons_2.setTextureOffset(365, 125).addBox(-13.5F, -65.75F, -75.5F, 1.0F, 3.0F, 5.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(200, 135).addBox(-15.75F, -65.75F, -75.25F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(200, 135).addBox(-15.75F, -65.75F, -72.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(200, 135).addBox(-18.25F, -65.75F, -72.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(200, 135).addBox(-18.25F, -65.75F, -75.25F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(200, 135).addBox(-20.75F, -65.75F, -72.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(200, 135).addBox(-20.75F, -65.75F, -75.25F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(365, 125).addBox(-22.0F, -65.75F, -75.5F, 1.0F, 3.0F, 5.0F, 0.0F, false);
		dummy_buttons_2.setTextureOffset(368, 191).addBox(-22.75F, -65.375F, -75.875F, 11.0F, 3.0F, 6.0F, 0.0F, false);

		siderib_5 = new ModelRenderer(this);
		siderib_5.setRotationPoint(22.5436F, -76.3203F, -38.9127F);
		ctrlset_5.addChild(siderib_5);
		setRotationAngle(siderib_5, 0.4363F, -0.5236F, 0.0F);
		

		rib_controls = new ModelRenderer(this);
		rib_controls.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(rib_controls);
		

		west_rib = new ModelRenderer(this);
		west_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_controls.addChild(west_rib);
		setRotationAngle(west_rib, 0.0F, -1.5708F, 0.0F);
		

		upper_rib = new ModelRenderer(this);
		upper_rib.setRotationPoint(0.5F, -69.75F, -61.5F);
		west_rib.addChild(upper_rib);
		setRotationAngle(upper_rib, 0.4363F, 0.0F, 0.0F);
		

		big_gauge = new ModelRenderer(this);
		big_gauge.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib.addChild(big_gauge);
		big_gauge.setTextureOffset(309, 47).addBox(-19.0F, -86.5F, -19.5F, 6.0F, 6.0F, 1.0F, 0.0F, false);
		big_gauge.setTextureOffset(309, 47).addBox(-14.5F, -86.0F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge.setTextureOffset(309, 47).addBox(-14.5F, -82.25F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge.setTextureOffset(309, 47).addBox(-18.5F, -86.0F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge.setTextureOffset(309, 47).addBox(-18.5F, -82.25F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge.setTextureOffset(309, 47).addBox(-19.0F, -86.5F, -32.5F, 6.0F, 6.0F, 1.0F, 0.0F, false);
		big_gauge.setTextureOffset(269, 199).addBox(-17.5F, -85.25F, -31.5F, 3.0F, 3.0F, 3.0F, 0.0F, false);
		big_gauge.setTextureOffset(351, 55).addBox(-17.5F, -85.25F, -33.0F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		big_gauge.setTextureOffset(316, 117).addBox(-17.5F, -85.25F, -28.5F, 3.0F, 3.0F, 9.0F, 0.0F, false);

		big_gauge2 = new ModelRenderer(this);
		big_gauge2.setRotationPoint(18.9661F, 82.15F, 47.785F);
		upper_rib.addChild(big_gauge2);
		big_gauge2.setTextureOffset(309, 47).addBox(-19.0F, -86.5F, -19.5F, 6.0F, 6.0F, 1.0F, 0.0F, false);
		big_gauge2.setTextureOffset(309, 47).addBox(-14.5F, -86.0F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge2.setTextureOffset(309, 47).addBox(-14.5F, -82.25F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge2.setTextureOffset(309, 47).addBox(-18.5F, -86.0F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge2.setTextureOffset(309, 47).addBox(-18.5F, -82.25F, -31.5F, 1.0F, 1.0F, 12.0F, 0.0F, false);
		big_gauge2.setTextureOffset(309, 47).addBox(-19.0F, -86.5F, -32.5F, 6.0F, 6.0F, 1.0F, 0.0F, false);
		big_gauge2.setTextureOffset(269, 199).addBox(-17.5F, -85.25F, -31.5F, 3.0F, 3.0F, 8.0F, 0.0F, false);
		big_gauge2.setTextureOffset(351, 55).addBox(-17.5F, -85.25F, -33.0F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		big_gauge2.setTextureOffset(316, 117).addBox(-17.5F, -85.25F, -23.5F, 3.0F, 3.0F, 4.0F, 0.0F, false);

		jumbo_slider = new ModelRenderer(this);
		jumbo_slider.setRotationPoint(10.9661F, 81.15F, 47.785F);
		upper_rib.addChild(jumbo_slider);
		

		jumbo_rotate_x = new ModelRenderer(this);
		jumbo_rotate_x.setRotationPoint(-9.75F, -31.5F, -50.0F);
		jumbo_slider.addChild(jumbo_rotate_x);
		jumbo_rotate_x.setTextureOffset(331, 192).addBox(1.5F, -49.5F, -3.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(347, 71).addBox(-7.5F, -51.5F, -3.5F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(347, 71).addBox(0.5F, -51.5F, -3.5F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(347, 71).addBox(-0.75F, -51.5F, -3.5F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(347, 71).addBox(-4.25F, -51.5F, -3.5F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(347, 71).addBox(-3.0F, -51.5F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(327, 180).addBox(-8.0F, -51.0F, -3.0F, 12.0F, 1.0F, 1.0F, 0.0F, false);
		jumbo_rotate_x.setTextureOffset(331, 192).addBox(-6.0F, -49.5F, -3.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		base = new ModelRenderer(this);
		base.setRotationPoint(8.0F, 2.5F, 13.0F);
		jumbo_slider.addChild(base);
		base.setTextureOffset(201, 135).addBox(-25.0F, -82.25F, -67.0F, 11.0F, 1.0F, 13.0F, 0.0F, false);
		base.setTextureOffset(214, 64).addBox(-26.0F, -82.5F, -66.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
		base.setTextureOffset(243, 65).addBox(-22.5F, -82.5F, -66.0F, 6.0F, 2.0F, 11.0F, 0.0F, false);
		base.setTextureOffset(214, 64).addBox(-15.0F, -82.5F, -66.0F, 2.0F, 2.0F, 11.0F, 0.0F, false);
		base.setTextureOffset(248, 64).addBox(-26.0F, -82.5F, -68.0F, 13.0F, 2.0F, 2.0F, 0.0F, false);
		base.setTextureOffset(248, 64).addBox(-26.0F, -82.5F, -55.0F, 13.0F, 2.0F, 2.0F, 0.0F, false);

		bevel_rib = new ModelRenderer(this);
		bevel_rib.setRotationPoint(0.5F, -60.75F, -72.5F);
		west_rib.addChild(bevel_rib);
		setRotationAngle(bevel_rib, 0.8727F, 0.0F, 0.0F);
		

		handbrake = new ModelRenderer(this);
		handbrake.setRotationPoint(10.9661F, 82.15F, 47.785F);
		bevel_rib.addChild(handbrake);
		handbrake.setTextureOffset(243, 88).addBox(-17.0F, -80.5F, -64.0F, 11.0F, 7.0F, 8.0F, 0.0F, false);
		handbrake.setTextureOffset(304, 72).addBox(-14.0F, -81.5F, -62.5F, 5.0F, 2.0F, 4.0F, 0.0F, false);

		handbreak_rotate_x = new ModelRenderer(this);
		handbreak_rotate_x.setRotationPoint(-11.2312F, -82.104F, 7.2344F);
		handbrake.addChild(handbreak_rotate_x);
		setRotationAngle(handbreak_rotate_x, -0.0873F, 0.0F, 0.0F);
		handbreak_rotate_x.setTextureOffset(339, 116).addBox(-1.4687F, -2.0F, -68.7344F, 2.0F, 11.0F, 2.0F, 0.0F, false);
		handbreak_rotate_x.setTextureOffset(344, 69).addBox(-2.0312F, -5.0F, -69.2656F, 3.0F, 3.0F, 3.0F, 0.0F, false);

		vertical_rib = new ModelRenderer(this);
		vertical_rib.setRotationPoint(0.5F, -69.75F, -61.5F);
		west_rib.addChild(vertical_rib);
		

		med_plate = new ModelRenderer(this);
		med_plate.setRotationPoint(10.9661F, 82.15F, 47.785F);
		vertical_rib.addChild(med_plate);
		

		nw_rib = new ModelRenderer(this);
		nw_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_controls.addChild(nw_rib);
		setRotationAngle(nw_rib, 0.0F, -0.5236F, 0.0F);
		

		throttle = new ModelRenderer(this);
		throttle.setRotationPoint(-0.4133F, -61.2385F, -54.2589F);
		nw_rib.addChild(throttle);
		setRotationAngle(throttle, 0.6981F, 0.0F, 0.0F);
		

		throttle_rotate_x = new ModelRenderer(this);
		throttle_rotate_x.setRotationPoint(0.3794F, -12.7782F, -13.4562F);
		throttle.addChild(throttle_rotate_x);
		setRotationAngle(throttle_rotate_x, 1.5708F, 0.0F, 0.0F);
		throttle_rotate_x.setTextureOffset(354, 64).addBox(-4.0F, -2.1487F, 4.4971F, 8.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_rotate_x.setTextureOffset(346, 128).addBox(3.25F, -1.3987F, -1.0029F, 1.0F, 1.0F, 7.0F, 0.0F, false);
		throttle_rotate_x.setTextureOffset(346, 128).addBox(-4.25F, -1.3987F, -1.0029F, 1.0F, 1.0F, 7.0F, 0.0F, false);

		throttle_base = new ModelRenderer(this);
		throttle_base.setRotationPoint(-0.6206F, 70.1385F, 37.7938F);
		throttle.addChild(throttle_base);
		throttle_base.setTextureOffset(322, 203).addBox(-4.0F, -82.5F, -53.0F, 10.0F, 2.0F, 1.0F, 0.0F, false);
		throttle_base.setTextureOffset(325, 201).addBox(-4.0F, -82.5F, -43.0F, 10.0F, 2.0F, 1.0F, 0.0F, false);
		throttle_base.setTextureOffset(342, 197).addBox(-3.0F, -83.5F, -42.0F, 8.0F, 3.0F, 1.0F, 0.0F, false);
		throttle_base.setTextureOffset(352, 140).addBox(0.5F, -83.5F, -44.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		throttle_base.setTextureOffset(314, 195).addBox(-5.0F, -82.5F, -46.0F, 12.0F, 2.0F, 3.0F, 0.0F, false);
		throttle_base.setTextureOffset(329, 210).addBox(-5.0F, -82.5F, -52.0F, 12.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_base.setTextureOffset(311, 191).addBox(3.0F, -82.5F, -50.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		throttle_base.setTextureOffset(316, 202).addBox(-5.0F, -82.5F, -50.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		throttle_base.setTextureOffset(325, 203).addBox(-3.0F, -83.5F, -54.0F, 8.0F, 3.0F, 1.0F, 0.0F, false);
		throttle_base.setTextureOffset(353, 203).addBox(7.0F, -83.5F, -51.0F, 1.0F, 3.0F, 7.0F, 0.0F, false);
		throttle_base.setTextureOffset(330, 197).addBox(-6.0F, -83.5F, -51.0F, 1.0F, 3.0F, 7.0F, 0.0F, false);

		lever_mount = new ModelRenderer(this);
		lever_mount.setRotationPoint(5.1294F, 68.6385F, 36.5438F);
		throttle.addChild(lever_mount);
		lever_mount.setTextureOffset(354, 64).addBox(-0.5F, -83.5F, -52.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
		lever_mount.setTextureOffset(354, 64).addBox(-2.5F, -83.5F, -52.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
		lever_mount.setTextureOffset(354, 64).addBox(-1.5F, -83.5F, -52.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		lever_mount.setTextureOffset(354, 64).addBox(-9.0F, -83.5F, -52.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		lever_mount.setTextureOffset(354, 64).addBox(-10.0F, -83.5F, -52.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
		lever_mount.setTextureOffset(354, 64).addBox(-8.0F, -83.5F, -52.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);

		upper_rib3 = new ModelRenderer(this);
		upper_rib3.setRotationPoint(0.5F, -69.75F, -61.5F);
		nw_rib.addChild(upper_rib3);
		setRotationAngle(upper_rib3, 0.4363F, 0.0F, 0.0F);
		

		fast_return = new ModelRenderer(this);
		fast_return.setRotationPoint(-33.7679F, 107.5772F, 85.4353F);
		upper_rib3.addChild(fast_return);
		fast_return.setTextureOffset(263, 209).addBox(34.5F, -112.335F, -65.2323F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		fast_return.setTextureOffset(334, 187).addBox(34.0F, -111.835F, -65.7323F, 3.0F, 1.0F, 3.0F, 0.0F, false);

		switches = new ModelRenderer(this);
		switches.setRotationPoint(44.734F, -25.4272F, -37.6504F);
		fast_return.addChild(switches);
		switches.setTextureOffset(309, 47).addBox(-17.0F, -85.75F, -30.0F, 11.0F, 1.0F, 11.0F, 0.0F, false);

		dummy_button2 = new ModelRenderer(this);
		dummy_button2.setRotationPoint(-33.7679F, 107.5772F, 89.4353F);
		upper_rib3.addChild(dummy_button2);
		dummy_button2.setTextureOffset(275, 204).addBox(34.5F, -112.335F, -65.2323F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_button2.setTextureOffset(334, 187).addBox(34.0F, -111.835F, -65.7323F, 3.0F, 1.0F, 3.0F, 0.0F, false);

		dummy_button3 = new ModelRenderer(this);
		dummy_button3.setRotationPoint(-38.7679F, 107.5772F, 89.4353F);
		upper_rib3.addChild(dummy_button3);
		dummy_button3.setTextureOffset(349, 122).addBox(34.5F, -113.335F, -64.2323F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_button3.setTextureOffset(349, 122).addBox(34.5F, -113.335F, -65.7323F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_button3.setTextureOffset(349, 122).addBox(34.5F, -113.335F, -67.2323F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_button3.setTextureOffset(349, 122).addBox(34.5F, -113.335F, -68.7323F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_button3.setTextureOffset(186, 148).addBox(34.0F, -111.835F, -69.7323F, 2.0F, 1.0F, 7.0F, 0.0F, false);

		bevel_nwr = new ModelRenderer(this);
		bevel_nwr.setRotationPoint(0.0F, -73.4443F, -51.9976F);
		nw_rib.addChild(bevel_nwr);
		setRotationAngle(bevel_nwr, -0.6981F, 0.0F, 0.0F);
		

		vertical_rib3 = new ModelRenderer(this);
		vertical_rib3.setRotationPoint(0.5F, -69.75F, -61.5F);
		nw_rib.addChild(vertical_rib3);
		

		med_plate3 = new ModelRenderer(this);
		med_plate3.setRotationPoint(10.9661F, 82.15F, 47.785F);
		vertical_rib3.addChild(med_plate3);
		

		sw_rib = new ModelRenderer(this);
		sw_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_controls.addChild(sw_rib);
		setRotationAngle(sw_rib, 0.0F, -2.618F, 0.0F);
		

		upper_rib6 = new ModelRenderer(this);
		upper_rib6.setRotationPoint(0.5F, -69.75F, -61.5F);
		sw_rib.addChild(upper_rib6);
		setRotationAngle(upper_rib6, 0.4363F, 0.0F, 0.0F);
		

		big_plate6 = new ModelRenderer(this);
		big_plate6.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib6.addChild(big_plate6);
		big_plate6.setTextureOffset(309, 47).addBox(-17.0F, -81.5F, -53.0F, 11.0F, 1.0F, 8.0F, 0.0F, false);
		big_plate6.setTextureOffset(394, 72).addBox(-13.5F, -86.0F, -31.0F, 9.0F, 6.0F, 10.0F, 0.0F, false);
		big_plate6.setTextureOffset(394, 72).addBox(-10.5F, -88.0F, -29.0F, 6.0F, 2.0F, 5.0F, 0.0F, false);

		resitsters = new ModelRenderer(this);
		resitsters.setRotationPoint(0.0F, -1.0F, 0.0F);
		big_plate6.addChild(resitsters);
		resitsters.setTextureOffset(252, 199).addBox(-16.0F, -82.5F, -51.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		resitsters.setTextureOffset(252, 199).addBox(-14.0F, -82.5F, -51.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		resitsters.setTextureOffset(252, 199).addBox(-12.0F, -82.5F, -51.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		resitsters.setTextureOffset(212, 136).addBox(-10.0F, -82.5F, -51.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		resitsters.setTextureOffset(252, 199).addBox(-8.0F, -82.5F, -51.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-16.0F, -82.5F, -52.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-14.0F, -82.5F, -52.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-12.0F, -82.5F, -52.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-10.0F, -82.5F, -52.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-8.0F, -82.5F, -52.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-16.0F, -82.5F, -47.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-14.0F, -82.5F, -47.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-12.0F, -82.5F, -47.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-10.0F, -82.5F, -47.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		resitsters.setTextureOffset(316, 205).addBox(-8.0F, -82.5F, -47.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		landing_rotate_x = new ModelRenderer(this);
		landing_rotate_x.setRotationPoint(-3.0447F, -83.5443F, -25.9124F);
		big_plate6.addChild(landing_rotate_x);
		landing_rotate_x.setTextureOffset(239, 127).addBox(-1.0F, -9.25F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		landing_rotate_x.setTextureOffset(239, 127).addBox(-1.0F, -13.75F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		landing_rotate_x.setTextureOffset(239, 127).addBox(-1.0F, -12.5F, -1.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		landing_rotate_x.setTextureOffset(299, 127).addBox(-1.0F, -6.25F, -1.0F, 2.0F, 4.0F, 2.0F, 0.0F, false);
		landing_rotate_x.setTextureOffset(299, 127).addBox(-0.5F, -14.25F, -0.5F, 1.0F, 8.0F, 1.0F, 0.0F, false);
		landing_rotate_x.setTextureOffset(299, 127).addBox(-2.0F, -2.25F, -2.0F, 3.0F, 4.0F, 4.0F, 0.0F, false);
		landing_rotate_x.setTextureOffset(298, 181).addBox(-4.75F, -1.25F, -1.0F, 6.0F, 2.0F, 2.0F, 0.0F, false);

		bevel_rib5 = new ModelRenderer(this);
		bevel_rib5.setRotationPoint(0.5F, -60.75F, -72.5F);
		sw_rib.addChild(bevel_rib5);
		setRotationAngle(bevel_rib5, 0.8727F, 0.0F, 0.0F);
		

		small_plate5 = new ModelRenderer(this);
		small_plate5.setRotationPoint(10.9661F, 82.15F, 47.785F);
		bevel_rib5.addChild(small_plate5);
		small_plate5.setTextureOffset(314, 76).addBox(-18.0F, -82.5F, -52.0F, 15.0F, 2.0F, 8.0F, 0.0F, false);
		small_plate5.setTextureOffset(313, 145).addBox(-17.0F, -83.0F, -46.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		small_plate5.setTextureOffset(313, 145).addBox(-17.0F, -83.0F, -49.25F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		small_plate5.setTextureOffset(196, 156).addBox(-14.0F, -83.0F, -51.25F, 9.0F, 2.0F, 6.0F, 0.0F, false);
		small_plate5.setTextureOffset(313, 145).addBox(-17.0F, -83.0F, -51.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		small_plate6 = new ModelRenderer(this);
		small_plate6.setRotationPoint(10.9661F, 88.15F, 71.785F);
		bevel_rib5.addChild(small_plate6);
		small_plate6.setTextureOffset(305, 76).addBox(-16.0F, -82.5F, -52.0F, 9.0F, 2.0F, 8.0F, 0.0F, false);
		small_plate6.setTextureOffset(323, 104).addBox(-15.0F, -82.75F, -47.0F, 7.0F, 1.0F, 2.0F, 0.0F, false);
		small_plate6.setTextureOffset(323, 104).addBox(-15.0F, -82.75F, -51.0F, 7.0F, 1.0F, 2.0F, 0.0F, false);

		vertical_rib6 = new ModelRenderer(this);
		vertical_rib6.setRotationPoint(0.5F, -69.75F, -61.5F);
		sw_rib.addChild(vertical_rib6);
		

		east_rib = new ModelRenderer(this);
		east_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_controls.addChild(east_rib);
		setRotationAngle(east_rib, 0.0F, 1.5708F, 0.0F);
		

		upper_rib5 = new ModelRenderer(this);
		upper_rib5.setRotationPoint(0.5F, -69.75F, -61.5F);
		east_rib.addChild(upper_rib5);
		setRotationAngle(upper_rib5, 0.4363F, 0.0F, 0.0F);
		

		refuler = new ModelRenderer(this);
		refuler.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib5.addChild(refuler);
		

		refuelknob_rotate = new ModelRenderer(this);
		refuelknob_rotate.setRotationPoint(-12.0F, -87.0288F, -54.3851F);
		refuler.addChild(refuelknob_rotate);
		refuelknob_rotate.setTextureOffset(308, 170).addBox(-0.5F, -1.2467F, -0.6128F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		refuelknob_rotate.setTextureOffset(242, 145).addBox(-1.0F, -1.0F, -2.5F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		tanks = new ModelRenderer(this);
		tanks.setRotationPoint(0.0F, 0.0F, 0.0F);
		refuler.addChild(tanks);
		tanks.setTextureOffset(186, 138).addBox(-15.5F, -86.1701F, -34.411F, 1.0F, 1.0F, 21.0F, 0.0F, false);
		tanks.setTextureOffset(186, 138).addBox(-9.5F, -86.1701F, -34.411F, 1.0F, 1.0F, 21.0F, 0.0F, false);
		tanks.setTextureOffset(350, 179).addBox(-10.0F, -86.6701F, -34.411F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		tanks.setTextureOffset(350, 179).addBox(-10.0F, -86.6701F, -18.411F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		tanks.setTextureOffset(350, 179).addBox(-16.0F, -86.6701F, -30.411F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		tanks.setTextureOffset(301, 134).addBox(-17.0F, -84.5F, -59.0F, 4.0F, 4.0F, 15.0F, 0.0F, false);
		tanks.setTextureOffset(301, 134).addBox(-11.0F, -84.5F, -59.0F, 4.0F, 4.0F, 15.0F, 0.0F, false);
		tanks.setTextureOffset(298, 97).addBox(-18.0F, -85.5F, -56.0F, 12.0F, 6.0F, 3.0F, 0.0F, false);
		tanks.setTextureOffset(311, 128).addBox(-16.5F, -84.0F, -44.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);
		tanks.setTextureOffset(311, 128).addBox(-10.5F, -84.0F, -44.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);

		bevel_rib4 = new ModelRenderer(this);
		bevel_rib4.setRotationPoint(0.5F, -60.75F, -72.5F);
		east_rib.addChild(bevel_rib4);
		setRotationAngle(bevel_rib4, 0.8727F, 0.0F, 0.0F);
		

		small_plate3 = new ModelRenderer(this);
		small_plate3.setRotationPoint(10.9661F, 82.15F, 47.785F);
		bevel_rib4.addChild(small_plate3);
		small_plate3.setTextureOffset(305, 76).addBox(-18.0F, -82.5F, -54.0F, 8.0F, 2.0F, 8.0F, 0.0F, false);
		small_plate3.setTextureOffset(301, 199).addBox(-17.5F, -84.0F, -53.5F, 7.0F, 2.0F, 7.0F, 0.0F, false);
		small_plate3.setTextureOffset(183, 142).addBox(-15.0F, -84.75F, -51.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		glow_dial = new ModelRenderer(this);
		glow_dial.setRotationPoint(0.0F, 0.0F, 0.0F);
		small_plate3.addChild(glow_dial);
		glow_dial.setTextureOffset(343, 11).addBox(-17.0F, -84.25F, -53.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

		plate3_needle = new ModelRenderer(this);
		plate3_needle.setRotationPoint(-13.8125F, -84.625F, -50.0937F);
		small_plate3.addChild(plate3_needle);
		plate3_needle.setTextureOffset(273, 204).addBox(-0.6875F, 0.125F, 0.0938F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		small_plate4 = new ModelRenderer(this);
		small_plate4.setRotationPoint(10.9661F, 88.15F, 71.785F);
		bevel_rib4.addChild(small_plate4);
		small_plate4.setTextureOffset(206, 142).addBox(-15.5F, -83.5F, -53.0F, 1.0F, 1.0F, 9.0F, 0.0F, false);
		small_plate4.setTextureOffset(206, 142).addBox(-9.5F, -83.5F, -53.0F, 1.0F, 1.0F, 9.0F, 0.0F, false);
		small_plate4.setTextureOffset(333, 185).addBox(-10.0F, -83.9532F, -44.7887F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		vertical_rib5 = new ModelRenderer(this);
		vertical_rib5.setRotationPoint(0.5F, -69.75F, -61.5F);
		east_rib.addChild(vertical_rib5);
		

		facing = new ModelRenderer(this);
		facing.setRotationPoint(10.9661F, 82.15F, 47.785F);
		vertical_rib5.addChild(facing);
		

		facing_rotate_z = new ModelRenderer(this);
		facing_rotate_z.setRotationPoint(-10.9661F, -59.65F, -68.285F);
		facing.addChild(facing_rotate_z);
		

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(9.4661F, 55.15F, 70.535F);
		facing_rotate_z.addChild(bone13);
		bone13.setTextureOffset(398, 203).addBox(-13.0F, -61.5F, -70.0F, 7.0F, 2.0F, 2.0F, 0.0F, false);
		bone13.setTextureOffset(398, 203).addBox(-13.0F, -50.5F, -70.0F, 7.0F, 2.0F, 2.0F, 0.0F, false);
		bone13.setTextureOffset(398, 203).addBox(-5.0F, -58.5F, -70.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);
		bone13.setTextureOffset(398, 203).addBox(-16.0F, -58.5F, -70.0F, 2.0F, 7.0F, 2.0F, 0.0F, false);
		bone13.setTextureOffset(398, 203).addBox(-14.5F, -55.5F, -69.25F, 10.0F, 1.0F, 1.0F, 0.0F, false);
		bone13.setTextureOffset(396, 64).addBox(-10.5F, -56.0F, -70.25F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		bone13.setTextureOffset(396, 64).addBox(-4.75F, -56.0F, -74.25F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		bone13.setTextureOffset(364, 183).addBox(-4.25F, -55.5F, -74.5F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		bone13.setTextureOffset(398, 203).addBox(-10.0F, -59.5F, -69.25F, 1.0F, 9.0F, 1.0F, 0.0F, false);

		bone14 = new ModelRenderer(this);
		bone14.setRotationPoint(-0.0339F, 0.15F, 1.785F);
		facing_rotate_z.addChild(bone14);
		setRotationAngle(bone14, 0.0F, 0.0F, 0.7854F);
		bone14.setTextureOffset(398, 203).addBox(-2.5F, -6.5F, -1.0F, 5.0F, 2.0F, 2.0F, 0.0F, false);
		bone14.setTextureOffset(398, 203).addBox(-2.5F, 4.5F, -1.0F, 5.0F, 2.0F, 2.0F, 0.0F, false);
		bone14.setTextureOffset(398, 203).addBox(4.5F, -2.5F, -1.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);
		bone14.setTextureOffset(398, 203).addBox(-6.5F, -2.5F, -1.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);

		winch_plate = new ModelRenderer(this);
		winch_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
		facing.addChild(winch_plate);
		winch_plate.setTextureOffset(396, 64).addBox(-17.0F, -65.5F, -64.0F, 12.0F, 12.0F, 1.0F, 0.0F, false);

		ne_rib = new ModelRenderer(this);
		ne_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_controls.addChild(ne_rib);
		setRotationAngle(ne_rib, 0.0F, 0.5236F, 0.0F);
		

		upper_rib4 = new ModelRenderer(this);
		upper_rib4.setRotationPoint(0.5F, -69.75F, -61.5F);
		ne_rib.addChild(upper_rib4);
		setRotationAngle(upper_rib4, 0.4363F, 0.0F, 0.0F);
		

		stablizers = new ModelRenderer(this);
		stablizers.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib4.addChild(stablizers);
		

		pull_rotate_x = new ModelRenderer(this);
		pull_rotate_x.setRotationPoint(-12.0859F, -85.5F, -47.9375F);
		stablizers.addChild(pull_rotate_x);
		setRotationAngle(pull_rotate_x, -2.8798F, 0.0F, 0.0F);
		pull_rotate_x.setTextureOffset(198, 138).addBox(-1.6641F, -2.0F, -2.0625F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(198, 138).addBox(-1.6641F, -1.0F, 1.9375F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(198, 138).addBox(-2.6641F, -1.0F, 3.9375F, 1.0F, 2.0F, 10.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(305, 182).addBox(-1.6641F, -1.0F, 11.9375F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(305, 182).addBox(2.8359F, -1.0F, 11.9375F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(305, 182).addBox(-0.4141F, -1.0F, 11.9375F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(240, 157).addBox(-0.9141F, -0.5F, 12.4375F, 5.0F, 1.0F, 1.0F, 0.0F, false);
		pull_rotate_x.setTextureOffset(305, 182).addBox(-3.3516F, -1.0F, -1.0625F, 4.0F, 2.0F, 2.0F, 0.0F, false);

		pull_leaverbase = new ModelRenderer(this);
		pull_leaverbase.setRotationPoint(0.0F, 0.0F, 0.0F);
		stablizers.addChild(pull_leaverbase);
		pull_leaverbase.setTextureOffset(185, 133).addBox(-16.0F, -81.5F, -53.0F, 11.0F, 1.0F, 11.0F, 0.0F, false);
		pull_leaverbase.setTextureOffset(186, 139).addBox(-16.0F, -81.5F, -57.0F, 8.0F, 1.0F, 4.0F, 0.0F, false);
		pull_leaverbase.setTextureOffset(339, 66).addBox(-6.5F, -82.0F, -44.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		pull_leaverbase.setTextureOffset(339, 66).addBox(-6.5F, -82.0F, -50.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		pull_leaverbase.setTextureOffset(198, 138).addBox(-15.0F, -87.5F, -50.0F, 1.0F, 6.0F, 4.0F, 0.0F, false);
		pull_leaverbase.setTextureOffset(198, 138).addBox(-14.0F, -82.5F, -50.0F, 4.0F, 1.0F, 7.0F, 0.0F, false);

		deskbell = new ModelRenderer(this);
		deskbell.setRotationPoint(12.2161F, 82.15F, 69.535F);
		upper_rib4.addChild(deskbell);
		deskbell.setTextureOffset(386, 73).addBox(-17.0F, -81.5F, -58.0F, 9.0F, 1.0F, 9.0F, 0.0F, false);
		deskbell.setTextureOffset(300, 202).addBox(-16.0F, -83.5F, -57.0F, 7.0F, 2.0F, 7.0F, 0.0F, false);
		deskbell.setTextureOffset(300, 202).addBox(-15.5F, -84.5F, -56.5F, 6.0F, 2.0F, 6.0F, 0.0F, false);
		deskbell.setTextureOffset(300, 202).addBox(-13.5F, -85.5F, -54.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		bevel_rib3 = new ModelRenderer(this);
		bevel_rib3.setRotationPoint(0.5F, -60.75F, -72.5F);
		ne_rib.addChild(bevel_rib3);
		setRotationAngle(bevel_rib3, 0.8727F, 0.0F, 0.0F);
		

		small_plate2 = new ModelRenderer(this);
		small_plate2.setRotationPoint(10.9661F, 82.15F, 47.785F);
		bevel_rib3.addChild(small_plate2);
		small_plate2.setTextureOffset(193, 133).addBox(-16.0F, -82.8139F, -47.4462F, 8.0F, 2.0F, 5.0F, 0.0F, false);

		vertical_rib4 = new ModelRenderer(this);
		vertical_rib4.setRotationPoint(0.5F, -69.75F, -61.5F);
		ne_rib.addChild(vertical_rib4);
		

		med_plate4 = new ModelRenderer(this);
		med_plate4.setRotationPoint(10.9661F, 82.15F, 47.785F);
		vertical_rib4.addChild(med_plate4);
		

		se_rib = new ModelRenderer(this);
		se_rib.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_controls.addChild(se_rib);
		setRotationAngle(se_rib, 0.0F, 2.618F, 0.0F);
		

		upper_rib2 = new ModelRenderer(this);
		upper_rib2.setRotationPoint(0.5F, -69.75F, -61.5F);
		se_rib.addChild(upper_rib2);
		setRotationAngle(upper_rib2, 0.4363F, 0.0F, 0.0F);
		

		monitor = new ModelRenderer(this);
		monitor.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib2.addChild(monitor);
		setRotationAngle(monitor, -0.3491F, 0.0F, 0.0F);
		monitor.setTextureOffset(334, 112).addBox(-29.0F, -99.5F, -49.0F, 35.0F, 23.0F, 2.0F, 0.0F, false);
		monitor.setTextureOffset(334, 112).addBox(-29.0F, -78.5F, -51.0F, 35.0F, 2.0F, 2.0F, 0.0F, false);
		monitor.setTextureOffset(183, 130).addBox(-27.0F, -98.5F, -48.0F, 31.0F, 16.0F, 4.0F, 0.0F, false);

		screen3 = new ModelRenderer(this);
		screen3.setRotationPoint(-11.5F, -89.0F, -48.75F);
		monitor.addChild(screen3);
		setRotationAngle(screen3, 0.0F, 0.0F, -1.5708F);
		screen3.setTextureOffset(51, 190).addBox(-9.5F, -16.5F, -0.5F, 19.0F, 33.0F, 1.0F, 0.0F, false);

		door_switch = new ModelRenderer(this);
		door_switch.setRotationPoint(-4.2886F, 0.2417F, 5.5714F);
		upper_rib2.addChild(door_switch);
		door_switch.setTextureOffset(296, 128).addBox(-1.0F, -3.025F, -1.5938F, 2.0F, 5.0F, 5.0F, 0.0F, false);
		door_switch.setTextureOffset(296, 128).addBox(-2.0F, -1.025F, -2.5938F, 4.0F, 4.0F, 5.0F, 0.0F, false);
		door_switch.setTextureOffset(296, 128).addBox(-3.0F, -1.025F, -1.5938F, 1.0F, 4.0F, 3.0F, 0.0F, false);
		door_switch.setTextureOffset(296, 128).addBox(2.0F, -1.025F, -1.5938F, 1.0F, 4.0F, 3.0F, 0.0F, false);
		door_switch.setTextureOffset(224, 154).addBox(-0.5F, -4.4F, -2.125F, 1.0F, 6.0F, 4.0F, 0.0F, false);

		capasitors = new ModelRenderer(this);
		capasitors.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib2.addChild(capasitors);
		capasitors.setTextureOffset(296, 128).addBox(-17.75F, -82.5F, -27.0F, 12.0F, 1.0F, 1.0F, 0.0F, false);
		capasitors.setTextureOffset(190, 133).addBox(-19.0F, -81.5F, -28.0F, 14.0F, 1.0F, 9.0F, 0.0F, false);
		capasitors.setTextureOffset(296, 128).addBox(-18.0F, -84.5F, -22.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		capasitors.setTextureOffset(296, 128).addBox(-15.0F, -84.5F, -22.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		capasitors.setTextureOffset(296, 128).addBox(-12.0F, -84.5F, -22.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		capasitors.setTextureOffset(264, 192).addBox(-8.75F, -84.5F, -22.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		capasitors.setTextureOffset(296, 128).addBox(-10.75F, -84.5F, -25.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		capasitors.setTextureOffset(296, 128).addBox(-13.75F, -84.5F, -25.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		capasitors.setTextureOffset(296, 128).addBox(-16.75F, -84.5F, -25.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		buttons = new ModelRenderer(this);
		buttons.setRotationPoint(10.9661F, 82.15F, 47.785F);
		upper_rib2.addChild(buttons);
		buttons.setTextureOffset(308, 205).addBox(-8.5F, -81.75F, -39.25F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		buttons.setTextureOffset(224, 154).addBox(-8.0F, -82.75F, -38.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		buttons.setTextureOffset(224, 154).addBox(-8.0F, -82.75F, -34.0F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		buttons.setTextureOffset(308, 205).addBox(-8.5F, -81.75F, -34.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		bevel_rib2 = new ModelRenderer(this);
		bevel_rib2.setRotationPoint(0.5F, -60.75F, -72.5F);
		se_rib.addChild(bevel_rib2);
		setRotationAngle(bevel_rib2, 0.8727F, 0.0F, 0.0F);
		

		increment_select = new ModelRenderer(this);
		increment_select.setRotationPoint(10.9661F, 82.15F, 47.785F);
		bevel_rib2.addChild(increment_select);
		

		crank_rotate_y = new ModelRenderer(this);
		crank_rotate_y.setRotationPoint(-10.9167F, -89.5F, -48.0F);
		increment_select.addChild(crank_rotate_y);
		crank_rotate_y.setTextureOffset(361, 64).addBox(-1.0833F, -1.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
		crank_rotate_y.setTextureOffset(306, 131).addBox(-7.0833F, -0.5F, -0.5F, 9.0F, 1.0F, 1.0F, 0.0F, false);
		crank_rotate_y.setTextureOffset(295, 64).addBox(-8.0833F, -6.0F, -1.0F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		crank_mount2 = new ModelRenderer(this);
		crank_mount2.setRotationPoint(0.0F, 0.0F, 0.0F);
		increment_select.addChild(crank_mount2);
		crank_mount2.setTextureOffset(305, 76).addBox(-15.0F, -82.5F, -52.0F, 8.0F, 2.0F, 8.0F, 0.0F, false);
		crank_mount2.setTextureOffset(305, 76).addBox(-14.0F, -88.5F, -51.0F, 6.0F, 1.0F, 6.0F, 0.0F, false);
		crank_mount2.setTextureOffset(304, 180).addBox(-13.0F, -83.5F, -50.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		crank_mount2.setTextureOffset(304, 180).addBox(-13.0F, -85.5F, -50.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		crank_mount2.setTextureOffset(304, 180).addBox(-13.0F, -87.5F, -50.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		crank_mount2.setTextureOffset(333, 73).addBox(-9.0F, -87.5F, -51.0F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		crank_mount2.setTextureOffset(333, 73).addBox(-14.0F, -87.5F, -51.0F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		crank_mount2.setTextureOffset(333, 73).addBox(-14.0F, -87.5F, -46.0F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		crank_mount2.setTextureOffset(333, 73).addBox(-9.0F, -87.5F, -46.0F, 1.0F, 5.0F, 1.0F, 0.0F, false);

		vertical_rib2 = new ModelRenderer(this);
		vertical_rib2.setRotationPoint(0.5F, -69.75F, -61.5F);
		se_rib.addChild(vertical_rib2);
		

		med_plate2 = new ModelRenderer(this);
		med_plate2.setRotationPoint(10.9661F, 82.15F, 47.785F);
		vertical_rib2.addChild(med_plate2);
		

		guts = new ModelRenderer(this);
		guts.setRotationPoint(0.0F, 12.0F, 0.0F);
		guts.setTextureOffset(295, 43).addBox(-25.0F, -23.0F, -38.0F, 50.0F, 12.0F, 19.0F, 0.0F, false);
		guts.setTextureOffset(193, 131).addBox(-14.0F, -15.0F, -38.0F, 15.0F, 12.0F, 8.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-25.0F, -23.0F, -19.0F, 50.0F, 12.0F, 19.0F, 0.0F, false);
		guts.setTextureOffset(188, 120).addBox(-30.0F, -16.0F, -8.0F, 17.0F, 12.0F, 19.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(3.0F, -16.0F, -20.0F, 2.0F, 9.0F, 7.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(0.0F, -16.0F, -20.0F, 2.0F, 9.0F, 7.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-3.0F, -16.0F, -20.0F, 2.0F, 9.0F, 7.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-27.0F, -16.0F, 9.0F, 12.0F, 10.0F, 9.0F, 0.0F, false);
		guts.setTextureOffset(334, 121).addBox(-18.0F, -16.0F, -23.0F, 2.0F, 8.0F, 15.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(-8.0F, -17.25F, -8.0F, 16.0F, 8.0F, 16.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(-8.0F, -7.5F, -8.0F, 16.0F, 4.0F, 16.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(-8.0F, -2.25F, -8.0F, 16.0F, 4.0F, 16.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(-8.0F, 2.75F, -8.0F, 16.0F, 4.0F, 16.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(11.0F, -3.0F, -20.0F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(20.0F, -11.0F, 10.0F, 6.0F, 22.0F, 6.0F, 0.0F, false);
		guts.setTextureOffset(185, 125).addBox(-25.0F, -4.0F, -1.0F, 6.0F, 15.0F, 6.0F, 0.0F, false);
		guts.setTextureOffset(342, 179).addBox(-11.0F, -16.0F, -21.0F, 17.0F, 8.0F, 9.0F, 0.0F, false);
		guts.setTextureOffset(342, 179).addBox(-20.0F, -16.0F, -27.0F, 6.0F, 10.0F, 6.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-2.0F, -16.0F, 4.0F, 17.0F, 12.0F, 19.0F, 0.0F, false);
		guts.setTextureOffset(304, 132).addBox(0.0F, -4.0F, 6.0F, 13.0F, 2.0F, 15.0F, 0.0F, false);
		guts.setTextureOffset(306, 115).addBox(6.0F, -16.0F, -23.0F, 15.0F, 12.0F, 13.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-25.0F, -23.0F, 0.0F, 50.0F, 12.0F, 19.0F, 0.0F, false);
		guts.setTextureOffset(293, 43).addBox(-25.0F, -23.0F, 19.0F, 50.0F, 12.0F, 19.0F, 0.0F, false);
		guts.setTextureOffset(207, 129).addBox(-18.0F, -16.0F, 23.0F, 13.0F, 12.0F, 15.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(25.0F, -23.0F, -30.0F, 13.0F, 12.0F, 30.0F, 0.0F, false);
		guts.setTextureOffset(194, 130).addBox(25.0F, -18.0F, -13.0F, 13.0F, 12.0F, 16.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-39.0F, -23.0F, -29.0F, 14.0F, 12.0F, 29.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(25.0F, -23.0F, 0.0F, 13.0F, 12.0F, 31.0F, 0.0F, false);
		guts.setTextureOffset(295, 43).addBox(-39.0F, -23.0F, 0.0F, 14.0F, 12.0F, 30.0F, 0.0F, false);

		bone35 = new ModelRenderer(this);
		bone35.setRotationPoint(0.0F, -0.25F, 0.0F);
		guts.addChild(bone35);
		bone35.setTextureOffset(185, 125).addBox(-8.0F, 8.0F, -6.0F, 2.0F, 4.0F, 12.0F, 0.0F, false);
		bone35.setTextureOffset(185, 125).addBox(6.0F, 8.0F, -6.0F, 2.0F, 4.0F, 12.0F, 0.0F, false);
		bone35.setTextureOffset(185, 125).addBox(-8.0F, 8.0F, -8.0F, 16.0F, 4.0F, 2.0F, 0.0F, false);
		bone35.setTextureOffset(185, 125).addBox(-8.0F, 8.0F, 6.0F, 16.0F, 4.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		
		glow.render(matrixStack, buffer, packedLight, packedOverlay);
		stations.render(matrixStack, buffer, packedLight, packedOverlay);
		controls.render(matrixStack, buffer, packedLight, packedOverlay);
		guts.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void translateMonitorPos(MatrixStack stack) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(XionConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		
		tile.getControl(HandbrakeControl.class).ifPresent(control -> {
			this.handbreak_rotate_x.rotateAngleX = (float) Math.toRadians(control.isFree() ? -5 : 0);
		});
		
		tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
			this.throttle_rotate_x.rotateAngleX = (float) Math.toRadians(90 - throttle.getAmount() * 90);
		});
		
		tile.getControl(DimensionControl.class).ifPresent(control -> {
			
			float angle = (control.getDimListIndex() / (float)control.getAvailableDimensions()) * 360.0F;
			
			angle -= control.getAnimationProgress() * 360.0;
			
			this.wheel_rotate_z3.rotateAngleZ = (float) Math.toRadians(angle);
		});
		
		tile.getControl(LandingTypeControl.class).ifPresent(control -> {
			this.landing_rotate_x.rotateAngleX = (float) Math.toRadians(control.getLandType() == EnumLandType.DOWN ? 40 : -40);
		});
		
		tile.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
			this.pull_rotate_x.rotateAngleX = (float) Math.toRadians(sys.isActivated() ? 165 : -10);
		});
		
		tile.getControl(RandomiserControl.class).ifPresent(rand -> {
			this.spinner.rotateAngleY = (float) Math.toRadians(rand.getAnimationProgress() * 360.0F);
		});
		
		tile.getControl(FacingControl.class).ifPresent(face -> {
			
			float angle = WorldHelper.getAngleFromFacing(face.getDirection()) - 90;
			
			angle -= face.getAnimationProgress() * 360.0;
			
			this.facing_rotate_z.rotateAngleZ = (float) Math.toRadians(angle);
		});
		
		tile.getControl(IncModControl.class).ifPresent(inc -> {
			this.crank_rotate_y.rotateAngleY = (float) Math.toRadians((inc.index / (float)IncModControl.COORD_MODS.length) * 240);
		});
		
		tile.getControl(DoorControl.class).ifPresent(door -> {
			this.door_switch.rotateAngleY = (float) Math.toRadians(0);
		});
		
		tile.getControl(RefuelerControl.class).ifPresent(control -> {
			this.refuelknob_rotate.rotateAngleY = (float) Math.toRadians(control.isRefueling() ? 90 : 0);
		});
		
		this.glow.setBright(1.0F);
		this.glow_rotor.setBright(1.0F);
		
		matrixStack.push();
		matrixStack.translate(0, Math.sin(MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), tile.prevFlightTicks, tile.flightTicks) * 0.1) + 0.4, 0);
		glow_rotor.render(matrixStack, buffer, packedLight, packedOverlay);
		matrixStack.pop();
		
		this.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}