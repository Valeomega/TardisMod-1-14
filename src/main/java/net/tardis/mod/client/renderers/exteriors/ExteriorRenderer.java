package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorRenderer<T extends ExteriorTile> extends TileEntityRenderer<T> {

    public ExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    @Override
    public void render(T tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        
        applyTransforms(matrixStackIn, tile);
        
        this.renderExterior(tile, partialTicks, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn, tile.getMatterState() == EnumMatterState.SOLID ? 1.0F : tile.alpha);

        matrixStackIn.pop();

        //Debug

        if(Minecraft.getInstance().player != null && PlayerHelper.InEitherHand(Minecraft.getInstance().player, stack -> stack.getItem() == TItems.DEBUG.get())) {
        	matrixStackIn.push();
        	matrixStackIn.translate(0,0,0);
            IRenderTypeBuffer.Impl buffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
            IVertexBuilder builder = buffer.getBuffer(RenderType.getLines());
            WorldRenderer.drawBoundingBox(matrixStackIn, builder, tile.getDoorAABB(), 1.0F, 0.0F, 0.0F, 0.75F);
            matrixStackIn.pop();
        }

    }

    public boolean floatInAir() {
        return true;
    }

    public abstract void renderExterior(T tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha);

    public static boolean isInverted(ExteriorTile tile) {
        if (tile.getCustomName().contains("Dinnerbone") || tile.getCustomName().contains("Grumm") || tile.getCustomName().contains("boti")) {
            return true;
        }
        return false;
    }

    public static void applyTransforms(MatrixStack matrixStack, ExteriorTile tile) {
            matrixStack.translate(0.5, -0.5, 0.5);
            matrixStack.rotate(Vector3f.ZN.rotationDegrees(180));
            
            if (tile.getBlockState() != null && tile.getBlockState().getBlock() instanceof ExteriorBlock) {
                Direction face = tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
                matrixStack.rotate(Vector3f.YP.rotationDegrees(face.getHorizontalAngle() - 180));
            }
            
            if (isInverted(tile)) {
                matrixStack.rotate(Vector3f.ZP.rotationDegrees(180));
                matrixStack.translate(0, 1.75, 0);
            }
            if (tile.getWorld() != null && tile.getWorld().getBlockState(tile.getPos().down(2)).isAir()) {
                double offY = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.06;
                double offX = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.07;
                double offZ = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.07;
                matrixStack.translate(offX, offY, offZ);
            }
    }
}
