package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.InteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class InteriorSchematicType extends SchematicType {


    @Override
    public Schematic deserialize(ResourceLocation key, JsonObject root) {
        InteriorUnlockSchematic schematic = new InteriorUnlockSchematic(this);
        schematic.setConsoleRoom(new ResourceLocation(root.get("result").getAsString()));
        schematic.setId(key);
        return schematic;
    }

    @Override
    public JsonObject serialize(Schematic schematic) {
        return null;
    }
}
