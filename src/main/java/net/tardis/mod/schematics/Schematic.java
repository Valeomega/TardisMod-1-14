package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.types.SchematicType;
import net.tardis.mod.tileentities.ConsoleTile;
/** Template for an wrapper object that can be used to unlock objects for a Tardis*/
public abstract class Schematic{
	
	protected ResourceLocation key;
	private SchematicType type;
	private String displayName;

	public Schematic(SchematicType type){
		this.type = type;
	}

	/** Handle what to do if the item holding this schematic was added to the console's sonic port*/
	public abstract void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player);

	public ResourceLocation getId(){
		return key;
	}

	public Schematic setId(ResourceLocation key){
		this.key = key;
		return this;
	}

	public SchematicType getType(){
		return this.type;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}
}
