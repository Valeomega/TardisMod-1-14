package net.tardis.mod.client.guis;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.BlockPosGuiContext;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ARSDeleteMessage;
import net.tardis.mod.sounds.TSounds;

public class ARSTabletKillScreen extends Screen {

    public static final TranslationTextComponent TITLE = new TranslationTextComponent(Constants.Strings.GUI + "ars_tablet.kill.title");
    public static final TranslationTextComponent CONFIRM = new TranslationTextComponent(Constants.Strings.GUI + "ars_tablet.kill.confirm");
    public static final TranslationTextComponent CLOSE = new TranslationTextComponent(Constants.Strings.GUI + "ars_tablet.kill.close");
    private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");
    public static int WIDTH = 256, HEIGHT = 256;
    private BlockPos killPos = BlockPos.ZERO;

    public ARSTabletKillScreen(GuiContext context) {
        super(new StringTextComponent(""));
        this.killPos = ((BlockPosGuiContext) context).pos;
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        this.renderBackground(matrixStack);
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        int width = 256, height = 173;
        this.blit(matrixStack, this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);

        String title = TITLE.getString();
        int titleWidth = font.getStringWidth(title);
        font.drawString(matrixStack, title, this.width / 2 - titleWidth / 2, this.height / 2 - 50, 0xFFFFFF);

        super.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);
    }

    @Override
    protected void init() {
        super.init();
        Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.SCREEN_BEEP_SINGLE.get(), 1.0F));
        
        this.buttons.clear();

        int x = width / 2 - 100, y = height / 2 + 45;

        this.addButton(new TextButton(x, y, CONFIRM.getString(), but -> {
            Network.sendToServer(new ARSDeleteMessage(this.killPos));
            Minecraft.getInstance().displayGuiScreen(null);
        }));
        this.addButton(new TextButton(x, y + font.FONT_HEIGHT + 2, CLOSE.getString(), but -> Minecraft.getInstance().displayGuiScreen(null)));

    }

}
