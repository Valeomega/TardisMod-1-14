package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class NavComData implements ConsoleData{

	public boolean hasCom = false;
	
	public NavComData(boolean hasCom) {
		this.hasCom = hasCom;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setNavCom(this.hasCom);
	}

	@Override
	public void serialize(PacketBuffer buf) {
		buf.writeBoolean(hasCom);
	}

	@Override
	public void deserialize(PacketBuffer buf) {
		this.hasCom = buf.readBoolean();
	}

	@Override
	public ConsoleUpdateMessage.DataTypes getDataType() {
		return ConsoleUpdateMessage.DataTypes.NAV_COM;
	}

}
