package net.tardis.mod.client.models.consoles;

import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.Direction.AxisDirection;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.TileModel;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.FastReturnControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;

// Made with Blockbench 3.7.5
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class NeutronConsoleModel extends Model implements TileModel<NeutronConsoleTile>{
	private final ModelRenderer rotor;
	private final LightModelRenderer glow_crystal_rotate_y;
	private final ModelRenderer y45;
	private final ModelRenderer z45;
	private final ModelRenderer x45;
	private final LightModelRenderer glow_outer_rotate_x;
	private final ModelRenderer bone61;
	private final ModelRenderer bone62;
	private final ModelRenderer bone63;
	private final LightModelRenderer glow_inner_rotate_x;
	private final ModelRenderer bone64;
	private final ModelRenderer bone65;
	private final ModelRenderer bone66;
	private final LightModelRenderer glow;
	private final LightModelRenderer glow_inner;
	private final LightModelRenderer glow_trim;
	private final ModelRenderer grill_corner_light;
	private final ModelRenderer gril_light;
	private final LightModelRenderer glow_bar;
	private final ModelRenderer glow_corner_fill;
	private final ModelRenderer glow_trim2;
	private final ModelRenderer grill_corner_light2;
	private final ModelRenderer gril_light2;
	private final ModelRenderer glow_bar2;
	private final ModelRenderer glow_corner_fill2;
	private final ModelRenderer glow_trim3;
	private final ModelRenderer grill_corner_light3;
	private final ModelRenderer gril_light3;
	private final ModelRenderer glow_bar3;
	private final ModelRenderer glow_corner_fill3;
	private final ModelRenderer glow_trim4;
	private final ModelRenderer grill_corner_light4;
	private final ModelRenderer gril_light4;
	private final ModelRenderer glow_bar4;
	private final ModelRenderer glow_corner_fill4;
	private final ModelRenderer glow_trim5;
	private final ModelRenderer grill_corner_light5;
	private final ModelRenderer gril_light5;
	private final ModelRenderer glow_bar5;
	private final ModelRenderer glow_corner_fill5;
	private final ModelRenderer glow_trim6;
	private final ModelRenderer grill_corner_light6;
	private final ModelRenderer gril_light6;
	private final ModelRenderer glow_bar6;
	private final ModelRenderer glow_corner_fill6;
	private final LightModelRenderer glow_posts;
	private final ModelRenderer basepost1;
	private final ModelRenderer basepost2;
	private final ModelRenderer basepost3;
	private final ModelRenderer basepost4;
	private final ModelRenderer basepost5;
	private final ModelRenderer basepost6;
	private final ModelRenderer glow_controls;
	private final LightModelRenderer controls8;
	private final ModelRenderer base_plate_7;
	private final ModelRenderer buttonstrip2;
	private final ModelRenderer buttons_lit2;
	private final ModelRenderer small_screen2;
	private final ModelRenderer indicator4;
	private final ModelRenderer indicator5;
	private final ModelRenderer indicator6;
	private final ModelRenderer coms2;
	private final LightModelRenderer controls9;
	private final ModelRenderer base_plate_8;
	private final ModelRenderer dummy_buttons13;
	private final ModelRenderer dummy_buttons14;
	private final ModelRenderer dummy_buttons15;
	private final LightModelRenderer controls10;
	private final ModelRenderer base_plate_9;
	private final ModelRenderer base2;
	private final ModelRenderer controls11;
	private final ModelRenderer base_plate_10;
	private final ModelRenderer coords_z2;
	private final ModelRenderer toggle_base4;
	private final LightModelRenderer glow_z_coord_blue;
	private final ModelRenderer coords_y2;
	private final ModelRenderer toggle_base5;
	private final LightModelRenderer glow_y_cord_green;
	private final ModelRenderer toggle5;
	private final ModelRenderer coords_x2;
	private final ModelRenderer toggle_base6;
	private final LightModelRenderer glow_x_cord_red;
	private final LightModelRenderer radar2_glow;
	private final ModelRenderer bigslider2;
	private final LightModelRenderer button_array2;
	private final ModelRenderer buttons2;
	private final LightModelRenderer controls12;
	private final ModelRenderer base_plate_11;
	private final ModelRenderer angeled_panel2;
	private final ModelRenderer dummy_buttons19;
	private final ModelRenderer refuler2;
	private final ModelRenderer glass2;
	private final LightModelRenderer controls13;
	private final ModelRenderer base_plate_12;
	private final ModelRenderer dummy_buttons24;
	private final ModelRenderer dial3;
	private final ModelRenderer dial_glass3;
	private final ModelRenderer dial4;
	private final ModelRenderer dial_glass4;
	private final LightModelRenderer rib_control2;
	private final ModelRenderer spine_con4;
	private final ModelRenderer plain10;
	private final ModelRenderer spine_con6;
	private final ModelRenderer plain12;
	private final ModelRenderer telepathic3;
	private final ModelRenderer microscope2;
	private final ModelRenderer spines;
	private final ModelRenderer silverib1;
	private final ModelRenderer spine1;
	private final ModelRenderer plain1;
	private final ModelRenderer bevel1;
	private final ModelRenderer front1;
	private final ModelRenderer bone;
	private final ModelRenderer ring;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer ring2;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer glow__innards;
	private final ModelRenderer undercurve1;
	private final ModelRenderer backbend1;
	private final ModelRenderer shin1;
	private final ModelRenderer bone16;
	private final ModelRenderer ribfoot1;
	private final ModelRenderer silverib2;
	private final ModelRenderer spine2;
	private final ModelRenderer plain2;
	private final ModelRenderer bevel2;
	private final ModelRenderer front2;
	private final ModelRenderer bone8;
	private final ModelRenderer ring5;
	private final ModelRenderer bone9;
	private final ModelRenderer bone10;
	private final ModelRenderer ring6;
	private final ModelRenderer bone13;
	private final ModelRenderer bone14;
	private final ModelRenderer glow__innards2;
	private final ModelRenderer undercurve2;
	private final ModelRenderer backbend2;
	private final ModelRenderer shin2;
	private final ModelRenderer bone15;
	private final ModelRenderer ribfoot2;
	private final ModelRenderer silverib3;
	private final ModelRenderer spine3;
	private final ModelRenderer plain3;
	private final ModelRenderer bevel3;
	private final ModelRenderer front3;
	private final ModelRenderer bone17;
	private final ModelRenderer ring7;
	private final ModelRenderer bone18;
	private final ModelRenderer bone19;
	private final ModelRenderer ring8;
	private final ModelRenderer bone20;
	private final ModelRenderer bone21;
	private final ModelRenderer glow__innards3;
	private final ModelRenderer undercurve3;
	private final ModelRenderer backbend3;
	private final ModelRenderer shin3;
	private final ModelRenderer bone22;
	private final ModelRenderer ribfoot3;
	private final ModelRenderer silverib4;
	private final ModelRenderer spine4;
	private final ModelRenderer plain4;
	private final ModelRenderer bevel4;
	private final ModelRenderer front4;
	private final ModelRenderer bone23;
	private final ModelRenderer ring9;
	private final ModelRenderer bone24;
	private final ModelRenderer bone25;
	private final ModelRenderer ring10;
	private final ModelRenderer bone26;
	private final ModelRenderer bone27;
	private final ModelRenderer glow__innards4;
	private final ModelRenderer undercurve4;
	private final ModelRenderer backbend4;
	private final ModelRenderer shin4;
	private final ModelRenderer bone28;
	private final ModelRenderer ribfoot4;
	private final ModelRenderer silverib5;
	private final ModelRenderer spine5;
	private final ModelRenderer plain5;
	private final ModelRenderer bevel5;
	private final ModelRenderer front5;
	private final ModelRenderer bone29;
	private final ModelRenderer ring11;
	private final ModelRenderer bone30;
	private final ModelRenderer bone31;
	private final ModelRenderer ring12;
	private final ModelRenderer bone32;
	private final ModelRenderer bone33;
	private final ModelRenderer glow__innards5;
	private final ModelRenderer undercurve5;
	private final ModelRenderer backbend5;
	private final ModelRenderer shin5;
	private final ModelRenderer bone34;
	private final ModelRenderer ribfoot5;
	private final ModelRenderer silverib6;
	private final ModelRenderer spine6;
	private final ModelRenderer plain6;
	private final ModelRenderer bevel6;
	private final ModelRenderer front6;
	private final ModelRenderer bone35;
	private final ModelRenderer ring13;
	private final ModelRenderer bone36;
	private final ModelRenderer bone37;
	private final ModelRenderer ring14;
	private final ModelRenderer bone38;
	private final ModelRenderer bone39;
	private final ModelRenderer glow__innards6;
	private final ModelRenderer undercurve6;
	private final ModelRenderer backbend6;
	private final ModelRenderer shin6;
	private final ModelRenderer bone40;
	private final ModelRenderer ribfoot6;
	private final ModelRenderer stations;
	private final ModelRenderer station1;
	private final ModelRenderer st_plain1;
	private final ModelRenderer st_bevel1;
	private final ModelRenderer st_front1;
	private final ModelRenderer rotorbevel;
	private final ModelRenderer ring4;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer ring3;
	private final ModelRenderer bone6;
	private final ModelRenderer bone7;
	private final ModelRenderer st_under1;
	private final ModelRenderer back1;
	private final ModelRenderer box1;
	private final ModelRenderer trim1;
	private final ModelRenderer station2;
	private final ModelRenderer st_plain2;
	private final ModelRenderer st_bevel2;
	private final ModelRenderer st_front2;
	private final ModelRenderer rotorbevel2;
	private final ModelRenderer ring15;
	private final ModelRenderer bone41;
	private final ModelRenderer bone42;
	private final ModelRenderer ring16;
	private final ModelRenderer bone43;
	private final ModelRenderer bone44;
	private final ModelRenderer st_under2;
	private final ModelRenderer back2;
	private final ModelRenderer box2;
	private final ModelRenderer trim2;
	private final ModelRenderer station3;
	private final ModelRenderer st_plain3;
	private final ModelRenderer st_bevel3;
	private final ModelRenderer st_front3;
	private final ModelRenderer rotorbevel3;
	private final ModelRenderer ring17;
	private final ModelRenderer bone45;
	private final ModelRenderer bone46;
	private final ModelRenderer ring18;
	private final ModelRenderer bone47;
	private final ModelRenderer bone48;
	private final ModelRenderer st_under3;
	private final ModelRenderer back3;
	private final ModelRenderer box3;
	private final ModelRenderer trim3;
	private final ModelRenderer station4;
	private final ModelRenderer st_plain4;
	private final ModelRenderer st_bevel4;
	private final ModelRenderer st_front4;
	private final ModelRenderer rotorbevel4;
	private final ModelRenderer ring19;
	private final ModelRenderer bone49;
	private final ModelRenderer bone50;
	private final ModelRenderer ring20;
	private final ModelRenderer bone51;
	private final ModelRenderer bone52;
	private final ModelRenderer st_under4;
	private final ModelRenderer back4;
	private final ModelRenderer box4;
	private final ModelRenderer trim4;
	private final ModelRenderer station5;
	private final ModelRenderer st_plain5;
	private final ModelRenderer st_bevel5;
	private final ModelRenderer st_front5;
	private final ModelRenderer rotorbevel5;
	private final ModelRenderer ring21;
	private final ModelRenderer bone53;
	private final ModelRenderer bone54;
	private final ModelRenderer ring22;
	private final ModelRenderer bone55;
	private final ModelRenderer bone56;
	private final ModelRenderer st_under5;
	private final ModelRenderer back5;
	private final ModelRenderer box5;
	private final ModelRenderer trim5;
	private final ModelRenderer station6;
	private final ModelRenderer st_plain6;
	private final ModelRenderer st_bevel6;
	private final ModelRenderer st_front6;
	private final ModelRenderer rotorbevel6;
	private final ModelRenderer ring23;
	private final ModelRenderer bone57;
	private final ModelRenderer bone58;
	private final ModelRenderer ring24;
	private final ModelRenderer bone59;
	private final ModelRenderer bone60;
	private final ModelRenderer st_under6;
	private final ModelRenderer back6;
	private final ModelRenderer box6;
	private final ModelRenderer trim6;
	private final ModelRenderer controls;
	private final ModelRenderer controls1;
	private final ModelRenderer base_plate_1;
	private final ModelRenderer turn_dial;
	private final ModelRenderer guide;
	private final ModelRenderer knob4;
	private final ModelRenderer buttonstrip;
	private final ModelRenderer buttons_lit;
	private final ModelRenderer small_screen;
	private final ModelRenderer four_square;
	private final ModelRenderer indicator;
	private final ModelRenderer indicator2;
	private final ModelRenderer indicator3;
	private final ModelRenderer coms;
	private final ModelRenderer com_needle_rotate_z;
	private final ModelRenderer fast_return_rotate_x;
	private final ModelRenderer bone68;
	private final ModelRenderer controls2;
	private final ModelRenderer base_plate_2;
	private final ModelRenderer lever;
	private final ModelRenderer lever_turn;
	private final ModelRenderer lever2;
	private final ModelRenderer door_turn;
	private final ModelRenderer wide_strip;
	private final ModelRenderer sonic_port;
	private final ModelRenderer controls3;
	private final ModelRenderer base_plate_3;
	private final ModelRenderer facing;
	private final ModelRenderer joystick;
	private final ModelRenderer base;
	private final ModelRenderer pull_tab;
	private final ModelRenderer pull_tab2;
	private final ModelRenderer pull_tab3;
	private final ModelRenderer randomizers;
	private final ModelRenderer key_white1;
	private final ModelRenderer key_black1;
	private final ModelRenderer key_white2;
	private final ModelRenderer key_black2;
	private final ModelRenderer key_white3;
	private final ModelRenderer key_white4;
	private final ModelRenderer key_black3;
	private final ModelRenderer key_white5;
	private final ModelRenderer controls4;
	private final ModelRenderer base_plate_4;
	private final ModelRenderer coords_z;
	private final ModelRenderer toggle_base;
	private final ModelRenderer z_cord_rotate_x;
	private final ModelRenderer coords_y;
	private final ModelRenderer toggle_base2;
	private final ModelRenderer y_cord_rotate_y;
	private final ModelRenderer coords_x;
	private final ModelRenderer toggle_base3;
	private final ModelRenderer z_cord_rotate_x2;
	private final ModelRenderer radar;
	private final ModelRenderer bigslider;
	private final ModelRenderer inc_slider_rotate_x;
	private final ModelRenderer slider_knob;
	private final ModelRenderer twist;
	private final ModelRenderer track;
	private final ModelRenderer button_array;
	private final ModelRenderer base_plate;
	private final ModelRenderer controls5;
	private final ModelRenderer base_plate_5;
	private final ModelRenderer landing_type_rotate_x;
	private final ModelRenderer knob;
	private final ModelRenderer angeled_panel;
	private final ModelRenderer dummy_buttons;
	private final ModelRenderer dark_buttons;
	private final LightModelRenderer glow_warning_lamp;
	private final LightModelRenderer glow_blue2;
	private final LightModelRenderer glow_white;
	private final LightModelRenderer glow_yellow;
	private final ModelRenderer dummy_buttons7;
	private final ModelRenderer dummy_buttons8;
	private final ModelRenderer bone67;
	private final ModelRenderer panels;
	private final LightModelRenderer glow_panels;
	private final ModelRenderer obtuse_panel;
	private final ModelRenderer refuler;
	private final ModelRenderer needle;
	private final ModelRenderer trim;
	private final ModelRenderer glass;
	private final ModelRenderer controls6;
	private final ModelRenderer base_plate_6;
	private final ModelRenderer bone69;
	private final ModelRenderer dummy_buttons9;
	private final ModelRenderer dial;
	private final ModelRenderer shield_health;
	private final ModelRenderer dial_trim;
	private final ModelRenderer dial_glass;
	private final ModelRenderer dial2;
	private final ModelRenderer dial_trim2;
	private final ModelRenderer dial_glass2;
	private final ModelRenderer flight_time;
	private final ModelRenderer slider_frames;
	private final ModelRenderer stablizers;
	private final ModelRenderer slider_left_rotate_x;
	private final ModelRenderer slider_right_rotate_x;
	private final ModelRenderer rib_control;
	private final ModelRenderer spine_con1;
	private final ModelRenderer plain7;
	private final ModelRenderer bar;
	private final ModelRenderer handbrake_x;
	private final ModelRenderer bevel7;
	private final ModelRenderer spine_con2;
	private final ModelRenderer plain8;
	private final ModelRenderer winch;
	private final ModelRenderer casing;
	private final ModelRenderer throttle_x;
	private final ModelRenderer bevel8;
	private final ModelRenderer spine_con3;
	private final ModelRenderer plain9;
	private final ModelRenderer telepathic2;
	private final ModelRenderer microscope;
	private final ModelRenderer center;
	private final ModelRenderer bottom;
	private final ModelRenderer innards;
	private final ModelRenderer top;

	public NeutronConsoleModel(Function<ResourceLocation, RenderType> function) {
		super(function);
		textureWidth = 256;
		textureHeight = 256;

		rotor = new ModelRenderer(this);
		rotor.setRotationPoint(0.0F, 34.0F, 0.0F);
		

		glow_crystal_rotate_y = new LightModelRenderer(this);
		glow_crystal_rotate_y.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor.addChild(glow_crystal_rotate_y);
		

		y45 = new ModelRenderer(this);
		y45.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_crystal_rotate_y.addChild(y45);
		setRotationAngle(y45, 0.0F, -0.7854F, 0.0F);
		y45.setTextureOffset(125, 1).addBox(-8.0F, -104.0F, -8.0F, 16.0F, 16.0F, 16.0F, 0.0F, false);

		z45 = new ModelRenderer(this);
		z45.setRotationPoint(0.0F, -96.0F, 0.0F);
		glow_crystal_rotate_y.addChild(z45);
		setRotationAngle(z45, 0.0F, 0.0F, 0.7854F);
		z45.setTextureOffset(125, 1).addBox(-8.0F, -8.0F, -8.0F, 16.0F, 16.0F, 16.0F, 0.0F, false);

		x45 = new ModelRenderer(this);
		x45.setRotationPoint(0.0F, -96.0F, 0.0F);
		glow_crystal_rotate_y.addChild(x45);
		setRotationAngle(x45, -0.7854F, 0.0F, 0.0F);
		x45.setTextureOffset(125, 1).addBox(-8.0F, -8.0F, -8.0F, 16.0F, 16.0F, 16.0F, 0.0F, false);

		glow_outer_rotate_x = new LightModelRenderer(this);
		glow_outer_rotate_x.setRotationPoint(0.0F, -97.0F, 0.0F);
		rotor.addChild(glow_outer_rotate_x);
		

		bone61 = new ModelRenderer(this);
		bone61.setRotationPoint(0.0934F, -0.0625F, -0.0611F);
		glow_outer_rotate_x.addChild(bone61);
		setRotationAngle(bone61, 0.0F, -0.5236F, 0.0F);
		bone61.setTextureOffset(210, 23).addBox(-10.0F, -1.5F, 16.3205F, 20.0F, 3.0F, 1.0F, 0.0F, false);
		bone61.setTextureOffset(210, 23).addBox(-10.0F, -1.5F, -17.3205F, 20.0F, 3.0F, 1.0F, 0.0F, false);

		bone62 = new ModelRenderer(this);
		bone62.setRotationPoint(-1.5684F, -0.0625F, 5.9867F);
		glow_outer_rotate_x.addChild(bone62);
		setRotationAngle(bone62, 0.0F, -1.5708F, 0.0F);
		bone62.setTextureOffset(210, 23).addBox(-16.0477F, -1.5F, 14.6587F, 20.0F, 3.0F, 1.0F, 0.0F, false);
		bone62.setTextureOffset(210, 23).addBox(-16.0477F, -1.5F, -18.9823F, 20.0F, 3.0F, 1.0F, 0.0F, false);

		bone63 = new ModelRenderer(this);
		bone63.setRotationPoint(-5.2957F, -0.0625F, 18.1362F);
		glow_outer_rotate_x.addChild(bone63);
		setRotationAngle(bone63, 0.0F, -2.618F, 0.0F);
		bone63.setTextureOffset(210, 23).addBox(-23.7658F, -1.5F, 29.3852F, 20.0F, 3.0F, 1.0F, 0.0F, false);
		bone63.setTextureOffset(210, 23).addBox(-23.7658F, -1.5F, -4.2558F, 20.0F, 3.0F, 1.0F, 0.0F, false);

		glow_inner_rotate_x = new LightModelRenderer(this);
		glow_inner_rotate_x.setRotationPoint(0.0F, -97.0F, 0.0F);
		rotor.addChild(glow_inner_rotate_x);
		

		bone64 = new ModelRenderer(this);
		bone64.setRotationPoint(-0.0925F, -0.3125F, -0.0206F);
		glow_inner_rotate_x.addChild(bone64);
		setRotationAngle(bone64, 0.0F, -0.5236F, 0.0F);
		bone64.setTextureOffset(210, 23).addBox(-9.0F, -2.5F, 14.5885F, 18.0F, 5.0F, 1.0F, 0.0F, false);
		bone64.setTextureOffset(210, 23).addBox(-9.0F, -2.5F, -15.5885F, 18.0F, 5.0F, 1.0F, 0.0F, false);

		bone65 = new ModelRenderer(this);
		bone65.setRotationPoint(-2.3141F, -0.3125F, 5.6948F);
		glow_inner_rotate_x.addChild(bone65);
		setRotationAngle(bone65, 0.0F, -1.5708F, 0.0F);
		bone65.setTextureOffset(210, 23).addBox(-14.7154F, -2.5F, 12.3668F, 18.0F, 5.0F, 1.0F, 0.0F, false);
		bone65.setTextureOffset(210, 23).addBox(-14.7154F, -2.5F, -17.8101F, 18.0F, 5.0F, 1.0F, 0.0F, false);

		bone66 = new ModelRenderer(this);
		bone66.setRotationPoint(-5.9332F, -0.3125F, 16.7198F);
		glow_inner_rotate_x.addChild(bone66);
		setRotationAngle(bone66, 0.0F, -2.618F, 0.0F);
		bone66.setTextureOffset(210, 23).addBox(-22.4284F, -2.5F, 26.1658F, 18.0F, 5.0F, 1.0F, 0.0F, false);
		bone66.setTextureOffset(210, 23).addBox(-22.4284F, -2.5F, -4.0111F, 18.0F, 5.0F, 1.0F, 0.0F, false);

		glow = new LightModelRenderer(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		glow_inner = new LightModelRenderer(this);
		glow_inner.setRotationPoint(-31.0F, 33.0F, -7.0F);
		glow.addChild(glow_inner);
		

		glow_trim = new LightModelRenderer(this);
		glow_trim.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim);
		

		grill_corner_light = new ModelRenderer(this);
		grill_corner_light.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim.addChild(grill_corner_light);
		setRotationAngle(grill_corner_light, -0.2182F, 0.0F, 0.0F);
		grill_corner_light.setTextureOffset(190, 22).addBox(-4.0F, 0.5188F, -26.2471F, 7.0F, 8.0F, 1.0F, 0.0F, false);

		gril_light = new ModelRenderer(this);
		gril_light.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim.addChild(gril_light);
		setRotationAngle(gril_light, -0.2618F, 0.5236F, 0.0F);
		gril_light.setTextureOffset(190, 21).addBox(-7.0F, -1.2227F, -23.3118F, 14.0F, 9.0F, 1.0F, 0.0F, false);

		glow_bar = new LightModelRenderer(this);
		glow_bar.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim.addChild(glow_bar);
		setRotationAngle(glow_bar, 0.0F, 0.5236F, 0.0F);
		glow_bar.setTextureOffset(198, 29).addBox(-12.0F, -3.7731F, -22.1137F, 24.0F, 1.0F, 1.0F, 0.0F, false);
		glow_bar.setTextureOffset(198, 29).addBox(-12.0F, -9.7731F, -19.6137F, 24.0F, 1.0F, 1.0F, 0.0F, false);

		glow_corner_fill = new ModelRenderer(this);
		glow_corner_fill.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim.addChild(glow_corner_fill);
		glow_corner_fill.setTextureOffset(208, 25).addBox(24.1556F, -119.1481F, -17.0123F, 14.0F, 2.0F, 1.0F, 0.0F, false);
		glow_corner_fill.setTextureOffset(200, 24).addBox(25.7091F, -125.1481F, -15.4142F, 11.0F, 2.0F, 2.0F, 0.0F, false);

		glow_trim2 = new ModelRenderer(this);
		glow_trim2.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim2);
		setRotationAngle(glow_trim2, 0.0F, -1.0472F, 0.0F);
		

		grill_corner_light2 = new ModelRenderer(this);
		grill_corner_light2.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim2.addChild(grill_corner_light2);
		setRotationAngle(grill_corner_light2, -0.2182F, 0.0F, 0.0F);
		grill_corner_light2.setTextureOffset(190, 22).addBox(-4.0F, 0.5188F, -26.2471F, 8.0F, 8.0F, 1.0F, 0.0F, false);

		gril_light2 = new ModelRenderer(this);
		gril_light2.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim2.addChild(gril_light2);
		setRotationAngle(gril_light2, -0.2618F, 0.5236F, 0.0F);
		gril_light2.setTextureOffset(190, 21).addBox(-7.0F, -1.2227F, -23.3118F, 15.0F, 9.0F, 1.0F, 0.0F, false);

		glow_bar2 = new ModelRenderer(this);
		glow_bar2.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim2.addChild(glow_bar2);
		setRotationAngle(glow_bar2, 0.0F, 0.5236F, 0.0F);
		glow_bar2.setTextureOffset(198, 29).addBox(-12.0F, -3.7731F, -22.1137F, 24.0F, 1.0F, 1.0F, 0.0F, false);
		glow_bar2.setTextureOffset(198, 29).addBox(-12.0F, -9.7731F, -19.6137F, 24.0F, 1.0F, 1.0F, 0.0F, false);

		glow_corner_fill2 = new ModelRenderer(this);
		glow_corner_fill2.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim2.addChild(glow_corner_fill2);
		glow_corner_fill2.setTextureOffset(208, 25).addBox(24.1556F, -119.1481F, -17.0123F, 14.0F, 2.0F, 1.0F, 0.0F, false);
		glow_corner_fill2.setTextureOffset(200, 24).addBox(25.7091F, -125.1481F, -15.4142F, 11.0F, 2.0F, 2.0F, 0.0F, false);

		glow_trim3 = new ModelRenderer(this);
		glow_trim3.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim3);
		setRotationAngle(glow_trim3, 0.0F, -2.0944F, 0.0F);
		

		grill_corner_light3 = new ModelRenderer(this);
		grill_corner_light3.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim3.addChild(grill_corner_light3);
		setRotationAngle(grill_corner_light3, -0.2182F, 0.0F, 0.0F);
		grill_corner_light3.setTextureOffset(190, 22).addBox(-4.0F, 0.5188F, -27.2471F, 7.0F, 8.0F, 1.0F, 0.0F, false);

		gril_light3 = new ModelRenderer(this);
		gril_light3.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim3.addChild(gril_light3);
		setRotationAngle(gril_light3, -0.2618F, 0.5236F, 0.0F);
		gril_light3.setTextureOffset(190, 21).addBox(-7.0F, -1.2227F, -24.3118F, 15.0F, 9.0F, 1.0F, 0.0F, false);

		glow_bar3 = new ModelRenderer(this);
		glow_bar3.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim3.addChild(glow_bar3);
		setRotationAngle(glow_bar3, 0.0F, 0.5236F, 0.0F);
		glow_bar3.setTextureOffset(198, 29).addBox(-12.0F, -3.7731F, -22.1137F, 24.0F, 1.0F, 1.0F, 0.0F, false);
		glow_bar3.setTextureOffset(198, 29).addBox(-12.0F, -9.7731F, -19.6137F, 24.0F, 1.0F, 1.0F, 0.0F, false);

		glow_corner_fill3 = new ModelRenderer(this);
		glow_corner_fill3.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim3.addChild(glow_corner_fill3);
		glow_corner_fill3.setTextureOffset(208, 25).addBox(24.1556F, -119.1481F, -17.0123F, 14.0F, 2.0F, 1.0F, 0.0F, false);
		glow_corner_fill3.setTextureOffset(200, 24).addBox(25.7091F, -125.1481F, -15.4142F, 11.0F, 2.0F, 2.0F, 0.0F, false);

		glow_trim4 = new ModelRenderer(this);
		glow_trim4.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim4);
		setRotationAngle(glow_trim4, 0.0F, 3.1416F, 0.0F);
		

		grill_corner_light4 = new ModelRenderer(this);
		grill_corner_light4.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim4.addChild(grill_corner_light4);
		setRotationAngle(grill_corner_light4, -0.2182F, 0.0F, 0.0F);
		grill_corner_light4.setTextureOffset(190, 22).addBox(-5.0F, 0.5188F, -27.2471F, 8.0F, 8.0F, 1.0F, 0.0F, false);

		gril_light4 = new ModelRenderer(this);
		gril_light4.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim4.addChild(gril_light4);
		setRotationAngle(gril_light4, -0.2618F, 0.5236F, 0.0F);
		gril_light4.setTextureOffset(190, 21).addBox(-8.0F, -1.2227F, -23.3118F, 15.0F, 9.0F, 1.0F, 0.0F, false);

		glow_bar4 = new ModelRenderer(this);
		glow_bar4.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim4.addChild(glow_bar4);
		setRotationAngle(glow_bar4, 0.0F, 0.5236F, 0.0F);
		glow_bar4.setTextureOffset(198, 29).addBox(-12.0F, -3.7731F, -22.1137F, 24.0F, 1.0F, 1.0F, 0.0F, false);
		glow_bar4.setTextureOffset(198, 29).addBox(-12.0F, -9.7731F, -19.6137F, 24.0F, 1.0F, 1.0F, 0.0F, false);

		glow_corner_fill4 = new ModelRenderer(this);
		glow_corner_fill4.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim4.addChild(glow_corner_fill4);
		glow_corner_fill4.setTextureOffset(208, 25).addBox(24.1556F, -119.1481F, -17.0123F, 14.0F, 2.0F, 1.0F, 0.0F, false);
		glow_corner_fill4.setTextureOffset(200, 24).addBox(25.7091F, -125.1481F, -15.4142F, 11.0F, 2.0F, 2.0F, 0.0F, false);

		glow_trim5 = new ModelRenderer(this);
		glow_trim5.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim5);
		setRotationAngle(glow_trim5, 0.0F, 2.0944F, 0.0F);
		

		grill_corner_light5 = new ModelRenderer(this);
		grill_corner_light5.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim5.addChild(grill_corner_light5);
		setRotationAngle(grill_corner_light5, -0.2182F, 0.0F, 0.0F);
		grill_corner_light5.setTextureOffset(190, 22).addBox(-5.0F, 0.5188F, -27.2471F, 8.0F, 8.0F, 1.0F, 0.0F, false);

		gril_light5 = new ModelRenderer(this);
		gril_light5.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim5.addChild(gril_light5);
		setRotationAngle(gril_light5, -0.2618F, 0.5236F, 0.0F);
		gril_light5.setTextureOffset(190, 21).addBox(-7.0F, -1.2227F, -24.3118F, 14.0F, 9.0F, 1.0F, 0.0F, false);

		glow_bar5 = new ModelRenderer(this);
		glow_bar5.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim5.addChild(glow_bar5);
		setRotationAngle(glow_bar5, 0.0F, 0.5236F, 0.0F);
		glow_bar5.setTextureOffset(198, 29).addBox(-12.0F, -3.7731F, -22.1137F, 24.0F, 1.0F, 1.0F, 0.0F, false);
		glow_bar5.setTextureOffset(198, 29).addBox(-12.0F, -9.7731F, -19.6137F, 24.0F, 1.0F, 1.0F, 0.0F, false);

		glow_corner_fill5 = new ModelRenderer(this);
		glow_corner_fill5.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim5.addChild(glow_corner_fill5);
		glow_corner_fill5.setTextureOffset(208, 25).addBox(24.1556F, -119.1481F, -17.0123F, 14.0F, 2.0F, 1.0F, 0.0F, false);
		glow_corner_fill5.setTextureOffset(200, 24).addBox(25.7091F, -125.1481F, -15.4142F, 11.0F, 2.0F, 2.0F, 0.0F, false);

		glow_trim6 = new ModelRenderer(this);
		glow_trim6.setRotationPoint(30.8125F, -109.0F, 6.75F);
		glow_inner.addChild(glow_trim6);
		setRotationAngle(glow_trim6, 0.0F, 1.0472F, 0.0F);
		

		grill_corner_light6 = new ModelRenderer(this);
		grill_corner_light6.setRotationPoint(0.6563F, 6.7081F, 1.8448F);
		glow_trim6.addChild(grill_corner_light6);
		setRotationAngle(grill_corner_light6, -0.2182F, 0.0F, 0.0F);
		grill_corner_light6.setTextureOffset(190, 22).addBox(-5.0F, 0.5188F, -27.2471F, 8.0F, 8.0F, 1.0F, 0.0F, false);

		gril_light6 = new ModelRenderer(this);
		gril_light6.setRotationPoint(0.1875F, 8.0F, 0.25F);
		glow_trim6.addChild(gril_light6);
		setRotationAngle(gril_light6, -0.2618F, 0.5236F, 0.0F);
		gril_light6.setTextureOffset(190, 21).addBox(-7.0F, -1.2227F, -24.3118F, 14.0F, 9.0F, 1.0F, 0.0F, false);

		glow_bar6 = new ModelRenderer(this);
		glow_bar6.setRotationPoint(0.1875F, 0.0F, -0.75F);
		glow_trim6.addChild(glow_bar6);
		setRotationAngle(glow_bar6, 0.0F, 0.5236F, 0.0F);
		glow_bar6.setTextureOffset(198, 29).addBox(-12.0F, -3.7731F, -22.1137F, 24.0F, 1.0F, 1.0F, 0.0F, false);
		glow_bar6.setTextureOffset(198, 29).addBox(-12.0F, -9.7731F, -19.6137F, 24.0F, 1.0F, 1.0F, 0.0F, false);

		glow_corner_fill6 = new ModelRenderer(this);
		glow_corner_fill6.setRotationPoint(-31.0F, 115.0F, -9.0F);
		glow_trim6.addChild(glow_corner_fill6);
		glow_corner_fill6.setTextureOffset(208, 25).addBox(24.1556F, -119.1481F, -17.0123F, 14.0F, 2.0F, 1.0F, 0.0F, false);
		glow_corner_fill6.setTextureOffset(200, 24).addBox(25.7091F, -125.1481F, -15.4142F, 11.0F, 2.0F, 2.0F, 0.0F, false);

		glow_posts = new LightModelRenderer(this);
		glow_posts.setRotationPoint(0.0F, -1.9685F, 0.0246F);
		glow.addChild(glow_posts);
		

		basepost1 = new ModelRenderer(this);
		basepost1.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost1);
		basepost1.setTextureOffset(190, 4).addBox(-3.0F, -23.0F, -26.25F, 6.0F, 19.0F, 6.0F, 0.0F, false);

		basepost2 = new ModelRenderer(this);
		basepost2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost2);
		setRotationAngle(basepost2, 0.0F, -1.0472F, 0.0F);
		basepost2.setTextureOffset(190, 4).addBox(-3.0F, -23.0F, -26.25F, 6.0F, 19.0F, 6.0F, 0.0F, false);

		basepost3 = new ModelRenderer(this);
		basepost3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost3);
		setRotationAngle(basepost3, 0.0F, -2.0944F, 0.0F);
		basepost3.setTextureOffset(190, 4).addBox(-3.0F, -23.0F, -26.25F, 6.0F, 19.0F, 6.0F, 0.0F, false);

		basepost4 = new ModelRenderer(this);
		basepost4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost4);
		setRotationAngle(basepost4, 0.0F, 1.0472F, 0.0F);
		basepost4.setTextureOffset(190, 4).addBox(-3.0F, -23.0F, -26.25F, 6.0F, 19.0F, 6.0F, 0.0F, false);

		basepost5 = new ModelRenderer(this);
		basepost5.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost5);
		setRotationAngle(basepost5, 0.0F, 2.0944F, 0.0F);
		basepost5.setTextureOffset(190, 4).addBox(-3.0F, -23.0F, -26.25F, 6.0F, 19.0F, 6.0F, 0.0F, false);

		basepost6 = new ModelRenderer(this);
		basepost6.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_posts.addChild(basepost6);
		setRotationAngle(basepost6, 0.0F, 3.1416F, 0.0F);
		basepost6.setTextureOffset(190, 4).addBox(-3.0F, -23.0F, -26.25F, 6.0F, 19.0F, 6.0F, 0.0F, false);

		glow_controls = new ModelRenderer(this);
		glow_controls.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_controls);
		setRotationAngle(glow_controls, 0.0F, -0.5236F, 0.0F);
		

		controls8 = new LightModelRenderer(this);
		controls8.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls8);
		setRotationAngle(controls8, 0.0F, -0.5236F, 0.0F);
		

		base_plate_7 = new ModelRenderer(this);
		base_plate_7.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls8.addChild(base_plate_7);
		setRotationAngle(base_plate_7, -1.309F, 0.0F, 0.0F);
		

		buttonstrip2 = new ModelRenderer(this);
		buttonstrip2.setRotationPoint(-32.0F, 93.3792F, 56.7684F);
		base_plate_7.addChild(buttonstrip2);
		

		buttons_lit2 = new ModelRenderer(this);
		buttons_lit2.setRotationPoint(0.0F, 0.0F, 0.0F);
		buttonstrip2.addChild(buttons_lit2);
		buttons_lit2.setTextureOffset(164, 80).addBox(15.5F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(33.5F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(24.5F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(42.5F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(20.0F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(38.0F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(29.0F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(47.0F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(17.75F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(35.75F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(26.75F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(44.75F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(22.25F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(40.25F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		buttons_lit2.setTextureOffset(164, 80).addBox(31.25F, -87.2042F, -56.6047F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		small_screen2 = new ModelRenderer(this);
		small_screen2.setRotationPoint(9.0F, 0.1749F, -2.3363F);
		base_plate_7.addChild(small_screen2);
		setRotationAngle(small_screen2, 0.6109F, 0.0F, 0.0F);
		small_screen2.setTextureOffset(208, 23).addBox(-2.2188F, -3.0F, 0.4375F, 6.0F, 6.0F, 1.0F, 0.0F, false);

		indicator4 = new ModelRenderer(this);
		indicator4.setRotationPoint(-7.25F, 2.7999F, 0.5387F);
		base_plate_7.addChild(indicator4);
		setRotationAngle(indicator4, -1.0472F, 0.0F, 0.0F);
		indicator4.setTextureOffset(167, 78).addBox(-1.0F, -0.625F, -0.875F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		indicator5 = new ModelRenderer(this);
		indicator5.setRotationPoint(-2.25F, 2.7999F, 0.5387F);
		base_plate_7.addChild(indicator5);
		setRotationAngle(indicator5, -1.0472F, 0.0F, 0.0F);
		indicator5.setTextureOffset(167, 78).addBox(-1.0F, -0.625F, -0.875F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		indicator6 = new ModelRenderer(this);
		indicator6.setRotationPoint(2.75F, 2.7999F, 0.5387F);
		base_plate_7.addChild(indicator6);
		setRotationAngle(indicator6, -1.0472F, 0.0F, 0.0F);
		indicator6.setTextureOffset(167, 78).addBox(-1.0F, -0.625F, -0.875F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		coms2 = new ModelRenderer(this);
		coms2.setRotationPoint(0.0F, -13.0751F, -0.0863F);
		base_plate_7.addChild(coms2);
		setRotationAngle(coms2, -0.7854F, 0.0F, 0.0F);
		coms2.setTextureOffset(164, 76).addBox(-5.5F, -1.5F, -1.5F, 11.0F, 3.0F, 3.0F, 0.0F, false);

		controls9 = new LightModelRenderer(this);
		controls9.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls9);
		setRotationAngle(controls9, 0.0F, -1.5708F, 0.0F);
		

		base_plate_8 = new ModelRenderer(this);
		base_plate_8.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls9.addChild(base_plate_8);
		setRotationAngle(base_plate_8, -1.309F, 0.0F, 0.0F);
		

		dummy_buttons13 = new ModelRenderer(this);
		dummy_buttons13.setRotationPoint(6.0F, -11.0751F, 0.5387F);
		base_plate_8.addChild(dummy_buttons13);
		dummy_buttons13.setTextureOffset(167, 76).addBox(-1.0F, -4.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons13.setTextureOffset(167, 76).addBox(-1.0F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons13.setTextureOffset(167, 76).addBox(-1.0F, 2.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);

		dummy_buttons14 = new ModelRenderer(this);
		dummy_buttons14.setRotationPoint(-6.0F, -11.0751F, 0.5387F);
		base_plate_8.addChild(dummy_buttons14);
		dummy_buttons14.setTextureOffset(167, 76).addBox(-1.0F, -4.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons14.setTextureOffset(167, 76).addBox(-1.0F, -1.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons14.setTextureOffset(167, 76).addBox(-1.0F, 2.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);

		dummy_buttons15 = new ModelRenderer(this);
		dummy_buttons15.setRotationPoint(-4.0F, 8.9249F, -0.2113F);
		base_plate_8.addChild(dummy_buttons15);
		dummy_buttons15.setTextureOffset(167, 76).addBox(-0.6875F, -4.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons15.setTextureOffset(167, 76).addBox(1.7188F, -4.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons15.setTextureOffset(167, 76).addBox(4.0938F, -4.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		dummy_buttons15.setTextureOffset(167, 76).addBox(6.4375F, -4.0F, -1.5F, 2.0F, 2.0F, 3.0F, 0.0F, false);

		controls10 = new LightModelRenderer(this);
		controls10.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls10);
		setRotationAngle(controls10, 0.0F, -2.618F, 0.0F);
		

		base_plate_9 = new ModelRenderer(this);
		base_plate_9.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls10.addChild(base_plate_9);
		setRotationAngle(base_plate_9, -1.309F, 0.0F, 0.0F);
		

		base2 = new ModelRenderer(this);
		base2.setRotationPoint(1.1071F, -0.2894F, 0.2976F);
		base_plate_9.addChild(base2);
		base2.setTextureOffset(199, 20).addBox(-5.6071F, -11.0357F, -1.1339F, 9.0F, 9.0F, 1.0F, 0.0F, false);
		base2.setTextureOffset(164, 73).addBox(-11.1071F, 4.9643F, -1.1339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base2.setTextureOffset(164, 73).addBox(-11.1071F, 0.9643F, -1.1339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base2.setTextureOffset(165, 74).addBox(5.8929F, 4.9643F, -1.1339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base2.setTextureOffset(165, 74).addBox(5.8929F, 0.9643F, -1.1339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base2.setTextureOffset(165, 74).addBox(5.8929F, -3.0357F, -1.1339F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		controls11 = new ModelRenderer(this);
		controls11.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls11);
		setRotationAngle(controls11, 0.0F, 2.618F, 0.0F);
		

		base_plate_10 = new ModelRenderer(this);
		base_plate_10.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls11.addChild(base_plate_10);
		setRotationAngle(base_plate_10, -1.309F, 0.0F, 0.0F);
		

		coords_z2 = new ModelRenderer(this);
		coords_z2.setRotationPoint(-12.5F, 6.3792F, 0.7684F);
		base_plate_10.addChild(coords_z2);
		

		toggle_base4 = new ModelRenderer(this);
		toggle_base4.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_z2.addChild(toggle_base4);
		toggle_base4.setTextureOffset(106, 10).addBox(34.0F, -86.4542F, -57.6047F, 4.0F, 3.0F, 1.0F, 0.0F, false);

		glow_z_coord_blue = new LightModelRenderer(this);
		glow_z_coord_blue.setRotationPoint(0.0F, 0.0F, 0.0F);
		toggle_base4.addChild(glow_z_coord_blue);
		glow_z_coord_blue.setTextureOffset(129, 56).addBox(32.0F, -86.4542F, -57.6047F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		coords_y2 = new ModelRenderer(this);
		coords_y2.setRotationPoint(-12.5F, 0.3792F, 0.7684F);
		base_plate_10.addChild(coords_y2);
		

		toggle_base5 = new ModelRenderer(this);
		toggle_base5.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_y2.addChild(toggle_base5);
		toggle_base5.setTextureOffset(106, 10).addBox(34.0F, -86.4542F, -57.6047F, 4.0F, 3.0F, 1.0F, 0.0F, false);

		glow_y_cord_green = new LightModelRenderer(this);
		glow_y_cord_green.setRotationPoint(0.0F, 0.0F, 0.0F);
		toggle_base5.addChild(glow_y_cord_green);
		glow_y_cord_green.setTextureOffset(145, 73).addBox(32.0F, -86.4542F, -57.6047F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		toggle5 = new ModelRenderer(this);
		toggle5.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_y2.addChild(toggle5);
		setRotationAngle(toggle5, 0.2182F, 0.0F, 0.0F);
		

		coords_x2 = new ModelRenderer(this);
		coords_x2.setRotationPoint(-12.5F, -5.6208F, 0.7684F);
		base_plate_10.addChild(coords_x2);
		

		toggle_base6 = new ModelRenderer(this);
		toggle_base6.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_x2.addChild(toggle_base6);
		toggle_base6.setTextureOffset(106, 10).addBox(34.0F, -86.4542F, -57.6047F, 4.0F, 3.0F, 1.0F, 0.0F, false);

		glow_x_cord_red = new LightModelRenderer(this);
		glow_x_cord_red.setRotationPoint(0.0F, 0.0F, 0.0F);
		toggle_base6.addChild(glow_x_cord_red);
		glow_x_cord_red.setTextureOffset(129, 39).addBox(32.0F, -86.4542F, -57.6047F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		radar2_glow = new LightModelRenderer(this);
		radar2_glow.setRotationPoint(9.5F, 2.4249F, 0.2887F);
		base_plate_10.addChild(radar2_glow);
		radar2_glow.setTextureOffset(146, 72).addBox(-5.5F, -2.5F, -0.5F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		radar2_glow.setTextureOffset(146, 72).addBox(4.5F, -2.5F, -0.5F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		radar2_glow.setTextureOffset(146, 72).addBox(-4.5F, -4.5F, -0.5F, 2.0F, 9.0F, 1.0F, 0.0F, false);
		radar2_glow.setTextureOffset(146, 72).addBox(2.5F, -4.5F, -0.5F, 2.0F, 9.0F, 1.0F, 0.0F, false);
		radar2_glow.setTextureOffset(146, 72).addBox(-2.5F, -5.5F, -0.5F, 5.0F, 11.0F, 1.0F, 0.0F, false);

		bigslider2 = new ModelRenderer(this);
		bigslider2.setRotationPoint(-0.5F, 0.4898F, -0.2209F);
		base_plate_10.addChild(bigslider2);
		

		button_array2 = new LightModelRenderer(this);
		button_array2.setRotationPoint(-31.75F, 91.3792F, 56.7684F);
		base_plate_10.addChild(button_array2);
		

		buttons2 = new ModelRenderer(this);
		buttons2.setRotationPoint(0.0F, 0.0F, -0.25F);
		button_array2.addChild(buttons2);
		buttons2.setTextureOffset(165, 76).addBox(24.0F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(28.5F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(33.0F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(26.25F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(30.75F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(35.25F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(37.5F, -107.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(24.0F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(28.5F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(33.0F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(26.25F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(30.75F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(35.25F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(37.5F, -105.4542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(24.0F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(28.5F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(33.0F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(26.25F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(30.75F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(35.25F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		buttons2.setTextureOffset(165, 76).addBox(37.5F, -102.9542F, -56.3547F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		controls12 = new LightModelRenderer(this);
		controls12.setRotationPoint(0.0F, 0.0F, -0.75F);
		glow_controls.addChild(controls12);
		setRotationAngle(controls12, 0.0F, 1.5708F, 0.0F);
		

		base_plate_11 = new ModelRenderer(this);
		base_plate_11.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls12.addChild(base_plate_11);
		setRotationAngle(base_plate_11, -1.309F, 0.0F, 0.0F);
		

		angeled_panel2 = new ModelRenderer(this);
		angeled_panel2.setRotationPoint(-32.0F, 92.8792F, 58.4871F);
		base_plate_11.addChild(angeled_panel2);
		

		dummy_buttons19 = new ModelRenderer(this);
		dummy_buttons19.setRotationPoint(12.5F, -22.75F, -1.25F);
		angeled_panel2.addChild(dummy_buttons19);
		dummy_buttons19.setTextureOffset(166, 77).addBox(13.0F, -82.2042F, -57.6047F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons19.setTextureOffset(148, 75).addBox(17.0F, -82.2042F, -57.6047F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons19.setTextureOffset(166, 77).addBox(19.0F, -82.2042F, -57.6047F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons19.setTextureOffset(166, 77).addBox(23.0F, -82.2042F, -57.6047F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		refuler2 = new ModelRenderer(this);
		refuler2.setRotationPoint(-44.25F, 92.1292F, 58.3934F);
		base_plate_11.addChild(refuler2);
		

		glass2 = new ModelRenderer(this);
		glass2.setRotationPoint(0.0F, 0.0F, -0.25F);
		refuler2.addChild(glass2);
		glass2.setTextureOffset(164, 75).addBox(36.25F, -90.4542F, -57.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		glass2.setTextureOffset(164, 75).addBox(44.25F, -90.4542F, -57.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		glass2.setTextureOffset(164, 75).addBox(37.25F, -92.4542F, -57.6047F, 2.0F, 5.0F, 1.0F, 0.0F, false);
		glass2.setTextureOffset(164, 75).addBox(42.25F, -92.4542F, -57.6047F, 2.0F, 5.0F, 1.0F, 0.0F, false);
		glass2.setTextureOffset(164, 75).addBox(39.25F, -93.4542F, -57.6047F, 3.0F, 6.0F, 1.0F, 0.0F, false);

		controls13 = new LightModelRenderer(this);
		controls13.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(controls13);
		setRotationAngle(controls13, 0.0F, 0.5236F, 0.0F);
		

		base_plate_12 = new ModelRenderer(this);
		base_plate_12.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls13.addChild(base_plate_12);
		setRotationAngle(base_plate_12, -1.309F, 0.0F, 0.0F);
		

		dummy_buttons24 = new ModelRenderer(this);
		dummy_buttons24.setRotationPoint(-15.0F, 90.5979F, 56.5184F);
		base_plate_12.addChild(dummy_buttons24);
		dummy_buttons24.setTextureOffset(147, 77).addBox(14.0F, -101.4542F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons24.setTextureOffset(147, 77).addBox(14.0F, -104.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons24.setTextureOffset(168, 76).addBox(11.5F, -102.9542F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons24.setTextureOffset(166, 74).addBox(16.5F, -105.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons24.setTextureOffset(168, 77).addBox(20.5F, -98.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons24.setTextureOffset(167, 78).addBox(7.5F, -95.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons24.setTextureOffset(166, 75).addBox(4.5F, -95.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		dial3 = new ModelRenderer(this);
		dial3.setRotationPoint(-14.0F, 3.4898F, -3.2209F);
		base_plate_12.addChild(dial3);
		

		dial_glass3 = new ModelRenderer(this);
		dial_glass3.setRotationPoint(3.5F, -0.6106F, 4.1143F);
		dial3.addChild(dial_glass3);
		dial_glass3.setTextureOffset(165, 75).addBox(-2.0F, -2.4542F, -1.6047F, 3.0F, 5.0F, 1.0F, 0.0F, false);
		dial_glass3.setTextureOffset(165, 75).addBox(1.0F, -1.4542F, -1.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		dial_glass3.setTextureOffset(165, 75).addBox(-3.0F, -1.4542F, -1.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		dial_glass3.setTextureOffset(165, 75).addBox(2.0F, -0.4542F, -1.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		dial_glass3.setTextureOffset(165, 75).addBox(-4.0F, -0.4542F, -1.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		dial4 = new ModelRenderer(this);
		dial4.setRotationPoint(11.0F, 4.4898F, -0.2209F);
		base_plate_12.addChild(dial4);
		

		dial_glass4 = new ModelRenderer(this);
		dial_glass4.setRotationPoint(-22.5F, 87.3894F, 56.1143F);
		dial4.addChild(dial_glass4);
		dial_glass4.setTextureOffset(165, 74).addBox(21.0F, -91.4542F, -56.6047F, 3.0F, 5.0F, 1.0F, 0.0F, false);
		dial_glass4.setTextureOffset(165, 74).addBox(24.0F, -90.4542F, -56.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		dial_glass4.setTextureOffset(165, 74).addBox(20.0F, -90.4542F, -56.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		dial_glass4.setTextureOffset(165, 74).addBox(25.0F, -89.4542F, -56.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		dial_glass4.setTextureOffset(165, 74).addBox(19.0F, -89.4542F, -56.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		rib_control2 = new LightModelRenderer(this);
		rib_control2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_controls.addChild(rib_control2);
		setRotationAngle(rib_control2, 0.0F, -0.5236F, 0.0F);
		

		spine_con4 = new ModelRenderer(this);
		spine_con4.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control2.addChild(spine_con4);
		setRotationAngle(spine_con4, 0.0F, -0.5236F, 0.0F);
		

		plain10 = new ModelRenderer(this);
		plain10.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con4.addChild(plain10);
		setRotationAngle(plain10, -1.309F, 0.0F, 0.0F);
		plain10.setTextureOffset(164, 70).addBox(-2.5F, -13.0751F, -2.8363F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		plain10.setTextureOffset(164, 70).addBox(-2.5F, -16.0751F, -2.8363F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		plain10.setTextureOffset(164, 70).addBox(-2.5F, -19.0751F, -2.8363F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		plain10.setTextureOffset(164, 70).addBox(0.5F, -13.0751F, -2.8363F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		plain10.setTextureOffset(164, 70).addBox(0.5F, -16.0751F, -2.8363F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		plain10.setTextureOffset(164, 70).addBox(0.5F, -19.0751F, -2.8363F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		spine_con6 = new ModelRenderer(this);
		spine_con6.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control2.addChild(spine_con6);
		setRotationAngle(spine_con6, 0.0F, 1.5708F, 0.0F);
		

		plain12 = new ModelRenderer(this);
		plain12.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con6.addChild(plain12);
		setRotationAngle(plain12, -1.309F, 0.0F, 0.0F);
		

		telepathic3 = new ModelRenderer(this);
		telepathic3.setRotationPoint(-31.5F, 90.6292F, 59.7684F);
		plain12.addChild(telepathic3);
		

		microscope2 = new ModelRenderer(this);
		microscope2.setRotationPoint(31.5F, -77.7189F, -56.9429F);
		telepathic3.addChild(microscope2);
		setRotationAngle(microscope2, 0.5236F, 0.0F, 0.0F);
		microscope2.setTextureOffset(163, 72).addBox(-2.0F, -6.6013F, -3.9118F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		microscope2.setTextureOffset(110, 5).addBox(-1.0F, -5.6013F, -7.4118F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		microscope2.setTextureOffset(148, 59).addBox(-1.5F, -6.1013F, -20.9118F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		spines = new ModelRenderer(this);
		spines.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		silverib1 = new ModelRenderer(this);
		silverib1.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib1);
		setRotationAngle(silverib1, 0.0F, -1.0472F, 0.0F);
		

		spine1 = new ModelRenderer(this);
		spine1.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib1.addChild(spine1);
		setRotationAngle(spine1, 0.0F, -0.5236F, 0.0F);
		

		plain1 = new ModelRenderer(this);
		plain1.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine1.addChild(plain1);
		setRotationAngle(plain1, -1.309F, 0.0F, 0.0F);
		plain1.setTextureOffset(1, 134).addBox(-10.0F, -7.0751F, -0.5863F, 20.0F, 24.0F, 2.0F, 0.0F, false);
		plain1.setTextureOffset(1, 133).addBox(-10.0F, -24.0751F, -0.5863F, 20.0F, 17.0F, 2.0F, 0.0F, false);
		plain1.setTextureOffset(67, 135).addBox(-4.5F, -24.0751F, -2.5863F, 9.0F, 14.0F, 2.0F, 0.0F, false);
		plain1.setTextureOffset(66, 85).addBox(7.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);
		plain1.setTextureOffset(73, 81).addBox(-8.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);

		bevel1 = new ModelRenderer(this);
		bevel1.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine1.addChild(bevel1);
		setRotationAngle(bevel1, -0.6981F, 0.0F, 0.0F);
		bevel1.setTextureOffset(1, 158).addBox(-10.0F, -2.6563F, -1.5652F, 20.0F, 12.0F, 2.0F, 0.0F, false);
		bevel1.setTextureOffset(67, 155).addBox(-4.5F, -25.9206F, 12.283F, 9.0F, 4.0F, 2.0F, 0.0F, false);
		bevel1.setTextureOffset(69, 102).addBox(7.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);
		bevel1.setTextureOffset(40, 109).addBox(-8.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);

		front1 = new ModelRenderer(this);
		front1.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine1.addChild(front1);
		front1.setTextureOffset(45, 136).addBox(-10.0F, 3.5169F, -2.9925F, 20.0F, 8.0F, 2.0F, 0.0F, false);
		front1.setTextureOffset(68, 108).addBox(7.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		front1.setTextureOffset(44, 103).addBox(-8.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front1.addChild(bone);
		setRotationAngle(bone, -0.2618F, 0.0F, 0.0F);
		bone.setTextureOffset(49, 169).addBox(-10.0F, 10.5419F, 4.3666F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(49, 169).addBox(-7.0F, 8.0504F, 4.3019F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(49, 169).addBox(-7.0F, 5.2834F, 4.4313F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(49, 169).addBox(-7.0F, 2.5419F, 4.3666F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(49, 169).addBox(-11.0F, 0.5419F, 4.3666F, 22.0F, 2.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(50, 162).addBox(-10.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);
		bone.setTextureOffset(49, 159).addBox(7.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		ring = new ModelRenderer(this);
		ring.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front1.addChild(ring);
		ring.setTextureOffset(14, 195).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring.addChild(bone2);
		setRotationAngle(bone2, -0.2618F, 0.0F, 0.0F);
		bone2.setTextureOffset(14, 195).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring.addChild(bone3);
		setRotationAngle(bone3, 0.2618F, 0.0F, 0.0F);
		bone3.setTextureOffset(14, 195).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		ring2 = new ModelRenderer(this);
		ring2.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front1.addChild(ring2);
		ring2.setTextureOffset(19, 179).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring2.addChild(bone4);
		setRotationAngle(bone4, -0.2618F, 0.0F, 0.0F);
		bone4.setTextureOffset(19, 179).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring2.addChild(bone5);
		setRotationAngle(bone5, 0.2618F, 0.0F, 0.0F);
		bone5.setTextureOffset(19, 179).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		glow__innards = new ModelRenderer(this);
		glow__innards.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring2.addChild(glow__innards);
		

		undercurve1 = new ModelRenderer(this);
		undercurve1.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine1.addChild(undercurve1);
		setRotationAngle(undercurve1, 1.309F, 0.0F, 0.0F);
		undercurve1.setTextureOffset(32, 142).addBox(0.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve1.setTextureOffset(32, 142).addBox(-10.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve1.setTextureOffset(65, 154).addBox(-10.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve1.setTextureOffset(65, 154).addBox(0.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve1.setTextureOffset(53, 86).addBox(7.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);
		undercurve1.setTextureOffset(38, 91).addBox(-8.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);

		backbend1 = new ModelRenderer(this);
		backbend1.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine1.addChild(backbend1);
		setRotationAngle(backbend1, 0.3491F, 0.0F, 0.0F);
		backbend1.setTextureOffset(23, 147).addBox(-9.0F, -3.8396F, 13.8373F, 18.0F, 26.0F, 3.0F, 0.0F, false);
		backbend1.setTextureOffset(111, 8).addBox(-4.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend1.setTextureOffset(111, 8).addBox(3.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend1.setTextureOffset(53, 90).addBox(7.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);
		backbend1.setTextureOffset(27, 87).addBox(-8.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);

		shin1 = new ModelRenderer(this);
		shin1.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine1.addChild(shin1);
		setRotationAngle(shin1, -0.2618F, 0.0F, 0.0F);
		

		bone16 = new ModelRenderer(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin1.addChild(bone16);
		

		ribfoot1 = new ModelRenderer(this);
		ribfoot1.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine1.addChild(ribfoot1);
		ribfoot1.setTextureOffset(50, 67).addBox(-9.5F, -5.0F, -39.0F, 19.0F, 5.0F, 10.0F, 0.0F, false);

		silverib2 = new ModelRenderer(this);
		silverib2.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib2);
		

		spine2 = new ModelRenderer(this);
		spine2.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib2.addChild(spine2);
		setRotationAngle(spine2, 0.0F, -0.5236F, 0.0F);
		

		plain2 = new ModelRenderer(this);
		plain2.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine2.addChild(plain2);
		setRotationAngle(plain2, -1.309F, 0.0F, 0.0F);
		plain2.setTextureOffset(1, 134).addBox(-10.0F, -7.0751F, -0.5863F, 20.0F, 24.0F, 2.0F, 0.0F, false);
		plain2.setTextureOffset(1, 133).addBox(-10.0F, -24.0751F, -0.5863F, 20.0F, 17.0F, 2.0F, 0.0F, false);
		plain2.setTextureOffset(67, 135).addBox(-4.5F, -24.0751F, -2.5863F, 9.0F, 14.0F, 2.0F, 0.0F, false);
		plain2.setTextureOffset(66, 85).addBox(7.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);
		plain2.setTextureOffset(73, 81).addBox(-8.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);

		bevel2 = new ModelRenderer(this);
		bevel2.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine2.addChild(bevel2);
		setRotationAngle(bevel2, -0.6981F, 0.0F, 0.0F);
		bevel2.setTextureOffset(1, 158).addBox(-10.0F, -2.6563F, -1.5652F, 20.0F, 12.0F, 2.0F, 0.0F, false);
		bevel2.setTextureOffset(67, 155).addBox(-4.5F, -25.9206F, 12.283F, 9.0F, 4.0F, 2.0F, 0.0F, false);
		bevel2.setTextureOffset(69, 102).addBox(7.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);
		bevel2.setTextureOffset(40, 109).addBox(-8.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);

		front2 = new ModelRenderer(this);
		front2.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine2.addChild(front2);
		front2.setTextureOffset(45, 136).addBox(-10.0F, 3.5169F, -2.9925F, 20.0F, 8.0F, 2.0F, 0.0F, false);
		front2.setTextureOffset(68, 108).addBox(7.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		front2.setTextureOffset(44, 103).addBox(-8.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front2.addChild(bone8);
		setRotationAngle(bone8, -0.2618F, 0.0F, 0.0F);
		bone8.setTextureOffset(49, 169).addBox(-10.0F, 10.5419F, 4.3666F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		bone8.setTextureOffset(49, 169).addBox(-7.0F, 8.0504F, 4.3019F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone8.setTextureOffset(49, 169).addBox(-7.0F, 5.2834F, 4.4313F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone8.setTextureOffset(49, 169).addBox(-7.0F, 2.5419F, 4.3666F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone8.setTextureOffset(49, 169).addBox(-11.0F, 0.5419F, 4.3666F, 22.0F, 2.0F, 4.0F, 0.0F, false);
		bone8.setTextureOffset(50, 162).addBox(-10.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);
		bone8.setTextureOffset(49, 159).addBox(7.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		ring5 = new ModelRenderer(this);
		ring5.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front2.addChild(ring5);
		ring5.setTextureOffset(14, 195).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring5.addChild(bone9);
		setRotationAngle(bone9, -0.2618F, 0.0F, 0.0F);
		bone9.setTextureOffset(14, 195).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring5.addChild(bone10);
		setRotationAngle(bone10, 0.2618F, 0.0F, 0.0F);
		bone10.setTextureOffset(14, 195).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		ring6 = new ModelRenderer(this);
		ring6.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front2.addChild(ring6);
		ring6.setTextureOffset(19, 179).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring6.addChild(bone13);
		setRotationAngle(bone13, -0.2618F, 0.0F, 0.0F);
		bone13.setTextureOffset(19, 179).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone14 = new ModelRenderer(this);
		bone14.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring6.addChild(bone14);
		setRotationAngle(bone14, 0.2618F, 0.0F, 0.0F);
		bone14.setTextureOffset(19, 179).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		glow__innards2 = new ModelRenderer(this);
		glow__innards2.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring6.addChild(glow__innards2);
		

		undercurve2 = new ModelRenderer(this);
		undercurve2.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine2.addChild(undercurve2);
		setRotationAngle(undercurve2, 1.309F, 0.0F, 0.0F);
		undercurve2.setTextureOffset(32, 142).addBox(0.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve2.setTextureOffset(32, 142).addBox(-10.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve2.setTextureOffset(65, 154).addBox(-10.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve2.setTextureOffset(65, 154).addBox(0.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve2.setTextureOffset(53, 86).addBox(7.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);
		undercurve2.setTextureOffset(38, 91).addBox(-8.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);

		backbend2 = new ModelRenderer(this);
		backbend2.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine2.addChild(backbend2);
		setRotationAngle(backbend2, 0.3491F, 0.0F, 0.0F);
		backbend2.setTextureOffset(23, 147).addBox(-9.0F, -3.8396F, 13.8373F, 18.0F, 26.0F, 3.0F, 0.0F, false);
		backbend2.setTextureOffset(111, 8).addBox(-4.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend2.setTextureOffset(111, 8).addBox(3.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend2.setTextureOffset(53, 90).addBox(7.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);
		backbend2.setTextureOffset(27, 87).addBox(-8.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);

		shin2 = new ModelRenderer(this);
		shin2.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine2.addChild(shin2);
		setRotationAngle(shin2, -0.2618F, 0.0F, 0.0F);
		

		bone15 = new ModelRenderer(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin2.addChild(bone15);
		

		ribfoot2 = new ModelRenderer(this);
		ribfoot2.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine2.addChild(ribfoot2);
		ribfoot2.setTextureOffset(50, 67).addBox(-9.5F, -5.0F, -39.0F, 19.0F, 5.0F, 10.0F, 0.0F, false);

		silverib3 = new ModelRenderer(this);
		silverib3.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib3);
		setRotationAngle(silverib3, 0.0F, 1.0472F, 0.0F);
		

		spine3 = new ModelRenderer(this);
		spine3.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib3.addChild(spine3);
		setRotationAngle(spine3, 0.0F, -0.5236F, 0.0F);
		

		plain3 = new ModelRenderer(this);
		plain3.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine3.addChild(plain3);
		setRotationAngle(plain3, -1.309F, 0.0F, 0.0F);
		plain3.setTextureOffset(1, 154).addBox(-10.0F, 12.9249F, -0.5863F, 20.0F, 4.0F, 2.0F, 0.0F, false);
		plain3.setTextureOffset(16, 146).addBox(5.0F, 3.9249F, -0.5863F, 5.0F, 9.0F, 2.0F, 0.0F, false);
		plain3.setTextureOffset(1, 134).addBox(-10.0F, -7.0751F, -0.5863F, 20.0F, 11.0F, 2.0F, 0.0F, false);
		plain3.setTextureOffset(1, 134).addBox(-10.0F, 3.9249F, -0.5863F, 5.0F, 9.0F, 2.0F, 0.0F, false);
		plain3.setTextureOffset(1, 133).addBox(-10.0F, -24.0751F, -0.5863F, 20.0F, 17.0F, 2.0F, 0.0F, false);
		plain3.setTextureOffset(67, 135).addBox(-4.5F, -24.0751F, -2.5863F, 9.0F, 14.0F, 2.0F, 0.0F, false);
		plain3.setTextureOffset(66, 85).addBox(7.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);
		plain3.setTextureOffset(73, 81).addBox(-8.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);

		bevel3 = new ModelRenderer(this);
		bevel3.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine3.addChild(bevel3);
		setRotationAngle(bevel3, -0.6981F, 0.0F, 0.0F);
		bevel3.setTextureOffset(1, 158).addBox(-10.0F, -2.6563F, -1.5652F, 20.0F, 12.0F, 2.0F, 0.0F, false);
		bevel3.setTextureOffset(67, 155).addBox(-4.5F, -25.9206F, 12.283F, 9.0F, 4.0F, 2.0F, 0.0F, false);
		bevel3.setTextureOffset(69, 102).addBox(7.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);
		bevel3.setTextureOffset(40, 109).addBox(-8.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);

		front3 = new ModelRenderer(this);
		front3.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine3.addChild(front3);
		front3.setTextureOffset(45, 136).addBox(-10.0F, 3.5169F, -2.9925F, 20.0F, 8.0F, 2.0F, 0.0F, false);
		front3.setTextureOffset(68, 108).addBox(7.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		front3.setTextureOffset(44, 103).addBox(-8.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);

		bone17 = new ModelRenderer(this);
		bone17.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front3.addChild(bone17);
		setRotationAngle(bone17, -0.2618F, 0.0F, 0.0F);
		bone17.setTextureOffset(49, 169).addBox(-10.0F, 10.5419F, 4.3666F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		bone17.setTextureOffset(49, 169).addBox(-7.0F, 8.0504F, 4.3019F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone17.setTextureOffset(49, 169).addBox(-7.0F, 5.2834F, 4.4313F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone17.setTextureOffset(49, 169).addBox(-7.0F, 2.5419F, 4.3666F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone17.setTextureOffset(49, 169).addBox(-11.0F, 0.5419F, 4.3666F, 22.0F, 2.0F, 4.0F, 0.0F, false);
		bone17.setTextureOffset(50, 162).addBox(-10.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);
		bone17.setTextureOffset(49, 159).addBox(7.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		ring7 = new ModelRenderer(this);
		ring7.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front3.addChild(ring7);
		ring7.setTextureOffset(14, 195).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone18 = new ModelRenderer(this);
		bone18.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring7.addChild(bone18);
		setRotationAngle(bone18, -0.2618F, 0.0F, 0.0F);
		bone18.setTextureOffset(14, 195).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone19 = new ModelRenderer(this);
		bone19.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring7.addChild(bone19);
		setRotationAngle(bone19, 0.2618F, 0.0F, 0.0F);
		bone19.setTextureOffset(14, 195).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		ring8 = new ModelRenderer(this);
		ring8.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front3.addChild(ring8);
		ring8.setTextureOffset(19, 179).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone20 = new ModelRenderer(this);
		bone20.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring8.addChild(bone20);
		setRotationAngle(bone20, -0.2618F, 0.0F, 0.0F);
		bone20.setTextureOffset(19, 179).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone21 = new ModelRenderer(this);
		bone21.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring8.addChild(bone21);
		setRotationAngle(bone21, 0.2618F, 0.0F, 0.0F);
		bone21.setTextureOffset(19, 179).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		glow__innards3 = new ModelRenderer(this);
		glow__innards3.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring8.addChild(glow__innards3);
		

		undercurve3 = new ModelRenderer(this);
		undercurve3.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine3.addChild(undercurve3);
		setRotationAngle(undercurve3, 1.309F, 0.0F, 0.0F);
		undercurve3.setTextureOffset(32, 142).addBox(0.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve3.setTextureOffset(32, 142).addBox(-10.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve3.setTextureOffset(65, 154).addBox(-10.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve3.setTextureOffset(65, 154).addBox(0.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve3.setTextureOffset(53, 86).addBox(7.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);
		undercurve3.setTextureOffset(38, 91).addBox(-8.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);

		backbend3 = new ModelRenderer(this);
		backbend3.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine3.addChild(backbend3);
		setRotationAngle(backbend3, 0.3491F, 0.0F, 0.0F);
		backbend3.setTextureOffset(23, 147).addBox(-9.0F, -3.8396F, 13.8373F, 18.0F, 26.0F, 3.0F, 0.0F, false);
		backbend3.setTextureOffset(111, 8).addBox(-4.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend3.setTextureOffset(111, 8).addBox(3.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend3.setTextureOffset(53, 90).addBox(7.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);
		backbend3.setTextureOffset(27, 87).addBox(-8.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);

		shin3 = new ModelRenderer(this);
		shin3.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine3.addChild(shin3);
		setRotationAngle(shin3, -0.2618F, 0.0F, 0.0F);
		

		bone22 = new ModelRenderer(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin3.addChild(bone22);
		

		ribfoot3 = new ModelRenderer(this);
		ribfoot3.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine3.addChild(ribfoot3);
		ribfoot3.setTextureOffset(50, 67).addBox(-9.5F, -5.0F, -39.0F, 19.0F, 5.0F, 10.0F, 0.0F, false);

		silverib4 = new ModelRenderer(this);
		silverib4.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib4);
		setRotationAngle(silverib4, 0.0F, 2.0944F, 0.0F);
		

		spine4 = new ModelRenderer(this);
		spine4.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib4.addChild(spine4);
		setRotationAngle(spine4, 0.0F, -0.5236F, 0.0F);
		

		plain4 = new ModelRenderer(this);
		plain4.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine4.addChild(plain4);
		setRotationAngle(plain4, -1.309F, 0.0F, 0.0F);
		plain4.setTextureOffset(1, 134).addBox(-10.0F, -7.0751F, -0.5863F, 20.0F, 24.0F, 2.0F, 0.0F, false);
		plain4.setTextureOffset(1, 133).addBox(-10.0F, -24.0751F, -0.5863F, 20.0F, 17.0F, 2.0F, 0.0F, false);
		plain4.setTextureOffset(67, 135).addBox(-4.5F, -24.0751F, -2.5863F, 9.0F, 14.0F, 2.0F, 0.0F, false);
		plain4.setTextureOffset(66, 85).addBox(7.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);
		plain4.setTextureOffset(73, 81).addBox(-8.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);

		bevel4 = new ModelRenderer(this);
		bevel4.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine4.addChild(bevel4);
		setRotationAngle(bevel4, -0.6981F, 0.0F, 0.0F);
		bevel4.setTextureOffset(1, 158).addBox(-10.0F, -2.6563F, -1.5652F, 20.0F, 12.0F, 2.0F, 0.0F, false);
		bevel4.setTextureOffset(67, 155).addBox(-4.5F, -25.9206F, 12.283F, 9.0F, 4.0F, 2.0F, 0.0F, false);
		bevel4.setTextureOffset(69, 102).addBox(7.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);
		bevel4.setTextureOffset(40, 109).addBox(-8.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);

		front4 = new ModelRenderer(this);
		front4.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine4.addChild(front4);
		front4.setTextureOffset(45, 136).addBox(-10.0F, 3.5169F, -2.9925F, 20.0F, 8.0F, 2.0F, 0.0F, false);
		front4.setTextureOffset(68, 108).addBox(7.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		front4.setTextureOffset(44, 103).addBox(-8.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);

		bone23 = new ModelRenderer(this);
		bone23.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front4.addChild(bone23);
		setRotationAngle(bone23, -0.2618F, 0.0F, 0.0F);
		bone23.setTextureOffset(49, 169).addBox(-10.0F, 10.5419F, 4.3666F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		bone23.setTextureOffset(49, 169).addBox(-7.0F, 8.0504F, 4.3019F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone23.setTextureOffset(49, 169).addBox(-7.0F, 5.2834F, 4.4313F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone23.setTextureOffset(49, 169).addBox(-7.0F, 2.5419F, 4.3666F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone23.setTextureOffset(49, 169).addBox(-11.0F, 0.5419F, 4.3666F, 22.0F, 2.0F, 4.0F, 0.0F, false);
		bone23.setTextureOffset(50, 162).addBox(-10.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);
		bone23.setTextureOffset(49, 159).addBox(7.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		ring9 = new ModelRenderer(this);
		ring9.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front4.addChild(ring9);
		ring9.setTextureOffset(14, 195).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone24 = new ModelRenderer(this);
		bone24.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring9.addChild(bone24);
		setRotationAngle(bone24, -0.2618F, 0.0F, 0.0F);
		bone24.setTextureOffset(14, 195).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone25 = new ModelRenderer(this);
		bone25.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring9.addChild(bone25);
		setRotationAngle(bone25, 0.2618F, 0.0F, 0.0F);
		bone25.setTextureOffset(14, 195).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		ring10 = new ModelRenderer(this);
		ring10.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front4.addChild(ring10);
		ring10.setTextureOffset(19, 179).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone26 = new ModelRenderer(this);
		bone26.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring10.addChild(bone26);
		setRotationAngle(bone26, -0.2618F, 0.0F, 0.0F);
		bone26.setTextureOffset(19, 179).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone27 = new ModelRenderer(this);
		bone27.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring10.addChild(bone27);
		setRotationAngle(bone27, 0.2618F, 0.0F, 0.0F);
		bone27.setTextureOffset(19, 179).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		glow__innards4 = new ModelRenderer(this);
		glow__innards4.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring10.addChild(glow__innards4);
		

		undercurve4 = new ModelRenderer(this);
		undercurve4.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine4.addChild(undercurve4);
		setRotationAngle(undercurve4, 1.309F, 0.0F, 0.0F);
		undercurve4.setTextureOffset(32, 142).addBox(0.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve4.setTextureOffset(32, 142).addBox(-10.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve4.setTextureOffset(65, 154).addBox(-10.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve4.setTextureOffset(65, 154).addBox(0.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve4.setTextureOffset(53, 86).addBox(7.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);
		undercurve4.setTextureOffset(38, 91).addBox(-8.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);

		backbend4 = new ModelRenderer(this);
		backbend4.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine4.addChild(backbend4);
		setRotationAngle(backbend4, 0.3491F, 0.0F, 0.0F);
		backbend4.setTextureOffset(23, 147).addBox(-9.0F, -3.8396F, 13.8373F, 18.0F, 26.0F, 3.0F, 0.0F, false);
		backbend4.setTextureOffset(111, 8).addBox(-4.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend4.setTextureOffset(111, 8).addBox(3.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend4.setTextureOffset(53, 90).addBox(7.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);
		backbend4.setTextureOffset(27, 87).addBox(-8.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);

		shin4 = new ModelRenderer(this);
		shin4.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine4.addChild(shin4);
		setRotationAngle(shin4, -0.2618F, 0.0F, 0.0F);
		

		bone28 = new ModelRenderer(this);
		bone28.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin4.addChild(bone28);
		

		ribfoot4 = new ModelRenderer(this);
		ribfoot4.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine4.addChild(ribfoot4);
		ribfoot4.setTextureOffset(50, 67).addBox(-9.5F, -5.0F, -39.0F, 19.0F, 5.0F, 10.0F, 0.0F, false);

		silverib5 = new ModelRenderer(this);
		silverib5.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib5);
		setRotationAngle(silverib5, 0.0F, 3.1416F, 0.0F);
		

		spine5 = new ModelRenderer(this);
		spine5.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib5.addChild(spine5);
		setRotationAngle(spine5, 0.0F, -0.5236F, 0.0F);
		

		plain5 = new ModelRenderer(this);
		plain5.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine5.addChild(plain5);
		setRotationAngle(plain5, -1.309F, 0.0F, 0.0F);
		plain5.setTextureOffset(1, 134).addBox(-10.0F, -7.0751F, -0.5863F, 20.0F, 24.0F, 2.0F, 0.0F, false);
		plain5.setTextureOffset(1, 133).addBox(-10.0F, -24.0751F, -0.5863F, 20.0F, 17.0F, 2.0F, 0.0F, false);
		plain5.setTextureOffset(67, 135).addBox(-4.5F, -24.0751F, -2.5863F, 9.0F, 14.0F, 2.0F, 0.0F, false);
		plain5.setTextureOffset(66, 85).addBox(7.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);
		plain5.setTextureOffset(73, 81).addBox(-8.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);

		bevel5 = new ModelRenderer(this);
		bevel5.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine5.addChild(bevel5);
		setRotationAngle(bevel5, -0.6981F, 0.0F, 0.0F);
		bevel5.setTextureOffset(1, 158).addBox(-10.0F, -2.6563F, -1.5652F, 20.0F, 12.0F, 2.0F, 0.0F, false);
		bevel5.setTextureOffset(67, 155).addBox(-4.5F, -25.9206F, 12.283F, 9.0F, 4.0F, 2.0F, 0.0F, false);
		bevel5.setTextureOffset(69, 102).addBox(7.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);
		bevel5.setTextureOffset(40, 109).addBox(-8.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);

		front5 = new ModelRenderer(this);
		front5.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine5.addChild(front5);
		front5.setTextureOffset(45, 136).addBox(-10.0F, 3.5169F, -2.9925F, 20.0F, 8.0F, 2.0F, 0.0F, false);
		front5.setTextureOffset(68, 108).addBox(7.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		front5.setTextureOffset(44, 103).addBox(-8.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);

		bone29 = new ModelRenderer(this);
		bone29.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front5.addChild(bone29);
		setRotationAngle(bone29, -0.2618F, 0.0F, 0.0F);
		bone29.setTextureOffset(49, 169).addBox(-10.0F, 10.5419F, 4.3666F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		bone29.setTextureOffset(49, 169).addBox(-7.0F, 8.0504F, 4.3019F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone29.setTextureOffset(49, 169).addBox(-7.0F, 5.2834F, 4.4313F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone29.setTextureOffset(49, 169).addBox(-7.0F, 2.5419F, 4.3666F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone29.setTextureOffset(49, 169).addBox(-11.0F, 0.5419F, 4.3666F, 22.0F, 2.0F, 4.0F, 0.0F, false);
		bone29.setTextureOffset(50, 162).addBox(-10.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);
		bone29.setTextureOffset(49, 159).addBox(7.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		ring11 = new ModelRenderer(this);
		ring11.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front5.addChild(ring11);
		ring11.setTextureOffset(14, 195).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone30 = new ModelRenderer(this);
		bone30.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring11.addChild(bone30);
		setRotationAngle(bone30, -0.2618F, 0.0F, 0.0F);
		bone30.setTextureOffset(14, 195).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone31 = new ModelRenderer(this);
		bone31.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring11.addChild(bone31);
		setRotationAngle(bone31, 0.2618F, 0.0F, 0.0F);
		bone31.setTextureOffset(14, 195).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		ring12 = new ModelRenderer(this);
		ring12.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front5.addChild(ring12);
		ring12.setTextureOffset(19, 179).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone32 = new ModelRenderer(this);
		bone32.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring12.addChild(bone32);
		setRotationAngle(bone32, -0.2618F, 0.0F, 0.0F);
		bone32.setTextureOffset(19, 179).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone33 = new ModelRenderer(this);
		bone33.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring12.addChild(bone33);
		setRotationAngle(bone33, 0.2618F, 0.0F, 0.0F);
		bone33.setTextureOffset(19, 179).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		glow__innards5 = new ModelRenderer(this);
		glow__innards5.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring12.addChild(glow__innards5);
		

		undercurve5 = new ModelRenderer(this);
		undercurve5.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine5.addChild(undercurve5);
		setRotationAngle(undercurve5, 1.309F, 0.0F, 0.0F);
		undercurve5.setTextureOffset(32, 142).addBox(0.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve5.setTextureOffset(32, 142).addBox(-10.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve5.setTextureOffset(65, 154).addBox(-10.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve5.setTextureOffset(65, 154).addBox(0.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve5.setTextureOffset(53, 86).addBox(7.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);
		undercurve5.setTextureOffset(38, 91).addBox(-8.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);

		backbend5 = new ModelRenderer(this);
		backbend5.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine5.addChild(backbend5);
		setRotationAngle(backbend5, 0.3491F, 0.0F, 0.0F);
		backbend5.setTextureOffset(23, 147).addBox(-9.0F, -3.8396F, 13.8373F, 18.0F, 26.0F, 3.0F, 0.0F, false);
		backbend5.setTextureOffset(111, 8).addBox(-4.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend5.setTextureOffset(111, 8).addBox(3.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend5.setTextureOffset(53, 90).addBox(7.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);
		backbend5.setTextureOffset(27, 87).addBox(-8.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);

		shin5 = new ModelRenderer(this);
		shin5.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine5.addChild(shin5);
		setRotationAngle(shin5, -0.2618F, 0.0F, 0.0F);
		

		bone34 = new ModelRenderer(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin5.addChild(bone34);
		

		ribfoot5 = new ModelRenderer(this);
		ribfoot5.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine5.addChild(ribfoot5);
		ribfoot5.setTextureOffset(50, 67).addBox(-9.5F, -5.0F, -39.0F, 19.0F, 5.0F, 10.0F, 0.0F, false);

		silverib6 = new ModelRenderer(this);
		silverib6.setRotationPoint(0.0F, 0.0F, 0.0F);
		spines.addChild(silverib6);
		setRotationAngle(silverib6, 0.0F, -2.0944F, 0.0F);
		

		spine6 = new ModelRenderer(this);
		spine6.setRotationPoint(0.0F, 0.0F, 0.0F);
		silverib6.addChild(spine6);
		setRotationAngle(spine6, 0.0F, -0.5236F, 0.0F);
		

		plain6 = new ModelRenderer(this);
		plain6.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine6.addChild(plain6);
		setRotationAngle(plain6, -1.309F, 0.0F, 0.0F);
		plain6.setTextureOffset(1, 134).addBox(-10.0F, -7.0751F, -0.5863F, 20.0F, 24.0F, 2.0F, 0.0F, false);
		plain6.setTextureOffset(1, 133).addBox(-10.0F, -24.0751F, -0.5863F, 20.0F, 17.0F, 2.0F, 0.0F, false);
		plain6.setTextureOffset(67, 135).addBox(-4.5F, -24.0751F, -2.5863F, 9.0F, 14.0F, 2.0F, 0.0F, false);
		plain6.setTextureOffset(66, 85).addBox(7.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);
		plain6.setTextureOffset(73, 81).addBox(-8.0F, -24.0751F, 1.4137F, 1.0F, 41.0F, 4.0F, 0.0F, false);

		bevel6 = new ModelRenderer(this);
		bevel6.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine6.addChild(bevel6);
		setRotationAngle(bevel6, -0.6981F, 0.0F, 0.0F);
		bevel6.setTextureOffset(1, 158).addBox(-10.0F, -2.6563F, -1.5652F, 20.0F, 12.0F, 2.0F, 0.0F, false);
		bevel6.setTextureOffset(67, 155).addBox(-4.5F, -25.9206F, 12.283F, 9.0F, 4.0F, 2.0F, 0.0F, false);
		bevel6.setTextureOffset(69, 102).addBox(7.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);
		bevel6.setTextureOffset(40, 109).addBox(-8.0F, -1.6563F, 0.4348F, 1.0F, 10.0F, 4.0F, 0.0F, false);

		front6 = new ModelRenderer(this);
		front6.setRotationPoint(0.0F, -50.0F, -72.0F);
		spine6.addChild(front6);
		front6.setTextureOffset(45, 136).addBox(-10.0F, 3.5169F, -2.9925F, 20.0F, 8.0F, 2.0F, 0.0F, false);
		front6.setTextureOffset(68, 108).addBox(7.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);
		front6.setTextureOffset(44, 103).addBox(-8.0F, 3.5169F, -0.9925F, 1.0F, 8.0F, 4.0F, 0.0F, false);

		bone35 = new ModelRenderer(this);
		bone35.setRotationPoint(0.0F, -29.2731F, 42.3863F);
		front6.addChild(bone35);
		setRotationAngle(bone35, -0.2618F, 0.0F, 0.0F);
		bone35.setTextureOffset(49, 169).addBox(-10.0F, 10.5419F, 4.3666F, 20.0F, 2.0F, 4.0F, 0.0F, false);
		bone35.setTextureOffset(49, 169).addBox(-7.0F, 8.0504F, 4.3019F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone35.setTextureOffset(49, 169).addBox(-7.0F, 5.2834F, 4.4313F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone35.setTextureOffset(49, 169).addBox(-7.0F, 2.5419F, 4.3666F, 14.0F, 1.0F, 4.0F, 0.0F, false);
		bone35.setTextureOffset(49, 169).addBox(-11.0F, 0.5419F, 4.3666F, 22.0F, 2.0F, 4.0F, 0.0F, false);
		bone35.setTextureOffset(50, 162).addBox(-10.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);
		bone35.setTextureOffset(49, 159).addBox(7.0F, 2.5419F, 4.3666F, 3.0F, 8.0F, 4.0F, 0.0F, false);

		ring13 = new ModelRenderer(this);
		ring13.setRotationPoint(0.6667F, 27.5756F, 63.2954F);
		front6.addChild(ring13);
		ring13.setTextureOffset(14, 195).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone36 = new ModelRenderer(this);
		bone36.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring13.addChild(bone36);
		setRotationAngle(bone36, -0.2618F, 0.0F, 0.0F);
		bone36.setTextureOffset(14, 195).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone37 = new ModelRenderer(this);
		bone37.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring13.addChild(bone37);
		setRotationAngle(bone37, 0.2618F, 0.0F, 0.0F);
		bone37.setTextureOffset(14, 195).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		ring14 = new ModelRenderer(this);
		ring14.setRotationPoint(0.6667F, 21.5756F, 66.2954F);
		front6.addChild(ring14);
		ring14.setTextureOffset(19, 179).addBox(-10.6667F, -58.8488F, -21.9092F, 20.0F, 4.0F, 7.0F, 0.0F, false);

		bone38 = new ModelRenderer(this);
		bone38.setRotationPoint(0.3333F, -54.8488F, -16.4092F);
		ring14.addChild(bone38);
		setRotationAngle(bone38, -0.2618F, 0.0F, 0.0F);
		bone38.setTextureOffset(19, 179).addBox(-11.0F, -1.5765F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		bone39 = new ModelRenderer(this);
		bone39.setRotationPoint(0.3333F, -58.8488F, -16.4092F);
		ring14.addChild(bone39);
		setRotationAngle(bone39, 0.2618F, 0.0F, 0.0F);
		bone39.setTextureOffset(19, 179).addBox(-11.0F, -1.4235F, -5.3126F, 20.0F, 3.0F, 7.0F, 0.0F, false);

		glow__innards6 = new ModelRenderer(this);
		glow__innards6.setRotationPoint(-31.6667F, 61.4244F, -1.2954F);
		ring14.addChild(glow__innards6);
		

		undercurve6 = new ModelRenderer(this);
		undercurve6.setRotationPoint(0.0F, -37.2952F, -57.6086F);
		spine6.addChild(undercurve6);
		setRotationAngle(undercurve6, 1.309F, 0.0F, 0.0F);
		undercurve6.setTextureOffset(32, 142).addBox(0.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve6.setTextureOffset(32, 142).addBox(-10.0F, 0.901F, -3.3518F, 10.0F, 22.0F, 2.0F, 0.0F, false);
		undercurve6.setTextureOffset(65, 154).addBox(-10.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve6.setTextureOffset(65, 154).addBox(0.0F, -17.099F, -3.3518F, 10.0F, 18.0F, 2.0F, 0.0F, false);
		undercurve6.setTextureOffset(53, 86).addBox(7.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);
		undercurve6.setTextureOffset(38, 91).addBox(-8.0F, -13.099F, -1.3518F, 1.0F, 36.0F, 4.0F, 0.0F, false);

		backbend6 = new ModelRenderer(this);
		backbend6.setRotationPoint(0.0F, -20.714F, -51.1042F);
		spine6.addChild(backbend6);
		setRotationAngle(backbend6, 0.3491F, 0.0F, 0.0F);
		backbend6.setTextureOffset(23, 147).addBox(-9.0F, -3.8396F, 13.8373F, 18.0F, 26.0F, 3.0F, 0.0F, false);
		backbend6.setTextureOffset(111, 8).addBox(-4.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend6.setTextureOffset(111, 8).addBox(3.0F, -5.8656F, 11.0182F, 1.0F, 28.0F, 3.0F, 0.0F, false);
		backbend6.setTextureOffset(53, 90).addBox(7.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);
		backbend6.setTextureOffset(27, 87).addBox(-8.0F, -6.8396F, 16.8373F, 1.0F, 30.0F, 10.0F, 0.0F, false);

		shin6 = new ModelRenderer(this);
		shin6.setRotationPoint(0.0F, -15.9761F, -51.8165F);
		spine6.addChild(shin6);
		setRotationAngle(shin6, -0.2618F, 0.0F, 0.0F);
		

		bone40 = new ModelRenderer(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		shin6.addChild(bone40);
		

		ribfoot6 = new ModelRenderer(this);
		ribfoot6.setRotationPoint(0.0F, 0.0F, 0.0F);
		spine6.addChild(ribfoot6);
		ribfoot6.setTextureOffset(50, 67).addBox(-9.5F, -5.0F, -39.0F, 19.0F, 5.0F, 10.0F, 0.0F, false);

		stations = new ModelRenderer(this);
		stations.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(stations, 0.0F, -0.5236F, 0.0F);
		

		station1 = new ModelRenderer(this);
		station1.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station1);
		setRotationAngle(station1, 0.0F, -0.5236F, 0.0F);
		

		st_plain1 = new ModelRenderer(this);
		st_plain1.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station1.addChild(st_plain1);
		setRotationAngle(st_plain1, -1.309F, 0.0F, 0.0F);
		st_plain1.setTextureOffset(4, 63).addBox(-26.0F, -6.0751F, 1.1637F, 52.0F, 16.0F, 4.0F, 0.0F, false);
		st_plain1.setTextureOffset(33, 75).addBox(-10.0F, -23.0751F, -2.5863F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		st_plain1.setTextureOffset(4, 63).addBox(-17.0F, -25.0751F, 1.1637F, 34.0F, 19.0F, 4.0F, 0.0F, false);

		st_bevel1 = new ModelRenderer(this);
		st_bevel1.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station1.addChild(st_bevel1);
		setRotationAngle(st_bevel1, -0.6981F, 0.0F, 0.0F);
		st_bevel1.setTextureOffset(2, 31).addBox(-25.4375F, -3.4063F, 3.9348F, 55.0F, 4.0F, 4.0F, 0.0F, false);
		st_bevel1.setTextureOffset(4, 80).addBox(-23.5923F, -5.2695F, 5.4769F, 52.0F, 2.0F, 5.0F, 0.0F, false);
		st_bevel1.setTextureOffset(3, 15).addBox(-27.4375F, 0.5937F, 4.4348F, 58.0F, 3.0F, 2.0F, 0.0F, false);

		st_front1 = new ModelRenderer(this);
		st_front1.setRotationPoint(0.0F, -50.0F, -72.0F);
		station1.addChild(st_front1);
		st_front1.setTextureOffset(0, 35).addBox(-29.8452F, 4.0727F, 4.9399F, 59.0F, 5.0F, 4.0F, 0.0F, false);

		rotorbevel = new ModelRenderer(this);
		rotorbevel.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front1.addChild(rotorbevel);
		setRotationAngle(rotorbevel, -0.2443F, 0.0F, 0.0F);
		rotorbevel.setTextureOffset(54, 168).addBox(-5.0938F, -9.0F, 0.2862F, 11.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel.setTextureOffset(54, 168).addBox(-4.0938F, -11.5F, 0.2862F, 9.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel.setTextureOffset(54, 168).addBox(-4.5938F, -16.25F, -0.7138F, 10.0F, 3.0F, 4.0F, 0.0F, false);
		rotorbevel.setTextureOffset(54, 168).addBox(-6.0F, -3.25F, -0.7138F, 13.0F, 4.0F, 6.0F, 0.0F, false);
		rotorbevel.setTextureOffset(54, 168).addBox(-5.125F, -6.25F, 0.5362F, 11.0F, 2.0F, 3.0F, 0.0F, false);
		rotorbevel.setTextureOffset(54, 168).addBox(-5.5625F, -4.25F, 0.2862F, 12.0F, 1.0F, 3.0F, 0.0F, false);

		ring4 = new ModelRenderer(this);
		ring4.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front1.addChild(ring4);
		ring4.setTextureOffset(22, 207).addBox(24.1556F, -120.2731F, -22.0123F, 14.0F, 4.0F, 5.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring4.addChild(bone11);
		setRotationAngle(bone11, -0.2618F, 0.0F, 0.0F);
		bone11.setTextureOffset(22, 207).addBox(-7.3444F, -2.2145F, -6.6635F, 13.0F, 4.0F, 2.0F, 0.0F, false);
		bone11.setTextureOffset(22, 207).addBox(-6.3444F, -0.2145F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring4.addChild(bone12);
		setRotationAngle(bone12, 0.2618F, 0.0F, 0.0F);
		bone12.setTextureOffset(22, 207).addBox(-7.3444F, -1.7855F, -6.6635F, 13.0F, 1.0F, 2.0F, 0.0F, false);
		bone12.setTextureOffset(22, 207).addBox(-6.3444F, -1.7855F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		ring3 = new ModelRenderer(this);
		ring3.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front1.addChild(ring3);
		ring3.setTextureOffset(29, 205).addBox(25.7091F, -120.2731F, -21.9142F, 11.0F, 4.0F, 4.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring3.addChild(bone6);
		setRotationAngle(bone6, -0.2618F, 0.0F, 0.0F);
		bone6.setTextureOffset(29, 205).addBox(-5.8221F, -2.2399F, -6.5687F, 10.0F, 4.0F, 2.0F, 0.0F, false);
		bone6.setTextureOffset(29, 205).addBox(-4.8221F, -0.2399F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring3.addChild(bone7);
		setRotationAngle(bone7, 0.2618F, 0.0F, 0.0F);
		bone7.setTextureOffset(29, 205).addBox(-5.7909F, -1.7601F, -6.5687F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		bone7.setTextureOffset(29, 205).addBox(-4.7909F, -1.7601F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		st_under1 = new ModelRenderer(this);
		st_under1.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station1.addChild(st_under1);
		setRotationAngle(st_under1, 1.309F, 0.0F, 0.0F);
		st_under1.setTextureOffset(3, 30).addBox(-28.2702F, -11.2279F, 4.1025F, 58.0F, 8.0F, 2.0F, 0.0F, false);
		st_under1.setTextureOffset(11, 63).addBox(-24.0F, -4.6316F, 5.2895F, 49.0F, 13.0F, 3.0F, 0.0F, false);
		st_under1.setTextureOffset(0, 103).addBox(-18.0F, 8.3684F, 5.2895F, 37.0F, 8.0F, 3.0F, 0.0F, false);
		st_under1.setTextureOffset(77, 54).addBox(-6.0F, 18.3684F, 2.2895F, 13.0F, 16.0F, 3.0F, 0.0F, false);
		st_under1.setTextureOffset(80, 70).addBox(-4.0F, 26.3684F, -0.7105F, 9.0F, 9.0F, 4.0F, 0.0F, false);
		st_under1.setTextureOffset(9, 99).addBox(-14.0F, 16.3684F, 5.2895F, 29.0F, 17.0F, 3.0F, 0.0F, false);

		back1 = new ModelRenderer(this);
		back1.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station1.addChild(back1);
		setRotationAngle(back1, 0.3491F, 0.0F, 0.0F);
		back1.setTextureOffset(11, 106).addBox(-12.0F, -4.895F, 17.2141F, 24.0F, 8.0F, 3.0F, 0.0F, false);
		back1.setTextureOffset(12, 113).addBox(-11.0F, 3.105F, 17.2141F, 20.0F, 8.0F, 3.0F, 0.0F, false);

		box1 = new ModelRenderer(this);
		box1.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station1.addChild(box1);
		setRotationAngle(box1, -0.0873F, 0.0F, 0.0F);
		box1.setTextureOffset(14, 106).addBox(-12.0F, -1.9924F, 20.5911F, 22.0F, 10.0F, 3.0F, 0.0F, false);
		box1.setTextureOffset(42, 69).addBox(-7.0F, 8.0076F, 11.5911F, 14.0F, 4.0F, 13.0F, 0.0F, false);

		trim1 = new ModelRenderer(this);
		trim1.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station1.addChild(trim1);
		setRotationAngle(trim1, -1.5708F, 0.0F, 0.0F);
		trim1.setTextureOffset(19, 58).addBox(-12.0F, -43.0F, -4.0F, 24.0F, 16.0F, 4.0F, 0.0F, false);

		station2 = new ModelRenderer(this);
		station2.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station2);
		setRotationAngle(station2, 0.0F, 0.5236F, 0.0F);
		

		st_plain2 = new ModelRenderer(this);
		st_plain2.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station2.addChild(st_plain2);
		setRotationAngle(st_plain2, -1.309F, 0.0F, 0.0F);
		st_plain2.setTextureOffset(4, 63).addBox(-26.0F, -6.0751F, 1.1637F, 52.0F, 16.0F, 4.0F, 0.0F, false);
		st_plain2.setTextureOffset(33, 75).addBox(-10.0F, -23.0751F, -2.5863F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		st_plain2.setTextureOffset(4, 63).addBox(-17.0F, -25.0751F, 1.1637F, 34.0F, 19.0F, 4.0F, 0.0F, false);

		st_bevel2 = new ModelRenderer(this);
		st_bevel2.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station2.addChild(st_bevel2);
		setRotationAngle(st_bevel2, -0.6981F, 0.0F, 0.0F);
		st_bevel2.setTextureOffset(2, 31).addBox(-25.4375F, -3.4063F, 3.9348F, 55.0F, 4.0F, 4.0F, 0.0F, false);
		st_bevel2.setTextureOffset(4, 80).addBox(-23.5923F, -5.2695F, 5.4769F, 52.0F, 2.0F, 5.0F, 0.0F, false);
		st_bevel2.setTextureOffset(3, 15).addBox(-27.4375F, 0.5937F, 4.4348F, 58.0F, 3.0F, 2.0F, 0.0F, false);

		st_front2 = new ModelRenderer(this);
		st_front2.setRotationPoint(0.0F, -50.0F, -72.0F);
		station2.addChild(st_front2);
		st_front2.setTextureOffset(0, 35).addBox(-29.8452F, 4.0727F, 4.9399F, 59.0F, 5.0F, 4.0F, 0.0F, false);

		rotorbevel2 = new ModelRenderer(this);
		rotorbevel2.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front2.addChild(rotorbevel2);
		setRotationAngle(rotorbevel2, -0.2443F, 0.0F, 0.0F);
		rotorbevel2.setTextureOffset(54, 168).addBox(-5.0938F, -9.0F, 0.2862F, 11.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel2.setTextureOffset(54, 168).addBox(-4.0938F, -11.5F, 0.2862F, 9.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel2.setTextureOffset(54, 168).addBox(-4.5938F, -16.25F, -0.7138F, 10.0F, 3.0F, 4.0F, 0.0F, false);
		rotorbevel2.setTextureOffset(54, 168).addBox(-6.0F, -3.25F, -0.7138F, 13.0F, 4.0F, 6.0F, 0.0F, false);
		rotorbevel2.setTextureOffset(54, 168).addBox(-5.125F, -6.25F, 0.5362F, 11.0F, 2.0F, 3.0F, 0.0F, false);
		rotorbevel2.setTextureOffset(54, 168).addBox(-5.5625F, -4.25F, 0.2862F, 12.0F, 1.0F, 3.0F, 0.0F, false);

		ring15 = new ModelRenderer(this);
		ring15.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front2.addChild(ring15);
		ring15.setTextureOffset(22, 207).addBox(24.1556F, -120.2731F, -22.0123F, 14.0F, 4.0F, 5.0F, 0.0F, false);

		bone41 = new ModelRenderer(this);
		bone41.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring15.addChild(bone41);
		setRotationAngle(bone41, -0.2618F, 0.0F, 0.0F);
		bone41.setTextureOffset(22, 207).addBox(-7.3444F, -2.2145F, -6.6635F, 13.0F, 4.0F, 2.0F, 0.0F, false);
		bone41.setTextureOffset(22, 207).addBox(-6.3444F, -0.2145F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		bone42 = new ModelRenderer(this);
		bone42.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring15.addChild(bone42);
		setRotationAngle(bone42, 0.2618F, 0.0F, 0.0F);
		bone42.setTextureOffset(22, 207).addBox(-7.3444F, -1.7855F, -6.6635F, 13.0F, 1.0F, 2.0F, 0.0F, false);
		bone42.setTextureOffset(22, 207).addBox(-6.3444F, -1.7855F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		ring16 = new ModelRenderer(this);
		ring16.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front2.addChild(ring16);
		ring16.setTextureOffset(29, 205).addBox(25.7091F, -120.2731F, -21.9142F, 11.0F, 4.0F, 4.0F, 0.0F, false);

		bone43 = new ModelRenderer(this);
		bone43.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring16.addChild(bone43);
		setRotationAngle(bone43, -0.2618F, 0.0F, 0.0F);
		bone43.setTextureOffset(29, 205).addBox(-5.8221F, -2.2399F, -6.5687F, 10.0F, 4.0F, 2.0F, 0.0F, false);
		bone43.setTextureOffset(29, 205).addBox(-4.8221F, -0.2399F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		bone44 = new ModelRenderer(this);
		bone44.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring16.addChild(bone44);
		setRotationAngle(bone44, 0.2618F, 0.0F, 0.0F);
		bone44.setTextureOffset(29, 205).addBox(-5.7909F, -1.7601F, -6.5687F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		bone44.setTextureOffset(29, 205).addBox(-4.7909F, -1.7601F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		st_under2 = new ModelRenderer(this);
		st_under2.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station2.addChild(st_under2);
		setRotationAngle(st_under2, 1.309F, 0.0F, 0.0F);
		st_under2.setTextureOffset(3, 30).addBox(-28.2702F, -11.2279F, 4.1025F, 58.0F, 8.0F, 2.0F, 0.0F, false);
		st_under2.setTextureOffset(11, 63).addBox(-24.0F, -4.6316F, 5.2895F, 49.0F, 13.0F, 3.0F, 0.0F, false);
		st_under2.setTextureOffset(0, 103).addBox(-18.0F, 8.3684F, 5.2895F, 37.0F, 8.0F, 3.0F, 0.0F, false);
		st_under2.setTextureOffset(77, 54).addBox(-6.0F, 18.3684F, 2.2895F, 13.0F, 16.0F, 3.0F, 0.0F, false);
		st_under2.setTextureOffset(80, 70).addBox(-4.0F, 26.3684F, -0.7105F, 9.0F, 9.0F, 4.0F, 0.0F, false);
		st_under2.setTextureOffset(9, 99).addBox(-14.0F, 16.3684F, 5.2895F, 29.0F, 17.0F, 3.0F, 0.0F, false);

		back2 = new ModelRenderer(this);
		back2.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station2.addChild(back2);
		setRotationAngle(back2, 0.3491F, 0.0F, 0.0F);
		back2.setTextureOffset(11, 106).addBox(-12.0F, -4.895F, 17.2141F, 24.0F, 8.0F, 3.0F, 0.0F, false);
		back2.setTextureOffset(12, 113).addBox(-11.0F, 3.105F, 17.2141F, 20.0F, 8.0F, 3.0F, 0.0F, false);

		box2 = new ModelRenderer(this);
		box2.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station2.addChild(box2);
		setRotationAngle(box2, -0.0873F, 0.0F, 0.0F);
		box2.setTextureOffset(14, 106).addBox(-12.0F, -1.9924F, 20.5911F, 22.0F, 10.0F, 3.0F, 0.0F, false);
		box2.setTextureOffset(42, 69).addBox(-7.0F, 8.0076F, 11.5911F, 14.0F, 4.0F, 13.0F, 0.0F, false);

		trim2 = new ModelRenderer(this);
		trim2.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station2.addChild(trim2);
		setRotationAngle(trim2, -1.5708F, 0.0F, 0.0F);
		trim2.setTextureOffset(19, 58).addBox(-12.0F, -43.0F, -4.0F, 24.0F, 16.0F, 4.0F, 0.0F, false);

		station3 = new ModelRenderer(this);
		station3.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station3);
		setRotationAngle(station3, 0.0F, 1.5708F, 0.0F);
		

		st_plain3 = new ModelRenderer(this);
		st_plain3.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station3.addChild(st_plain3);
		setRotationAngle(st_plain3, -1.309F, 0.0F, 0.0F);
		st_plain3.setTextureOffset(4, 63).addBox(-26.0F, -6.0751F, 1.1637F, 52.0F, 16.0F, 4.0F, 0.0F, false);
		st_plain3.setTextureOffset(33, 75).addBox(-10.0F, -23.0751F, -2.5863F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		st_plain3.setTextureOffset(4, 63).addBox(-17.0F, -25.0751F, 1.1637F, 34.0F, 19.0F, 4.0F, 0.0F, false);

		st_bevel3 = new ModelRenderer(this);
		st_bevel3.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station3.addChild(st_bevel3);
		setRotationAngle(st_bevel3, -0.6981F, 0.0F, 0.0F);
		st_bevel3.setTextureOffset(2, 31).addBox(-25.4375F, -3.4063F, 3.9348F, 55.0F, 4.0F, 4.0F, 0.0F, false);
		st_bevel3.setTextureOffset(4, 80).addBox(-23.5923F, -5.2695F, 5.4769F, 52.0F, 2.0F, 5.0F, 0.0F, false);
		st_bevel3.setTextureOffset(3, 15).addBox(-27.4375F, 0.5937F, 4.4348F, 58.0F, 3.0F, 2.0F, 0.0F, false);

		st_front3 = new ModelRenderer(this);
		st_front3.setRotationPoint(0.0F, -50.0F, -72.0F);
		station3.addChild(st_front3);
		st_front3.setTextureOffset(0, 35).addBox(-29.8452F, 4.0727F, 4.9399F, 59.0F, 5.0F, 4.0F, 0.0F, false);

		rotorbevel3 = new ModelRenderer(this);
		rotorbevel3.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front3.addChild(rotorbevel3);
		setRotationAngle(rotorbevel3, -0.2443F, 0.0F, 0.0F);
		rotorbevel3.setTextureOffset(54, 168).addBox(-5.0938F, -9.0F, 0.2862F, 11.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel3.setTextureOffset(54, 168).addBox(-4.0938F, -11.5F, 0.2862F, 9.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel3.setTextureOffset(54, 168).addBox(-4.5938F, -16.25F, -0.7138F, 10.0F, 3.0F, 4.0F, 0.0F, false);
		rotorbevel3.setTextureOffset(54, 168).addBox(-6.0F, -3.25F, -0.7138F, 13.0F, 4.0F, 6.0F, 0.0F, false);
		rotorbevel3.setTextureOffset(54, 168).addBox(-5.125F, -6.25F, 0.5362F, 11.0F, 2.0F, 3.0F, 0.0F, false);
		rotorbevel3.setTextureOffset(54, 168).addBox(-5.5625F, -4.25F, 0.2862F, 12.0F, 1.0F, 3.0F, 0.0F, false);

		ring17 = new ModelRenderer(this);
		ring17.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front3.addChild(ring17);
		ring17.setTextureOffset(22, 207).addBox(24.1556F, -120.2731F, -22.0123F, 14.0F, 4.0F, 5.0F, 0.0F, false);

		bone45 = new ModelRenderer(this);
		bone45.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring17.addChild(bone45);
		setRotationAngle(bone45, -0.2618F, 0.0F, 0.0F);
		bone45.setTextureOffset(22, 207).addBox(-7.3444F, -2.2145F, -6.6635F, 13.0F, 4.0F, 2.0F, 0.0F, false);
		bone45.setTextureOffset(22, 207).addBox(-6.3444F, -0.2145F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		bone46 = new ModelRenderer(this);
		bone46.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring17.addChild(bone46);
		setRotationAngle(bone46, 0.2618F, 0.0F, 0.0F);
		bone46.setTextureOffset(22, 207).addBox(-7.3444F, -1.7855F, -6.6635F, 13.0F, 1.0F, 2.0F, 0.0F, false);
		bone46.setTextureOffset(22, 207).addBox(-6.3444F, -1.7855F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		ring18 = new ModelRenderer(this);
		ring18.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front3.addChild(ring18);
		ring18.setTextureOffset(29, 205).addBox(25.7091F, -120.2731F, -21.9142F, 11.0F, 4.0F, 4.0F, 0.0F, false);

		bone47 = new ModelRenderer(this);
		bone47.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring18.addChild(bone47);
		setRotationAngle(bone47, -0.2618F, 0.0F, 0.0F);
		bone47.setTextureOffset(29, 205).addBox(-5.8221F, -2.2399F, -6.5687F, 10.0F, 4.0F, 2.0F, 0.0F, false);
		bone47.setTextureOffset(29, 205).addBox(-4.8221F, -0.2399F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		bone48 = new ModelRenderer(this);
		bone48.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring18.addChild(bone48);
		setRotationAngle(bone48, 0.2618F, 0.0F, 0.0F);
		bone48.setTextureOffset(29, 205).addBox(-5.7909F, -1.7601F, -6.5687F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		bone48.setTextureOffset(29, 205).addBox(-4.7909F, -1.7601F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		st_under3 = new ModelRenderer(this);
		st_under3.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station3.addChild(st_under3);
		setRotationAngle(st_under3, 1.309F, 0.0F, 0.0F);
		st_under3.setTextureOffset(3, 30).addBox(-28.2702F, -11.2279F, 4.1025F, 58.0F, 8.0F, 2.0F, 0.0F, false);
		st_under3.setTextureOffset(11, 63).addBox(-24.0F, -4.6316F, 5.2895F, 49.0F, 13.0F, 3.0F, 0.0F, false);
		st_under3.setTextureOffset(0, 103).addBox(-18.0F, 8.3684F, 5.2895F, 37.0F, 8.0F, 3.0F, 0.0F, false);
		st_under3.setTextureOffset(77, 54).addBox(-6.0F, 18.3684F, 2.2895F, 13.0F, 16.0F, 3.0F, 0.0F, false);
		st_under3.setTextureOffset(80, 70).addBox(-4.0F, 26.3684F, -0.7105F, 9.0F, 9.0F, 4.0F, 0.0F, false);
		st_under3.setTextureOffset(9, 99).addBox(-14.0F, 16.3684F, 5.2895F, 29.0F, 17.0F, 3.0F, 0.0F, false);

		back3 = new ModelRenderer(this);
		back3.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station3.addChild(back3);
		setRotationAngle(back3, 0.3491F, 0.0F, 0.0F);
		back3.setTextureOffset(11, 106).addBox(-12.0F, -4.895F, 17.2141F, 24.0F, 8.0F, 3.0F, 0.0F, false);
		back3.setTextureOffset(12, 113).addBox(-11.0F, 3.105F, 17.2141F, 20.0F, 8.0F, 3.0F, 0.0F, false);

		box3 = new ModelRenderer(this);
		box3.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station3.addChild(box3);
		setRotationAngle(box3, -0.0873F, 0.0F, 0.0F);
		box3.setTextureOffset(14, 106).addBox(-12.0F, -1.9924F, 20.5911F, 22.0F, 10.0F, 3.0F, 0.0F, false);
		box3.setTextureOffset(42, 69).addBox(-7.0F, 8.0076F, 11.5911F, 14.0F, 4.0F, 13.0F, 0.0F, false);

		trim3 = new ModelRenderer(this);
		trim3.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station3.addChild(trim3);
		setRotationAngle(trim3, -1.5708F, 0.0F, 0.0F);
		trim3.setTextureOffset(19, 58).addBox(-12.0F, -43.0F, -4.0F, 24.0F, 16.0F, 4.0F, 0.0F, false);

		station4 = new ModelRenderer(this);
		station4.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station4);
		setRotationAngle(station4, 0.0F, 2.618F, 0.0F);
		

		st_plain4 = new ModelRenderer(this);
		st_plain4.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station4.addChild(st_plain4);
		setRotationAngle(st_plain4, -1.309F, 0.0F, 0.0F);
		st_plain4.setTextureOffset(4, 63).addBox(-26.0F, -6.0751F, 1.1637F, 52.0F, 16.0F, 4.0F, 0.0F, false);
		st_plain4.setTextureOffset(33, 75).addBox(-10.0F, -23.0751F, -2.5863F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		st_plain4.setTextureOffset(4, 63).addBox(-17.0F, -25.0751F, 1.1637F, 34.0F, 19.0F, 4.0F, 0.0F, false);

		st_bevel4 = new ModelRenderer(this);
		st_bevel4.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station4.addChild(st_bevel4);
		setRotationAngle(st_bevel4, -0.6981F, 0.0F, 0.0F);
		st_bevel4.setTextureOffset(2, 31).addBox(-25.4375F, -3.4063F, 3.9348F, 55.0F, 4.0F, 4.0F, 0.0F, false);
		st_bevel4.setTextureOffset(4, 80).addBox(-23.5923F, -5.2695F, 5.4769F, 52.0F, 2.0F, 5.0F, 0.0F, false);
		st_bevel4.setTextureOffset(3, 15).addBox(-27.4375F, 0.5937F, 4.4348F, 58.0F, 3.0F, 2.0F, 0.0F, false);

		st_front4 = new ModelRenderer(this);
		st_front4.setRotationPoint(0.0F, -50.0F, -72.0F);
		station4.addChild(st_front4);
		st_front4.setTextureOffset(0, 35).addBox(-29.8452F, 4.0727F, 4.9399F, 59.0F, 5.0F, 4.0F, 0.0F, false);

		rotorbevel4 = new ModelRenderer(this);
		rotorbevel4.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front4.addChild(rotorbevel4);
		setRotationAngle(rotorbevel4, -0.2443F, 0.0F, 0.0F);
		rotorbevel4.setTextureOffset(54, 168).addBox(-5.0938F, -9.0F, 0.2862F, 11.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel4.setTextureOffset(54, 168).addBox(-4.0938F, -11.5F, 0.2862F, 9.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel4.setTextureOffset(54, 168).addBox(-4.5938F, -16.25F, -0.7138F, 10.0F, 3.0F, 4.0F, 0.0F, false);
		rotorbevel4.setTextureOffset(54, 168).addBox(-6.0F, -3.25F, -0.7138F, 13.0F, 4.0F, 6.0F, 0.0F, false);
		rotorbevel4.setTextureOffset(54, 168).addBox(-5.125F, -6.25F, 0.5362F, 11.0F, 2.0F, 3.0F, 0.0F, false);
		rotorbevel4.setTextureOffset(54, 168).addBox(-5.5625F, -4.25F, 0.2862F, 12.0F, 1.0F, 3.0F, 0.0F, false);

		ring19 = new ModelRenderer(this);
		ring19.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front4.addChild(ring19);
		ring19.setTextureOffset(22, 207).addBox(24.1556F, -120.2731F, -22.0123F, 14.0F, 4.0F, 5.0F, 0.0F, false);

		bone49 = new ModelRenderer(this);
		bone49.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring19.addChild(bone49);
		setRotationAngle(bone49, -0.2618F, 0.0F, 0.0F);
		bone49.setTextureOffset(22, 207).addBox(-7.3444F, -2.2145F, -6.6635F, 13.0F, 4.0F, 2.0F, 0.0F, false);
		bone49.setTextureOffset(22, 207).addBox(-6.3444F, -0.2145F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		bone50 = new ModelRenderer(this);
		bone50.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring19.addChild(bone50);
		setRotationAngle(bone50, 0.2618F, 0.0F, 0.0F);
		bone50.setTextureOffset(22, 207).addBox(-7.3444F, -1.7855F, -6.6635F, 13.0F, 1.0F, 2.0F, 0.0F, false);
		bone50.setTextureOffset(22, 207).addBox(-6.3444F, -1.7855F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		ring20 = new ModelRenderer(this);
		ring20.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front4.addChild(ring20);
		ring20.setTextureOffset(29, 205).addBox(25.7091F, -120.2731F, -21.9142F, 11.0F, 4.0F, 4.0F, 0.0F, false);

		bone51 = new ModelRenderer(this);
		bone51.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring20.addChild(bone51);
		setRotationAngle(bone51, -0.2618F, 0.0F, 0.0F);
		bone51.setTextureOffset(29, 205).addBox(-5.8221F, -2.2399F, -6.5687F, 10.0F, 4.0F, 2.0F, 0.0F, false);
		bone51.setTextureOffset(29, 205).addBox(-4.8221F, -0.2399F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		bone52 = new ModelRenderer(this);
		bone52.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring20.addChild(bone52);
		setRotationAngle(bone52, 0.2618F, 0.0F, 0.0F);
		bone52.setTextureOffset(29, 205).addBox(-5.7909F, -1.7601F, -6.5687F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		bone52.setTextureOffset(29, 205).addBox(-4.7909F, -1.7601F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		st_under4 = new ModelRenderer(this);
		st_under4.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station4.addChild(st_under4);
		setRotationAngle(st_under4, 1.309F, 0.0F, 0.0F);
		st_under4.setTextureOffset(3, 30).addBox(-28.2702F, -11.2279F, 4.1025F, 58.0F, 8.0F, 2.0F, 0.0F, false);
		st_under4.setTextureOffset(11, 63).addBox(-24.0F, -4.6316F, 5.2895F, 49.0F, 13.0F, 3.0F, 0.0F, false);
		st_under4.setTextureOffset(0, 103).addBox(-18.0F, 8.3684F, 5.2895F, 37.0F, 8.0F, 3.0F, 0.0F, false);
		st_under4.setTextureOffset(77, 54).addBox(-6.0F, 18.3684F, 2.2895F, 13.0F, 16.0F, 3.0F, 0.0F, false);
		st_under4.setTextureOffset(80, 70).addBox(-4.0F, 26.3684F, -0.7105F, 9.0F, 9.0F, 4.0F, 0.0F, false);
		st_under4.setTextureOffset(9, 99).addBox(-14.0F, 16.3684F, 5.2895F, 29.0F, 17.0F, 3.0F, 0.0F, false);

		back4 = new ModelRenderer(this);
		back4.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station4.addChild(back4);
		setRotationAngle(back4, 0.3491F, 0.0F, 0.0F);
		back4.setTextureOffset(11, 106).addBox(-12.0F, -4.895F, 17.2141F, 24.0F, 8.0F, 3.0F, 0.0F, false);
		back4.setTextureOffset(12, 113).addBox(-11.0F, 3.105F, 17.2141F, 20.0F, 8.0F, 3.0F, 0.0F, false);

		box4 = new ModelRenderer(this);
		box4.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station4.addChild(box4);
		setRotationAngle(box4, -0.0873F, 0.0F, 0.0F);
		box4.setTextureOffset(14, 106).addBox(-12.0F, -1.9924F, 20.5911F, 22.0F, 10.0F, 3.0F, 0.0F, false);
		box4.setTextureOffset(42, 69).addBox(-7.0F, 8.0076F, 11.5911F, 14.0F, 4.0F, 13.0F, 0.0F, false);

		trim4 = new ModelRenderer(this);
		trim4.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station4.addChild(trim4);
		setRotationAngle(trim4, -1.5708F, 0.0F, 0.0F);
		trim4.setTextureOffset(19, 58).addBox(-12.0F, -43.0F, -4.0F, 24.0F, 16.0F, 4.0F, 0.0F, false);

		station5 = new ModelRenderer(this);
		station5.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station5);
		setRotationAngle(station5, 0.0F, -2.618F, 0.0F);
		

		st_plain5 = new ModelRenderer(this);
		st_plain5.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station5.addChild(st_plain5);
		setRotationAngle(st_plain5, -1.309F, 0.0F, 0.0F);
		st_plain5.setTextureOffset(4, 63).addBox(-26.0F, -6.0751F, 1.1637F, 52.0F, 16.0F, 4.0F, 0.0F, false);
		st_plain5.setTextureOffset(33, 75).addBox(-10.0F, -23.0751F, -2.5863F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		st_plain5.setTextureOffset(4, 63).addBox(-17.0F, -25.0751F, 1.1637F, 34.0F, 19.0F, 4.0F, 0.0F, false);

		st_bevel5 = new ModelRenderer(this);
		st_bevel5.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station5.addChild(st_bevel5);
		setRotationAngle(st_bevel5, -0.6981F, 0.0F, 0.0F);
		st_bevel5.setTextureOffset(2, 31).addBox(-25.4375F, -3.4063F, 3.9348F, 55.0F, 4.0F, 4.0F, 0.0F, false);
		st_bevel5.setTextureOffset(4, 80).addBox(-23.5923F, -5.2695F, 5.4769F, 52.0F, 2.0F, 5.0F, 0.0F, false);
		st_bevel5.setTextureOffset(3, 15).addBox(-27.4375F, 0.5937F, 4.4348F, 58.0F, 3.0F, 2.0F, 0.0F, false);

		st_front5 = new ModelRenderer(this);
		st_front5.setRotationPoint(0.0F, -50.0F, -72.0F);
		station5.addChild(st_front5);
		st_front5.setTextureOffset(0, 35).addBox(-29.8452F, 4.0727F, 4.9399F, 59.0F, 5.0F, 4.0F, 0.0F, false);

		rotorbevel5 = new ModelRenderer(this);
		rotorbevel5.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front5.addChild(rotorbevel5);
		setRotationAngle(rotorbevel5, -0.2443F, 0.0F, 0.0F);
		rotorbevel5.setTextureOffset(54, 168).addBox(-5.0938F, -9.0F, 0.2862F, 11.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel5.setTextureOffset(54, 168).addBox(-4.0938F, -11.5F, 0.2862F, 9.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel5.setTextureOffset(54, 168).addBox(-4.5938F, -16.25F, -0.7138F, 10.0F, 3.0F, 4.0F, 0.0F, false);
		rotorbevel5.setTextureOffset(54, 168).addBox(-6.0F, -3.25F, -0.7138F, 13.0F, 4.0F, 6.0F, 0.0F, false);
		rotorbevel5.setTextureOffset(54, 168).addBox(-5.125F, -6.25F, 0.5362F, 11.0F, 2.0F, 3.0F, 0.0F, false);
		rotorbevel5.setTextureOffset(54, 168).addBox(-5.5625F, -4.25F, 0.2862F, 12.0F, 1.0F, 3.0F, 0.0F, false);

		ring21 = new ModelRenderer(this);
		ring21.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front5.addChild(ring21);
		ring21.setTextureOffset(22, 207).addBox(24.1556F, -120.2731F, -22.0123F, 14.0F, 4.0F, 5.0F, 0.0F, false);

		bone53 = new ModelRenderer(this);
		bone53.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring21.addChild(bone53);
		setRotationAngle(bone53, -0.2618F, 0.0F, 0.0F);
		bone53.setTextureOffset(22, 207).addBox(-7.3444F, -2.2145F, -6.6635F, 13.0F, 4.0F, 2.0F, 0.0F, false);
		bone53.setTextureOffset(22, 207).addBox(-6.3444F, -0.2145F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		bone54 = new ModelRenderer(this);
		bone54.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring21.addChild(bone54);
		setRotationAngle(bone54, 0.2618F, 0.0F, 0.0F);
		bone54.setTextureOffset(22, 207).addBox(-7.3444F, -1.7855F, -6.6635F, 13.0F, 1.0F, 2.0F, 0.0F, false);
		bone54.setTextureOffset(22, 207).addBox(-6.3444F, -1.7855F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		ring22 = new ModelRenderer(this);
		ring22.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front5.addChild(ring22);
		ring22.setTextureOffset(29, 205).addBox(25.7091F, -120.2731F, -21.9142F, 11.0F, 4.0F, 4.0F, 0.0F, false);

		bone55 = new ModelRenderer(this);
		bone55.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring22.addChild(bone55);
		setRotationAngle(bone55, -0.2618F, 0.0F, 0.0F);
		bone55.setTextureOffset(29, 205).addBox(-5.8221F, -2.2399F, -6.5687F, 10.0F, 4.0F, 2.0F, 0.0F, false);
		bone55.setTextureOffset(29, 205).addBox(-4.8221F, -0.2399F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		bone56 = new ModelRenderer(this);
		bone56.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring22.addChild(bone56);
		setRotationAngle(bone56, 0.2618F, 0.0F, 0.0F);
		bone56.setTextureOffset(29, 205).addBox(-5.7909F, -1.7601F, -6.5687F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		bone56.setTextureOffset(29, 205).addBox(-4.7909F, -1.7601F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		st_under5 = new ModelRenderer(this);
		st_under5.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station5.addChild(st_under5);
		setRotationAngle(st_under5, 1.309F, 0.0F, 0.0F);
		st_under5.setTextureOffset(3, 30).addBox(-28.2702F, -11.2279F, 4.1025F, 58.0F, 8.0F, 2.0F, 0.0F, false);
		st_under5.setTextureOffset(11, 63).addBox(-24.0F, -4.6316F, 5.2895F, 49.0F, 13.0F, 3.0F, 0.0F, false);
		st_under5.setTextureOffset(0, 103).addBox(-18.0F, 8.3684F, 5.2895F, 37.0F, 8.0F, 3.0F, 0.0F, false);
		st_under5.setTextureOffset(77, 54).addBox(-6.0F, 18.3684F, 2.2895F, 13.0F, 16.0F, 3.0F, 0.0F, false);
		st_under5.setTextureOffset(80, 70).addBox(-4.0F, 26.3684F, -0.7105F, 9.0F, 9.0F, 4.0F, 0.0F, false);
		st_under5.setTextureOffset(9, 99).addBox(-14.0F, 16.3684F, 5.2895F, 29.0F, 17.0F, 3.0F, 0.0F, false);

		back5 = new ModelRenderer(this);
		back5.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station5.addChild(back5);
		setRotationAngle(back5, 0.3491F, 0.0F, 0.0F);
		back5.setTextureOffset(11, 106).addBox(-12.0F, -4.895F, 17.2141F, 24.0F, 8.0F, 3.0F, 0.0F, false);
		back5.setTextureOffset(12, 113).addBox(-11.0F, 3.105F, 17.2141F, 20.0F, 8.0F, 3.0F, 0.0F, false);

		box5 = new ModelRenderer(this);
		box5.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station5.addChild(box5);
		setRotationAngle(box5, -0.0873F, 0.0F, 0.0F);
		box5.setTextureOffset(14, 106).addBox(-12.0F, -1.9924F, 20.5911F, 22.0F, 10.0F, 3.0F, 0.0F, false);
		box5.setTextureOffset(42, 69).addBox(-7.0F, 8.0076F, 11.5911F, 14.0F, 4.0F, 13.0F, 0.0F, false);

		trim5 = new ModelRenderer(this);
		trim5.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station5.addChild(trim5);
		setRotationAngle(trim5, -1.5708F, 0.0F, 0.0F);
		trim5.setTextureOffset(19, 58).addBox(-12.0F, -43.0F, -4.0F, 24.0F, 16.0F, 4.0F, 0.0F, false);

		station6 = new ModelRenderer(this);
		station6.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station6);
		setRotationAngle(station6, 0.0F, -1.5708F, 0.0F);
		

		st_plain6 = new ModelRenderer(this);
		st_plain6.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		station6.addChild(st_plain6);
		setRotationAngle(st_plain6, -1.309F, 0.0F, 0.0F);
		st_plain6.setTextureOffset(4, 63).addBox(-26.0F, -6.0751F, 1.1637F, 52.0F, 16.0F, 4.0F, 0.0F, false);
		st_plain6.setTextureOffset(33, 75).addBox(-10.0F, -23.0751F, -2.5863F, 20.0F, 4.0F, 4.0F, 0.0F, false);
		st_plain6.setTextureOffset(4, 63).addBox(-17.0F, -25.0751F, 1.1637F, 34.0F, 19.0F, 4.0F, 0.0F, false);

		st_bevel6 = new ModelRenderer(this);
		st_bevel6.setRotationPoint(-2.4077F, -51.2809F, -67.6473F);
		station6.addChild(st_bevel6);
		setRotationAngle(st_bevel6, -0.6981F, 0.0F, 0.0F);
		st_bevel6.setTextureOffset(2, 31).addBox(-25.4375F, -3.4063F, 3.9348F, 55.0F, 4.0F, 4.0F, 0.0F, false);
		st_bevel6.setTextureOffset(4, 80).addBox(-23.5923F, -5.2695F, 5.4769F, 52.0F, 2.0F, 5.0F, 0.0F, false);
		st_bevel6.setTextureOffset(3, 15).addBox(-27.4375F, 0.5937F, 4.4348F, 58.0F, 3.0F, 2.0F, 0.0F, false);

		st_front6 = new ModelRenderer(this);
		st_front6.setRotationPoint(0.0F, -50.0F, -72.0F);
		station6.addChild(st_front6);
		st_front6.setTextureOffset(0, 35).addBox(-29.8452F, 4.0727F, 4.9399F, 59.0F, 5.0F, 4.0F, 0.0F, false);

		rotorbevel6 = new ModelRenderer(this);
		rotorbevel6.setRotationPoint(-0.4375F, -11.7731F, 41.0615F);
		st_front6.addChild(rotorbevel6);
		setRotationAngle(rotorbevel6, -0.2443F, 0.0F, 0.0F);
		rotorbevel6.setTextureOffset(54, 168).addBox(-5.0938F, -9.0F, 0.2862F, 11.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel6.setTextureOffset(54, 168).addBox(-4.0938F, -11.5F, 0.2862F, 9.0F, 1.0F, 3.0F, 0.0F, false);
		rotorbevel6.setTextureOffset(54, 168).addBox(-4.5938F, -16.25F, -0.7138F, 10.0F, 3.0F, 4.0F, 0.0F, false);
		rotorbevel6.setTextureOffset(54, 168).addBox(-6.0F, -3.25F, -0.7138F, 13.0F, 4.0F, 6.0F, 0.0F, false);
		rotorbevel6.setTextureOffset(54, 168).addBox(-5.125F, -6.25F, 0.5362F, 11.0F, 2.0F, 3.0F, 0.0F, false);
		rotorbevel6.setTextureOffset(54, 168).addBox(-5.5625F, -4.25F, 0.2862F, 12.0F, 1.0F, 3.0F, 0.0F, false);

		ring23 = new ModelRenderer(this);
		ring23.setRotationPoint(-31.1875F, 89.0F, 62.75F);
		st_front6.addChild(ring23);
		ring23.setTextureOffset(22, 207).addBox(24.1556F, -120.2731F, -22.0123F, 14.0F, 4.0F, 5.0F, 0.0F, false);

		bone57 = new ModelRenderer(this);
		bone57.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring23.addChild(bone57);
		setRotationAngle(bone57, -0.2618F, 0.0F, 0.0F);
		bone57.setTextureOffset(22, 207).addBox(-7.3444F, -2.2145F, -6.6635F, 13.0F, 4.0F, 2.0F, 0.0F, false);
		bone57.setTextureOffset(22, 207).addBox(-6.3444F, -0.2145F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		bone58 = new ModelRenderer(this);
		bone58.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring23.addChild(bone58);
		setRotationAngle(bone58, 0.2618F, 0.0F, 0.0F);
		bone58.setTextureOffset(22, 207).addBox(-7.3444F, -1.7855F, -6.6635F, 13.0F, 1.0F, 2.0F, 0.0F, false);
		bone58.setTextureOffset(22, 207).addBox(-6.3444F, -1.7855F, -4.6635F, 11.0F, 2.0F, 4.0F, 0.0F, false);

		ring24 = new ModelRenderer(this);
		ring24.setRotationPoint(-31.1875F, 83.0F, 65.25F);
		st_front6.addChild(ring24);
		ring24.setTextureOffset(29, 205).addBox(25.7091F, -120.2731F, -21.9142F, 11.0F, 4.0F, 4.0F, 0.0F, false);

		bone59 = new ModelRenderer(this);
		bone59.setRotationPoint(32.0F, -116.2731F, -15.1137F);
		ring24.addChild(bone59);
		setRotationAngle(bone59, -0.2618F, 0.0F, 0.0F);
		bone59.setTextureOffset(29, 205).addBox(-5.8221F, -2.2399F, -6.5687F, 10.0F, 4.0F, 2.0F, 0.0F, false);
		bone59.setTextureOffset(29, 205).addBox(-4.8221F, -0.2399F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		bone60 = new ModelRenderer(this);
		bone60.setRotationPoint(32.0F, -120.2731F, -15.1137F);
		ring24.addChild(bone60);
		setRotationAngle(bone60, 0.2618F, 0.0F, 0.0F);
		bone60.setTextureOffset(29, 205).addBox(-5.7909F, -1.7601F, -6.5687F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		bone60.setTextureOffset(29, 205).addBox(-4.7909F, -1.7601F, -4.5687F, 8.0F, 2.0F, 3.0F, 0.0F, false);

		st_under6 = new ModelRenderer(this);
		st_under6.setRotationPoint(-0.575F, -34.0586F, -57.2766F);
		station6.addChild(st_under6);
		setRotationAngle(st_under6, 1.309F, 0.0F, 0.0F);
		st_under6.setTextureOffset(3, 30).addBox(-28.2702F, -11.2279F, 4.1025F, 58.0F, 8.0F, 2.0F, 0.0F, false);
		st_under6.setTextureOffset(11, 63).addBox(-24.0F, -4.6316F, 5.2895F, 49.0F, 13.0F, 3.0F, 0.0F, false);
		st_under6.setTextureOffset(0, 103).addBox(-18.0F, 8.3684F, 5.2895F, 37.0F, 8.0F, 3.0F, 0.0F, false);
		st_under6.setTextureOffset(77, 54).addBox(-6.0F, 18.3684F, 2.2895F, 13.0F, 16.0F, 3.0F, 0.0F, false);
		st_under6.setTextureOffset(80, 70).addBox(-4.0F, 26.3684F, -0.7105F, 9.0F, 9.0F, 4.0F, 0.0F, false);
		st_under6.setTextureOffset(9, 99).addBox(-14.0F, 16.3684F, 5.2895F, 29.0F, 17.0F, 3.0F, 0.0F, false);

		back6 = new ModelRenderer(this);
		back6.setRotationPoint(0.0F, -20.714F, -40.1042F);
		station6.addChild(back6);
		setRotationAngle(back6, 0.3491F, 0.0F, 0.0F);
		back6.setTextureOffset(11, 106).addBox(-12.0F, -4.895F, 17.2141F, 24.0F, 8.0F, 3.0F, 0.0F, false);
		back6.setTextureOffset(12, 113).addBox(-11.0F, 3.105F, 17.2141F, 20.0F, 8.0F, 3.0F, 0.0F, false);

		box6 = new ModelRenderer(this);
		box6.setRotationPoint(0.0F, -15.9761F, -40.8165F);
		station6.addChild(box6);
		setRotationAngle(box6, -0.0873F, 0.0F, 0.0F);
		box6.setTextureOffset(14, 106).addBox(-12.0F, -1.9924F, 20.5911F, 22.0F, 10.0F, 3.0F, 0.0F, false);
		box6.setTextureOffset(42, 69).addBox(-7.0F, 8.0076F, 11.5911F, 14.0F, 4.0F, 13.0F, 0.0F, false);

		trim6 = new ModelRenderer(this);
		trim6.setRotationPoint(0.0F, 0.0068F, -64.2239F);
		station6.addChild(trim6);
		setRotationAngle(trim6, -1.5708F, 0.0F, 0.0F);
		trim6.setTextureOffset(19, 58).addBox(-12.0F, -43.0F, -4.0F, 24.0F, 16.0F, 4.0F, 0.0F, false);

		controls = new ModelRenderer(this);
		controls.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(controls, 0.0F, -0.5236F, 0.0F);
		

		controls1 = new ModelRenderer(this);
		controls1.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls1);
		setRotationAngle(controls1, 0.0F, -0.5236F, 0.0F);
		

		base_plate_1 = new ModelRenderer(this);
		base_plate_1.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls1.addChild(base_plate_1);
		setRotationAngle(base_plate_1, -1.309F, 0.0F, 0.0F);
		base_plate_1.setTextureOffset(4, 117).addBox(-15.0F, -4.0751F, 0.1637F, 4.0F, 8.0F, 1.0F, 0.0F, false);
		base_plate_1.setTextureOffset(4, 117).addBox(5.75F, -7.0751F, 0.1637F, 8.0F, 11.0F, 1.0F, 0.0F, false);
		base_plate_1.setTextureOffset(4, 117).addBox(-12.0F, -2.0751F, -1.8363F, 1.0F, 4.0F, 2.0F, 0.0F, false);
		base_plate_1.setTextureOffset(4, 117).addBox(-15.0F, -2.0751F, -1.8363F, 1.0F, 4.0F, 2.0F, 0.0F, false);
		base_plate_1.setTextureOffset(4, 117).addBox(-7.0F, -14.8251F, -0.3363F, 14.0F, 5.0F, 1.0F, 0.0F, false);
		base_plate_1.setTextureOffset(52, 165).addBox(-8.0F, -17.0751F, 0.1637F, 16.0F, 8.0F, 1.0F, 0.0F, false);
		base_plate_1.setTextureOffset(73, 167).addBox(-3.0F, -9.0751F, 0.1637F, 6.0F, 6.0F, 1.0F, 0.0F, false);

		turn_dial = new ModelRenderer(this);
		turn_dial.setRotationPoint(-39.5F, 86.8792F, 56.5184F);
		base_plate_1.addChild(turn_dial);
		

		guide = new ModelRenderer(this);
		guide.setRotationPoint(0.0F, 0.0F, 0.0F);
		turn_dial.addChild(guide);
		guide.setTextureOffset(7, 9).addBox(38.5F, -94.9542F, -56.6047F, 2.0F, 4.0F, 1.0F, 0.0F, false);
		guide.setTextureOffset(7, 9).addBox(37.5F, -94.4542F, -56.6047F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		knob4 = new ModelRenderer(this);
		knob4.setRotationPoint(39.75F, -92.8292F, -56.1672F);
		turn_dial.addChild(knob4);
		setRotationAngle(knob4, 0.0F, 0.0F, -0.9599F);
		knob4.setTextureOffset(8, 121).addBox(-0.75F, -1.4688F, -0.9375F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		knob4.setTextureOffset(8, 121).addBox(-1.25F, -0.9688F, -0.9375F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		knob4.setTextureOffset(77, 93).addBox(-0.75F, -0.9688F, -1.4375F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		knob4.setTextureOffset(164, 80).addBox(-1.75F, -0.5938F, -1.1875F, 2.0F, 1.0F, 3.0F, 0.0F, false);

		buttonstrip = new ModelRenderer(this);
		buttonstrip.setRotationPoint(-32.0F, 93.3792F, 56.7684F);
		base_plate_1.addChild(buttonstrip);
		buttonstrip.setTextureOffset(48, 169).addBox(15.0F, -87.7042F, -56.1047F, 17.0F, 3.0F, 1.0F, 0.0F, false);
		buttonstrip.setTextureOffset(51, 170).addBox(32.0F, -87.7042F, -56.1047F, 17.0F, 3.0F, 1.0F, 0.0F, false);

		buttons_lit = new ModelRenderer(this);
		buttons_lit.setRotationPoint(0.0F, 0.0F, 0.0F);
		buttonstrip.addChild(buttons_lit);
		

		small_screen = new ModelRenderer(this);
		small_screen.setRotationPoint(9.0F, 0.1749F, -2.3363F);
		base_plate_1.addChild(small_screen);
		setRotationAngle(small_screen, 0.6109F, 0.0F, 0.0F);
		small_screen.setTextureOffset(7, 109).addBox(-2.75F, -4.0F, 0.5F, 7.0F, 8.0F, 10.0F, 0.0F, false);

		four_square = new ModelRenderer(this);
		four_square.setRotationPoint(-32.0F, 87.6292F, 57.0184F);
		base_plate_1.addChild(four_square);
		four_square.setTextureOffset(164, 42).addBox(21.75F, -91.9542F, -56.6047F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		four_square.setTextureOffset(130, 60).addBox(24.75F, -91.9542F, -56.6047F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		four_square.setTextureOffset(149, 75).addBox(24.75F, -94.9542F, -56.6047F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		four_square.setTextureOffset(129, 42).addBox(21.75F, -94.9542F, -56.6047F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		indicator = new ModelRenderer(this);
		indicator.setRotationPoint(-7.25F, 2.7999F, 0.5387F);
		base_plate_1.addChild(indicator);
		setRotationAngle(indicator, -1.0472F, 0.0F, 0.0F);
		indicator.setTextureOffset(34, 111).addBox(-1.5F, -1.375F, -1.125F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		indicator2 = new ModelRenderer(this);
		indicator2.setRotationPoint(-2.25F, 2.7999F, 0.5387F);
		base_plate_1.addChild(indicator2);
		setRotationAngle(indicator2, -1.0472F, 0.0F, 0.0F);
		indicator2.setTextureOffset(34, 111).addBox(-1.5F, -1.375F, -1.125F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		indicator3 = new ModelRenderer(this);
		indicator3.setRotationPoint(2.75F, 2.7999F, 0.5387F);
		base_plate_1.addChild(indicator3);
		setRotationAngle(indicator3, -1.0472F, 0.0F, 0.0F);
		indicator3.setTextureOffset(34, 111).addBox(-1.5F, -1.375F, -1.125F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		coms = new ModelRenderer(this);
		coms.setRotationPoint(0.0F, -13.0751F, -0.0863F);
		base_plate_1.addChild(coms);
		setRotationAngle(coms, -0.7854F, 0.0F, 0.0F);
		coms.setTextureOffset(7, 92).addBox(-7.5F, -3.25F, -2.0F, 15.0F, 5.0F, 1.0F, 0.0F, false);

		com_needle_rotate_z = new ModelRenderer(this);
		com_needle_rotate_z.setRotationPoint(0.0F, -49.75F, -28.5F);
		coms.addChild(com_needle_rotate_z);
		setRotationAngle(com_needle_rotate_z, 0.0F, 0.0F, 0.0873F);
		com_needle_rotate_z.setTextureOffset(130, 40).addBox(-0.5F, 48.5F, 28.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);

		fast_return_rotate_x = new ModelRenderer(this);
		fast_return_rotate_x.setRotationPoint(-12.8438F, 0.0299F, 0.1605F);
		base_plate_1.addChild(fast_return_rotate_x);
		fast_return_rotate_x.setTextureOffset(8, 8).addBox(-1.1563F, -1.99F, -2.1998F, 2.0F, 4.0F, 4.0F, 0.0F, false);

		bone68 = new ModelRenderer(this);
		bone68.setRotationPoint(-0.1563F, 0.01F, -0.1998F);
		fast_return_rotate_x.addChild(bone68);
		setRotationAngle(bone68, -0.7854F, 0.0F, 0.0F);
		bone68.setTextureOffset(8, 8).addBox(-0.5F, -2.5F, -2.5F, 1.0F, 5.0F, 5.0F, 0.0F, false);

		controls2 = new ModelRenderer(this);
		controls2.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls2);
		setRotationAngle(controls2, 0.0F, -1.5708F, 0.0F);
		

		base_plate_2 = new ModelRenderer(this);
		base_plate_2.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls2.addChild(base_plate_2);
		setRotationAngle(base_plate_2, -1.309F, 0.0F, 0.0F);
		base_plate_2.setTextureOffset(69, 208).addBox(-19.0F, 0.9249F, 0.1637F, 8.0F, 8.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(68, 209).addBox(11.0F, 0.9249F, 0.1637F, 8.0F, 8.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(48, 208).addBox(-9.0F, 0.9249F, 0.1637F, 18.0F, 8.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(6, 77).addBox(-6.0F, 3.9249F, -0.3363F, 12.0F, 4.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(10, 8).addBox(-5.5F, 4.4249F, -0.5863F, 11.0F, 3.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(80, 213).addBox(-11.0F, 3.4249F, 0.9137F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(80, 213).addBox(9.0F, 3.4249F, 0.9137F, 2.0F, 3.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(29, 202).addBox(-9.0F, -17.0751F, 0.1637F, 18.0F, 12.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(8, 69).addBox(-8.0F, -16.0751F, -0.2113F, 4.0F, 10.0F, 1.0F, 0.0F, false);
		base_plate_2.setTextureOffset(8, 69).addBox(4.0F, -16.0751F, -0.2113F, 4.0F, 10.0F, 1.0F, 0.0F, false);

		lever = new ModelRenderer(this);
		lever.setRotationPoint(-61.5F, 91.3792F, 56.2684F);
		base_plate_2.addChild(lever);
		lever.setTextureOffset(61, 117).addBox(43.5F, -89.4542F, -56.6047F, 6.0F, 6.0F, 1.0F, 0.0F, false);
		lever.setTextureOffset(10, 10).addBox(44.5F, -88.4542F, -57.1047F, 4.0F, 4.0F, 1.0F, 0.0F, false);

		lever_turn = new ModelRenderer(this);
		lever_turn.setRotationPoint(46.5F, -86.3292F, -58.6047F);
		lever.addChild(lever_turn);
		setRotationAngle(lever_turn, 0.0F, 0.0F, 0.0F);
		lever_turn.setTextureOffset(10, 10).addBox(-0.5F, -1.375F, -1.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		lever_turn.setTextureOffset(10, 10).addBox(-1.0F, -1.125F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		lever2 = new ModelRenderer(this);
		lever2.setRotationPoint(-31.5F, 91.3792F, 56.2684F);
		base_plate_2.addChild(lever2);
		lever2.setTextureOffset(61, 117).addBox(43.5F, -89.4542F, -56.6047F, 6.0F, 6.0F, 1.0F, 0.0F, false);
		lever2.setTextureOffset(10, 10).addBox(44.5F, -88.4542F, -57.1047F, 4.0F, 4.0F, 1.0F, 0.0F, false);

		door_turn = new ModelRenderer(this);
		door_turn.setRotationPoint(46.5F, -86.5792F, -58.6047F);
		lever2.addChild(door_turn);
		setRotationAngle(door_turn, 0.0F, 0.0F, 0.0F);
		door_turn.setTextureOffset(10, 10).addBox(-0.5F, -1.125F, -1.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		door_turn.setTextureOffset(10, 10).addBox(-1.0F, -0.875F, -0.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		wide_strip = new ModelRenderer(this);
		wide_strip.setRotationPoint(-4.5F, -2.5751F, 0.4137F);
		base_plate_2.addChild(wide_strip);
		setRotationAngle(wide_strip, 0.7854F, 0.0F, 0.0F);
		wide_strip.setTextureOffset(16, 210).addBox(-6.5F, -2.25F, -0.5F, 22.0F, 4.0F, 3.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(-5.75F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(-2.75F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(0.25F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(3.25F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(6.25F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(9.25F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		wide_strip.setTextureOffset(130, 42).addBox(12.25F, -1.75F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		sonic_port = new ModelRenderer(this);
		sonic_port.setRotationPoint(-32.0F, 91.1292F, 56.7684F);
		base_plate_2.addChild(sonic_port);
		sonic_port.setTextureOffset(2, 111).addBox(30.0F, -103.4542F, -57.6047F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		sonic_port.setTextureOffset(5, 75).addBox(30.0F, -99.4542F, -58.1047F, 4.0F, 1.0F, 2.0F, 0.0F, false);
		sonic_port.setTextureOffset(5, 75).addBox(30.0F, -104.4542F, -58.1047F, 4.0F, 1.0F, 2.0F, 0.0F, false);
		sonic_port.setTextureOffset(5, 75).addBox(29.0F, -104.4542F, -58.1047F, 1.0F, 6.0F, 2.0F, 0.0F, false);
		sonic_port.setTextureOffset(5, 75).addBox(34.0F, -104.4542F, -58.1047F, 1.0F, 6.0F, 2.0F, 0.0F, false);

		controls3 = new ModelRenderer(this);
		controls3.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls3);
		setRotationAngle(controls3, 0.0F, -2.618F, 0.0F);
		

		base_plate_3 = new ModelRenderer(this);
		base_plate_3.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls3.addChild(base_plate_3);
		setRotationAngle(base_plate_3, -1.309F, 0.0F, 0.0F);
		

		facing = new ModelRenderer(this);
		facing.setRotationPoint(15.5F, 6.6749F, -0.0863F);
		base_plate_3.addChild(facing);
		

		joystick = new ModelRenderer(this);
		joystick.setRotationPoint(0.0F, 0.0F, 0.0F);
		facing.addChild(joystick);
		joystick.setTextureOffset(67, 165).addBox(-0.5F, -0.5F, -3.375F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		joystick.setTextureOffset(9, 124).addBox(-1.5F, -1.5F, -5.125F, 3.0F, 3.0F, 2.0F, 0.0F, false);

		base = new ModelRenderer(this);
		base.setRotationPoint(1.1071F, -0.2894F, 0.2976F);
		base_plate_3.addChild(base);
		base.setTextureOffset(75, 165).addBox(11.8929F, 2.2143F, -0.1339F, 5.0F, 7.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(74, 162).addBox(-17.1071F, -0.7857F, -0.1339F, 5.0F, 10.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(131, 44).addBox(12.3929F, 4.7143F, -0.5089F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(82, 157).addBox(-12.1071F, -6.7857F, -0.1339F, 2.0F, 16.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(82, 156).addBox(7.8929F, -6.7857F, -0.1339F, 2.0F, 16.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(5, 61).addBox(-6.1071F, -11.7857F, -0.8839F, 10.0F, 15.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(5, 203).addBox(-5.6071F, -0.0357F, -1.1339F, 9.0F, 2.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(78, 95).addBox(-16.1071F, 5.7143F, -0.6339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(78, 95).addBox(-16.1071F, 1.7143F, -0.6339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(78, 95).addBox(-10.8571F, -5.2857F, -0.6339F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(61, 165).addBox(-7.1071F, -13.7857F, -0.1339F, 12.0F, 7.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(80, 152).addBox(-10.1071F, -11.7857F, -0.1339F, 3.0F, 21.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(78, 151).addBox(4.8929F, -11.7857F, -0.1339F, 3.0F, 21.0F, 1.0F, 0.0F, false);
		base.setTextureOffset(62, 160).addBox(-7.1071F, -6.7857F, -0.1339F, 12.0F, 13.0F, 1.0F, 0.0F, false);

		pull_tab = new ModelRenderer(this);
		pull_tab.setRotationPoint(-14.6071F, 7.2143F, -0.3839F);
		base.addChild(pull_tab);
		pull_tab.setTextureOffset(42, 154).addBox(-0.5F, -0.5F, -1.25F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		pull_tab.setTextureOffset(110, 5).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		pull_tab2 = new ModelRenderer(this);
		pull_tab2.setRotationPoint(-14.6071F, 3.2143F, -2.3839F);
		base.addChild(pull_tab2);
		pull_tab2.setTextureOffset(37, 152).addBox(-0.5F, -0.5F, -1.25F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		pull_tab2.setTextureOffset(110, 5).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		pull_tab3 = new ModelRenderer(this);
		pull_tab3.setRotationPoint(-9.3571F, -3.5357F, -0.3839F);
		base.addChild(pull_tab3);
		pull_tab3.setTextureOffset(35, 151).addBox(-0.5F, -0.5F, -1.25F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		pull_tab3.setTextureOffset(110, 5).addBox(-1.0F, -1.0F, -2.25F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		randomizers = new ModelRenderer(this);
		randomizers.setRotationPoint(-8.5F, 67.4898F, 55.0291F);
		base_plate_3.addChild(randomizers);
		

		key_white1 = new ModelRenderer(this);
		key_white1.setRotationPoint(4.25F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white1);
		key_white1.setTextureOffset(3, 8).addBox(-1.0F, 0.0F, -0.5F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		key_black1 = new ModelRenderer(this);
		key_black1.setRotationPoint(5.25F, -62.8149F, -55.1154F);
		randomizers.addChild(key_black1);
		key_black1.setTextureOffset(4, 122).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		key_white2 = new ModelRenderer(this);
		key_white2.setRotationPoint(6.375F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white2);
		key_white2.setTextureOffset(3, 8).addBox(-1.0F, 0.0F, -0.5F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		key_black2 = new ModelRenderer(this);
		key_black2.setRotationPoint(7.5F, -62.8149F, -55.1154F);
		randomizers.addChild(key_black2);
		key_black2.setTextureOffset(4, 122).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		key_white3 = new ModelRenderer(this);
		key_white3.setRotationPoint(8.5F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white3);
		key_white3.setTextureOffset(3, 8).addBox(-1.0F, 0.0F, -0.5F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		key_white4 = new ModelRenderer(this);
		key_white4.setRotationPoint(10.5938F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white4);
		key_white4.setTextureOffset(3, 8).addBox(-1.0F, 0.0F, -0.5F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		key_black3 = new ModelRenderer(this);
		key_black3.setRotationPoint(11.75F, -62.8149F, -55.1154F);
		randomizers.addChild(key_black3);
		key_black3.setTextureOffset(4, 122).addBox(-0.5F, 0.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		key_white5 = new ModelRenderer(this);
		key_white5.setRotationPoint(12.75F, -62.8149F, -54.6154F);
		randomizers.addChild(key_white5);
		key_white5.setTextureOffset(3, 8).addBox(-1.0F, 0.0F, -0.5F, 2.0F, 5.0F, 1.0F, 0.0F, false);

		controls4 = new ModelRenderer(this);
		controls4.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls4);
		setRotationAngle(controls4, 0.0F, 2.618F, 0.0F);
		

		base_plate_4 = new ModelRenderer(this);
		base_plate_4.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls4.addChild(base_plate_4);
		setRotationAngle(base_plate_4, -1.309F, 0.0F, 0.0F);
		base_plate_4.setTextureOffset(12, 205).addBox(-14.0F, -7.0751F, 0.1637F, 9.0F, 4.0F, 1.0F, 0.0F, false);
		base_plate_4.setTextureOffset(12, 205).addBox(-14.0F, -1.0751F, 0.1637F, 9.0F, 4.0F, 1.0F, 0.0F, false);
		base_plate_4.setTextureOffset(12, 205).addBox(-14.0F, 4.9249F, 0.1637F, 9.0F, 4.0F, 1.0F, 0.0F, false);
		base_plate_4.setTextureOffset(12, 205).addBox(3.0F, -4.0751F, 0.1637F, 13.0F, 13.0F, 1.0F, 0.0F, false);
		base_plate_4.setTextureOffset(14, 193).addBox(-3.0F, -8.3251F, 0.7574F, 5.0F, 18.0F, 1.0F, 0.0F, false);

		coords_z = new ModelRenderer(this);
		coords_z.setRotationPoint(-12.5F, 6.3792F, 0.7684F);
		base_plate_4.addChild(coords_z);
		

		toggle_base = new ModelRenderer(this);
		toggle_base.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_z.addChild(toggle_base);
		toggle_base.setTextureOffset(113, 5).addBox(30.0F, -86.4542F, -57.6047F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		z_cord_rotate_x = new ModelRenderer(this);
		z_cord_rotate_x.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_z.addChild(z_cord_rotate_x);
		setRotationAngle(z_cord_rotate_x, 0.2182F, 0.0F, 0.0F);
		z_cord_rotate_x.setTextureOffset(7, 96).addBox(-0.5F, -0.8618F, -0.9489F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		coords_y = new ModelRenderer(this);
		coords_y.setRotationPoint(-12.5F, 0.3792F, 0.7684F);
		base_plate_4.addChild(coords_y);
		

		toggle_base2 = new ModelRenderer(this);
		toggle_base2.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_y.addChild(toggle_base2);
		toggle_base2.setTextureOffset(113, 5).addBox(30.0F, -86.4542F, -57.6047F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		y_cord_rotate_y = new ModelRenderer(this);
		y_cord_rotate_y.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_y.addChild(y_cord_rotate_y);
		setRotationAngle(y_cord_rotate_y, 0.2182F, 0.0F, 0.0F);
		y_cord_rotate_y.setTextureOffset(7, 96).addBox(-0.5F, -0.8618F, -0.9489F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		coords_x = new ModelRenderer(this);
		coords_x.setRotationPoint(-12.5F, -5.6208F, 0.7684F);
		base_plate_4.addChild(coords_x);
		

		toggle_base3 = new ModelRenderer(this);
		toggle_base3.setRotationPoint(-31.0F, 85.5F, 56.5F);
		coords_x.addChild(toggle_base3);
		toggle_base3.setTextureOffset(113, 5).addBox(30.0F, -86.4542F, -57.6047F, 2.0F, 3.0F, 1.0F, 0.0F, false);

		z_cord_rotate_x2 = new ModelRenderer(this);
		z_cord_rotate_x2.setRotationPoint(0.0F, 0.5458F, -0.6047F);
		coords_x.addChild(z_cord_rotate_x2);
		setRotationAngle(z_cord_rotate_x2, 0.2182F, 0.0F, 0.0F);
		z_cord_rotate_x2.setTextureOffset(7, 96).addBox(-0.5F, -0.8618F, -0.9489F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		radar = new ModelRenderer(this);
		radar.setRotationPoint(9.5F, 2.4249F, 0.2887F);
		base_plate_4.addChild(radar);
		

		bigslider = new ModelRenderer(this);
		bigslider.setRotationPoint(-0.5F, 0.4898F, -0.2209F);
		base_plate_4.addChild(bigslider);
		

		inc_slider_rotate_x = new ModelRenderer(this);
		inc_slider_rotate_x.setRotationPoint(0.5F, 1.0F, 14.0F);
		bigslider.addChild(inc_slider_rotate_x);
		

		slider_knob = new ModelRenderer(this);
		slider_knob.setRotationPoint(-32.0F, 83.8894F, 40.9893F);
		inc_slider_rotate_x.addChild(slider_knob);
		

		twist = new ModelRenderer(this);
		twist.setRotationPoint(31.5F, -83.9542F, -57.3547F);
		slider_knob.addChild(twist);
		setRotationAngle(twist, 0.0F, 0.0F, -0.7854F);
		twist.setTextureOffset(129, 38).addBox(-1.5F, -1.5F, -0.25F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		twist.setTextureOffset(129, 38).addBox(-1.0F, -1.0F, -0.75F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		twist.setTextureOffset(106, 37).addBox(-0.5F, -0.5F, 0.0F, 1.0F, 1.0F, 5.0F, 0.0F, false);

		track = new ModelRenderer(this);
		track.setRotationPoint(-31.5F, 90.8894F, 56.9893F);
		bigslider.addChild(track);
		track.setTextureOffset(32, 145).addBox(30.0F, -97.4542F, -56.6047F, 1.0F, 14.0F, 1.0F, 0.0F, false);
		track.setTextureOffset(32, 145).addBox(30.0F, -83.4542F, -56.6047F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		track.setTextureOffset(32, 145).addBox(32.0F, -97.4542F, -56.6047F, 1.0F, 14.0F, 1.0F, 0.0F, false);
		track.setTextureOffset(32, 145).addBox(30.0F, -98.4542F, -56.6047F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		track.setTextureOffset(22, 101).addBox(30.4688F, -97.4542F, -56.2297F, 2.0F, 14.0F, 1.0F, 0.0F, false);

		button_array = new ModelRenderer(this);
		button_array.setRotationPoint(-31.75F, 91.3792F, 56.7684F);
		base_plate_4.addChild(button_array);
		

		base_plate = new ModelRenderer(this);
		base_plate.setRotationPoint(0.0F, 0.0F, 0.75F);
		button_array.addChild(base_plate);
		base_plate.setTextureOffset(17, 196).addBox(23.25F, -108.4542F, -56.6047F, 17.0F, 8.0F, 1.0F, 0.0F, false);

		controls5 = new ModelRenderer(this);
		controls5.setRotationPoint(0.0F, 0.0F, -0.75F);
		controls.addChild(controls5);
		setRotationAngle(controls5, 0.0F, 1.5708F, 0.0F);
		

		base_plate_5 = new ModelRenderer(this);
		base_plate_5.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls5.addChild(base_plate_5);
		setRotationAngle(base_plate_5, -1.309F, 0.0F, 0.0F);
		

		landing_type_rotate_x = new ModelRenderer(this);
		landing_type_rotate_x.setRotationPoint(5.5F, -1.0751F, 26.4137F);
		base_plate_5.addChild(landing_type_rotate_x);
		setRotationAngle(landing_type_rotate_x, 0.0873F, 0.0F, 0.0F);
		landing_type_rotate_x.setTextureOffset(130, 43).addBox(-2.0F, -0.5F, -26.5F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		knob = new ModelRenderer(this);
		knob.setRotationPoint(13.5191F, 6.622F, 0.2493F);
		base_plate_5.addChild(knob);
		setRotationAngle(knob, 0.0F, 0.0F, -0.7854F);
		knob.setTextureOffset(75, 123).addBox(-1.5F, -1.5F, -0.75F, 3.0F, 3.0F, 2.0F, 0.0F, false);
		knob.setTextureOffset(63, 100).addBox(-1.0F, -2.0F, -0.5F, 2.0F, 4.0F, 1.0F, 0.0F, false);
		knob.setTextureOffset(63, 100).addBox(-2.0F, -1.0F, -0.5F, 4.0F, 2.0F, 1.0F, 0.0F, false);

		angeled_panel = new ModelRenderer(this);
		angeled_panel.setRotationPoint(-32.0F, 92.8792F, 58.4871F);
		base_plate_5.addChild(angeled_panel);
		angeled_panel.setTextureOffset(74, 163).addBox(18.0F, -99.4542F, -57.5735F, 4.0F, 9.0F, 2.0F, 0.0F, false);
		angeled_panel.setTextureOffset(68, 164).addBox(14.0F, -90.4542F, -57.6047F, 8.0F, 8.0F, 2.0F, 0.0F, false);

		dummy_buttons = new ModelRenderer(this);
		dummy_buttons.setRotationPoint(-4.0F, -2.0F, -1.0F);
		angeled_panel.addChild(dummy_buttons);
		

		dark_buttons = new ModelRenderer(this);
		dark_buttons.setRotationPoint(-4.0F, -1.5F, 0.0F);
		dummy_buttons.addChild(dark_buttons);
		dark_buttons.setTextureOffset(9, 94).addBox(25.0F, -81.7042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dark_buttons.setTextureOffset(9, 94).addBox(22.5F, -81.7042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dark_buttons.setTextureOffset(9, 94).addBox(25.0F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dark_buttons.setTextureOffset(9, 94).addBox(22.5F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		dark_buttons.setTextureOffset(9, 94).addBox(20.0F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		glow_warning_lamp = new LightModelRenderer(this);
		glow_warning_lamp.setRotationPoint(3.5F, 1.25F, 0.0F);
		dummy_buttons.addChild(glow_warning_lamp);
		glow_warning_lamp.setTextureOffset(131, 45).addBox(20.0F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		glow_blue2 = new LightModelRenderer(this);
		glow_blue2.setRotationPoint(3.5F, -1.5F, 0.0F);
		dummy_buttons.addChild(glow_blue2);
		glow_blue2.setTextureOffset(131, 59).addBox(20.0F, -92.7042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_blue2.setTextureOffset(131, 59).addBox(20.0F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		glow_white = new LightModelRenderer(this);
		glow_white.setRotationPoint(1.0F, -7.0F, 0.0F);
		dummy_buttons.addChild(glow_white);
		glow_white.setTextureOffset(112, 6).addBox(17.5F, -81.7042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_white.setTextureOffset(112, 6).addBox(20.0F, -81.7042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_white.setTextureOffset(112, 6).addBox(20.0F, -87.2042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_white.setTextureOffset(112, 6).addBox(22.5F, -89.9542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_white.setTextureOffset(112, 6).addBox(22.5F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		glow_yellow = new LightModelRenderer(this);
		glow_yellow.setRotationPoint(3.5F, -7.0F, 0.0F);
		dummy_buttons.addChild(glow_yellow);
		glow_yellow.setTextureOffset(164, 42).addBox(20.0F, -81.7042F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		glow_yellow.setTextureOffset(165, 42).addBox(17.5F, -84.4542F, -57.6047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		dummy_buttons7 = new ModelRenderer(this);
		dummy_buttons7.setRotationPoint(31.0F, -101.4542F, -58.6047F);
		angeled_panel.addChild(dummy_buttons7);
		setRotationAngle(dummy_buttons7, -0.8727F, 0.0F, 0.0F);
		dummy_buttons7.setTextureOffset(9, 94).addBox(-5.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons7.setTextureOffset(9, 94).addBox(-3.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons7.setTextureOffset(9, 94).addBox(-1.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons7.setTextureOffset(9, 94).addBox(0.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons7.setTextureOffset(9, 94).addBox(2.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons7.setTextureOffset(9, 94).addBox(4.5F, -0.5F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		dummy_buttons8 = new ModelRenderer(this);
		dummy_buttons8.setRotationPoint(12.5F, -22.75F, -1.25F);
		angeled_panel.addChild(dummy_buttons8);
		dummy_buttons8.setTextureOffset(131, 61).addBox(15.0F, -82.2042F, -57.6047F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dummy_buttons8.setTextureOffset(165, 43).addBox(21.0F, -82.2042F, -57.6047F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		bone67 = new ModelRenderer(this);
		bone67.setRotationPoint(18.0F, -93.9542F, -57.8235F);
		angeled_panel.addChild(bone67);
		setRotationAngle(bone67, 0.0F, 0.0F, 0.5061F);
		bone67.setTextureOffset(76, 156).addBox(-2.75F, -4.75F, 0.25F, 4.0F, 16.0F, 1.0F, 0.0F, false);
		bone67.setTextureOffset(6, 150).addBox(1.25F, 2.25F, 0.25F, 1.0F, 9.0F, 1.0F, 0.0F, false);

		panels = new ModelRenderer(this);
		panels.setRotationPoint(-5.0F, 91.3792F, 56.7684F);
		base_plate_5.addChild(panels);
		panels.setTextureOffset(15, 160).addBox(8.0F, -96.4542F, -55.9797F, 1.0F, 15.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(15, 160).addBox(9.0F, -96.4542F, -55.9797F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(80, 163).addBox(9.0F, -89.4542F, -55.9797F, 3.0F, 8.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(15, 160).addBox(10.0F, -95.4542F, -55.9797F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(6, 121).addBox(9.0F, -95.4542F, -55.8547F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(6, 121).addBox(11.0F, -95.4542F, -55.8547F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(15, 160).addBox(12.0F, -96.4542F, -55.9797F, 1.0F, 15.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(57, 165).addBox(-3.0F, -105.4542F, -56.6047F, 14.0F, 7.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(77, 155).addBox(13.0F, -99.4542F, -55.9797F, 4.0F, 18.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(130, 39).addBox(13.75F, -93.7042F, -56.2297F, 3.0F, 6.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(15, 65).addBox(13.75F, -98.4542F, -56.2297F, 3.0F, 4.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(80, 162).addBox(17.0F, -91.4542F, -55.9797F, 2.0F, 10.0F, 1.0F, 0.0F, false);
		panels.setTextureOffset(78, 165).addBox(19.0F, -88.4542F, -55.9797F, 3.0F, 7.0F, 1.0F, 0.0F, false);

		glow_panels = new LightModelRenderer(this);
		glow_panels.setRotationPoint(-4.25F, 10.0F, 0.375F);
		panels.addChild(glow_panels);
		glow_panels.setTextureOffset(110, 7).addBox(17.0F, -96.7042F, -56.6047F, 3.0F, 4.0F, 1.0F, 0.0F, false);
		glow_panels.setTextureOffset(110, 7).addBox(13.0F, -96.4542F, -56.6047F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		obtuse_panel = new ModelRenderer(this);
		obtuse_panel.setRotationPoint(19.25F, -92.4542F, -56.1047F);
		panels.addChild(obtuse_panel);
		setRotationAngle(obtuse_panel, 0.0F, 0.0F, -0.48F);
		obtuse_panel.setTextureOffset(76, 157).addBox(-2.7635F, -7.248F, 0.25F, 4.0F, 17.0F, 1.0F, 0.0F, false);

		refuler = new ModelRenderer(this);
		refuler.setRotationPoint(-44.25F, 92.1292F, 58.3934F);
		base_plate_5.addChild(refuler);
		

		needle = new ModelRenderer(this);
		needle.setRotationPoint(40.75F, -86.9542F, -57.511F);
		refuler.addChild(needle);
		setRotationAngle(needle, 0.0F, 0.0F, -1.0472F);
		needle.setTextureOffset(134, 40).addBox(-0.5F, -4.5F, -0.5F, 1.0F, 5.0F, 1.0F, 0.0F, false);

		trim = new ModelRenderer(this);
		trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		refuler.addChild(trim);
		trim.setTextureOffset(8, 114).addBox(35.25F, -91.4542F, -57.6047F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		trim.setTextureOffset(8, 114).addBox(45.25F, -91.4542F, -57.6047F, 1.0F, 5.0F, 1.0F, 0.0F, false);
		trim.setTextureOffset(8, 114).addBox(36.25F, -93.4542F, -57.6047F, 2.0F, 9.0F, 1.0F, 0.0F, false);
		trim.setTextureOffset(8, 114).addBox(43.25F, -93.4542F, -57.6047F, 2.0F, 9.0F, 1.0F, 0.0F, false);
		trim.setTextureOffset(8, 114).addBox(38.25F, -94.4542F, -57.6047F, 5.0F, 11.0F, 1.0F, 0.0F, false);
		trim.setTextureOffset(8, 114).addBox(37.25F, -87.4542F, -58.1047F, 7.0F, 2.0F, 1.0F, 0.0F, false);

		glass = new ModelRenderer(this);
		glass.setRotationPoint(0.0F, 0.0F, -0.25F);
		refuler.addChild(glass);
		

		controls6 = new ModelRenderer(this);
		controls6.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls6);
		setRotationAngle(controls6, 0.0F, 0.5236F, 0.0F);
		

		base_plate_6 = new ModelRenderer(this);
		base_plate_6.setRotationPoint(0.0F, -55.4898F, -50.7791F);
		controls6.addChild(base_plate_6);
		setRotationAngle(base_plate_6, -1.309F, 0.0F, 0.0F);
		base_plate_6.setTextureOffset(10, 203).addBox(-17.0F, -1.0751F, 0.1637F, 12.0F, 10.0F, 1.0F, 0.0F, false);
		base_plate_6.setTextureOffset(4, 190).addBox(-5.0F, -16.0751F, 0.1637F, 10.0F, 25.0F, 1.0F, 0.0F, false);
		base_plate_6.setTextureOffset(15, 206).addBox(-4.5F, -6.3251F, -0.5863F, 9.0F, 4.0F, 1.0F, 0.0F, false);
		base_plate_6.setTextureOffset(10, 120).addBox(-3.5F, -5.3251F, -0.8363F, 7.0F, 2.0F, 1.0F, 0.0F, false);
		base_plate_6.setTextureOffset(7, 200).addBox(5.0F, -1.0751F, 0.1637F, 12.0F, 10.0F, 1.0F, 0.0F, false);

		bone69 = new ModelRenderer(this);
		bone69.setRotationPoint(-11.0F, -6.0751F, 0.6637F);
		base_plate_6.addChild(bone69);
		setRotationAngle(bone69, 0.0F, 0.0F, 0.7854F);
		bone69.setTextureOffset(10, 203).addBox(0.0F, -9.0F, -0.25F, 8.0F, 17.0F, 1.0F, 0.0F, false);
		bone69.setTextureOffset(10, 203).addBox(7.0F, -15.0F, -0.25F, 15.0F, 8.0F, 1.0F, 0.0F, false);

		dummy_buttons9 = new ModelRenderer(this);
		dummy_buttons9.setRotationPoint(-15.0F, 90.5979F, 56.5184F);
		base_plate_6.addChild(dummy_buttons9);
		dummy_buttons9.setTextureOffset(129, 40).addBox(16.0F, -98.4542F, -56.6047F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(16.5F, -100.7042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(11.5F, -100.7042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(16.5F, -102.9542F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(20.5F, -95.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(23.5F, -95.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(7.5F, -98.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(17, 76).addBox(11.5F, -105.2042F, -56.6047F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(5, 8).addBox(17.0F, -95.4542F, -57.8547F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		dummy_buttons9.setTextureOffset(5, 8).addBox(12.0F, -95.4542F, -57.8547F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		dial = new ModelRenderer(this);
		dial.setRotationPoint(-14.0F, 3.4898F, -3.2209F);
		base_plate_6.addChild(dial);
		

		shield_health = new ModelRenderer(this);
		shield_health.setRotationPoint(3.0F, 2.9351F, 2.8846F);
		dial.addChild(shield_health);
		setRotationAngle(shield_health, 0.0F, 0.0F, -0.6981F);
		shield_health.setTextureOffset(133, 41).addBox(-0.5F, -4.25F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		dial_trim = new ModelRenderer(this);
		dial_trim.setRotationPoint(-20.0F, 87.8894F, 59.4893F);
		dial.addChild(dial_trim);
		dial_trim.setTextureOffset(5, 113).addBox(21.0F, -91.4542F, -56.6047F, 4.0F, 8.0F, 1.0F, 0.0F, false);
		dial_trim.setTextureOffset(5, 113).addBox(21.0F, -85.9542F, -57.3547F, 4.0F, 2.0F, 1.0F, 0.0F, false);
		dial_trim.setTextureOffset(5, 113).addBox(25.0F, -90.4542F, -56.6047F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		dial_trim.setTextureOffset(5, 113).addBox(20.0F, -90.4542F, -56.6047F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		dial_trim.setTextureOffset(5, 113).addBox(26.0F, -89.4542F, -56.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		dial_trim.setTextureOffset(5, 113).addBox(19.0F, -89.4542F, -56.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		dial_glass = new ModelRenderer(this);
		dial_glass.setRotationPoint(3.5F, -0.6106F, 4.1143F);
		dial.addChild(dial_glass);
		

		dial2 = new ModelRenderer(this);
		dial2.setRotationPoint(11.0F, 4.4898F, -0.2209F);
		base_plate_6.addChild(dial2);
		

		dial_trim2 = new ModelRenderer(this);
		dial_trim2.setRotationPoint(-23.0F, 86.8894F, 56.4893F);
		dial2.addChild(dial_trim2);
		dial_trim2.setTextureOffset(5, 113).addBox(21.0F, -85.9542F, -57.3547F, 4.0F, 2.0F, 1.0F, 0.0F, false);
		dial_trim2.setTextureOffset(5, 113).addBox(21.0F, -91.4542F, -56.6047F, 4.0F, 8.0F, 1.0F, 0.0F, false);
		dial_trim2.setTextureOffset(5, 113).addBox(25.0F, -90.4542F, -56.6047F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		dial_trim2.setTextureOffset(5, 113).addBox(20.0F, -90.4542F, -56.6047F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		dial_trim2.setTextureOffset(5, 113).addBox(26.0F, -89.4542F, -56.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		dial_trim2.setTextureOffset(5, 113).addBox(19.0F, -89.4542F, -56.6047F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		dial_glass2 = new ModelRenderer(this);
		dial_glass2.setRotationPoint(-22.5F, 87.3894F, 56.1143F);
		dial2.addChild(dial_glass2);
		

		flight_time = new ModelRenderer(this);
		flight_time.setRotationPoint(0.0F, 1.9351F, -0.1154F);
		dial2.addChild(flight_time);
		setRotationAngle(flight_time, 0.0F, 0.0F, -0.6981F);
		flight_time.setTextureOffset(133, 41).addBox(-0.5F, -4.25F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		slider_frames = new ModelRenderer(this);
		slider_frames.setRotationPoint(-2.5F, 4.3792F, -0.2316F);
		base_plate_6.addChild(slider_frames);
		slider_frames.setTextureOffset(11, 7).addBox(-1.5F, -5.4542F, -0.6047F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(0.5F, -5.4542F, -0.6047F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(9, 116).addBox(-0.5F, -4.4542F, -0.1047F, 1.0F, 7.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(-0.5F, -5.4542F, -0.6047F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(-0.5F, 2.5458F, -0.6047F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(3.5F, -5.4542F, -0.6047F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(5.5F, -5.4542F, -0.6047F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(10, 105).addBox(4.5F, -4.4542F, -0.1047F, 1.0F, 7.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(4.5F, -5.4542F, -0.6047F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		slider_frames.setTextureOffset(11, 7).addBox(4.5F, 2.5458F, -0.6047F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		stablizers = new ModelRenderer(this);
		stablizers.setRotationPoint(2.0F, 4.3792F, 0.7684F);
		base_plate_6.addChild(stablizers);
		

		slider_left_rotate_x = new ModelRenderer(this);
		slider_left_rotate_x.setRotationPoint(0.5F, -3.9542F, 51.8953F);
		stablizers.addChild(slider_left_rotate_x);
		slider_left_rotate_x.setTextureOffset(132, 44).addBox(-1.5F, -0.5F, -54.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		slider_right_rotate_x = new ModelRenderer(this);
		slider_right_rotate_x.setRotationPoint(-4.5F, 2.0458F, 43.8953F);
		stablizers.addChild(slider_right_rotate_x);
		slider_right_rotate_x.setTextureOffset(130, 43).addBox(-1.5F, -0.5F, -46.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		rib_control = new ModelRenderer(this);
		rib_control.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(rib_control);
		setRotationAngle(rib_control, 0.0F, -0.5236F, 0.0F);
		

		spine_con1 = new ModelRenderer(this);
		spine_con1.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control.addChild(spine_con1);
		setRotationAngle(spine_con1, 0.0F, -0.5236F, 0.0F);
		

		plain7 = new ModelRenderer(this);
		plain7.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con1.addChild(plain7);
		setRotationAngle(plain7, -1.309F, 0.0F, 0.0F);
		plain7.setTextureOffset(80, 74).addBox(-9.0F, 4.9249F, 0.9137F, 18.0F, 9.0F, 2.0F, 0.0F, false);
		plain7.setTextureOffset(96, 97).addBox(-8.0F, 5.9249F, -0.0863F, 16.0F, 7.0F, 1.0F, 0.0F, false);
		plain7.setTextureOffset(96, 97).addBox(-3.5F, 8.4249F, -3.0863F, 7.0F, 2.0F, 3.0F, 0.0F, false);
		plain7.setTextureOffset(96, 97).addBox(-6.75F, 8.4249F, -3.0863F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		plain7.setTextureOffset(96, 97).addBox(4.75F, 8.4249F, -3.0863F, 2.0F, 2.0F, 3.0F, 0.0F, false);
		plain7.setTextureOffset(98, 51).addBox(-3.5F, -20.0751F, -1.3363F, 7.0F, 10.0F, 2.0F, 0.0F, false);

		bar = new ModelRenderer(this);
		bar.setRotationPoint(0.25F, 9.4249F, -4.0863F);
		plain7.addChild(bar);
		setRotationAngle(bar, -0.7854F, 0.0F, 0.0F);
		bar.setTextureOffset(96, 97).addBox(-7.75F, -2.0F, -2.0F, 3.0F, 4.0F, 4.0F, 0.0F, false);
		bar.setTextureOffset(97, 140).addBox(-8.0F, -1.5F, -1.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);
		bar.setTextureOffset(97, 140).addBox(6.5F, -1.5F, -1.5F, 1.0F, 3.0F, 3.0F, 0.0F, false);
		bar.setTextureOffset(96, 97).addBox(4.25F, -2.0F, -2.0F, 3.0F, 4.0F, 4.0F, 0.0F, false);
		bar.setTextureOffset(96, 97).addBox(-3.75F, -2.0F, -2.0F, 7.0F, 4.0F, 4.0F, 0.0F, false);

		handbrake_x = new ModelRenderer(this);
		handbrake_x.setRotationPoint(0.5F, 9.4249F, -4.0863F);
		plain7.addChild(handbrake_x);
		handbrake_x.setTextureOffset(96, 141).addBox(-5.0F, -2.5915F, -2.4755F, 1.0F, 5.0F, 5.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(-5.0F, -3.5915F, -1.9755F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(-5.0F, -2.0915F, -3.4755F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(-5.0F, -2.0915F, 2.5245F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(3.0F, -2.5915F, -2.4755F, 1.0F, 5.0F, 5.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(3.0F, -3.5915F, -1.9755F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(3.0F, -2.0915F, -3.4755F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(3.0F, -2.0915F, 2.5245F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(-5.0F, 2.4085F, -0.9755F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(3.0F, 2.4085F, -0.9755F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		handbrake_x.setTextureOffset(96, 141).addBox(-6.5F, 4.9085F, -0.4755F, 12.0F, 1.0F, 1.0F, 0.0F, false);
		handbrake_x.setTextureOffset(130, 43).addBox(-6.0F, 4.4085F, -0.9755F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		handbrake_x.setTextureOffset(130, 43).addBox(2.0F, 4.4085F, -0.9755F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		handbrake_x.setTextureOffset(130, 43).addBox(0.75F, 4.4085F, -0.9755F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		handbrake_x.setTextureOffset(130, 43).addBox(-2.75F, 4.4085F, -0.9755F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		handbrake_x.setTextureOffset(130, 43).addBox(-1.5F, 4.4085F, -0.9755F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bevel7 = new ModelRenderer(this);
		bevel7.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine_con1.addChild(bevel7);
		setRotationAngle(bevel7, -0.6981F, 0.0F, 0.0F);
		

		spine_con2 = new ModelRenderer(this);
		spine_con2.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control.addChild(spine_con2);
		setRotationAngle(spine_con2, 0.0F, -2.618F, 0.0F);
		

		plain8 = new ModelRenderer(this);
		plain8.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con2.addChild(plain8);
		setRotationAngle(plain8, -1.309F, 0.0F, 0.0F);
		plain8.setTextureOffset(96, 97).addBox(-4.0F, 5.9249F, 0.4137F, 8.0F, 8.0F, 1.0F, 0.0F, false);
		plain8.setTextureOffset(96, 97).addBox(1.0F, 6.9249F, -2.5863F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		plain8.setTextureOffset(96, 97).addBox(-2.0F, 6.9249F, -2.5863F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		plain8.setTextureOffset(99, 74).addBox(-5.0F, 4.9249F, 0.9137F, 10.0F, 10.0F, 1.0F, 0.0F, false);

		winch = new ModelRenderer(this);
		winch.setRotationPoint(-0.75F, 8.4249F, -2.0863F);
		plain8.addChild(winch);
		setRotationAngle(winch, -0.6109F, 0.0F, 0.0F);
		

		casing = new ModelRenderer(this);
		casing.setRotationPoint(0.3889F, 1.9444F, -1.0F);
		winch.addChild(casing);
		casing.setTextureOffset(93, 164).addBox(-2.6389F, -1.4444F, -3.5F, 1.0F, 3.0F, 6.0F, 0.0F, false);
		casing.setTextureOffset(93, 164).addBox(2.3611F, -1.4444F, -3.5F, 1.0F, 3.0F, 6.0F, 0.0F, false);
		casing.setTextureOffset(93, 164).addBox(2.3611F, -2.4444F, -3.0F, 1.0F, 5.0F, 5.0F, 0.0F, false);
		casing.setTextureOffset(93, 164).addBox(2.3611F, -2.9444F, -2.0F, 1.0F, 6.0F, 3.0F, 0.0F, false);
		casing.setTextureOffset(93, 164).addBox(-2.6389F, -2.9444F, -2.0F, 1.0F, 6.0F, 3.0F, 0.0F, false);
		casing.setTextureOffset(93, 164).addBox(-2.6389F, -2.4444F, -3.0F, 1.0F, 5.0F, 5.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-4.6389F, -1.4444F, -3.5F, 2.0F, 3.0F, 6.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(3.3611F, -1.4444F, -3.5F, 2.0F, 3.0F, 6.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-1.6389F, -1.4444F, -3.5F, 4.0F, 3.0F, 6.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-4.6389F, -2.9444F, -2.0F, 2.0F, 6.0F, 3.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(3.3611F, -2.9444F, -2.0F, 2.0F, 6.0F, 3.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-1.6389F, -2.9444F, -2.0F, 4.0F, 6.0F, 3.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-1.6389F, -1.4444F, 0.0F, 1.0F, 2.0F, 8.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(1.3611F, -1.4444F, 0.0F, 1.0F, 2.0F, 8.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(3.3611F, -2.4444F, -3.0F, 3.0F, 5.0F, 5.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-1.6389F, -2.4444F, -3.0F, 4.0F, 5.0F, 5.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-5.6389F, -2.4444F, -3.0F, 3.0F, 5.0F, 5.0F, 0.0F, false);
		casing.setTextureOffset(96, 97).addBox(-6.6389F, -1.9444F, -2.5F, 1.0F, 4.0F, 4.0F, 0.0F, false);

		throttle_x = new ModelRenderer(this);
		throttle_x.setRotationPoint(9.3393F, 2.0357F, -1.5F);
		winch.addChild(throttle_x);
		setRotationAngle(throttle_x, 0.8727F, 0.0F, 0.0F);
		throttle_x.setTextureOffset(5, 160).addBox(-2.3393F, -2.0357F, -2.0F, 1.0F, 4.0F, 4.0F, 0.0F, false);
		throttle_x.setTextureOffset(8, 165).addBox(-2.3393F, 1.9643F, -1.5F, 1.0F, 5.0F, 3.0F, 0.0F, false);
		throttle_x.setTextureOffset(92, 135).addBox(-3.0893F, -1.0357F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_x.setTextureOffset(92, 135).addBox(-2.5893F, 4.9643F, -0.5F, 7.0F, 1.0F, 1.0F, 0.0F, false);
		throttle_x.setTextureOffset(132, 42).addBox(-1.0268F, 4.4643F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_x.setTextureOffset(132, 42).addBox(0.0982F, 4.4643F, -1.0F, 3.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_x.setTextureOffset(132, 42).addBox(3.2857F, 4.4643F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);

		bevel8 = new ModelRenderer(this);
		bevel8.setRotationPoint(0.0F, -52.6347F, -67.7874F);
		spine_con2.addChild(bevel8);
		setRotationAngle(bevel8, -0.6981F, 0.0F, 0.0F);
		

		spine_con3 = new ModelRenderer(this);
		spine_con3.setRotationPoint(0.0F, -2.0F, 0.0F);
		rib_control.addChild(spine_con3);
		setRotationAngle(spine_con3, 0.0F, 1.5708F, 0.0F);
		

		plain9 = new ModelRenderer(this);
		plain9.setRotationPoint(0.0F, -59.4898F, -50.7791F);
		spine_con3.addChild(plain9);
		setRotationAngle(plain9, -1.309F, 0.0F, 0.0F);
		

		telepathic2 = new ModelRenderer(this);
		telepathic2.setRotationPoint(-31.5F, 90.6292F, 59.7684F);
		plain9.addChild(telepathic2);
		telepathic2.setTextureOffset(94, 72).addBox(26.0F, -86.4542F, -58.1047F, 11.0F, 11.0F, 2.0F, 0.0F, false);
		telepathic2.setTextureOffset(96, 97).addBox(33.0F, -85.4542F, -59.1047F, 2.0F, 7.0F, 2.0F, 0.0F, false);
		telepathic2.setTextureOffset(96, 97).addBox(28.0F, -85.4542F, -59.1047F, 2.0F, 7.0F, 2.0F, 0.0F, false);

		microscope = new ModelRenderer(this);
		microscope.setRotationPoint(31.5F, -77.7189F, -56.9429F);
		telepathic2.addChild(microscope);
		setRotationAngle(microscope, 0.5236F, 0.0F, 0.0F);
		microscope.setTextureOffset(96, 97).addBox(0.5F, -1.6013F, -9.6618F, 1.0F, 1.0F, 9.0F, 0.0F, false);
		microscope.setTextureOffset(96, 97).addBox(-1.5F, -1.6013F, -9.6618F, 1.0F, 1.0F, 9.0F, 0.0F, false);
		microscope.setTextureOffset(96, 97).addBox(-0.5F, -3.6013F, -3.6618F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		microscope.setTextureOffset(96, 97).addBox(-2.0F, -6.6013F, -15.6618F, 4.0F, 4.0F, 8.0F, 0.0F, false);
		microscope.setTextureOffset(56, 115).addBox(-2.0F, -6.6013F, -20.6618F, 4.0F, 4.0F, 2.0F, 0.0F, false);
		microscope.setTextureOffset(96, 97).addBox(-1.5F, -6.1013F, -19.6618F, 3.0F, 3.0F, 13.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(-0.5F, -0.6013F, -9.1618F, 1.0F, 1.0F, 9.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(-3.0F, -1.1013F, -10.6618F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(-3.0F, -5.6013F, -16.6618F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(2.0F, -5.6013F, -16.6618F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(-2.0F, -0.6013F, -10.1618F, 4.0F, 1.0F, 1.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(-3.5F, -5.1013F, -16.1618F, 7.0F, 1.0F, 1.0F, 0.0F, false);
		microscope.setTextureOffset(93, 136).addBox(2.0F, -1.1013F, -10.6618F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		microscope.setTextureOffset(96, 97).addBox(-1.0F, -3.6013F, -9.6618F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		center = new ModelRenderer(this);
		center.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		bottom = new ModelRenderer(this);
		bottom.setRotationPoint(0.0F, 37.0068F, 27.7761F);
		center.addChild(bottom);
		bottom.setTextureOffset(5, 56).addBox(-17.0F, -41.0F, -41.0F, 34.0F, 1.0F, 21.0F, 0.0F, false);
		bottom.setTextureOffset(5, 56).addBox(-30.0F, -41.0F, -41.0F, 13.0F, 1.0F, 21.0F, 0.0F, false);
		bottom.setTextureOffset(5, 56).addBox(17.0F, -41.0F, -41.0F, 13.0F, 1.0F, 21.0F, 0.0F, false);
		bottom.setTextureOffset(5, 56).addBox(0.0F, -41.0F, -20.0F, 17.0F, 1.0F, 19.0F, 0.0F, false);
		bottom.setTextureOffset(5, 56).addBox(-21.0F, -41.0F, -20.0F, 21.0F, 1.0F, 19.0F, 0.0F, false);
		bottom.setTextureOffset(5, 56).addBox(-19.0F, -41.0F, -57.0F, 21.0F, 1.0F, 16.0F, 0.0F, false);
		bottom.setTextureOffset(5, 56).addBox(2.0F, -41.0F, -57.0F, 21.0F, 1.0F, 16.0F, 0.0F, false);

		innards = new ModelRenderer(this);
		innards.setRotationPoint(1.0357F, -60.4932F, -2.2954F);
		center.addChild(innards);
		

		top = new ModelRenderer(this);
		top.setRotationPoint(0.0F, -15.0F, 0.0F);
		innards.addChild(top);
		setRotationAngle(top, 0.0F, -0.5236F, 0.0F);
		top.setTextureOffset(5, 56).addBox(-12.2857F, -0.5F, -19.9286F, 25.0F, 1.0F, 19.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-17.2857F, -0.5F, -17.9286F, 5.0F, 1.0F, 17.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(12.7143F, -0.5F, -17.9286F, 5.0F, 1.0F, 17.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-17.2857F, -0.5F, 6.0714F, 5.0F, 1.0F, 17.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(12.7143F, -0.5F, 6.0714F, 5.0F, 1.0F, 17.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-20.2857F, -0.5F, -10.9286F, 3.0F, 1.0F, 10.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(17.7143F, -0.5F, -10.9286F, 3.0F, 1.0F, 10.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(17.7143F, -0.5F, 6.0714F, 3.0F, 1.0F, 10.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-20.2857F, -0.5F, 6.0714F, 3.0F, 1.0F, 10.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-23.2857F, -0.5F, -7.9286F, 3.0F, 1.0F, 7.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(20.7143F, -0.5F, -7.9286F, 3.0F, 1.0F, 7.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(20.7143F, -0.5F, 6.0714F, 3.0F, 1.0F, 7.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-23.2857F, -0.5F, 6.0714F, 3.0F, 1.0F, 7.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-12.2857F, -0.5F, 6.0714F, 25.0F, 1.0F, 19.0F, 0.0F, false);
		top.setTextureOffset(5, 56).addBox(-25.2857F, -0.5F, -0.9286F, 51.0F, 1.0F, 7.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		glow.render(matrixStack, buffer, packedLight, packedOverlay);
		spines.render(matrixStack, buffer, packedLight, packedOverlay);
		stations.render(matrixStack, buffer, packedLight, packedOverlay);
		controls.render(matrixStack, buffer, packedLight, packedOverlay);
		center.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(NeutronConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		
		tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
			this.throttle_x.rotateAngleX = (float) Math.toRadians(25 - (throttle.getAmount() * 175));
		});
		
		tile.getControl(HandbrakeControl.class).ifPresent(control -> {
			this.handbrake_x.rotateAngleX = (float) Math.toRadians(control.isFree() ? 0 : 180);
		});
		
		tile.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {

			float angle = sys.isControlActivated() ? 7 : 0;
			
			this.slider_left_rotate_x.rotateAngleX = (float) Math.toRadians(angle);
			this.slider_right_rotate_x.rotateAngleX = (float) Math.toRadians(-angle);
		});
		
		tile.getControl(RandomiserControl.class).ifPresent(control -> {
			this.key_white2.rotateAngleX = (float) Math.toRadians(control.getAnimationProgress() > 0.25 ? 8 : 0);
			this.key_white4.rotateAngleX = (float) Math.toRadians(control.getAnimationProgress() > 0.5 ? 8 : 0);
			this.key_white5.rotateAngleX = (float) Math.toRadians(control.getAnimationProgress() > 0.75 ? 8 : 0);
		});
		
		tile.getControl(IncModControl.class).ifPresent(inc -> {
			this.inc_slider_rotate_x.rotateAngleX = (float) Math.toRadians(20 - (inc.index / (float)IncModControl.COORD_MODS.length) * 50);
		});
		
		tile.getControl(XControl.class).ifPresent(control -> {
			this.z_cord_rotate_x2.rotateAngleX = (float)Math.toRadians(control.getAnimationTicks() > 0 ? -10 : 10);
			this.glow_x_cord_red.setBright(control.getAnimationTicks() > 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(YControl.class).ifPresent(control -> {
			this.y_cord_rotate_y.rotateAngleX = (float)Math.toRadians(control.getAnimationTicks() > 0 ? -10 : 10);
			this.glow_y_cord_green.setBright(control.getAnimationTicks() > 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(ZControl.class).ifPresent(control -> {
			this.z_cord_rotate_x.rotateAngleX = (float)Math.toRadians(control.getAnimationTicks() > 0 ? -10 : 10);
			this.glow_z_coord_blue.setBright(control.getAnimationTicks() > 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(FacingControl.class).ifPresent(control -> {
			
			Axis axis = control.getDirection().getAxis();
			AxisDirection axisDir = control.getDirection().getAxisDirection();
			
			if(axis == Axis.X) {
				this.joystick.rotateAngleY = (float) Math.toRadians(axisDir == AxisDirection.POSITIVE ? -25 : 25);
				this.joystick.rotateAngleX = 0;
			}
			else {
				this.joystick.rotateAngleX = (float) Math.toRadians(axisDir == AxisDirection.POSITIVE ? 25 : -25);
				this.joystick.rotateAngleY = 0;
			}
		});
		
		tile.getControl(DoorControl.class).ifPresent(doorControl -> {
			float angle = 0;
			
			DoorEntity door = tile.getDoor().orElse(null);
			if(door != null)
				angle = door.getOpenState() == EnumDoorState.CLOSED ? 0 : 90;
			
			angle += doorControl.getAnimationProgress() * 360.0F;
			
			this.door_turn.rotateAngleZ = (float)Math.toRadians(angle);
		});
		
		tile.getControl(LandingTypeControl.class).ifPresent(land -> {
			this.landing_type_rotate_x.rotateAngleX = (float)Math.toRadians(land.getLandType() == EnumLandType.DOWN ? 6 : -5);
		});
		
		tile.getControl(RefuelerControl.class).ifPresent(control -> {
			
			float angle = control.getAnimationProgress() * 140;
			if(control.isRefueling())
				angle *= -1;
			
			angle -= control.isRefueling() ? -70 : 70;
			
			this.needle.rotateAngleZ = (float) Math.toRadians(angle);
		});
		
		tile.getControl(FastReturnControl.class).ifPresent(control -> {
			this.fast_return_rotate_x.rotateAngleX = (float) Math.toRadians(control.getAnimationProgress() * 360.0F);
		});
		
		tile.getControl(CommunicatorControl.class).ifPresent(control -> {
			this.com_needle_rotate_z.rotateAngleZ = (float) Math.toRadians(5 - (control.getAnimationProgress() * 10));
			this.knob4.rotateAngleZ = (float) Math.toRadians(-50 + (control.getAnimationProgress() * 100));
		});
		
		this.glow_warning_lamp.setBright(tile.getWorld().getGameTime() % 40 < 20 ? 1.0F : 0.0F);
		
		int glowTime = (int)(tile.getWorld().getGameTime() % 120);
		
		this.glow_white.setBright(glowTime > 60 ? 1.0F : 0.0F);
		this.glow_yellow.setBright(glowTime < 60 ? 1.0F : 0.0F);
		this.glow_blue2.setBright(Minecraft.getInstance().world.getGameTime() % 120 > 30 ? 1.0F : 0.0F);
		
		this.radar2_glow.setBright(1.0F);
		this.glow_inner.setBright(1.0F);
		this.glow_posts.setBright(1.0F);
		this.rib_control2.setBright(1.0F);
		this.controls8.setBright(1.0F);
		this.controls9.setBright(1.0F);
		this.controls10.setBright(1.0F);
		this.controls12.setBright(1.0F);
		this.controls13.setBright(1.0F);
		this.button_array2.setBright(1.0F);
		this.glow_crystal_rotate_y.setBright(1.0F);
		this.glow_outer_rotate_x.setBright(1.0F);
		this.glow_inner_rotate_x.setBright(1.0F);
		
		matrixStack.push();
		
		float anim = MathHelper.lerp(Minecraft.getInstance().getRenderPartialTicks(), tile.prevFlightTicks, tile.flightTicks);
		
		matrixStack.translate(0, (Math.cos(anim * 0.1) * 0.5) - 0.5, 0);
		
		this.glow_inner_rotate_x.rotateAngleX = (float) Math.toRadians(anim * 2);
		this.glow_outer_rotate_x.rotateAngleX = (float) -Math.toRadians(anim * 2);
		this.glow_crystal_rotate_y.rotateAngleY = (float) Math.toRadians(anim);
		
		rotor.render(matrixStack, buffer, packedLight, packedOverlay);
		matrixStack.pop();
		
		this.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}