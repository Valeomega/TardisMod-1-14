package net.tardis.mod.datagen.compat;

import net.minecraft.block.Block;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.BrokenExteriorBlock;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.blocks.RoundelBlock;
import net.tardis.mod.tags.TardisBlockTags;

/* Created by Craig on 16/02/2021 */
public class WABlockTagGen extends BlockTagsProvider {

    public static final ITag.INamedTag<Block> ANGEL_PROOF = TardisBlockTags.makeBlock(new ResourceLocation("weeping_angels", "angel_proof"));

    public WABlockTagGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
        super(generatorIn, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        for (Block block : ForgeRegistries.BLOCKS) {
            if(block instanceof RoundelBlock || block instanceof BrokenExteriorBlock || block instanceof ExteriorBlock){
                add(ANGEL_PROOF, block);
            }
        }
    }

    public void add(ITag.INamedTag<Block> branch, Block block) {
        this.getOrCreateBuilder(branch).add(block);
    }

    public void add(ITag.INamedTag<Block> branch, Block... block) {
        this.getOrCreateBuilder(branch).add(block);
    }
}
