package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.ExteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class ExteriorSchematicType extends SchematicType{

    @Override
    public Schematic deserialize(ResourceLocation key, JsonObject root) {
        ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic(this);
        schematic.setExterior(new ResourceLocation(root.get("result").getAsString()));
        return schematic;
    }

    @Override
    public JsonObject serialize(Schematic schematic) {
        return null;
    }
}
