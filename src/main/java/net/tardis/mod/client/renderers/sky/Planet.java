package net.tardis.mod.client.renderers.sky;

public class Planet {

	public float minHU, maxHU, minHV, maxHV;
	//public double minHU, maxHU, minHV, maxHV;
	
	public float minVU, maxVU, minVV, maxVV;
	//public double minVU, maxVU, minVV, maxVV;
	
	public Planet() {}
	
	//public void setHorizontalUVs(double minU, double minV, double maxU, double maxV) {
	public void setHorizontalUVs(float minU, float minV, float maxU, float maxV) {
		this.minHU = minU;
		this.minHV = minV;
		this.maxHU = maxU;
		this.maxHV = maxV;
	}
	
	//public void setVerticleUVs(double minU, double minV, double maxU, double maxV) {
	public void setVerticleUVs(float minU, float minV, float maxU, float maxV) {
		this.minVU = minU;
		this.minVV = minV;
		this.maxVU = maxU;
		this.maxVV = maxV;
	}
}
