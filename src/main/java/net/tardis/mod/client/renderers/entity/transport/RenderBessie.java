package net.tardis.mod.client.renderers.entity.transport;

import javax.annotation.Nullable;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.transport.BessieModel;
import net.tardis.mod.entity.BessieEntity;

/**
 * Created by Swirtzly
 * on 08/04/2020 @ 23:04
 */
public class RenderBessie extends LivingRenderer<BessieEntity, BessieModel> {
	public static ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/entity/transport/bessie.png");
	
	public static BessieModel model = new BessieModel();
	
    public RenderBessie(EntityRendererManager rendererManager) {
        super(rendererManager, model, 1);
    }

    @Override
	public BessieModel getEntityModel() {
		return model;
	}

	@Override
	public void render(BessieEntity entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
    	super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
    	matrixStackIn.push();
    	matrixStackIn.rotate(Vector3f.YN.rotation((float)Math.toRadians(entityYaw)));
    	matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180F)); //required so it's flipped the correct way
    	model.render(entityIn, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(TEXTURE)), packedLightIn, OverlayTexture.NO_OVERLAY);
    	matrixStackIn.pop();
    }

	@Override
	protected boolean canRenderName(BessieEntity entity) {
		return false;
	}

	@Nullable
    @Override
    public ResourceLocation getEntityTexture(BessieEntity entity) {
        return TEXTURE;
    }
}
