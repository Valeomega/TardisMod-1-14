package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.chunk.Chunk;
import net.tardis.mod.tileentities.ConsoleTile;

public class RiftCapability implements IRift {

    private Chunk chunk;
    private float artron = 0F;
    private boolean isRift = false;
    private float maxArtron = 0;

    public RiftCapability(Chunk chunk) {
        this.chunk = chunk;
    }

    @Override
    public float takeArtron(float amt) {
        float given = this.artron > amt ? amt : artron;
        this.artron -= given;
        return given;
    }

    @Override
    public float getRiftEnergy() {
        return this.artron;
    }

    @Override
    public boolean isRift() {
        return this.isRift;
    }

    @Override
    public void setRift(boolean rift) {
        this.isRift = rift;
        if (this.isRift) {
            this.maxArtron = 250 + ConsoleTile.rand.nextInt(750);
        }
        chunk.markDirty();
    }

    @Override
    public void addEnergy(float energy) {
        if (this.artron < this.maxArtron) {
            this.artron += energy;
            chunk.markDirty();
        }

        if (this.artron > this.maxArtron)
            this.artron = this.maxArtron;
    }

    @Override
    public void tick() {
        if (this.isRift())
            this.artron += 1;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putBoolean("rift", this.isRift);
        tag.putFloat("energy", this.artron);
        tag.putFloat("max_energy", this.maxArtron);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.isRift = nbt.getBoolean("rift");
        this.maxArtron = nbt.getFloat("max_energy");
        this.artron = nbt.getFloat("energy");
    }

}
