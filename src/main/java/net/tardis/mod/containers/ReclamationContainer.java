package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.tardis.mod.containers.slot.FilteredSlot;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tileentities.ReclamationTile;

public class ReclamationContainer extends BaseContainer{

	public ReclamationContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	/** Client Only constructor */
	public ReclamationContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.RECLAMATION_UNIT.get(), id);
		
		TileEntity te = inv.player.world.getTileEntity(buf.readBlockPos());
		if(te instanceof ReclamationTile)
			init(inv, ((ReclamationTile)te));
	}
	
	/** Server Only constructor */
	public ReclamationContainer(int id, PlayerInventory inv, ReclamationTile tileInv) {
		super(TContainers.RECLAMATION_UNIT.get(), id);
		init(inv, tileInv);
	}
	
	//Common
	public void init(PlayerInventory player, ReclamationTile tile) {
		
		for(int i = 0; i < 54; ++i) {
			FilteredSlot slot = new FilteredSlot(tile, i, 8 + (i % 9) * 18, -10 + (i / 9) * 18, stack -> false);
			slot.setAction(s -> {
				tile.sort();
				tile.openInventory(player.player);
			});
			this.addSlot(slot);
		}
		
		TInventoryHelper.addPlayerInvContainer(this, player, 0, 26);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		Slot slot = this.getSlot(index);
		if(slot.inventory instanceof ReclamationTile) {
			playerIn.inventory.addItemStackToInventory(slot.getStack().copy());
			slot.inventory.removeStackFromSlot(index);
		}
		
		return ItemStack.EMPTY;
	}

}
