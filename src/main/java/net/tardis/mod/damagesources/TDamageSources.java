package net.tardis.mod.damagesources;

import net.minecraft.util.DamageSource;
import net.tardis.mod.Tardis;

public class TDamageSources {
	
	public static final DamageSource SPACE = new DamageSource("damagesrc.tardis.space").setDamageBypassesArmor().setDamageIsAbsolute();
	public static final DamageSource DALEK = new TSource("dalek", false);
	public static final DamageSource DALEK_SPEC = new TSource("dalek_spec", false);
	public static final DamageSource CYBERMAN = new TSource("cyberman", false);
	public static final DamageSource LASER = new TSource("laser");
    public static final DamageSource LASER_SONIC = new TSource("laser_sonic");
	public static final DamageSource CIRCUITS = new TSource("circuits", false);
	public static final DamageSource MERCURY = new TSource("mercury");
}
