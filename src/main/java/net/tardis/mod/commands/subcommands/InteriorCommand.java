package net.tardis.mod.commands.subcommands;

import java.util.Collection;
import java.util.Collections;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;

public class InteriorCommand extends TCommand{
	
	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	private static int teleportEntityToInterior(CommandContext<CommandSource> context, Entity entityToTeleport, ServerWorld target){
        final CommandSource source = context.getSource();
        if (TardisHelper.getConsoleInWorld(target).isPresent()) {
        	WorldHelper.teleportEntities(entityToTeleport, target, TardisHelper.TARDIS_POS.getX() + 0.5, TardisHelper.TARDIS_POS.getY() + 1, TardisHelper.TARDIS_POS.getZ() + 0.5, entityToTeleport.rotationPitch, entityToTeleport.rotationYaw);
            TardisHelper.getConsole(source.getServer(), target).ifPresent(tile -> {
                tile.getDoor().ifPresent(door -> door.setOpenState(EnumDoorState.CLOSED));
            });
            TextComponent tardisIdentifier = TextHelper.getTardisDimObject(target);
            source.sendFeedback(new TranslationTextComponent("command.tardis.interior.success", entityToTeleport.getDisplayName(), tardisIdentifier), true);
            return Command.SINGLE_SUCCESS;
        }
        else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(Constants.Translations.NO_TARDIS_FOUND, target.getDimensionKey().getLocation().toString()));
        	return 0;
        }
        
    }
    
    private static int teleportToInterior(CommandContext<CommandSource> context, ServerPlayerEntity playerToTeleport, ServerWorld target) throws CommandSyntaxException {
        return teleportEntityToInterior(context, playerToTeleport, target);
    }
    
    private static int teleportToInterior(CommandContext<CommandSource> context, Collection<? extends Entity> entitiesToTeleport, ServerWorld target) throws CommandSyntaxException {
        entitiesToTeleport.forEach(entity -> {
            teleportEntityToInterior(context, entity, target);
        });
        return Command.SINGLE_SUCCESS;
    }
    
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("interior")
               .requires(context -> context.hasPermissionLevel(2))
                   .executes(context -> teleportToInterior(context, context.getSource().asPlayer(), context.getSource().asPlayer().getServerWorld())
                            )
                       .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                           .executes(context -> teleportToInterior(context, context.getSource().asPlayer(), DimensionArgument.getDimensionArgument(context, "tardis"))
                        		    )//End execution
                           )//End tardis specification for this player
                       .then(Commands.argument("players", EntityArgument.players())
                           .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                               .executes(context -> teleportToInterior(context, EntityArgument.getPlayers(context, "players"), DimensionArgument.getDimensionArgument(context, "tardis"))
                            		    )//End execution
                               )//End tardis specification
                           );//End player target
    }

}
