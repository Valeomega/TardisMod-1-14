package net.tardis.mod.world.structures;

import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.FlatGenerationSettings;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.gen.feature.structure.IStructurePieceType;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;

public class TStructures {
    
    public static class Structures{
        public static final DeferredRegister<Structure<?>> STRUCTURES = DeferredRegister.create(ForgeRegistries.STRUCTURE_FEATURES, Tardis.MODID);
        
        /** The Structure registry object. This isn't actually setup yet, see {@link TStructures#setupStructure(Structure, StructureSeparationSettings, boolean)} */ 
        public static final RegistryObject<Structure<ProbabilityConfig>> DALEK_SHIP = setupStructure("dalek_ship", () -> (new DalekShipStructure(ProbabilityConfig.CODEC)));
        /** Static instance of our structure so we can reference it before registry stuff happens and use it to make configured structures in ConfiguredStructures */
        public static IStructurePieceType DALEK_SHIP_PIECE = registerStructurePiece(DalekShipStructurePieces.Piece::new, "dalek_ship_piece");

        public static final RegistryObject<Structure<ProbabilityConfig>> CRASHED_STRUCTURE = setupStructure("crashed_structure", () -> (new CrashedStructure(ProbabilityConfig.CODEC)));
        public static IStructurePieceType CRASHED_PIECE = registerStructurePiece(CrashedStructurePieces.Piece::new, "crashed_piece");
        
        public static final RegistryObject<Structure<ProbabilityConfig>> SPACE_STATION_DRONE = setupStructure("space_station_drone", () -> (new SpaceStationStructure(ProbabilityConfig.CODEC)));
        public static IStructurePieceType SPACE_STATION_DRONE_PIECE = registerStructurePiece(SpaceStationStructurePieces.Piece::new, "space_station_drone_piece");
        /**
         * RegistryKey used to spawn the structure in a MiniMission
         */
        public static final RegistryKey<Structure<?>> SPACE_STATION_KEY = RegistryKey.getOrCreateKey(Registry.STRUCTURE_FEATURE_KEY, new ResourceLocation(Tardis.MODID, "space_station_drone"));
        
        public static final RegistryObject<Structure<ProbabilityConfig>> ABANDONED_SPACESHIP = setupStructure("abandoned_spaceship", () -> (new AbandonedSpaceshipStructure(ProbabilityConfig.CODEC)));
        public static IStructurePieceType ABANDONED_SPACESHIP_PIECE = registerStructurePiece(AbandonedSpaceshipStructurePieces.Piece::new, "abandoned_spaceship_piece");
        
        public static final RegistryKey<Structure<?>> MOON_LANDER_KEY = RegistryKey.getOrCreateKey(Registry.STRUCTURE_FEATURE_KEY, new ResourceLocation(Tardis.MODID, "moon_lander"));
        
        public static final RegistryObject<Structure<ProbabilityConfig>> MOON_LANDER = setupStructure("moon_lander", () -> (new MoonLanderStructure(ProbabilityConfig.CODEC)));
        public static IStructurePieceType MOON_LANDER_PIECE = registerStructurePiece(MoonLanderStructurePieces.Piece::new, "moon_lander");
        
    }
    
    /** Configure the structure so it can be placed in the world. <br> Register Configured Structures in Common Setup. There is currently no Forge Registry for configured structures because configure structures are a dynamic registry and can cause issues if it were a Forge registry.*/
    public static class ConfiguredStructures{
        /** Static instance of our configured structure feature so we can reference it for registration*/
       public static StructureFeature<?, ?> CONFIGURED_CRASHED_STRUCTURE = Structures.CRASHED_STRUCTURE.get().withConfiguration(new ProbabilityConfig((float)(TConfig.COMMON.crashedStructureSpawnChance.get()/100)));
       
       public static StructureFeature<?, ?> CONFIGURED_SPACE_STATION_DRONE = Structures.SPACE_STATION_DRONE.get().withConfiguration(new ProbabilityConfig((float)(TConfig.COMMON.spaceStationMissionSpawnChance.get() / 100)));
       public static StructureFeature<?, ?> CONFIGURED_DALEK_SHIP = Structures.DALEK_SHIP.get().withConfiguration(new ProbabilityConfig((float)(TConfig.COMMON.dalekSaucerSpawnChance.get()/100)));
       
       public static final RegistryKey<StructureFeature<?, ?>> SPACE_STATION_KEY = RegistryKey.getOrCreateKey(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY, new ResourceLocation(Tardis.MODID, "space_station_drone"));
       
       public static StructureFeature<?, ?> CONFIGURED_ABANDONED_SPACESHIP = Structures.ABANDONED_SPACESHIP.get().withConfiguration(new ProbabilityConfig((float)TConfig.COMMON.abandonedSpaceshipSpawnChance.get()/100));
       
       public static StructureFeature<?, ?> CONFIGURED_MOON_LANDER = Structures.MOON_LANDER.get().withConfiguration(new ProbabilityConfig((float)TConfig.COMMON.moonLanderSpawnChance.get()/100));
       
       public static void registerConfiguredStructures() {
            registerConfiguredStructure("crashed_structure", Structures.CRASHED_STRUCTURE, CONFIGURED_CRASHED_STRUCTURE);//We have to add this to flatGeneratorSettings to account for mods that add custom chunk generators or superflat world type
            registerConfiguredStructure("space_station_drone", Structures.SPACE_STATION_DRONE, CONFIGURED_SPACE_STATION_DRONE);
            registerConfiguredStructure("dalek_ship", Structures.DALEK_SHIP, CONFIGURED_DALEK_SHIP);
            registerConfiguredStructure("abandoned_spaceship", Structures.ABANDONED_SPACESHIP, CONFIGURED_ABANDONED_SPACESHIP);
            registerConfiguredStructure("moon_lander", Structures.MOON_LANDER, CONFIGURED_MOON_LANDER);
       }
    }
    
     /** Setup the structure and add the rarity settings.
      * <br> Call this in CommonSetup in a deferred work task to reduce concurrent modification issues as we are modifying multiple maps we ATed*/
    public static void setupStructures() {
    	//Set transformSurroundingLand to false so that we don't carve shapes into the terrain. We use the structure void blocks inside the template to preserve terrain blocks
        setupStructure(Structures.CRASHED_STRUCTURE.get(), new StructureSeparationSettings(TConfig.COMMON.crashedStructureSeperation.get(), TConfig.COMMON.crashedStructureSpacing.get(), 1234567890), false); //Maximum of 100 chunks apart, 60 chunk spawn radius, chunk seed respectively.
        //Set transformSurroundingLand to false so that we don't accidentally take terrain blocks into the sky, which is where these ships will be spawning
        setupStructure(Structures.SPACE_STATION_DRONE.get(), new StructureSeparationSettings(TConfig.COMMON.spaceStationMissionSeperation.get(), TConfig.COMMON.spaceStationMissionSpacing.get(), 1234562890), false); //Maximum of 120 chunk spawn radius, 30 chunks apart, chunk seed respectively
        setupStructure(Structures.DALEK_SHIP.get(), new StructureSeparationSettings(TConfig.COMMON.dalekSaucerSeperation.get(), TConfig.COMMON.dalekSaucerSpacing.get(), 1234567890), false); //Maximum of 200 chunks apart, 50 chunk spawn radius, chunk seed respectively
        setupStructure(Structures.ABANDONED_SPACESHIP.get(), new StructureSeparationSettings(TConfig.COMMON.abandonedSpaceshipSeperation.get(), TConfig.COMMON.abandonedSpaceshipSpacing.get(), 1234567890), false); //Maximum of 50 chunks apart, 20 chunk spawn radius, chunk seed respectively
        setupStructure(Structures.MOON_LANDER.get(), new StructureSeparationSettings(TConfig.COMMON.moonLanderSeperation.get(), TConfig.COMMON.moonLanderSpacing.get(), 1234567890), false);
    }
    
    private static <T extends Structure<?>> void registerConfiguredStructure(String registryName, Supplier<T> structure, StructureFeature<?, ?> configuredStructure) {
       Registry<StructureFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_STRUCTURE_FEATURE;
       Registry.register(registry, new ResourceLocation(Tardis.MODID, registryName), configuredStructure);
       /** Add your structure to FlatGenerationSettings to
        * prevent any sort of crash or issue with other mod's custom ChunkGenerators. 
        * <br> If they use FlatGenerationSettings.STRUCTURES in it and you don't add your structure to it, the game
        * could crash later when you attempt to add the StructureSeparationSettings to the dimension.

        * <br> (It would also crash with superflat worldtype if you omit the below line
        * and attempt to add the structure's StructureSeparationSettings to the world)
        * <br> <br> Note: If you want your structure to spawn in superflat, remove the FlatChunkGenerator check
        * in EventHandler.addDimensionalSpacing and then create a superflat world, exit it,
        * and re-enter it and your structures will be spawning. 
        * <br> <br> I could not figure out why it needs the restart but honestly, superflat is really buggy and shouldn't be your main focus in my opinion. */
       FlatGenerationSettings.STRUCTURES.put(structure.get(), configuredStructure);
   }
    
    private static <T extends Structure<?>> RegistryObject<T> setupStructure(String name, Supplier<T> structure) {
       return Structures.STRUCTURES.register(name, structure);
   }
   
   /** Add Structure to the structure registry map and setup the seperation settings.*/
   public static <F extends Structure<?>> void setupStructure(F structure, StructureSeparationSettings structureSeparationSettings, boolean transformSurroundingLand){
       /*
       * We need to add our structures into the map in Structure alongside vanilla
       * structures or else it will cause errors. Called by registerStructure.
       *
       * If the registration is setup properly for the structure, getRegistryName() should never return null.
       */
       Structure.NAME_STRUCTURE_BIMAP.put(structure.getRegistryName().toString(), structure);
       /*
        * Will add land at the base of the structure like it does for Villages and Outposts.
        * Doesn't work well on structures that have pieces stacked vertically or change in heights.
        */
       if(transformSurroundingLand){ 
           Structure.field_236384_t_ = ImmutableList.<Structure<?>>builder().addAll(Structure.field_236384_t_).add(structure).build();
       }
       /*
        * Adds the structure's spacing into several places so that the structure's spacing remains
        * correct in any dimension or worldtype instead of not spawning.
        *
        * However, it seems it doesn't always work for code made dimensions as they read from
        * this list beforehand. Use the WorldEvent.Load event to add
        * the structure spacing from this list into that dimension.
        */
       DimensionStructuresSettings.field_236191_b_ =
               ImmutableMap.<Structure<?>, StructureSeparationSettings>builder()
                       .putAll(DimensionStructuresSettings.field_236191_b_)
                       .put(structure, structureSeparationSettings)
                       .build();
   }
   
   /** Register the pieces of your structure if this has not been done by a jigsaw pool.
    * <br> You MUST call this method to allow the chunk to save. 
    * <br> Otherwise the chunk won't save and complain it's missing a registry id for the structure piece. Darn vanilla...
    * */
   public static IStructurePieceType registerStructurePiece(IStructurePieceType type, String key) {
       return Registry.register(Registry.STRUCTURE_PIECE, new ResourceLocation(Tardis.MODID, key), type);
   }

}
