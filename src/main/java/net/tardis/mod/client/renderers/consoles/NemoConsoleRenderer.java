package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.NemoConsoleModel;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class NemoConsoleRenderer extends TileEntityRenderer<NemoConsoleTile> {
    
	private static final NemoConsoleModel MODEL = new NemoConsoleModel(text -> RenderType.getEntityCutout(text));
	
	
	public NemoConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	
	@Override
	public void render(NemoConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 0.35, 0.5);
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/nemo.json.png");
		if(console.getVariant() != null) {
			texture = console.getVariant().getTexture();
		}
		RenderSystem.enableRescaleNormal();
		float scale = 0.25F;
		matrixStackIn.scale(scale, scale, scale);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		MODEL.render(console, scale, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
		RenderSystem.disableRescaleNormal();
		
		matrixStackIn.push();
		matrixStackIn.scale(2, 2, 2);
		matrixStackIn.translate(0.4, -1, 2);
		matrixStackIn.rotate(Vector3f.XN.rotationDegrees(45));
		matrixStackIn.translate(-0.1, 0.6, 0.1);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
		
		matrixStackIn.pop();
	}

}
