package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.containers.ReclamationContainer;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ReclamationTile;

public class ReclamationBlock extends TileBlock implements IDontBreak {

    public ReclamationBlock() {
        super(Prop.Blocks.BASIC_TECH.get().hardnessAndResistance(999F));
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            ReclamationTile tile = (ReclamationTile) worldIn.getTileEntity(pos);
            NetworkHooks.openGui((ServerPlayerEntity) player, new INamedContainerProvider() {

                @Override
                public Container createMenu(int id, PlayerInventory inv, PlayerEntity p_createMenu_3_) {
                    return new ReclamationContainer(id, inv, tile);
                }

                @Override
                public ITextComponent getDisplayName() {
                    return new TranslationTextComponent("container.tardis.reclamation_unit");
                }
            }, buf -> buf.writeBlockPos(pos));

        }
        return ActionResultType.PASS;
    }
}
