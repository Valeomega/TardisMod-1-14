package net.tardis.mod.tileentities.machines;

import com.google.common.collect.Lists;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RecipeWrapper;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.recipe.SpectrometerRecipe;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.tileentities.TTiles;

import javax.annotation.Nullable;
import java.util.List;

public class NeutronicSpectrometerTile extends TileEntity implements ITickableTileEntity {

    public static final int INPUT_SLOT = 0;
    public static final int SONIC_SLOT = 1;

    private int progressTicks;
    private int downloadTicks;
    private ItemStackHandler inventory;
    private List<Schematic> schematics = Lists.newArrayList();

    private RecipeWrapper recipeWrapper;

    private int maxTicks = 0;

    public NeutronicSpectrometerTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
        this.inventory = new ItemStackHandler(2);
        this.recipeWrapper = new RecipeWrapper(this.inventory);
    }

    public NeutronicSpectrometerTile(){
        this(TTiles.SPECTROMETER.get());
    }


    @Override
    public void tick() {

        SpectrometerRecipe currentRecipe = this.getRecipe();

        if(currentRecipe != null && this.canAdvanceProgress(currentRecipe)){
            this.maxTicks = currentRecipe.getTicks();
            ++this.progressTicks;

            if(this.progressTicks >= currentRecipe.getTicks()){
                this.finish(currentRecipe);
            }

        }
        else this.progressTicks = 0;

        this.downloadToSonic();

    }

    public void downloadToSonic(){
        ItemStack sonicStack = this.inventory.getStackInSlot(SONIC_SLOT);
        if(sonicStack.getItem() instanceof SonicItem && !this.schematics.isEmpty()){

            ++this.downloadTicks;

            if(this.downloadTicks > 200){

                sonicStack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
                    this.downloadTicks = 0;
                    for(Schematic s : this.schematics){
                        cap.addSchematic(s);
                    }
                    this.schematics.clear();
                    this.markDirty();
                });

            }

        }
        else this.downloadTicks = 0;
    }

    public boolean canAdvanceProgress(SpectrometerRecipe rec){
        return rec.matches(this.recipeWrapper, world);
    }

    public void finish(SpectrometerRecipe recipe){
        this.inventory.extractItem(INPUT_SLOT, 1, false);
        this.schematics.add(recipe.getSchematicObject());
        this.markDirty();
    }

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    @Nullable
    public SpectrometerRecipe getRecipe(){

        for(SpectrometerRecipe rec : world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.SPECTROMETER_TYPE)){
            if(rec != null && rec.matches(this.recipeWrapper, world)){
                return rec;
            }
        }

        return null;

    }

    public float getProgress() {
        return this.maxTicks == 0  ? 0 : this.progressTicks / (float)maxTicks;
    }

    public float getDownloadProgress(){
        return this.downloadTicks / 200.0F;
    }

    /** Check if the this tile has any schematics stored
     * <br> For client side use in the container gui ONLY */
    public boolean hasSchematics() {
        return !this.schematics.isEmpty();
    }
}
