package net.tardis.mod.containers;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.containers.slot.EngineSlot;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.items.ArtronCapacitorItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class EngineContainer extends Container{

	Direction panel;
	
	protected EngineContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	/** Client Only constructor */
	public EngineContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		this(TContainers.ENGINE.get(), id);
		this.panel = Direction.byIndex(buf.readInt());
		ClientHelper.getClientWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(world -> {
    		this.init(inv, world.getEngineInventoryForSide(this.panel));
		});
	}
	
	/** Server Only constructor */
	public EngineContainer(int id, PlayerInventory inv, PanelInventory engine, Direction panel) {
		this(TContainers.ENGINE.get(), id);
		this.panel = panel;
		this.init(inv, engine);
	}
	
	public void init(PlayerInventory inv, PanelInventory engine) {
		
		if(engine.getPanelDirection() == Direction.NORTH) {
			
			for(int i = 0; i < 4; ++i) {
				EngineSlot slot = new EngineSlot(engine.getPanelDirection(), engine, i, 18 + i * 42, 1);
				
				if(i == 0)
					slot.setFilter(stack -> stack.getItem() == TItems.DEMAT_CIRCUIT.get());
				if(i == 1)
					slot.setFilter(stack -> stack.getItem() == TItems.NAV_COM.get());
				if(i == 2)
					slot.setFilter(stack -> stack.getItem() == TItems.CHAMELEON_CIRCUIT.get());
				if(i == 3)
					slot.setFilter(stack -> stack.getItem() == TItems.TEMPORAL_GRACE.get());
				
				this.addSlot(slot);
			}
			
			for(int i = 0; i < 4; ++i) {
				
				EngineSlot slot = new EngineSlot(engine.getPanelDirection(), engine, i + 4, 18 + i * 42, 22);
				
				if(i == 0)
					slot.setFilter(stack -> stack.getItem() == TItems.FLUID_LINK.get());
				if(i == 1)
					slot.setFilter(stack -> stack.getItem() == TItems.STABILIZERS.get());
				if(i == 2)
					slot.setFilter(stack -> stack.getItem() == TItems.INTERSTITIAL_ANTENNA.get());
				if(i == 3)
					slot.setFilter(stack -> stack.getItem() == TItems.SHEILD_GENERATOR.get());
				
				this.addSlot(slot);
			}
			
		}
		else if(engine.getPanelDirection() == Direction.SOUTH) {
			for(int i = 0; i < 10; ++i) {
				this.addSlot(new EngineSlot(engine.getPanelDirection(), engine, i, 16 + (int)Math.ceil((i / 2) * 32.5), 1 + (i % 2) * 21));
			}
		}
		
		else if(engine.getPanelDirection() == Direction.EAST) {
			for(int i = 0; i < 4; ++i) {
				this.addSlot(new SlotItemHandler(engine, i, 32 + (i / 2) * 22, -1 + (i % 2) * 22));
			}
			
			this.addSlot(new SlotItemHandler(engine, 4, 119, 10));
			
		}
		
		else if(engine.getPanelDirection() == Direction.WEST) {
			for(int i = 0; i < 10; ++i) {
				this.addSlot(new EngineSlot(stack -> stack.getItem() instanceof ArtronCapacitorItem, this.getPanelDirection(), engine, i, 19 + (i / 2) * 31, 1 + (i % 2) * 21));
			}
			
		}
		
		//Add Players
		for(Slot s : TInventoryHelper.createPlayerInv(inv, 0, -19)) {
			this.addSlot(s);
		}
		
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}

	/** Stop players from shift clicking as they need to actually place items via left click for the minigame system */
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
	   return ItemStack.EMPTY;
	}

	/** Get the panel inventory for a specific direction
     * <p> NORTH - Subsystems
     * <br> EAST - Refueling/Attunement
     * <br> SOUTH - Upgrades
     * <br> WEST - Artron Banks*/
	public Direction getPanelDirection(){
		return this.panel;
	}

}
