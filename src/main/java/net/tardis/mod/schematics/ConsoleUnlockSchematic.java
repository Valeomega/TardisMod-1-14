package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.schematics.types.SchematicType;
import net.tardis.mod.tileentities.ConsoleTile;

public class ConsoleUnlockSchematic extends Schematic{

    private ResourceLocation console;

    public ConsoleUnlockSchematic(SchematicType type) {
        super(type);
    }

    public void setConsole(ResourceLocation console){
        this.console = console;
    }

    @Override
    public void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player) {
        Console console = ConsoleRegistry.CONSOLE_REGISTRY.get().getValue(this.console);

        if(console != null && !tile.getUnlockManager().getUnlockedConsoles().contains(console)){
            tile.getUnlockManager().addConsole(console);
            player.sendStatusMessage(new TranslationTextComponent(Constants.Translations.UNLOCKED_CONSOLE, this.getDisplayName()), true);
        }

    }
}
