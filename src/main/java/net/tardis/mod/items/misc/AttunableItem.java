package net.tardis.mod.items.misc;

import net.minecraft.item.ItemStack;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.items.BaseItem;
import net.tardis.mod.tileentities.ConsoleTile;
/** Generic Item class that adds nbt tags to the item stack when attuned
 * * <br> If you do not want behaviours of either {@linkplain IAttunable} or {@linkplain IConsoleBound}, such as turning the input item into a different item, use the {@linkplain AttunableRecipeGen} to make a recipe json that does this*/
public class AttunableItem extends BaseItem implements IAttunable{
	
	public AttunableItem(Properties properties) {
		super(properties);
	}

    /** In this implementation, add NBT tags to the item stack that sets the correct Tardis World Key and Tardis Name.
     * <br> This can be used to allow tooltips to have the correct values*/
    @Override
	public ItemStack onAttuned(ItemStack stack, ConsoleTile tile) {
		stack.getOrCreateTag().putString(Constants.CONSOLE_ATTUNMENT_NBT_KEY, tile.getWorld().getDimensionKey().getLocation().toString());
		tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> stack.getOrCreateTag().putString(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY, data.getTARDISName()));
		return stack;
	}

    @Override
	public int getAttunementTime() {
		return Constants.ATTUNEMENT_TIME_LESSER;
	}

	/**
	 * Allows an ItemStack that does not implement {@linkplain IAttunable} to recieve similar nbt tags
	 * @param stack
	 * @param tile
	 * @return
	 */
	public static ItemStack completeAttunement(ItemStack stack, ConsoleTile tile) {
		stack.getOrCreateTag().putString(Constants.CONSOLE_ATTUNMENT_NBT_KEY, tile.getWorld().getDimensionKey().getLocation().toString());
		tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> stack.getOrCreateTag().putString(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY, data.getTARDISName()));
		return stack;
	}
}
