package net.tardis.mod.helper;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.tileentities.ConsoleTile;

public class LandingSystem {
	
	
	public static boolean isSafe(World world, BlockPos pos) {
		return world.getBlockState(pos).getMaterial().isReplaceable();
	}
	
	public static boolean canLand(World world, BlockPos pos, AbstractExterior exterior, ConsoleTile console) {
		BlockPos up = new BlockPos(exterior.getWidth(console), exterior.getHeight(console), exterior.getWidth(console));
		BlockPos down = new BlockPos(exterior.getWidth(console), 0, exterior.getWidth(console));
		for(BlockPos check : BlockPos.getAllInBoxMutable(pos.subtract(down), pos.add(up))){
			if(!isSafe(world, check))
				return false;
		}
		return world.getBlockState(pos.down()).isSolid();
	}
	
	public static BlockPos findVaildLandSpot(World world, BlockPos dest, EnumLandType type, AbstractExterior ext, ConsoleTile console) {
		
		if(canLand(world, dest, ext, console))
			return dest;
		
		if(type == EnumLandType.UP) {
			return getLandSpotUp(world, dest, ext, console);
		}
		
		if(type == EnumLandType.DOWN) {
			return getLandSpotDown(world, dest, ext, console);
		}
		
		return BlockPos.ZERO;
		
	}
	
	public static BlockPos getLandSpotUp(World world, BlockPos dest, AbstractExterior ext, ConsoleTile console) {
		for(int y = dest.getY(); y < world.getHeight(); ++y) {
			BlockPos test = new BlockPos(dest.getX(), y, dest.getZ());
			if(canLand(world, test, ext, console))
				return test;
		}
		return BlockPos.ZERO;
	}
	
	public static BlockPos getLandSpotDown(World world, BlockPos dest, AbstractExterior ext, ConsoleTile console) {
		for(int y = dest.getY(); y > 0; --y) {
			BlockPos test = new BlockPos(dest.getX(), y, dest.getZ());
			if(canLand(world, test, ext, console))
				return test;
		}
		return BlockPos.ZERO;
	}
	
	/**
	 * Searches for a suitable position that isn't above the Nether Roof
	 * @implNote This is better than using world heightmap, because on worlds with a bedrock roof, it will allow you to get past the roof
	 * @param world
	 * @param pos
	 * @return
	 */
	public static BlockPos getTopBlock(World world, BlockPos pos) {
		for(int y = world.getHeight(); y > 0; --y) {
			BlockPos newPos = new BlockPos(pos.getX(), y, pos.getZ());
			BlockState underState = world.getBlockState(newPos.down());
			
			if(!willBlockStateCauseSuffocation(world, pos) && underState.isSolid() && !isPosBelowOrAboveWorld(world, newPos.getY())) {
				return newPos;
			}
		}
		return pos;
	}
	
	/**
	 * Checks if BlockPos is within World Border and not beyond it
	 * @param world
	 * @param pos
	 * @return true if within border, false if beyond border
	 */
	public static boolean isBlockWithinWorldBorder(World world, BlockPos pos) {
		return world.getWorldBorder().contains(pos);
	}
	
	/**
	 * Checks if BlockPos is within World Border and not beyond it
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @return true if within border, false if beyond border
	 */
	public static boolean isBlockWithinWorldBorder(World world, int x, int y, int z) {
		return world.getWorldBorder().contains(new BlockPos(x,y,z));
	}
	
	public static boolean isPosBelowOrAboveWorld(int y) {
		return y <= 0 || y >= 256;
	}
	
	public static BlockPos getLand(World world, BlockPos dest, EnumLandType type, ConsoleTile console) {
		BlockPos pos = findVaildLandSpot(world, dest, type, console.getExteriorType(), console);
		if(pos.equals(BlockPos.ZERO))
			pos = findVaildLandSpot(world, dest, type == EnumLandType.DOWN ? EnumLandType.UP : EnumLandType.DOWN, console.getExteriorType(), console);
		return pos;
	}
	/**
	 * Dimension Specific version of the check, prevents users from teleporting to the Nether roof
	 * @implNote If returns false, will search for safe spot that is not above the Nether roof
	 * @param dim
	 * @param y
	 * @return
	 */
	public static boolean isPosBelowOrAboveWorld(World dim, int y) {
		if (dim.getDimensionKey() == World.THE_NETHER) {
			return y <= 0 || y >= 126;
		}
		return y <= 0 || y >= 256;
	}
	
	/**
	 * Checks if the blockstate will cause entity to suffocate in it.
	 * <br> This attempts to reproduce behaviour shown in the private AbstractBlock suffocates predicate
	 * <br> Do not use AbstractBlock#causesSuffocation because that does not actually relate to suffocation at all, it is a rendering related method misnamed in 1.16 mappings 
	 * @param world
	 * @param pos
	 * @return true if causes suffocating, false if it doesn't
	 */
    public static boolean willBlockStateCauseSuffocation(World world, BlockPos pos) {
    	BlockState state = world.getBlockState(pos);
    	return state.getMaterial().blocksMovement() && state.hasOpaqueCollisionShape(world, pos);
    }
    
	public static BlockPos validateBlockPos(BlockPos pos, int maxHeight) {
		if(pos.getY() < 0)
			return new BlockPos(pos.getX(), 0, pos.getZ());
		if(pos.getY() > maxHeight)
			return new BlockPos(pos.getX(), maxHeight, pos.getZ());
		return pos;
	}

	/**
	 *
	 * @param world
	 * @param pos - Block Below the exterior
	 * @return True if the TARDIS should turn into an entity and fall
	 */
	public static boolean shouldTARDISFall(World world, BlockPos pos){
		return world.getBlockState(pos).getCollisionShape(world, pos).isEmpty();
	}

}
