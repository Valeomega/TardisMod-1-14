package net.tardis.mod.helper;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class BlockPosHelper {
    public static BlockPos lerp(BlockPos a,BlockPos b, float t){
        t = MathHelper.clamp(t, 0f, 1f);

        return a.add(
                (b.getX() - a.getX()) * t,
                (b.getY() - a.getY()) * t,
                (b.getZ() - a.getZ()) * t);
    }
}
