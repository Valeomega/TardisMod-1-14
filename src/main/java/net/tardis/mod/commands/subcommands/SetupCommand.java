package net.tardis.mod.commands.subcommands;


import java.util.Collections;
import java.util.Optional;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.KeyItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.subsystem.ChameleonSubsystem;
import net.tardis.mod.subsystem.FlightSubsystem;
import net.tardis.mod.subsystem.FluidLinksSubsystem;
import net.tardis.mod.subsystem.NavComSubsystem;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.subsystem.TemporalGraceSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class SetupCommand extends TCommand{

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	private static int setUpTardis(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		ItemStack key = new ItemStack(TItems.KEY.get());
		ServerWorld world = player.getServerWorld();
		if (TardisHelper.getConsoleInWorld(world).isPresent()) {
			if (key.getItem() instanceof KeyItem) {
				KeyItem key2 = (KeyItem)key.getItem();
				ItemStack attunedKey = key2.onAttuned(key, TardisHelper.getConsoleInWorld(world).get());
				if (attunedKey != null)
				    player.addItemStackToInventory(attunedKey);
				else
			        player.addItemStackToInventory(key);
			}
			return setUpTardis(context, world);
		}
		else {
			context.getSource().sendErrorMessage(new TranslationTextComponent(Constants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
			return 0;
		}
		
	}
	
	private static int setUpTardis(CommandContext<CommandSource> context, ServerWorld world){
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
        CommandSource source = context.getSource();
        console.ifPresent(tile -> {
        	tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        		//Subsystem setup
    			PanelInventory inv = data.getEngineInventoryForSide(Direction.NORTH);
    			//Have to manually set each slot because each slot is filtered
    			tile.getSubsystem(FlightSubsystem.class).ifPresent(flight -> {inv.setStackInSlot(0, new ItemStack(flight.getItemKey())); flight.setActivated(true);});
    			tile.getSubsystem(NavComSubsystem.class).ifPresent(navcom -> {inv.setStackInSlot(1, new ItemStack(navcom.getItemKey())); navcom.setActivated(true);});
    			tile.getSubsystem(ChameleonSubsystem.class).ifPresent(chameleon -> {inv.setStackInSlot(2, new ItemStack(chameleon.getItemKey())); chameleon.setActivated(true);});
    			tile.getSubsystem(TemporalGraceSubsystem.class).ifPresent(grace -> {inv.setStackInSlot(3, new ItemStack(grace.getItemKey())); grace.setActivated(true);});
    			tile.getSubsystem(FluidLinksSubsystem.class).ifPresent(links -> {inv.setStackInSlot(4, new ItemStack(links.getItemKey())); links.setActivated(true);});
    			tile.getSubsystem(StabilizerSubsystem.class).ifPresent(stabilizer -> {inv.setStackInSlot(5, new ItemStack(stabilizer.getItemKey())); stabilizer.setActivated(true);});
    			tile.getSubsystem(AntennaSubsystem.class).ifPresent(antenna -> {inv.setStackInSlot(6, new ItemStack(antenna.getItemKey())); antenna.setActivated(true);});
    			tile.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> {inv.setStackInSlot(7, new ItemStack(shield.getItemKey())); shield.setActivated(true);});
    			
    			//Artron Capacitors
    			PanelInventory artronInv = data.getEngineInventoryForSide(Direction.WEST);
    			for(int i = 0; i < artronInv.getSlots(); ++i) {
    				artronInv.setStackInSlot(i, new ItemStack(TItems.ARTRON_CAPACITOR_HIGH.get()));
    			}
				TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);
    			
    			tile.updateArtronValues();
    			tile.setArtron(Float.MAX_VALUE - 1);
    			tile.updateClient();
    			source.sendFeedback(new TranslationTextComponent("command.tardis.setup.success", tardisIdentifier), true);
    		});
        });
        return Command.SINGLE_SUCCESS;
    }

	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher){
	    return Commands.literal("setup").requires(context -> context.hasPermissionLevel(2))
		    .executes(context -> { return setUpTardis(context, context.getSource().asPlayer());})
		        .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
		            .executes(context -> setUpTardis(context, DimensionArgument.getDimensionArgument(context, "tardis"))));
	}

}