package net.tardis.mod.world.dimensions;

import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ICloudRenderHandler;
import net.minecraftforge.client.ISkyRenderHandler;
import net.minecraftforge.client.IWeatherRenderHandler;
import net.tardis.mod.client.renderers.sky.MoonSkyRenderer;

@OnlyIn(Dist.CLIENT) //This is one of the few cases where we should be using OnlyIn - Vanilla uses this is a marker hack to strip bytecode from one side (server or client)
public class MoonSkyProperty extends DimensionRenderInfo {
    
	public MoonSkyProperty() {
		super(Float.NaN, true, FogType.NONE, true, false);
	}
	
    public MoonSkyProperty(float cloudLevel, boolean hasGround, FogType skyType, boolean forceBrightLightmap,
            boolean constantAmbientLight) {
        super(cloudLevel, hasGround, skyType, forceBrightLightmap, constantAmbientLight);
    }

    //adjustSkyColor
    @Override
    public Vector3d func_230494_a_(Vector3d color, float sunHeight) {
        return new Vector3d(0,0,0);
    }

    //isFoggyAt
    @Override
    public boolean func_230493_a_(int camX, int camY) {
        return false;
    }

    @Override
    public ICloudRenderHandler getCloudRenderHandler() {
        return super.getCloudRenderHandler();
    }

    @Override
    public IWeatherRenderHandler getWeatherRenderHandler() {
        return super.getWeatherRenderHandler();
    }

    @Override
    public ISkyRenderHandler getSkyRenderHandler() {
        return new MoonSkyRenderer();
    }
}