package net.tardis.mod.client.guis.manual;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.SharedLibrary;

public class CoverPage extends Page{

    private ResourceLocation texture;
    private String title;

    @Override
    public void render(MatrixStack stack, FontRenderer font, int x, int y, int width, int height) {

        //Draw title
        font.drawString(stack, this.title, x, y, 0x00000);

        //Draw icon
       if(this.texture != null){
           RenderSystem.enableBlend();
           Minecraft.getInstance().getTextureManager().bindTexture(this.texture);
           BufferBuilder bb = Tessellator.getInstance().getBuffer();

           int w = WIDTH, h = WIDTH;

           x += (int)Math.floor(w / 2.0) - 10;
           y = y + h / 2;

           bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);

           bb.pos(x, y, 0).tex(0, 0).endVertex();
           bb.pos(x, y + h, 0).tex(0, 1).endVertex();
           bb.pos(x + w, y + h, 0).tex(1, 1).endVertex();
           bb.pos(x + w, y, 0).tex(1, 0).endVertex();


           Tessellator.getInstance().draw();

           RenderSystem.disableBlend();
       }

    }

    public void setIcon(ResourceLocation loc){
        this.texture = new ResourceLocation(loc.getNamespace(), "textures/manual/" + loc.getPath() + ".png");
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
