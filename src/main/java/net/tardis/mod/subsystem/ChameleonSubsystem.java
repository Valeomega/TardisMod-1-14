package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class ChameleonSubsystem extends Subsystem{


	public ChameleonSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return super.serializeNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
	}

	
	@Override
	public boolean stopsFlight() {
		return false;
	}

    @Override
    public void onTakeoff() {}

    @Override
    public void onLand() {
        this.damage(null, 1);
        if (console.hasWorld() && console.getWorld().rand.nextDouble() < 0.05)
            this.damage((ServerPlayerEntity)this.console.getPilot(), (int) (console.getWorld().rand.nextDouble() * 50));
    }

    @Override
    public void onFlightSecond() {}
    
    @Override
	public SparkingLevel getSparkState() {
		return SparkingLevel.NONE;
	}
    
    public boolean isActiveCamo(boolean checkDamage) {
    	if(this.console.getExteriorType() != ExteriorRegistry.DISGUISE.get())
    		return false;
    	return checkDamage ? this.canBeUsed() : true;
    	
    }

}
