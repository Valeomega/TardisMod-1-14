package net.tardis.mod.misc;

import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.items.misc.AttunableItem;
import net.tardis.mod.items.misc.IAttunable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.AttunementProgressMessage;
import net.tardis.mod.recipe.AttunableRecipe;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.tileentities.inventory.PanelInventoryWrapper;

public class AttunementHandler {

    public static final int ATTUNEMENT_SLOT = 4;

    private Supplier<PanelInventory> inventorySupplier;
    private World world;
    private int currentAttunementTime;
    private int maxAttunementTime;
    private AttunableRecipe recipe;

    /** Only for client, to update progress bar */
    private float clientAttunementProgress = 0;

    public AttunementHandler(Supplier<PanelInventory> inv, World world){
        this.inventorySupplier = inv;
        this.world = world;
    }

    //If ticking, in flight
    public void tick(ConsoleTile tile){
        if(this.isAttuning()){
            this.maxAttunementTime = this.getMaxAttunementTicks();
            ++this.currentAttunementTime;
            if(this.currentAttunementTime >= this.maxAttunementTime)
                this.complete(tile);
            else this.updateClients();
        }
        else if(needsResetProgress())
            this.resetAttuning(ItemStack.EMPTY);
    }

    public boolean needsResetProgress() {
        return this.currentAttunementTime != 0 || this.maxAttunementTime != 0;
    }

    public boolean isAttuning(){

        //If attunable
        if(this.getAttuningItem().getItem() instanceof IAttunable){
            return true;
        }
        //Look for recipe, if (none were found or the recipe was empty), return false, else true
        this.recipe = this.findRecipe();
        return (this.recipe != null && this.recipe != AttunableRecipe.EMPTY_RECIPE);

    }

    public void complete(ConsoleTile tile){

        ItemStack attuningItem = this.getAttuningItem();

        if(attuningItem.getItem() instanceof IAttunable){
            this.inventorySupplier.get().setStackInSlot(ATTUNEMENT_SLOT, ((IAttunable)attuningItem.getItem()).onAttuned(attuningItem, tile));
        }
        //else recipe stuff

        if(this.recipe != null && this.recipe != AttunableRecipe.EMPTY_RECIPE){
        	ItemStack resultStack = recipe.getRecipeOutput();
        	if (attuningItem.getCount() > recipe.getRecipeOutput().getCount()) //Account for itemstack whose count is larger than 1
    			resultStack.setCount(attuningItem.getCount());
            this.inventorySupplier.get().setStackInSlot(ATTUNEMENT_SLOT, recipe.shouldAddNBTTags() ? AttunableItem.completeAttunement(resultStack, tile) : resultStack);
            this.recipe = AttunableRecipe.EMPTY_RECIPE; //set recipe to empty so we will look for a new one, but also not set to null to prevent hard crashes if a logic issue occurs
        }
        this.resetAttuning(ItemStack.EMPTY);
        world.playSound(null, tile.getPos(), TSounds.REACHED_DESTINATION.get(), SoundCategory.BLOCKS, 0.3F, 1F);
    }

    public void serialize(CompoundNBT nbt){
        nbt.putInt("attunement_ticks", this.currentAttunementTime);
    }

    public void deserialize(CompoundNBT nbt){
        this.currentAttunementTime = nbt.getInt("attunement_ticks");
    }

    public ItemStack getAttuningItem(){
        return inventorySupplier.get().getStackInSlot(ATTUNEMENT_SLOT);
    }

    public void resetAttuning(ItemStack itemStack){
        //Reset if empty
        if(itemStack.isEmpty()){
            this.currentAttunementTime = 0;
            this.maxAttunementTime = 0;
            return;
        }
        //If item implements attunable interface
        if(itemStack.getItem() instanceof IAttunable){
            this.currentAttunementTime = 0;
            this.clientAttunementProgress = 0;
            this.maxAttunementTime = ((IAttunable)itemStack.getItem()).getAttunementTime();
        }
        //If the recipe is empty (i.e. the recipe was finished completing)
        if(recipe == AttunableRecipe.EMPTY_RECIPE){
            this.currentAttunementTime = 0;
            this.clientAttunementProgress = 0;
            this.maxAttunementTime = recipe.getAttunementTicks();
        }
        this.updateClients();
    }

    public AttunableRecipe findRecipe(){
    	if (this.recipe == AttunableRecipe.EMPTY_RECIPE || this.recipe == null) {
    		for (AttunableRecipe recipe : AttunableRecipe.getAllRecipes(world)) {
                if (recipe.matches(new PanelInventoryWrapper(inventorySupplier.get()), world)) {
                    this.recipe = recipe;
                }
            }
    	}
        return this.recipe; //don't return null otherwise we will never find the recipe
    }

    public int getMaxAttunementTicks(){
        ItemStack stack = this.getAttuningItem();

        if(stack.getItem() instanceof IAttunable)
            return ((IAttunable)stack.getItem()).getAttunementTime();

        if((this.recipe = this.findRecipe()) != null && this.recipe != AttunableRecipe.EMPTY_RECIPE) {
        	if (stack.getCount() > recipe.getRecipeOutput().getCount()) {
        		int adjustedTime = this.recipe.getAttunementTicks() * stack.getCount();
        		return adjustedTime;
        	}
        	else {
        		return this.recipe.getAttunementTicks();
        	}
        }

        return 0;

    }

    public void updateClients(){
        Network.sendToAllInWorld(new AttunementProgressMessage(this.currentAttunementTime / (float)this.maxAttunementTime), (ServerWorld)world);
    }
    /** Get the attunement progress for use in rendering. <br> For Client-Side USE ONLY*/
    public float getClientAttunementProgress() {
        return this.clientAttunementProgress;
    }
    /** Set the Client-Side attunement progress*/
    public void setClientAttunementProgress(float progress) {
        this.clientAttunementProgress = progress;
    }
}
