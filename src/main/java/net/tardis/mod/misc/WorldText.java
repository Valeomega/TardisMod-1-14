package net.tardis.mod.misc;

import java.util.List;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.culling.ClippingHelper;
import net.minecraft.client.renderer.tileentity.SignTileEntityRenderer;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.text.Style;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.constants.Constants;
import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class WorldText {

    private static boolean renderBounds = false;
    public float width, height, scale;
    private int color = 0xFFFFFF;
    private boolean dropShadow = false;

    public WorldText(float width, float height, float scale, int color) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.scale = scale;
    }

    public WorldText(float width, float height, float scale, TextFormatting color) {
        this(width, height, scale, color.getColor());
    }

    public void setDropShadow(boolean shadow){
        this.dropShadow = shadow;
    }

    @OnlyIn(Dist.CLIENT)
    public void renderText(MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, String... lines){

        FontRenderer fr = Minecraft.getInstance().fontRenderer;

        float offsetY = 0;

        for(int i = 0; i < lines.length; ++i){
            IReorderingProcessor processor = IReorderingProcessor.fromString(lines[i], Style.EMPTY);

            matrixStack.push();

            float scale = this.getScale(fr, lines[i]);
            matrixStack.translate(0, offsetY, 0);
            matrixStack.scale(scale, scale, scale);

            fr.drawEntityText(processor, 0, 0, this.color, this.dropShadow, matrixStack.getLast().getMatrix(), buffer, false, 0, combinedLight);

            matrixStack.pop();
            offsetY += this.scale * (fr.FONT_HEIGHT + 1);

        }

    }

    public float getScale(FontRenderer fr, String line){

        int lineWidth = fr.getStringWidth(line);
        float scale = this.width / (float)lineWidth;
        scale = MathHelper.clamp(scale, 0, this.scale);

        return scale;
    }

    @OnlyIn(Dist.CLIENT)
    private void renderBounds() {
        BufferBuilder bb = Tessellator.getInstance().getBuffer();
        GlStateManager.disableTexture();
        bb.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);

        bb.pos(0, 0, 0).color(1F, 0, 0, 1F).endVertex();
        bb.pos(width, 0, 0).color(1F, 0, 0, 1).endVertex();

        bb.pos(0, height, 0).color(1F, 0, 0, 1F).endVertex();
        bb.pos(width, height, 0).color(1F, 0, 0, 1).endVertex();

        bb.pos(0, 0, 0).color(0, 1F, 0, 1F).endVertex();
        bb.pos(0, height, 0).color(0, 1F, 0, 1F).endVertex();

        bb.pos(width, 0, 0).color(0, 1F, 0, 1F).endVertex();
        bb.pos(width, height, 0).color(0, 1F, 0, 1F).endVertex();

        Tessellator.getInstance().draw();

        GlStateManager.enableTexture();
    }
}
