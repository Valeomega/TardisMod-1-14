package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class LoyaltyCommand extends TCommand{

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	private static int checkTardisLoyalty(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		return checkTardisLoyalty(context, player.getServerWorld());
	}

	private static int checkTardisLoyalty(CommandContext<CommandSource> context, ServerWorld world){
        CommandSource source = context.getSource();
        if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
        	Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
        	console.ifPresent(tile -> {
        	tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {

				EmotionHandler emotionHandler = tile.getEmotionHandler();

				TextComponent playerLoyaltiesTextComponent = new StringTextComponent("");
				playerLoyaltiesTextComponent.modifyStyle(style -> {
					style.setFormatting(TextFormatting.WHITE);
					style.setHoverEvent(null);
					style.setClickEvent(null);
					return style;
				});

				Set<UUID> crew = emotionHandler.getLoyaltyTrackingCrew();
				for (UUID id : crew) {
					if (id == null)
						continue; //Allow the null id to be passed in to the player text component. We will handle null ids in the TextHelper#getPlayerTextObject

					TextComponent thisPlayer = TextHelper.getPlayerTextObject(world, id);
					StringTextComponent loyaltyValue = new StringTextComponent(String.format(" - (%d)\n", emotionHandler.getLoyalty(id)));
					playerLoyaltiesTextComponent
							.appendSibling(thisPlayer)//this has special formatting, so will not get style from playerLoyaltiesTextComponent
							.appendSibling(loyaltyValue);//loyalty value does not, and will just use the style from playerLoyaltiesTextComponent
				}
				TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);

				source.sendFeedback(new TranslationTextComponent("command.tardis.loyalty.check", tardisIdentifier, playerLoyaltiesTextComponent), true);
    		    });
            });
            return Command.SINGLE_SUCCESS;
        }
        else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(Constants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
    }

	private static int addTardisLoyalty(CommandContext<CommandSource> context, ServerPlayerEntity player, int value) {
		return addTardisLoyalty(context, player.getServerWorld(), player, value);
	}

	private static int addTardisLoyalty(CommandContext<CommandSource> context, ServerWorld world, ServerPlayerEntity player, int value) {
		CommandSource source = context.getSource();
		if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
			Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
			console.ifPresent(tile -> {
				tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
					EmotionHandler emotionHandler = tile.getEmotionHandler();
					if (emotionHandler.getLoyaltyTrackingCrew().contains(player.getUniqueID())) {
						emotionHandler.addLoyalty(player, value);
						TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);
						source.sendFeedback(new TranslationTextComponent("command.tardis.loyalty.success", tardisIdentifier, player.getName(), emotionHandler.getLoyalty(player.getUniqueID())), true);
					}
					else {
						source.sendErrorMessage(new TranslationTextComponent("command.tardis.loyalty.no_player_found", player.getDisplayName()));
					}
					
				});
			});
			return Command.SINGLE_SUCCESS;
		}
		else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(Constants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
	}

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("loyalty")
                .requires(context -> context.hasPermissionLevel(2))
                .then(Commands.literal("check")
                        .executes(context -> checkTardisLoyalty(context, context.getSource().asPlayer()))
                        .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                                .executes(context -> checkTardisLoyalty(context, DimensionArgument.getDimensionArgument(context, "tardis")))
                        )
                ) // end check
                .then(Commands.literal("add")
                        .then(Commands.argument("value", IntegerArgumentType.integer())
								//if no tardis name specified, then add to the current dimension tardis
                                .executes(context -> addTardisLoyalty(context, context.getSource().asPlayer(), IntegerArgumentType.getInteger(context, "value"))
                                )//end above
								//else if a tardis name is specified, add it to them
                                .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                                        .executes(context -> addTardisLoyalty(context, DimensionArgument.getDimensionArgument(context, "tardis"), context.getSource().asPlayer(), IntegerArgumentType.getInteger(context, "value"))
                                        ) //If no player specified, add to the command executor
                                    .then(Commands.argument("player", EntityArgument.player())
                                        .executes(context -> addTardisLoyalty(context, EntityArgument.getPlayer(context, "player"), IntegerArgumentType.getInteger(context, "value")))
                                        ) //If a player was specified, use it
                        		)//end loyalty add to specific tardis
                        )//end value
                ); // end add
    }

}