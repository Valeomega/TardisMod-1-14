package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.ConsoleUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class ConsoleSchematicType extends SchematicType{

    @Override
    public Schematic deserialize(ResourceLocation key, JsonObject root) {

        ConsoleUnlockSchematic schematic = new ConsoleUnlockSchematic(this);
        schematic.setConsole(new ResourceLocation(root.get("result").getAsString()));
        return schematic;
    }

    @Override
    public JsonObject serialize(Schematic schematic) {
        return null;
    }
}
