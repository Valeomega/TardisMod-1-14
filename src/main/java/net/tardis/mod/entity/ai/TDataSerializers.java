package net.tardis.mod.entity.ai;

import net.minecraft.network.PacketBuffer;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.IDataSerializer;
import net.minecraft.util.ResourceLocation;

public class TDataSerializers {

    public static void register(){
        DataSerializers.registerSerializer(RESOURCE_LOCATION);
    }


    public static final IDataSerializer<ResourceLocation> RESOURCE_LOCATION = new IDataSerializer<ResourceLocation>() {

        @Override
        public void write(PacketBuffer buf, ResourceLocation value) {
            buf.writeResourceLocation(value);
        }

        public ResourceLocation read(PacketBuffer buf) {
            return buf.readResourceLocation();
        }

        @Override
        public ResourceLocation copyValue(ResourceLocation value) {
            return value;
        }
    };

}
