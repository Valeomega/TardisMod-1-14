package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.CrashType;
import net.tardis.mod.misc.CrashTypes;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisCollideRecieve extends FlightEvent{

	public static final Supplier<ArrayList<ResourceLocation>> CONTROLS = () -> Lists.newArrayList(
			ControlRegistry.COMMUNICATOR.get().getRegistryName(),
			ControlRegistry.RANDOM.get().getRegistryName(),
			ControlRegistry.DIMENSION.get().getRegistryName()
	);
	
	ConsoleTile other;
	
	public TardisCollideRecieve(FlightEventFactory entry, ArrayList<ResourceLocation> list) {
		super(entry, list);
	}
	
	public TardisCollideRecieve setOtherTARDIS(ConsoleTile other) {
		this.other = other;
		return this;
	}

	@Override
	public boolean onComplete(ConsoleTile tile) {
		boolean success = super.onComplete(tile);
		
		if(success && other != null) {
			other.setDestination(tile.getWorld().getDimensionKey(), TardisHelper.TARDIS_POS.south(3 + tile.getWorld().rand.nextInt(5)));
			other.initLand();
		}
		
		return success;
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		tile.damage(40);
		
		if(other != null) {
			other.damage(40);
			
			other.crash(CrashTypes.DEFAULT);
			other.updateClient();
		}
		
		tile.crash(CrashTypes.DEFAULT);
		tile.updateClient();
		
	}
	
	@Override
	public int calcTime(ConsoleTile console) {
		return console.flightTicks + 200;
	}
}
