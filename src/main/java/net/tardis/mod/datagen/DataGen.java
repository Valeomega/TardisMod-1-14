package net.tardis.mod.datagen;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.datagen.compat.EntityPickupBlacklistGen;
import net.tardis.mod.datagen.compat.MovableBlockBlacklistGen;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class DataGen {
	
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	@SubscribeEvent
	public static void onDataGen(GatherDataEvent event) {
		DataGenerator generator = event.getGenerator();
		ExistingFileHelper fileHelper = event.getExistingFileHelper();
//		WABlockTagGen tmBlockTagGen = new WABlockTagGen(generator, fileHelper);
//		generator.addProvider(new WAItemTagGen(generator, tmBlockTagGen, fileHelper));
//		generator.addProvider(new WABlockTagGen(generator, fileHelper));
//		generator.addProvider(new TardisLootTableGen(generator));
//	    generator.addProvider(new QuantiscopeRecipeGen(generator));
//	    generator.addProvider(new AlembicRecipeGen(generator));
//		generator.addProvider(new TardisBlockModelGen(generator));
//		generator.addProvider(new TardisBlockStateGen(generator));
//		generator.addProvider(new TardisBlockItemModelGen(generator));
//		generator.addProvider(new TardisPlantTag(generator));
		generator.addProvider(new TardisLangGen(generator));
//		generator.addProvider(new TardisARSTagGen(generator));
//		generator.addProvider(new TardisSoundFileGen(generator));
//		generator.addProvider(new AttunableRecipeGen(generator));
//		generator.addProvider(new SpectrometerRecipeGen(generator));
		generator.addProvider(new MovableBlockBlacklistGen(generator, fileHelper));
		generator.addProvider(new EntityPickupBlacklistGen(generator, fileHelper));
	}

}
