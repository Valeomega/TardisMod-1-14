package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.tileentities.ConsoleTile;

public class Fuel implements ConsoleData{

	float fuel;
	float maxFuel;
	
	public Fuel(float fuel, float maxFuel) {
		this.fuel = fuel;
		this.maxFuel = maxFuel;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setMaxArtron(this.maxFuel);
		tile.setArtron(fuel);
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeFloat(fuel);
		buff.writeFloat(this.maxFuel);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.fuel = buff.readFloat();
		this.maxFuel = buff.readFloat();
	}

	@Override
	public ConsoleUpdateMessage.DataTypes getDataType() {
		return ConsoleUpdateMessage.DataTypes.FUEL;
	}

}
