package net.tardis.mod.commands.argument;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.ars.ConsoleRoom;

public class ConsoleRoomArgument implements ArgumentType<ResourceLocation>{
	private static final Collection<String> EXAMPLES = Stream.of(ConsoleRoom.STEAM, ConsoleRoom.JADE).map((room) -> {
	      return room != null ? room.getRegistryName().toString() : ""; //This could be null when the world is being loaded, as the resource manager is reloaded
	   }).collect(Collectors.toList());
    private static final DynamicCommandExceptionType INVALID_CONSOLE_ROOM_EXCEPTION = new DynamicCommandExceptionType((room) -> {
      return new TranslationTextComponent("argument.tardis.console_room.invalid", room);
    });
	
	@Override
	public ResourceLocation parse(StringReader reader) throws CommandSyntaxException {
		return ResourceLocation.read(reader);
	}
	
	@Override
	public <S> CompletableFuture<Suggestions> listSuggestions(CommandContext<S> context, SuggestionsBuilder builder) {
		Collection<ResourceLocation> rooms = ConsoleRoom.getRegistry().keySet();
		return context.getSource() instanceof ISuggestionProvider ? ISuggestionProvider.func_212476_a(rooms.stream(), builder) : Suggestions.empty();
	}

	@Override
	public Collection<String> getExamples() {
		return EXAMPLES;
	}

	public static ConsoleRoomArgument getConsoleRoomArgument() {
	      return new ConsoleRoomArgument();
	}
	
	public static ConsoleRoom getConsoleRoom(CommandContext<CommandSource> context, String name) throws CommandSyntaxException {
		ResourceLocation resourcelocation = context.getArgument(name, ResourceLocation.class);
		ConsoleRoom room = ConsoleRoom.getRegistry().get(resourcelocation);
		if (room == null)
			throw INVALID_CONSOLE_ROOM_EXCEPTION.create(resourcelocation);
		else
			return room;
	}

}
