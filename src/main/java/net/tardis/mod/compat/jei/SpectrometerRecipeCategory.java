package net.tardis.mod.compat.jei;

import java.util.Arrays;
import java.util.List;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.SpectrometerRecipe;

public class SpectrometerRecipeCategory implements IRecipeCategory<SpectrometerRecipe> {

    private ResourceLocation TEXTURE = Helper.createRL("textures/gui/containers/spectrometer.png");
    public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "spectrometer");
    private IDrawable background;
	private IDrawable icon;
	
    public SpectrometerRecipeCategory(IGuiHelper gui) {
    	background = gui.createDrawable(TEXTURE, 0, 0, 176, 70);
		icon = gui.createDrawableIngredient(new ItemStack(TItems.SONIC.get()));
    }

    @Override
    public ResourceLocation getUid() {
        return NAME;
    }

    @Override
    public Class<? extends SpectrometerRecipe> getRecipeClass() {
        return SpectrometerRecipe.class;
    }

    @Override
    public String getTitle() {
        return "Spectrometer";
    }

    @Override
	public IDrawable getBackground() {
		return this.background;
	}
	
	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

    @Override
    public void setIngredients(SpectrometerRecipe recipe, IIngredients ingredients) {
    	ingredients.setInputIngredients(recipe.getIngredients());
    	ItemStack resultStack = new ItemStack(TItems.SONIC.get());
    	resultStack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
    		cap.addSchematic(recipe.getSchematicObject());
    	});
    	ingredients.setOutput(VanillaTypes.ITEM, resultStack); //Set the result stack to be a sonic which has our schematic
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, SpectrometerRecipe recipe, IIngredients ingredients) {
    	ItemStack[] item = new ItemStack[5];
		List<ItemStack> stacksInIngredient = Arrays.asList(recipe.getIngredient().getMatchingStacks());
		for(int i = 0; i < item.length; ++i) {
			if(i < stacksInIngredient.size())
				item[i] = stacksInIngredient.get(i);
			else item[i] = ItemStack.EMPTY;
		}
		
		JEIHelper.addInputSlot(recipeLayout, 0, 40, 26, item[0]);
		
		ItemStack resultStack = new ItemStack(TItems.SONIC.get());
    	resultStack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
    		cap.addSchematic(recipe.getSchematicObject());
    	});
    	
    	recipeLayout.getItemStacks().init(1, false, 146, 46);
		recipeLayout.getItemStacks().set(1, resultStack);
		
    }
}
