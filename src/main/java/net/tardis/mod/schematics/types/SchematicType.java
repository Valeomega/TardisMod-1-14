package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.schematics.Schematic;

public abstract class SchematicType extends ForgeRegistryEntry<SchematicType> {

    public abstract Schematic deserialize(ResourceLocation key, JsonObject root);
    public abstract JsonObject serialize(Schematic schematic);

    public static String readDisplayName(JsonObject root){
        return root.get("display_name").getAsString();
    }

}
