package net.tardis.mod.compat.jei;

import java.util.Arrays;
import java.util.List;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.recipe.WeldRecipe;

public class WeldRecipeCategory implements IRecipeCategory<WeldRecipe> {

	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "quantiscope_weld");
	private IDrawable background;
	private IDrawable icon;
	
	public WeldRecipeCategory(IGuiHelper gui) {
		background = gui.createDrawable(new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/weld_jei.png"), 12, 5, 153, 64);
		icon = gui.createDrawableIngredient(new ItemStack(TBlocks.quantiscope_brass.get()));
	}

	@Override
	public ResourceLocation getUid() {
		return NAME;
	}

	@Override
	public Class<WeldRecipe> getRecipeClass() {
		return WeldRecipe.class;
	}

	@Override
	public String getTitle() {
		return "Quantiscope Welding";
	}

	@Override
	public IDrawable getBackground() {
		return this.background;
	}

	@Override
	public IDrawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIngredients(WeldRecipe recipe, IIngredients ingredients) {
		
		ingredients.setInputIngredients(recipe.getIngredients());
		ingredients.setOutput(VanillaTypes.ITEM, new ItemStack(recipe.getResult().get().getOutput()));
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, WeldRecipe recipe, IIngredients ingredients) {
		
		ItemStack[] item = new ItemStack[5];
		List<ItemStack> stacksInIngredient = Arrays.asList(recipe.getRequiredIngredients().getMatchingStacks());
		for(int i = 0; i < item.length; ++i) {
			if(i < stacksInIngredient.size())
				item[i] = stacksInIngredient.get(i);
			else item[i] = ItemStack.EMPTY;
		}
		
		//Inputs
		
		JEIHelper.addInputSlot(recipeLayout, 0, 19, 3, item[0]);
		JEIHelper.addInputSlot(recipeLayout, 1, 40, 3, item[1]);
		JEIHelper.addInputSlot(recipeLayout, 2, 6, 23, item[2]);
		JEIHelper.addInputSlot(recipeLayout, 3, 19, 43, item[3]);
		JEIHelper.addInputSlot(recipeLayout, 4, 40, 43, item[4]);
		
		//Repair
		if(recipe.isRepair()) {
			ItemStack repair = new ItemStack(recipe.getResult().get().getOutput());
			repair.setDamage(repair.getMaxDamage() - 1);
			JEIHelper.addInputSlot(recipeLayout, 5, 54, 23, repair);
		}
		
		recipeLayout.getItemStacks().init(6, false, 110, 23);
		recipeLayout.getItemStacks().set(6, new ItemStack(recipe.getResult().get().getOutput()));
	}
}
