package net.tardis.mod.tileentities.exteriors;

import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SEntityVelocityPacket;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.*;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.boti.*;
import net.tardis.mod.boti.stores.TileStore;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animation.ExteriorAnimationEntry;
import net.tardis.mod.client.animation.IExteriorAnimation;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.entity.ai.FollowIntoTardisGoal;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.events.LivingEvents.TardisEnterEvent;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.network.packets.BOTITileMessage;
import net.tardis.mod.network.packets.RequestBOTIMessage;
import net.tardis.mod.registries.ExteriorAnimationRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class ExteriorTile extends TileEntity implements ITickableTileEntity, IBotiEnabled, IEnergyStorage{
    
    
    public static final AxisAlignedBB DEFAULT_DOOR_EAST = new AxisAlignedBB(0.5, -1, 0, 1.1, 1, 1);
    public static final AxisAlignedBB DEFAULT_DOOR_SOUTH = new AxisAlignedBB(0, -1, 0.5, 1, 1, 1.1);
    public static final AxisAlignedBB DEFAULT_DOOR_WEST = new AxisAlignedBB(-0.1, -1, 0, 0.5, 1, 1);
    public static final AxisAlignedBB DEFAULT_DOOR_NORTH = new AxisAlignedBB(0, -1, -0.1, 1, 1, 0.5);
    
    private boolean locked = false;
    private EnumDoorState openState = EnumDoorState.CLOSED;
    //MUST contain at least one state
    private EnumDoorState[] validDoorStates = new EnumDoorState[]{
            EnumDoorState.CLOSED,
            EnumDoorState.ONE,
            EnumDoorState.BOTH
    };
    private RegistryKey<World> interiorDimension;
    private EnumMatterState matterState = EnumMatterState.SOLID;
    private List<UUID> teleportedIDs = new ArrayList<>();
    private String customName = "";
    private IExteriorAnimation animation;
    private WorldShell shell;
    private boolean crashed = false;
    private boolean antiGravs = false;
    private boolean hasDemated = false;
    private int consoleInFlightTicks = 0;
    private AbstractExterior exterior = ExteriorRegistry.STEAMPUNK.get();
    private BotiHandler botiHandler;
    
    private List<TexVariant> variants = new ArrayList<TexVariant>();
    private int variantIndex = 0;
    
    //Render variables
    public float alpha = 1F;
    public float lightLevel = 1F;
    
    private ItemStackHandler buffer = new ItemStackHandler(0);
    private TardisEnergy energy = new TardisEnergy(0);
    
    private LazyOptional<ItemStackHandler> itemBuffer = LazyOptional.of(() -> this.buffer);
    private LazyOptional<TardisEnergy> energyBuffer = LazyOptional.of(() -> this.energy);
    
    private boolean hasSetupCaps = false;
    
    public final int minimumLocklevel = 0;
    /** Additional levels of locking on top of normal Key Locking
     * <br> Used to lock players out of the Tardis when interior change is active*/
    private int additionalLockLevel = 0;
    private boolean isInteriorRegenerating = false;
    
    
    public ExteriorTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
        animation = ExteriorAnimationRegistry.EXTERIOR_ANIMATION_REGISTRY.get().getValue(new ResourceLocation(Tardis.MODID, "classic"))
                .create(this);
        botiHandler = new BotiHandler(this);
        botiHandler.setDiameter(30);
    }
    
    //Required Minecraft stuff
    @Override
    public void read(BlockState state, CompoundNBT compound) {
        this.locked = compound.getBoolean("locked");
        this.openState = EnumDoorState.valueOf(compound.getString("state"));
        if(compound.contains("interior"))
            this.interiorDimension = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(compound.getString("interior")));
        if(compound.contains("matter_state"))
            this.matterState = EnumMatterState.values()[compound.getInt("matter_state")];
        this.lightLevel = compound.getFloat("light_level");
        this.customName = compound.getString("custom_name");
        this.animation = ExteriorAnimationRegistry.EXTERIOR_ANIMATION_REGISTRY.get().getValue(new ResourceLocation(compound.getString("animation")))
                .create(this);
        this.variantIndex = compound.getInt("variant_index");
        this.antiGravs = compound.getBoolean("anti_grav");
        this.hasDemated = compound.getBoolean("has_demated");
        this.antiGravs = compound.getBoolean("anti_grav");
        this.alpha = compound.getFloat("alpha");
        this.crashed = compound.getBoolean("crashed");
        if(compound.contains("exterior_type"))
            this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(compound.getString("exterior_type")));
        this.additionalLockLevel = compound.getInt("additional_lock_level");
        this.isInteriorRegenerating = compound.getBoolean("is_regenerating_interior");
        super.read(state, compound);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putBoolean("locked", this.locked);
        compound.putString("state", this.openState.name());
        if(this.interiorDimension != null)
            compound.putString("interior", this.interiorDimension.getLocation().toString());
        compound.putInt("matter_state", this.matterState.ordinal());
        compound.putFloat("light_level", this.lightLevel);
        compound.putString("custom_name", customName);
        compound.putString("animation", this.animation.getType().getRegistryName().toString());
        compound.putInt("variant_index", this.variantIndex);
        compound.putBoolean("anti_grav", this.antiGravs);
        compound.putBoolean("has_demated", this.hasDemated);
        compound.putBoolean("anti_grav", this.antiGravs);
        compound.putFloat("alpha", this.alpha);
        compound.putBoolean("crashed", this.crashed);
        if(this.exterior != null)
            compound.putString("exterior_type", this.exterior.getRegistryName().toString());
        compound.putInt("additional_lock_level", this.additionalLockLevel); 
        compound.putBoolean("is_regenerating_interior", this.isInteriorRegenerating);
        return super.write(compound);
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT tag = this.serializeNBT();
        tag.putFloat("alpha", this.alpha);
        tag.putInt("door_state", this.openState.ordinal());
        return tag;
    }
    
    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        super.onDataPacket(net, pkt);
        this.deserializeNBT(pkt.getNbtCompound());
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        super.handleUpdateTag(state, tag);
        this.alpha = tag.getFloat("alpha");
    }
    
    //Power
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
        
        if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
            return this.itemBuffer.cast();
        
        return cap == CapabilityEnergy.ENERGY ? LazyOptional.of(() -> (T)this) : super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        
        if(world != null && world.isRemote)
            Network.sendToServer(new RequestBOTIMessage(this.getPos()));
    }

    @Override
    public void tick() {
        
        this.transferEntities(world.getEntitiesWithinAABB(Entity.class, this.getDoorAABB().offset(this.getPos())));

        if(this.matterState != EnumMatterState.SOLID) {
            this.animation.tick();
            
            if(!world.isRemote) {
                //Teleport what we land on
                for(Entity ent : world.getEntitiesWithinAABB(Entity.class, new AxisAlignedBB(this.getPos().down()).expand(0, 1, 0))) {
                    ServerWorld serverWorld = world.getServer().getWorld(interiorDimension);
                    if(serverWorld != null) {
                        BlockPos pos = TardisHelper.TARDIS_POS.south(4);
                        WorldHelper.teleportEntities(ent, serverWorld, pos.getX(), pos.getY(), pos.getZ(), ent.rotationYaw, ent.rotationPitch);
                    }
                }
            }
        }
        
        if(!world.isRemote) {
            //Update console on position
            if(this.matterState == EnumMatterState.SOLID && world.getGameTime() % 90 == 0)
                TardisHelper.getConsole(world.getServer(), interiorDimension).ifPresent(tile -> tile.setCurrentLocation(world.getDimensionKey(), this.getPos().down()));
        }
        
        boolean fall = LandingSystem.shouldTARDISFall(world, pos.down(2));
        
        if(!world.isRemote && fall && !this.antiGravs)
            this.fall();
        
        if(this.getMatterState() == EnumMatterState.DEMAT) {
            if(this.alpha <= 0.0) {
                this.deleteExteriorBlocks();
            }
        }
        
        else if(this.getMatterState() == EnumMatterState.REMAT) {
            if(alpha >= 1.0)
                this.setMatterState(EnumMatterState.SOLID);
        }
        
        this.pushPower();
        
        //Crashed particle effects
        if(world.isRemote) {
            
            double x = this.getPos().getX() + (world.rand.nextFloat() - 0.5), y = this.getPos().getY() + 1, z = this.getPos().getZ() + (world.rand.nextFloat() - 0.5);
            if (this.crashed) {
                if(world.getGameTime() % 20 == 0) {
                    world.addParticle(ParticleTypes.LARGE_SMOKE, x, y, z, 0, 0.1, 0);
                    world.addParticle(ParticleTypes.SMOKE, x, y, z, 0, 0, 0);
                }
                if(world.getGameTime() % 40 == 0)
                    world.addParticle(ParticleTypes.LAVA, x, y, z, 0, 0, 0);
            }
        }
        
        if (this.isInteriorRegenerating) {
            if (world.isRemote()) {
                double x = this.getPos().getX() + (world.rand.nextFloat() - 0.5), y = this.getPos().getY() + 1, z = this.getPos().getZ() + (world.rand.nextFloat() - 0.5);
                if(world.getGameTime() % 20 == 0) {
                    world.addParticle(ParticleTypes.LARGE_SMOKE, x, y, z, 0, 0.1, 0);
                }
            }
            if (!world.isRemote()) {
                if (world.getGameTime() % 200 == 0) {
                    world.playSound(null, pos, TSounds.STEAM_HISS.get(), SoundCategory.BLOCKS, 0.1F, 1F);
                }
            }
        }
        
        if(!world.isRemote) {
            if(this.interiorDimension == null)
                ++this.consoleInFlightTicks;
            else {
                TardisHelper.getConsole(world.getServer(), interiorDimension).ifPresent(tile -> {
                    if(tile.isInFlight() && tile.getLandTime() <= 0 && this.matterState == EnumMatterState.SOLID)
                        ++this.consoleInFlightTicks;
                });
            }
            
            if(this.consoleInFlightTicks > 40)
                this.deleteExteriorBlocks();
            
            if(!this.hasSetupCaps && !this.world.isRemote) {
                world.getServer().enqueue(new TickDelayedTask(0, () -> {
                    world.getServer().getWorld(interiorDimension).getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                        this.buffer = data.getItemBuffer();
                        this.energy = data.getEnergy();
                    });
                }));
                this.hasSetupCaps = true;
            }
            
        }
        
        //Fetch initial world shell
        if(!world.isRemote && (this.shell == null || world.getGameTime() % 200 == 0)) {
            this.shell = this.buildBoti();
            if(shell != null)
                Network.sendToAllAround(new BOTIMessage(this.shell, this.getPos()), this.getWorld().getDimensionKey(), this.getPos(), 20);
        }
        
    }
    
    /** ==== Functions ==== */

    public void deleteExteriorBlocks() {
        this.hasDemated = true;
        world.setBlockState(getPos(), this.getBlockState().getFluidState().getFluid().getDefaultState().getBlockState());
        world.setBlockState(getPos().down(), Blocks.AIR.getDefaultState());
    }
    
    public void updateClient() {
        if(!world.isRemote)
            world.notifyBlockUpdate(this.getPos(), this.world.getBlockState(getPos()), this.world.getBlockState(getPos()), 3);
    }
    
    public void demat() {
        this.setMatterState(EnumMatterState.DEMAT);
        this.setDoorState(EnumDoorState.CLOSED);
        this.hasDemated = true;
        this.alpha = 1.0F;
        this.animation.reset();
        
    }
    
    public void remat() {
        this.setMatterState(EnumMatterState.REMAT);
        this.setDoorState(EnumDoorState.CLOSED);
        this.alpha = 0.0F;
        this.animation.reset();
    }
    
    public boolean isKeyValid(ItemStack stack) {
        return stack.hasTag() &&
                stack.getTag().contains(Constants.CONSOLE_ATTUNMENT_NBT_KEY) &
                interiorDimension.getLocation().toString().equals(stack.getTag().getString(Constants.CONSOLE_ATTUNMENT_NBT_KEY));
    }
    
    public void copyDoorStateToInteriorDoor() {
        if(!world.isRemote) {
            ServerWorld interior = world.getServer().getWorld(interiorDimension);
            if(interior != null) {
                TileEntity te = interior.getTileEntity(TardisHelper.TARDIS_POS);
                if(te instanceof ConsoleTile) {
                    WorldHelper.preLoadTardisInteriorChunks(interior, true);
                    ((ConsoleTile)te).getDoor().ifPresent(door -> {
                        door.setOpenState(this.getOpen());
                        door.setLocked(this.locked);
                        door.setAdditionalLockLevel(this.additionalLockLevel);
                        door.world.playSound(null, door.getPosition(), door.getOpenState().equals(EnumDoorState.CLOSED)  ? TSounds.DOOR_CLOSE.get() : TSounds.DOOR_OPEN.get(), SoundCategory.BLOCKS, 0.5F, 1F);
                        door.world.playSound(null, door.getPosition(), door.isLocked() ? TSounds.DOOR_LOCK.get() : TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 0.5F, 1F);
                    });
                }
            }
        }
    }
    
    public void transferEntities(List<Entity> entityList) {
        if(!world.isRemote && this.getOpen() != EnumDoorState.CLOSED) {
            if(this.interiorDimension == null)
                return;
            
            //Stop if no entities to move
            
            List<UUID> tempIDs = new ArrayList<>();
            for(Entity e : entityList) {
                if(this.teleportedIDs.contains(e.getUniqueID())) {
                    tempIDs.add(e.getUniqueID());
                }
            }
            this.teleportedIDs = tempIDs;
            
            if(entityList.isEmpty())
                return;
            
            double x = 0, y = TardisHelper.TARDIS_POS.getY(), z = 0;
            ConsoleTile console = null;
            ServerWorld ws = this.world.getServer().getWorld(this.interiorDimension);
            
            if(ws == null)
                return;
            
            //Get Console
            if(ws != null) {
                TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
                if(te instanceof ConsoleTile)
                    console = (ConsoleTile)te;
            }
            
            //If an interior door exists, put the player near it
            DoorEntity door = console != null ? console.getDoor().orElse(null) : null;
            float angle = (float)Math.toRadians(door.rotationYaw);
            
            if(door != null) {
                
                x = door.getPosX() + Math.sin(angle) * -0.8;
                z = door.getPosZ() - Math.cos(angle) * -0.8;
                y = door.getPosY();
                
            }
            
            List<Entity> secondPass = Lists.newArrayList();
            
            for(Entity e : entityList) {
                
                if(e.isBeingRidden() && !e.getPassengers().isEmpty()) {
                    secondPass.addAll(e.getPassengers());
                }
                
                if(this.teleportedIDs.contains(e.getUniqueID()) || !e.isAlive() || e.isBeingRidden())
                    continue;
    
                float realRot = e.rotationYaw;
                if(door != null) {
                    float extAngle = WorldHelper.getAngleFromFacing(this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING).getOpposite());
                    float diff = e.rotationYaw % 360.0F - extAngle;
                    realRot = door.rotationYaw + diff;
                }
                
                if(door != null)
                    door.addEntityToTeleportedList(e.getUniqueID());
                
                Vector3d oldMotion = e.getMotion();
                
                if(e instanceof LivingEntity)
                    MinecraftForge.EVENT_BUS.post(new TardisEnterEvent((LivingEntity)e, this, this.getWorld()));
                
                float dist = e.getWidth();
                
                WorldHelper.teleportEntities(e, ws, x + (Math.sin(angle) * dist), y, z - (Math.cos(angle) * dist), realRot, e.rotationPitch);
                
                //Motion
                
                Vector3d setMot = oldMotion.rotateYaw(-(float)Math.toRadians(realRot));
                
                world.getServer().enqueue(new TickDelayedTask(1, () -> {
                    Entity ent = ws.getEntityByUuid(e.getUniqueID());
                    if(ent != null)
                        ent.setMotion(setMot);
                    if(ent instanceof ServerPlayerEntity) {
                        ((ServerPlayerEntity)ent).connection.sendPacket(new SEntityVelocityPacket(ent));
                    }
                }));
                
                //Follow into TARDIS
                if(e instanceof PlayerEntity) {
                    for(MonsterEntity ent : world.getEntitiesWithinAABB(MonsterEntity.class, new AxisAlignedBB(this.getPos()).grow(20))) {
                        if(ent.getAttackTarget() == e) {
                            for(PrioritizedGoal goal : ent.goalSelector.goals) {
                                if(goal.getGoal() instanceof FollowIntoTardisGoal) {
                                    ((FollowIntoTardisGoal)goal.getGoal()).setTarget(this.getPos());
                                }
                            }
                        }
                    }
                }
            }
            
            if(!secondPass.isEmpty())
                this.transferEntities(secondPass);
            
        }
    }
    
    public void pushPower() {
        for(Direction dir : Direction.values()) {
            TileEntity te = world.getTileEntity(getPos().offset(dir));
            if(te != null)
                te.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite()).ifPresent(cap -> {
                    int power = this.getEnergyStored();
                    int accepted = cap.receiveEnergy(power, false);
                    this.extractEnergy(accepted, false);
                });
        }
    }
    
    public void addTeleportedEntity(UUID id) {
        this.teleportedIDs.add(id);
    }
    
    public EnumDoorState getNextDoorState() {
        int index = 0;
        for(EnumDoorState state : this.validDoorStates) {
            if(state == this.openState) {
                if(index + 1 < this.validDoorStates.length) 
                    return this.validDoorStates[index + 1];
                 else return this.validDoorStates[0];
            }
            ++index;
        }
        return EnumDoorState.CLOSED;
    }
    
    //Getters and setters
    
    public void setDoorState(EnumDoorState state) {
        this.openState = state;
        this.markDirty();
        this.updateClient();
    }
    
    public EnumDoorState getOpen() {
        return this.openState;
    }
    
    public void setLocked(boolean locked) {
        this.locked = locked;
        this.markDirty();
    }
    
    public boolean getLocked() {
        return this.locked;
    }
    
    public void toggleLocked() {
        this.locked = !this.locked;
        if(locked) {
            this.setDoorState(EnumDoorState.CLOSED);
            this.copyDoorStateToInteriorDoor();
        }
    }
    
    public void open(Entity opening) {
        if (!opening.getEntityWorld().isRemote()) {
            if(this.locked && !this.isExteriorDeadLocked()) {
                if(opening instanceof PlayerEntity)
                    ((PlayerEntity)opening).sendStatusMessage(ExteriorBlock.LOCKED, true);
                return;
            }
            else {
                if (this.isExteriorDeadLocked()) {
                    if(opening instanceof PlayerEntity) {
                        PlayerEntity player = (PlayerEntity)opening;
                        if (this.isInteriorRegenerating())
                            player.sendStatusMessage(new TranslationTextComponent(ExteriorBlock.INTERIOR_CHANGE, getRemainingInteriorChangeTime()), true);
                        else
                            player.sendStatusMessage(ExteriorBlock.DEADLOCKED, true);
                    }
                    this.copyDoorStateToInteriorDoor();
                    return;
                }
                else {
                    this.setDoorState(this.getNextDoorState());
                    this.copyDoorStateToInteriorDoor();
                    
                    if (getOpen().equals(EnumDoorState.CLOSED)) {
                        world.playSound(null, getPos(), this.exterior.getDoorSounds().getClosedSound(), SoundCategory.BLOCKS, 0.5F, 1F);
                    }
                    else {
                        world.playSound(null, getPos(), this.exterior.getDoorSounds().getOpenSound(), SoundCategory.BLOCKS, 0.5F, 1F);
                    }
                }
            }
            
            
        }
    }

    /** The level of additional locks on top of normal locking
     * <p> 0 - No additional lock levels. Exterior only has 1 lock level via the Tardis Key
     * <br> 1 - Double Locked. Prevents player from entering during interior changing
     * <br> 2 - Triple Locked. Other mods cannot bypass this unless specified.*/
    public int getAdditionalLockLevel() {
        return this.additionalLockLevel;
    }
    
    /** Set the level of additional locks on top of normal locking for the exterior
     * <p> 0 - No additional lock levels. Exterior only has 1 lock level via the Tardis Key
     * <br> 1 - Double Locked. Prevents player from entering during interior changing
     * <br> 2 - Triple Locked. Other mods cannot bypass this unless specified.*/
    public void setAdditionalLockLevel(int additionalLockLevel) {
        this.additionalLockLevel = additionalLockLevel;
        this.markDirty();
    }
    
    public boolean isExteriorDeadLocked() {
        return this.additionalLockLevel > 0;
    }
    
    public void setInteriorDimensionKey(World world) {
        this.interiorDimension = world.getDimensionKey();
        this.markDirty();
    }
    
    public RegistryKey<World> getInteriorDimensionKey() {
        return this.interiorDimension;
    }

    public EnumMatterState getMatterState() {
        return this.matterState;
    }
    
    public void setMatterState(EnumMatterState state) {
        this.matterState = state;
        this.markDirty();
        if(state != EnumMatterState.SOLID)
            this.setDoorState(EnumDoorState.CLOSED);
        this.updateClient();
    }
    
    public void setLightLevel(float percent) {
        this.lightLevel = (float) MathHelper.clamp(percent + 0.2, 0, 1F);
        this.markDirty();
        this.updateClient();
    }
    
    public float getLightLevel() {
        return this.lightLevel;
    }
    
    public String getCustomName() {
        return this.customName;
    }
    
    public void setCustomName(String name) {
        
        this.customName = name;
        this.markDirty();
        this.updateClient();
        
        if(!world.isRemote) {
            TardisHelper.getConsole(world.getServer(), this.interiorDimension)
                .ifPresent(tile -> tile.setCustomName(name));
        }
    }
    
    @Nullable
    public TardisEntity fall() {
        if(this.hasDemated)
            return null;
        
        TardisEntity ent = this.createEntity();
        if(ent != null) {
            ent.setExteriorTile(this);
            if(world.addEntity(ent))
                this.hasDemated = true;
                this.deleteExteriorBlocks();
                TardisHelper.getConsole(this.world.getServer(), this.interiorDimension).ifPresent(tile -> {
                    tile.setEntity(ent);
                });
        }
        return ent;
    }
    
    public void updateConsoleName() {
        if(!world.isRemote) {
            TardisHelper.getConsole(world.getServer(), this.interiorDimension)
                .ifPresent(tile -> tile.setCustomName(getCustomName()));
        }
    }
    
    public IExteriorAnimation getExteriorAnimation() {
        return this.animation;
    }
    
    public void setExteriorAnimation(ExteriorAnimationEntry anim) {
        this.animation = anim.create(this);
        this.markDirty();
        this.updateClient();
    }
    
    //Scrape the necessary Data, including dimension from console
    public void copyConsoleData(ConsoleTile console) {
        this.interiorDimension = console.getWorld().getDimensionKey();
        
        //Set door stuff
        this.locked = false;
        this.openState = EnumDoorState.CLOSED;
        
        //Override if door exists
        console.getDoor().ifPresent(ent -> {
            this.locked = ent.isLocked();
            this.openState = ent.getOpenState();
        });
        
        this.customName = console.getCustomName();
        ExteriorAnimationEntry entry = ExteriorAnimationRegistry.EXTERIOR_ANIMATION_REGISTRY.get().getValue(console.getExteriorManager().getExteriorAnimation());
        if (entry != null)
            this.animation = entry.create(this);
        this.lightLevel = MathHelper.clamp((console.getInteriorManager().getLight() / 15.0F) + 0.2F, 0, 1);
        this.variantIndex = console.getExteriorManager().getExteriorVariant();
        this.antiGravs = console.getAntiGrav();
        this.exterior = console.getExteriorType();
        this.crashed = console.isCrashing();
        this.isInteriorRegenerating = console.getInteriorManager().isInteriorStillRegenerating();
        this.updateClient();
        
    }
    
    @Override
    public WorldShell getBotiWorld() {
        return this.shell;
    }

    @Override
    public void setBotiWorld(WorldShell shell) {
        this.shell = shell;
    }

    public void setVariants(TexVariant... variants) {
        this.variants.clear();
        for(TexVariant t : variants) {
            this.variants.add(t);
        }
    }
    
    public void setVariant(int i) {
        this.variantIndex = i < this.variants.size() ? i : 0;
        this.markDirty();
        this.updateClient();
    }
    
    @Nullable
    public TexVariant getVariant() {
        return this.variantIndex < this.variants.size() ? this.variants.get(variantIndex) : null;
    }
    
    /*
     * If an entity is inside this it will be transfered to the TARDIS
     */
    public abstract AxisAlignedBB getDoorAABB();
    

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        return this.getEnergy().receiveEnergy(maxReceive, simulate);
    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {
        return this.getEnergy().extractEnergy(maxExtract, simulate);
    }

    @Override
    public int getEnergyStored() {
        return this.getEnergy().getEnergyStored();
    }

    @Override
    public int getMaxEnergyStored() {
        return this.getEnergy().getMaxEnergyStored();
    }

    @Override
    public boolean canExtract() {
        return this.getEnergy().canExtract();
    }

    @Override
    public boolean canReceive() {
        return this.getEnergy().canReceive();
    }
    
    public EnergyStorage getEnergy() {
        return this.energyBuffer.orElse(new TardisEnergy(0));
    }

    @Override
    public double getMaxRenderDistanceSquared() {
        return 16384.0;
    }
    
    @Nullable
    public TardisEntity createEntity() {
        if(!world.isRemote) {
            TardisEntity entity = TEntities.TARDIS.get().create(world);
            TardisHelper.getConsole(world.getServer(), interiorDimension).ifPresent(tile -> entity.setConsole(tile));
            entity.setPosition(this.getPos().getX() + 0.5, this.getPos().getY() - 1, this.getPos().getZ() + 0.5);
            entity.rotationYaw = WorldHelper.getAngleFromFacing(this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)) - 180;
            entity.setNoGravity(this.antiGravs);
            return entity;
        }
        return null;
    }
    
    
    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
        this.updateClient();
        this.markDirty();
    }

    public void damage(int damage) {
        if(!world.isRemote)
            TardisHelper.getConsole(world.getServer(), this.interiorDimension)
                .ifPresent(tile -> {
                    ShieldGeneratorSubsystem sys = tile.getSubsystem(ShieldGeneratorSubsystem.class).orElse(null);
                    if(sys != null && sys.canBeUsed())
                        sys.damage(null, damage);
                    else {
                        float realDam = damage / (float)tile.getSubSystems().size();
                        
                        for(Subsystem s : tile.getSubSystems()) {
                            s.damage(null, (int)Math.ceil(realDam));
                        }
                    }
                    
                    for(PlayerEntity player : tile.getWorld().getPlayers()) {
                        player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                            cap.setShaking(60, 10);
                            cap.update();
                        });
                    }
                    tile.getWorld().playSound(null, tile.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 0.75F, 0.6F);
                });
    }
    
    public void setAntiGravs(boolean antiGravs) {
        this.antiGravs = antiGravs;
        this.markDirty();
    }
    public boolean getAntiGravs() {
        return this.antiGravs;
    }

    public void placeExteriorBlocks() {}

    @Override
    public void remove() {
        super.remove();
        this.itemBuffer.invalidate();
        this.energyBuffer.invalidate();
        
        if(!world.isRemote && this.interiorDimension != null)
            TardisHelper.getConsole(world.getServer(), interiorDimension).ifPresent(tile -> {
                tile.getOrFindExteriorTile().invalidate();
            });
        
    }
    
    public AxisAlignedBB getDefaultEntryBox() {
        
        //Safety check
        if(world == null || !this.getBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING))
            return new AxisAlignedBB(0, 0, 0, 0, 0, 0);
        
        Direction dir = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
        switch(dir) {
            case EAST: return DEFAULT_DOOR_EAST;
            case SOUTH : return DEFAULT_DOOR_SOUTH;
            case WEST : return DEFAULT_DOOR_WEST;
            default: return DEFAULT_DOOR_NORTH;
        }
    }
    
    public WorldShell buildBoti() {
        if(!world.isRemote) {
            World interior = world.getServer().getWorld(this.getInteriorDimensionKey());
            if(interior != null) {
                
                TardisHelper.getConsoleInWorld(interior).ifPresent(tile -> {
                    tile.getDoor().ifPresent(door -> {
                        BlockPos origin = door.getPosition();
                        this.botiHandler.updateBoti(interior, origin, BlockPos.ZERO.offset(door.getHorizontalFacing().getOpposite(), 15));

                        this.getBotiWorld().setPortalDirection(door.getHorizontalFacing());

                        for(TileStore tileStore : this.botiHandler.getTileStores()){
                            Network.sendToTrackingTE(new BOTITileMessage(this.getPos(), tileStore), this);
                        }
                        this.botiHandler.getTileStores().clear();

                    });
                });
                
                return shell;
            }
        }
        return null;
    }

    public int getLightEmittingLevel() {
        return 3;
    }

    @Override
    public BOTIMessage createMessage(WorldShell shell) {
        return new BOTIMessage(shell, this.getPos());
    }
    
    public boolean isInteriorRegenerating() {
        return this.isInteriorRegenerating;
    }
    
    public void setInteriorRegenerating(boolean isInteriorRegenerating) {
        this.isInteriorRegenerating = isInteriorRegenerating;
        this.markDirty();
        this.updateClient();
    }
    
    /** Gets the remaining amount of time the interior change process has in seconds*/
    public int getRemainingInteriorChangeTime() {
        ServerWorld interior = world.getServer().getWorld(interiorDimension);
        if(interior != null) {
            TileEntity te = interior.getTileEntity(TardisHelper.TARDIS_POS);
            if(te instanceof ConsoleTile) {
                ConsoleTile console = (ConsoleTile)te;
                return console.getInteriorManager().getInteriorProcessingTime() / 20;
            }
        }
        return 0;
    }

}
