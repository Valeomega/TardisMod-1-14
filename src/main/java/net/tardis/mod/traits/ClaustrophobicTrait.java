package net.tardis.mod.traits;

import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class ClaustrophobicTrait extends TardisTrait{
	
	public ClaustrophobicTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		
		if(tile.getWorld().getGameTime() % 200 == 0) {
			tile.getOrFindExteriorTile().ifPresent(ext -> {
				if(!ext.getWorld().canBlockSeeSky(ext.getPos()) && tile.getEmotionHandler().getMood() < EnumHappyState.DISCONTENT.getTreshold()) {
					tile.getEmotionHandler().addMood(-1 + (this.getModifier() * 9));
					this.warnPlayerLooped(tile, TardisTrait.DISLIKES_LOCATION, 400);
				}
			});
		}
		
	}

}
