package net.tardis.api.space.cap;

import net.minecraft.util.math.BlockPos;

import java.util.List;
/** Interface that allows an object to define areas where oxgyen can be stored without it escaping
 * <br> For an example, see {@linkplain net.tardis.mod.tileentities.OxygenSealTile OxygenSealTile} */
public interface IOxygenSealer {

    List<BlockPos> getSealedPositions();

    void setSealedPositions(List<BlockPos> list);
}
